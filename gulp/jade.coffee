gulp = require 'gulp'

gulp.task 'jade', ->

  jade = require('gulp-jade')
  rename = require("gulp-rename")
  argv = require('yargs').argv

  gulp.src 'assets/jade/pages/*.jade'
    .pipe jade {
      basedir: 'assets/jade/pages/'
      pretty: true #!argv.production
      locals: site_domain: 'example.com'
    }
    .pipe rename (path)->
        #unless path.basename == 'index'
        #  path.dirname += "/" + path.basename
        #  path.basename = "index"
        path.extname = ".html"
    .pipe gulp.dest 'static/html'