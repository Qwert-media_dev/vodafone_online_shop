gulp = require 'gulp'

importer = require('gulp-fontello-import')
replace = require('gulp-replace')
fs = require('fs')

fontelloConfig = 'assets/img/icons/fontello.json'

# this eliminates bugs with font generation
gulp.task '_clear_config', ()->
  config = JSON.parse(fs.readFileSync(fontelloConfig, 'utf8'))
  config.glyphs = []
  fs.writeFileSync(fontelloConfig, JSON.stringify(config), 'utf8')

gulp.task '_icons-import-svg', (cb)->
  importer.importSvg({
    config: fontelloConfig
    svgsrc: 'assets/img/icons/'
  }, cb)

gulp.task '_icons_get', ['_icons-import-svg'], (cb)->
  importer.getFont({
    host: 'http://fontello.com'
    config: fontelloConfig
    css: 'public/css/icons'
    font: 'public/fonts/icons'
    scss: true
  }, cb)

gulp.task 'icons', ['_clear_config', '_icons_get'], ->
  # timeout to wait async disk write operation
  setTimeout (->
    gulp.src(['public/css/icons/icons.css'])
    .pipe(replace('/font/', '/fonts/icons/'))
    .pipe(gulp.dest('public/css'))

    # recompile styles with new glyphs
    gulp.start 'sass'
  ), 500