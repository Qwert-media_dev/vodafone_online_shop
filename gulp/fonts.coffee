gulp = require 'gulp'

gulp.task 'fonts', ->

  gulp.src 'assets/fonts/**/*'
    .pipe gulp.dest 'public/fonts'

  ###
  fontfacegen is too buggy...

  fontfacegen = require('fontfacegen')
  fs = require('fs')
  path = require('path')
  concat = require('concat-files')

  fonts = fs.readdirSync('assets/fonts/')
  cssFiles = []
  for font in fonts
    extension = path.extname(font)
    fontname = path.basename(font, extension)
    if extension == '.ttf' or extension == '.otf'
      fontfacegen
        source:  path.join('assets/fonts/', font),
        dest: 'public/fonts/',
        collate: true
        css: "public/fonts/#{fontname}.css"
        css_fontpath: "/"

      cssFiles.push "public/fonts/#{fontname}.css"

  concat cssFiles, 'public/fonts/fonts.css'

  ###