gulp = require 'gulp'

gulp.task 'sass', ->

  sass = require 'gulp-sass'
  sassGlob = require 'gulp-sass-glob'
  sourcemaps = require 'gulp-sourcemaps'
  csso = require 'gulp-csso'
  autoprefixer = require "gulp-autoprefixer"
  inline_base64 = require 'gulp-inline-base64'
  ifElse = require('gulp-if-else')
  argv = require('yargs').argv

  gulp.src ['assets/sass/*.sass', '!assets/sass/_*', '!assets/sass/_*/*', 'assets/sass/_partials/layout/inner-content.sass']
  .pipe sourcemaps.init()
  .pipe sassGlob()
  .pipe sass({
    outputStyle: 'expanded'
    comments: true
  }).on('error', sass.logError)
  .pipe inline_base64
    baseDir: 'assets'
    maxSize: 14 * 1024
    debug: !argv.production
  .pipe(autoprefixer(browsers: ['last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'], remove: false))
  .pipe ifElse(argv.production, csso)
  .pipe sourcemaps.write('./maps')
  .pipe gulp.dest 'public/css'
