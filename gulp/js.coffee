gulp = require 'gulp'

gulp.task 'js', ->
  uglify = require('gulp-uglify')

  gulp.src 'assets/js/**/*'
  .pipe uglify()
  .pipe gulp.dest 'public/js'