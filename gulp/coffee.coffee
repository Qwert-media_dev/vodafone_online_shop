gulp = require 'gulp'

gulp.task 'coffee', ->
  include = require 'gulp-include'
  uglify = require('gulp-uglify')
  coffee = require "gulp-coffee"
  ifElse = require('gulp-if-else')
  argv = require('yargs').argv

  gulp.src 'assets/coffee/*.coffee'
  .pipe include()
  .pipe coffee()
  .pipe ifElse(argv.production, uglify)
  .on 'error', (e)-> console.error e
  .pipe gulp.dest 'public/js'