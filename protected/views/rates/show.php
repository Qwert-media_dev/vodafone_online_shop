<?php
/* @var $item Rates */
/* @var $this RatesController */
?>
<div class="content-box">
    <?php
    echo $item->i18n[0]->content;
    if( $item->widget != '' ) {
        $classes = explode(',', $item->widget);
        foreach($classes as $class) {
            $class = trim($class);
            $class = $class . 'Widget';
            if (class_exists($class)) {
                $this->widget($class);
            }
        }

        echo $item->i18n[0]->content_after;
    }
    ?>
</div>
