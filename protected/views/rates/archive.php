<?php
/* @var $this RatesController */
/* @var $items Rates[] */
/* @var $inactive Rates[] */
/* @var $type string */
?>
<div class="page-title-container">
    <?php $this->widget('RatesMenuWidget'); ?>
    <h1 class="page-title"><?= Yii::t('app', 'Актуальнi тарифи') ?></h1>
</div>
<div class="new_tarif-banners columns-3">
    <?php
    $is_archive = true;
    foreach($items as $item) {
        $this->widget('ActiveRateItemWidget', compact('item', 'is_archive'));
    }
    ?>
</div>
<?php if(count($inactive)): ?>
<div class="new_tarif-closed">
    <h1><?= Yii::t('app', 'Закритi тарифи') ?></h1>
    <ul>
        <?php foreach($inactive as $item): ?>
        <li>
            <span><?= date('Y', strtotime($item->rate_date)) ?></span>
            <?= CHtml::link($item->i18n[0]->title.' '.$item->i18n[0]->second_title, ['rates/archiveshow', 'lang' => $this->language->code, 'type' => $type, 'alias' => $item->i18n[0]->alias]) ?>
        </li>
        <?php endforeach; ?>
    </ul>
</div>
<?php endif; ?>
