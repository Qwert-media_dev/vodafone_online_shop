<?php
/* @var $this RatesController */
/* @var $items Rates[] */
/* @var $type string */
?>
<div class="page-title-container">
    <?php $this->widget('RatesMenuWidget'); ?>
    <h1 class="page-title"><?= $this->getTypeLabel($type) ?></h1>
</div>
<div class="new_tarif-banners columns-3">
    <?php
    foreach($items as $item) {
        $this->widget('ActiveRateItemWidget', compact('item'));
    }
    ?>
</div>
