<?php
/* @var $this DialogController */
/* @var $count integer */
/* @var $items News[] */
/* @var $pages CPagination */
/* @var $page string */
?>

<?php foreach($items as $item): ?>
<article class="news-item column full">
	<div class="container">
		<h1><?php echo CHtml::link( $item->i18n[0]->title, array('dialog/show', 'lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title), 'page' => $page ) ); ?></h1>
		<div class="date"><?= $item->getFormatedNewsDT() ?></div>
		<div class="excerpt">
			<?= $item->i18n[0]->short ?>
		</div>
	</div>
</article>
<?php endforeach; ?>

<?php $this->widget('DamnPager', array('pages' => $pages)); ?>
