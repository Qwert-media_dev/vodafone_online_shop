<?php
/* @var $this NetworkController */
/* @var $item Pages */
/* @var $points array */
/* @var $states array */

$body = trim($item->i18n[0]->body);
?>


<?php if($body != ''): ?>
<div class="block">
    <?= $body ?>
</div>
<?php endif; ?>
<div class="block">
    <div class="filter">
        <div class="container">
            <div class="column full">
                <div class="legend" style="display:none">
                    <div
                        data-data="<?= url_to('network/load', array('lang' => $this->language->code, 'key' => 'points')) ?>"
                        class="legend-3g"><img
                            src="/img/map-marker-3g.png"/><?= Yii::t('app', 'точки доступу 3G до 42 Мбiт/сек') ?> </div>
                </div>
                <div class="selector state">
                    <label><?= Yii::t('app', 'Регіон') ?></label>

                    <div class="select">
                        <select
                            data-data="<?= url_to('network/load', array('lang' => $this->language->code, 'key' => 'states')) ?>"></select>
                    </div>
                </div>
                <div class="selector coverages">
                    <label><?= Yii::t('app', 'Покриття') ?></label>

                    <div class="select">
                        <select
                            data-data="<?= url_to('network/load', array('lang' => $this->language->code, 'key' => 'coverages')) ?>"></select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="map"></div>
    <div class="container points">
        <div class="column full">
            <?php foreach ($points as $state => $region): ?>
                    <div data-state="<?= $state ?>" class="list">
                        <h2><?= Yii::t('app', 'Спробуйте швидкісний Інтернет <em>3G Vodafone</em> до <em>42Мбіт/с</em> у') ?>
                            <strong><?= $states[$state]['t_title2'] ?></strong></h2>
                        <?php foreach ($region['groups'] as $group): ?>
                            <?php if (strlen($group['title']) > 0): ?>
                                <div class="district collapsed">
                                    <strong><?= $group['title'] ?></strong>
                                    <div class="collapse">
                            <?php endif; ?>
                            <ol>

                                <?php
                                $items = $group['items'];
                                //ksort($items);
                                foreach($items as $row): ?>
                                    <li><a href="#"
                                           data-point="<?= $row['id'] ?>"><?= $row['description'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ol>
                            <?php if (strlen($group['title']) > 0): ?>
                                </div></div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>