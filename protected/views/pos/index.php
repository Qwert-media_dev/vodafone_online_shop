<?php
/* @var $this PosController */
/* @var $count integer */
/* @var $items Pos[] */
/* @var $pages CPagination */
/* @var $cityTitles array */
?>
<?php foreach ($items as $item): ?>
    <article class="shop-item">
        <div class="container">
            <div class="row">
                <button class="print"></button>
                <div class="info column two-third">
                    <h1><?= $cityTitles[$item->pos_city_id] ?>,
                        <address><?= $item->i18n[0]->address ?></address>
                    </h1>
                    <ul>
                        <?php if ($item->i18n[0]->metro != '') { ?>
                            <li><strong><?= Yii::t('app', 'Метро:') ?></strong> <?= $item->i18n[0]->metro ?>
                            </li><?php } ?>
                        <?php if ($item->i18n[0]->how_to_find != '') { ?>
                            <li>
                            <strong><?= Yii::t('app', 'Розташування магазину:') ?></strong> <?= $item->i18n[0]->how_to_find ?>
                            </li><?php } ?>
                    </ul>
                    <button class="show-map"><?= Yii::t('app', 'На мапi') ?></button>
                </div>
                <div class="time column third">
                    <?php
                    $rows = $item->i18n[0]->formattedWorkHours();
                    if (count($rows)) {
                        ?>
                        <h2><?= Yii::t('app', 'Час роботи') ?></h2>
                        <ul>
                            <?php foreach ($rows as $row): ?>
                                <li><?= $row ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div data-lat="<?= $item->lat ?>" data-lng="<?= $item->lng ?>" class="map-container"></div>
    </article>
<?php endforeach; ?>

<?php $this->widget('DamnPager', array('pages' => $pages)); ?>