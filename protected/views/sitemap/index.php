<?php
/* @var $this SitemapController */
?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php foreach( $this->languages as $lang ): ?>
    <sitemap>
        <loc><?= app()->createAbsoluteUrl('sitemap/sitemap', array('lang' => $lang->code)) ?></loc>
        <lastmod><?= date('c') ?></lastmod>
    </sitemap>
    <?php endforeach; ?>
</sitemapindex>