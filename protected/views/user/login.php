<?php echo CHtml::errorSummary($form, '<div class="alert alert-danger" role="alert">', '</div>'); ?>

<?php echo CHtml::beginForm('', 'post', array('class' => 'form-signin', 'role' => 'form' )); ?>
    <h2 class="form-signin-heading">Авторизация!</h2>
    <input name="form[email]" type="email" class="form-control" placeholder="Email" required autofocus>
    <input name="form[password]" type="password" class="form-control" placeholder="Пароль" required>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
<?php echo CHtml::endForm(); ?>