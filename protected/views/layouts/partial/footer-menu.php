<?php
/**
 *  Двухуровневое меню в шапке
 *  Есть костыль исключающий отображение бизнеса
 */

/* @var $this Controller */
?>

<?php foreach($this->mainMenu as $idx => $item): ?>
    <?php //if(param('skipBusiness') && $idx == 1) continue; ?>
<nav class="<?= $item['alias'] ?>">
	<strong>
        <?php
        if($item['type0'] == Pages::TYPE_NOT_A_LINK ) {
            echo CHtml::tag('span', [], $item['title']);
        } else {
            $param = array();
            if( $item['type'] == Pages::TYPE_LINK ) {
                $url = $item['alias'];
            } else {
                $url = sprintf('/%s/%s', $this->language->code, $item['alias']);
            }
            if( $item['in_new_window'] )
                $param['target'] = '_blank';
            echo CHtml::link($item['title'], $url, $param);
        }
        ?>
    </strong>
	<?php if( count($item['children']) ): ?>
	<ul>
		<?php foreach($item['children'] as $second): ?>
		<li>
            <?php
            if($second['type0'] == Pages::TYPE_NOT_A_LINK) {
                echo CHtml::tag('span', [], $second['title']);
            } else {
                $param = array();
                if( $second['type'] == Pages::TYPE_LINK ) {
                    $url = $second['alias'];
                } else {
                    $url = sprintf('/%s/%s/%s', $this->language->code, $item['alias'], $second['alias']);
                }
                if( $second['in_new_window'] )
                    $param['target'] = '_blank';
                echo CHtml::link($second['title'], $url, $param);
            }
            ?>
        </li>
		<?php endforeach; ?>
	</ul>
	<?php endif;?>
    <?php if (count($this->mainMenu) === $idx + 1): ?>
    <strong class="sitemap"><?= CHtml::link(Yii::t('app', 'Site map'), array('site/sitemap', 'lang' => $this->language->code)) ?></strong>
    <?php endif;?>
</nav>
<?php endforeach; ?>
