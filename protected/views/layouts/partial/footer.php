<footer id="footer">
    <div class="container">
        <div class="contact">
            <div class="info">

                <?= Yii::t('app', 'Телефон служби підтримки:') ?>
                <address>111</address>
                <?= Yii::t('app', '(безкоштовно в мережі МТС)') ?>
                <br>
                <address>0800 400 111</address>
                <?= Yii::t('app', '(безкоштовно з усіх мереж зв’язку України) або') ?>

            </div>
        </div>
        <div class="menu">
            <?php include 'footer-menu.php'; ?>
            <!--<nav class="sitemap">
                <strong><?= CHtml::link(Yii::t('app', 'Site map'), array('site/sitemap', 'lang' => $this->language->code)) ?></strong>
            </nav>-->
            <nav class="external-links">
                <ul>
                    <li><a href="https://www.instagram.com/vodafone_ukraine/" target="_blank" class="instagram"><?= Yii::t('social_in_footer', '@vodafone_ukraine') ?></a></li>
                    <li><a href="https://vk.com/vfukraine" target="_blank" class="vkontakte"><?= Yii::t('social_in_footer', 'Vkontakte') ?></a></li>
                    <li><a href="https://www.facebook.com/VFUkraine/" target="_blank" class="facebook"><?= Yii::t('social_in_footer', 'Facebook') ?></a></li>
                    <li><a href="https://twitter.com/Vodafone_UA" target="_blank" class="twitter"><?= Yii::t('social_in_footer', 'Twitter') ?></a></li>
                    <li><a href="https://www.youtube.com/channel/UCca4LDwZ0UFczThXXqMa0BA" target="_blank" class="youtube"><?= Yii::t('social_in_footer', 'Youtube') ?></a></li>
                </ul>
            </nav>
        </div>
        <div class="mobile-langs">
            <?php $this->widget('LanguagesWidget', array('short' => true)) ?>
        </div>
        <div class="mixed">
            <div class="copyrights"><?= Yii::t('app', '© Victory. Всі права захищені.') ?></div>
        </div>
    </div>
</footer>

<?php $this->widget('ContactsWidget'); ?>

<script src="/js/scripts.js"></script>
<?php include 'ga.php'; ?>