<?php
/* @var $this Controller */
/* @var $segments array */
$pos = 0; $items = array();
foreach($this->mainMenu as $idx => $item) {
	if( $item['alias'] == $segments[1] ) {
		$pos = $idx;
	}
}

if($pos > 1) {
	$items = $this->mainMenu[$pos]['children'];
	$left = false;
} elseif( $segments[2] != '' ) {
	foreach($this->mainMenu[$pos]['children'] as $item) {
		if($item['alias'] == $segments[2]) {
			$items = $item['children'];
			$left = true;
		}
	}
}
?>

<?php if( count($items) ): ?>
	<div class="<?= $wrapClass ?>">
		<?php
		foreach($items as $item) {
			if($item['is_tabbed_menu']) {
				$param = $item['alias'] == $segments[$left ? 3 : 2] ? array('class' => 'active') : array();

				if ($item['type'] == Pages::TYPE_LINK) {
					if ($item['in_new_window'])
						$param['target'] = '_blank';
					$url = $item['alias'];
				} else {
					if ($left)
						$url = sprintf('/%s/%s/%s/%s', $this->language->code, $segments[1], $segments[2], $item['alias']);
					else
						$url = sprintf('/%s/%s/%s', $this->language->code, $segments[1], $item['alias']);
				}
				echo CHtml::link($item['title'], $url, $param);

 			}
		}
		?>
	</div>
<?php endif; ?>
