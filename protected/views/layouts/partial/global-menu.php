<?php
/**
 * Вывод первого уровня меню
 * плюс плашка с выбором языка
 */

/* @var $this Controller */
/* @var $segments array */

$landingID = is_null(app()->session->get('landingID')) ? $this->landing->id1 : app()->session->get('landingID');
$comparePages = param('skipBusiness') ? [$this->mainMenu[0]['alias']] : [$this->mainMenu[0]['alias'], $this->mainMenu[1]['alias']];
?>
<nav class="global-menu">
    <div class="container">
        <ul class="sections">
            <?php
            // вывожу первые 2 уровня меню слева
            $till = param('skipBusiness') ? 0 : 1;
            foreach (range(0, $till) as $i) {
                /**
                 * Если текущий сегмент или он пустой и это первый элемент меню
                 * или если сегмент не один из первых двух, тогда автоматом делаем ад
                 */

                if ($segments[1] == '' && $landingID == $this->mainMenu[$i]['id']) {
                    $param = array('class' => 'active');
                } elseif (in_array($segments[1], $comparePages) && $segments[1] == $this->mainMenu[$i]['alias']) {
                    $param = array('class' => 'active');
                } elseif (!in_array($segments[1], $comparePages) && $landingID == $this->mainMenu[$i]['id']) {
                    $param = array('class' => 'active');
                } else {
                    $param = array();
                }

                echo CHtml::openTag('li', $param);
                if ($this->mainMenu[$i]['type'] == Pages::TYPE_NOT_A_LINK) {
                    echo CHtml::tag('span', [], $this->mainMenu[$i]['title']);
                } else {
                    $param = array();
                    if ($this->mainMenu[$i]['type'] == Pages::TYPE_LINK) {
                        if ($this->mainMenu[$i]['in_new_window'])
                            $param = array('target' => '_blank');
                        $url = $this->mainMenu[$i]['alias'];
                    } else {
                        $url = sprintf('/%s/%s', $this->language->code, $this->mainMenu[$i]['alias']);
                    }
                    echo CHtml::link($this->mainMenu[$i]['title'], $url, $param);
                }
                echo CHtml::closeTag('li');

                // костыль для запуска без бизнеса
                #if(param('skipBusiness'))
                #    break;
            }
            ?>
        </ul>
        <ul class="mixed">
            <?php
            // вывожу оставшиеся пункты меню справа
            foreach (range($till + 1, count($this->mainMenu) - 1) as $i) {

                // меню с подразделами
                if (count($this->mainMenu[$i]['children'])) {
                    $class = array('dropdown');
                    if ($segments[1] == $this->mainMenu[$i]['alias'])
                        $class[] = 'active';

                    echo CHtml::openTag('li', array('class' => implode(' ', $class)));
                    $url = $this->mainMenu[$i]['type'] == Pages::TYPE_LINK ? $this->mainMenu[$i]['alias'] : sprintf('/%s/%s', $this->language->code, $this->mainMenu[$i]['alias']);
                    $urlParam = $this->mainMenu[$i]['type'] == Pages::TYPE_LINK && $this->mainMenu[$i]['in_new_window'] ? array('target' => '_blank') : array();
                    if ($this->mainMenu[$i]['type'] == Pages::TYPE_NOT_A_LINK) {
                        echo CHtml::tag('span', [], $this->mainMenu[$i]['title']);
                    } else {
                        echo CHtml::link($this->mainMenu[$i]['title'], $url, $urlParam);
                    }

                    echo CHtml::openTag('ul');
                    foreach ($this->mainMenu[$i]['children'] as $second) {
                        $param = $segments[2] == $second['alias'] ? array('class' => 'active') : array();
                        echo CHtml::openTag('li', $param);

                        $url = $second['type'] == Pages::TYPE_LINK ? $second['alias'] : sprintf('/%s/%s/%s', $this->language->code, $this->mainMenu[$i]['alias'], $second['alias']);
                        $urlParam = $second['type'] == Pages::TYPE_LINK && $second['in_new_window'] ? array('target' => '_blank') : array();
                        if ($second['type'] == Pages::TYPE_NOT_A_LINK) {
                            echo CHtml::tag('span', [], $second['title']);
                        } else {
                            echo CHtml::link($second['title'], $url, $urlParam);
                        }

                        echo CHtml::closeTag('li');
                    }
                    echo CHtml::closeTag('ul');

                    echo CHtml::closeTag('li');
                } else {
                    $param = $segments[1] == $this->mainMenu[$i]['alias'] ? array('class' => 'active') : array();
                    echo CHtml::openTag('li', $param);
                    $url = $this->mainMenu[$i]['type'] == Pages::TYPE_LINK ? $this->mainMenu[$i]['alias'] : sprintf('/%s/%s', $this->language->code, $this->mainMenu[$i]['alias']);
                    $urlParam = $this->mainMenu[$i]['type'] == Pages::TYPE_LINK && $this->mainMenu[$i]['in_new_window'] ? array('target' => '_blank') : array();
                    if ($this->mainMenu[$i]['type'] == Pages::TYPE_NOT_A_LINK) {
                        echo CHtml::tag('span', [], $this->mainMenu[$i]['title']);
                    } else {
                        echo CHtml::link($this->mainMenu[$i]['title'], $url, $urlParam);
                    }

                    echo CHtml::closeTag('li');
                }
            }
            ?>
            <li class="separator">&nbsp;</li>
            <?php $this->widget('GeoPopupWidget') ?>
            <li class="dropdown"><span><?= $this->language->title ?></span>
                <?php $this->widget('LanguagesWidget', array('righted' => true)) ?>
            </li>
        </ul>
    </div>
    <div class="highlighter">
        <div class="tip"></div>
    </div>
</nav>