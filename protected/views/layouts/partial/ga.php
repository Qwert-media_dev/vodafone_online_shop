<?php if( param('ga') != '' ): ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', '<?= param('ga') ?>', 'auto');
    ga('send', 'pageview');

</script>
<?php endif; ?>

<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1470062626546109');
    fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1470062626546109&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=YiCxmiSqx4Tgw*9WA01OmxEf243CRilzW8s9oLB3PQgkQ*eMrSYkuMSr*OZe/LLosKV2TMp8Bm1R6ZQd2s2uDnyLAjubyJgDluN*fTEDXLFou6GAAYWdq7YLkB6EUB/DgodDG6Dcqc9TPLrWhb0OVFYWadJBRm9rY9kSuq53M4Q-';</script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=WyWkT9b4/b4F3lPYLLwOfmpas2G/9VaLz3MlWaRP9ihx1xKh2lqgE0Wqb5bwD/RDwc519sGqv86/NXW4lyD6/4nU0TsvOn81L5WQdZARUrD31WDHgiuDb/2sTNFP9eIf/1uuV7R2aPgFBArqFzIP5T0P6Szcdu1rdLFVwpDQR/E-';</script>
