<?php
/* @var $this SiteController */
$segments = $folders = FS::getSegments();
?>
<div class="langs">
    <?php
    // active
    foreach($this->languages as $lang) {
        $params = array();
        if($lang->code == $this->language->code) $params['class'] = 'active';
        $segments[0] = $lang->code;
        echo CHtml::link( $lang->title, '/'.implode('/', $segments), $params );
    }
    ?>
</div>
