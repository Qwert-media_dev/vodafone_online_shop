<?php
/* @var $this Controller */
$protocol = app()->request->isSecureConnection ? 'https' : 'http';
?>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?= $this->getSeoParam('seo_title') ?></title>
<meta name="description" content="<?= $this->getSeoParam('seo_description') ?>">
<link rel="stylesheet" href="/fonts/vodafone/stylesheet.css"/>
<link rel="stylesheet" href="/css/styles.css?1"/>
<link rel="stylesheet" href="/css/table_fix.css"/>
<link rel="apple-touch-icon" sizes="57x57" href="/img/fav/57x57.png"/>
<link rel="apple-touch-icon" sizes="114x114" href="/img/fav/114x114.png"/>
<link rel="apple-touch-icon" sizes="72x72" href="/img/fav/72x72.png"/>
<link rel="apple-touch-icon" sizes="144x144" href="/img/fav/144x144.png"/>
<link rel="apple-touch-icon" sizes="60x60" href="/img/fav/60x60.png"/>
<link rel="apple-touch-icon" sizes="120x120" href="/img/fav/120x120.png"/>
<link rel="apple-touch-icon" sizes="76x76" href="/img/fav/76x76.png"/>
<link rel="apple-touch-icon" sizes="152x152" href="/img/fav/152x152.png"/>
<link rel="icon" type="image/png" href="/img/fav/16x16.png" sizes="16x16"/>
<link rel="icon" type="image/png" href="/img/fav/32x32.png" sizes="32x32"/>
<link rel="icon" type="image/png" href="/img/fav/96x96.png" sizes="96x96"/>
<link rel="icon" type="image/png" href="/img/fav/160x160.png" sizes="160x160"/>
<link rel="icon" type="image/png" href="/img/fav/196x196.png" sizes="196x196"/>
<meta name="msapplication-TileColor" content="#fff"/>
<meta name="msapplication-TileImage" content="/img/fav/144x144.png"/>
<meta name="google-site-verification" content="JxvnpDvBRrRV0ZYcbU7kL8XL2sOZlVatMd4uVFeCb9c" />
<script>
    var oldieLink = '<?= url_to('site/oldie',['lang' => $this->language->code]) ?>';
    document.createElement("picture");
</script>
<script src="/vendor/ua-parser-js-master/dist/ua-parser.min.js"></script>
<script src="/js/checkBrowser.js"></script>
<script src="/vendor/promise-ajax-js/promise.min.js"></script>
<script src="/vendor/velocity/velocity.min.js"></script>
<script src="/vendor/hammer.js/hammer.min.js"></script>
<script src="/vendor/picturefill/dist/picturefill.min.js" async="async"></script>
<?php if($this->useGoogleMap): ?>
<script id="g-maps-api" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFftRlEFofw71iJ4AsB9ow8QS20zI256c&amp;sensor=false&amp;libraries=places&amp;extension=.js&amp;language=<?= $this->language->code ?>"></script>
<?php endif; ?>
<script src="/vendor/js-marker-clusterer/src/markerclusterer_compiled.js"></script>
<meta property="og:url" content="<?= sprintf('%s://%s/%s', $protocol, $_SERVER['HTTP_HOST'], cleanupPathInfo(app()->request->getPathInfo()) ) ?>"/>
<meta property="og:title" content="<?= $this->getOgParam('og_title') ?>"/>
<meta property="og:description" content="<?= $this->getOgParam('og_description') ?>"/>
<meta property="og:image" content="<?= sprintf('%s://%s%s', $protocol, $_SERVER['HTTP_HOST'], $this->getOgParam('og_image')) ?>"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="<?= Yii::t('app', 'Vodafone Україна') ?>"/>
<meta property="fb:app_id" content="1746838905537955"/>
<?php if( $this->noIndex ): ?>
<meta name="robots" content="noindex, nofollow">
<?php endif; ?>
<?php if($this->language->code == param('defaultLang')): ?>
<link rel="canonical" href="<?= sprintf('%s/%s', app()->request->getHostInfo(), cleanupPathInfo(app()->request->getPathInfo())) ?>" />
<?php endif; ?>
<?php Alts::instance()->render(); ?>
<?php if($this->isAppLink): ?>
<meta name="apple-itunes-app" content="app-id=<?= $this->appLinkData['store_id'] ?>">
<meta name="google-play-app" content="app-id=<?= $this->appLinkData['package'] ?>">
<link rel="stylesheet" href="/css/jquery.smartbanner.css" type="text/css" media="screen">
<script src="/js/jquery.smartbanner.js"></script>
<script type="text/javascript">
    $(function(){
        $.smartbanner({title:"<?= $this->appLinkData['name'] ?>", daysHidden: 0, daysReminder: 0, icon: "/images/play-market-tv.png"});
    });
</script>
<meta property="al:ios:app_store_id" content="<?= $this->appLinkData['store_id'] ?>" />
<meta property="al:ios:app_name" content="<?= $this->appLinkData['name'] ?>" />
<meta property="al:android:app_name"  content="<?= $this->appLinkData['name'] ?>" />
<meta property="al:android:package" content="<?= $this->appLinkData['package'] ?>" />
<?php endif; ?>
<?php if($this->isTv): ?>
<link type="text/css" rel="stylesheet" media="screen" href="/css/normalize.css" />
<link type="text/css" rel="stylesheet" media="screen" href="/css/app.css" />
<script type="text/javascript" src="/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
<?php endif; ?>
<?php
echo $this->extra_css;
echo $this->extra_js;
?>