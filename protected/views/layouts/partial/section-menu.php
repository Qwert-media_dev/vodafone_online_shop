<?php
/**
 * Второй уровень меню плюс плащка с поиском
 * а так же выбор языков для мобильной версии
 */

/* @var $this Controller */
/* @var $segments array */
$segments = PathRegistry::instance()->getSegments();
$landingID = is_null(app()->session->get('landingID')) ? $this->landing->id1 : app()->session->get('landingID');
$alias = '';
$comparePages = param('skipBusiness') ? [$this->mainMenu[0]['alias'] ] : [$this->mainMenu[0]['alias'], $this->mainMenu[1]['alias'] ] ;
if( $segments[1] == '' ) {
    foreach($this->mainMenu as $item) {
        if($item['id'] == $landingID) {
            $alias = $item['alias'];
        }
    }
} elseif( in_array($segments[1], $comparePages) ) {
    foreach($this->mainMenu as $item) {
        if($item['alias'] == $segments[1]) {
            $alias = $item['alias'];
        }
    }
} else {
    foreach($this->mainMenu as $item) {
        if($item['id'] == $landingID) {
            $alias = $item['alias'];
        }
    }
}
?>

<nav class="section-menu">
	<button class="search-button"></button>
	<button class="lang-button"><?= $this->language->title ?></button>
    <button class="location-button"></button>
	<div class="main_menu">
		<div class="sections">
            <?php $this->widget('GeoConfirmWidget') ?>
			<ul>
                <?php
                // вывод меню на красной плашки, только для мобильной версии
                foreach($this->mainMenu as $item) {
                    $param = $item['alias'] == $segments[1] ? array('class' => 'active') : array();
                    echo CHtml::openTag('li', $param);

                    $param = [];
                    if( sizeof($item['children']) )
                        $param['data-section'] = URLify::filter($item['alias']);
                    if($item['type'] == Pages::TYPE_NOT_A_LINK) {
                        echo CHtml::tag('span', $param, $item['title']);
                    } else {
                        if($item['type'] == Pages::TYPE_LINK) {
                            if( $item['in_new_window'] )
                                $param['target'] = '_blank';
                            $url = $item['alias'];
                        } else {
                            $url = sprintf('/%s/%s', $this->language->code, $item['alias']);
                        }
                        echo CHtml::link( $item['title'], $url, $param);
                    }
                    echo CHtml::closeTag('li');
                }
                ?>
			</ul>
		</div>
        <?php
        // красной плашки и подразделов для каждого раздела, если есть
        foreach($this->mainMenu as $item): ?>
        <?php   if( count($item['children']) ): ?>
		<ul class="menu-list <?= URLify::filter($item['alias']) ?> <?php if($item['alias'] == $alias ) echo ' current'; ?>">
            <?php
                foreach($item['children'] as  $second) {
                    if( count($second['children']) ) {
                        $param = $item['alias'] == $alias && $second['alias'] == $segments[2] ? array('class' => 'dropdown active') : array('class' => 'dropdown');
                        echo CHtml::openTag('li', $param);
                            echo CHtml::tag('span', array(), $second['title']);
                            echo CHtml::openTag('div', array('class' => 'submenu'));
                                echo CHtml::openTag('ul');
                                foreach($second['children'] as $third) {
                                    $param = $item['alias'] == $alias && $second['alias'] == $segments[2] && URLify::filter($third['alias']) == $segments[3] ? array('class' => 'active') : array();
                                    echo CHtml::openTag('li', $param);
                                    if($third['type'] == Pages::TYPE_NOT_A_LINK) {
                                        echo CHtml::tag('span', [], $third['title']);
                                    } else {
                                        $param = array();
                                        if($third['type']==Pages::TYPE_LINK ) {
                                            if($third['in_new_window'])
                                                $param = array('target' => '_blank');
                                            $url = $third['alias'];
                                        } else {
                                            $url = sprintf('/%s/%s/%s/%s', $this->language->code, $item['alias'], $second['alias'], $third['alias']);
                                        }
                                        echo CHtml::link( $third['title'], $url, $param );
                                    }
                                    echo CHtml::closeTag('li');
                                }
                                echo CHtml::closeTag('ul');
                            echo CHtml::closeTag('div');
                        echo CHtml::closeTag('li');
                    } else {
                        $param = $item['alias'] == $alias && $second['alias'] == $segments[2] ? array('class' => 'active') : array();
                        echo CHtml::openTag('li', $param);
                        if( $second['type'] == Pages::TYPE_NOT_A_LINK ) {
                            echo CHtml::tag('span', [], $second['title']);
                        } else {
                            $param = array();
                            if($second['type'] == Pages::TYPE_LINK ) {
                                if( $second['in_new_window'] )
                                    $param = array('target' => '_blank');
                                $url = $second['alias'];
                            } else {
                                $url = sprintf('/%s/%s/%s', $this->language->code, $item['alias'], $second['alias']);
                            }
                            echo CHtml::link( $second['title'], $url, $param );
                        }
                        echo CHtml::closeTag('li');
                    }
                }
            ?>
		</ul>
        <?php   endif; ?>
        <?php endforeach; ?>
		<button class="close"><?= Yii::t('app', 'Закрити') ?></button>
	</div>
	<div class="search">
        <?= CHtml::beginForm(array('search/index', 'lang' => $this->language->code), 'get') ?>
			<div class="submit-wrap">
				<button type="submit" class="submit"><span><?= Yii::t('app', 'Шукати') ?></span></button>
			</div>
			<div class="search-input-wrap">
				<input type="search" name="q" placeholder="<?= Yii::t('app', 'Шукати...') ?>" class="search-input"/>
			</div>
		<?= CHtml::endForm() ?>
		<button class="close"><?= Yii::t('app', 'Закрити') ?></button>
	</div>
	<div class="langs">
		<?php $this->widget('LanguagesWidget')?>
		<button class="close"><?= Yii::t('app', 'Закрити') ?></button>
	</div>
    <?php $this->widget('GeoLocationsWidget') ?>
</nav>
