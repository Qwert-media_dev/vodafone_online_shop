<?php
/* @var $this Controller */
/* @var $segments array */

$segments = PathRegistry::instance()->getSegments();

$limit = param('skipBusiness') ? 0 : 1;
$pos = 0; $items = array();
foreach($this->mainMenu as $idx => $item) {
	if( $item['alias'] == $segments[1] ) {
		$pos = $idx;
	}
}


if($pos > $limit) {
    $items = $this->mainMenu[$pos]['children'];
    $left = false;
} elseif( $segments[2] != '' ) {
    foreach($this->mainMenu[$pos]['children'] as $item) {
        if($item['alias'] == $segments[2]) {
            $items = $item['children'];
            $left = true;
        }
    }
}
?>

<?php if( count($items) ): ?>
<nav class="inner-menu">
	<ul>
		<?php
		foreach($items as $item) {
            if(!$item['is_tabbed_menu']) {
                $param = ($item['alias'] == $segments[$left ? 3 : 2] || URLify::filter($item['alias']) == $segments[$left ? 3 : 2] ) ? array('class' => 'active') : array();
                echo CHtml::openTag('li', $param);
                if ($item['hide_title']) {
                    echo CHtml::tag('span', [], $item['title']);
                } else {
                    $param = array();
                    if ($item['type'] == Pages::TYPE_LINK) {
                        if ($item['in_new_window'])
                            $param['target'] = '_blank';
                        $url = $item['alias'];
                    } else {
                        if ($left)
                            $url = sprintf('/%s/%s/%s/%s', $this->language->code, $segments[1], $segments[2], $item['alias']);
                        else
                            $url = sprintf('/%s/%s/%s', $this->language->code, $segments[1], $item['alias']);
                    }
                    echo CHtml::link($item['title'], $url, $param);
                }
                echo CHtml::closeTag('li');
            }
		}
		?>
	</ul>
</nav>
<?php endif; ?>