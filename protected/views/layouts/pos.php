<?php
/* @var $this Controller */
/* @var $content string */
require 'partial/top.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'partial/meta.php'; ?>
</head>
<body>
<?php include 'partial/after-body.php'; ?>
<!--[if lt IE 9]>
<style>
	body {
		overflow: hidden
	}
</style>
<div id="oldie" class="oldie">
	<div class="txt">Your browser is too old, outdated and insecure.<br>Please update</div>
</div><![endif]-->
<!--+facebook_init('')-->
<header id="header">
	<?php include 'partial/global-menu.php'; ?>
	<div class="promo-section">
		<div class="section-menu-container">
			<div class="logo"><div class="romb"></div><a href="<?= url_to('site/index', array('lang' => $this->language->code)) ?>"><img src="/img/logo.png"/></a></div>
			<button class="burger"></button>
			<?php include 'partial/section-menu.php'; ?>
		</div>
		<div class="inner-info-container">
			<div class="wrap">
				<div class="inner-navs">
                    <?php $this->widget('FrontBreadcrumpsWidget', array('breadcrumbs' => $this->breadcrumbs )); ?>
				</div>
				<div class="shops-page-title-container">
					<?php $this->widget('PosFilterWidget'); ?>
					<h1 class="page-title"><?= $this->pageTitle ?></h1>
				</div>
			</div>
		</div>
	</div>
</header>
<section id="container">
	<div class="inner-content-container shops">
		<?= $content ?>
	</div>
</section>
<?php include 'partial/footer.php'; ?>
</body>
</html>