<?php
/* @var $this Controller */
/* @var $content string */
require 'partial/top.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'partial/meta.php'; ?>
</head>
<body class="main-page">
<?php include 'partial/after-body.php'; ?>
<!--[if lt IE 9]>
<style>
    body {
        overflow: hidden
    }
</style>
<div id="oldie" class="oldie">
    <div class="txt">Your browser is too old, outdated and insecure.<br>Please update</div>
</div><![endif]-->
<header id="header">
    <?php include 'partial/global-menu.php'; ?>
    <div class="promo-section has-slider">
        <div class="section-menu-container">
            <?php if( app()->controller->action->id =='index' ): ?>
            <div class="logo"><div class="romb"></div><img src="/img/logo.png"/></div>
            <?php else: ?>
            <div class="logo"><div class="romb"></div><a href="<?= url_to('site/index', array('lang' => $this->language->code)) ?>"><img src="/img/logo.png"/></a></div>
            <?php endif; ?>
            <button class="burger"></button>
            <?php include 'partial/section-menu.php'; ?>
        </div>
        <?= $this->widgetContent ?>
    </div>
</header>
<section id="container">
    <div id="page_main">
        <?= $content; ?>
        <?= $this->footerBanner ?>
    </div>
</section>
<?php include 'partial/footer.php'; ?>
</body>
</html>