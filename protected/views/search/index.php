<?php
/* @var $this SearchController */
/* @var $search Search */
?>
<div class="inner-content-container search-results">
	<div class="wrap">
		<div class="search-box">
			<div class="container">
				<?= CHtml::beginForm(array('search/index', 'lang' => $this->language->code), 'get') ?>
					<div class="submit-wrap">
						<button type="submit" class="submit"><span><?= Yii::t('app', 'Шукати') ?></span></button>
					</div>
					<div class="search-input-wrap">
						<input type="search" name="q" value="<?= CHtml::encode($_GET['q']) ?>" placeholder="<?= Yii::t('app', 'Шукати...') ?>" class="search-input"/>
					</div>
				<?= CHtml::endForm() ?>
				<div class="counter"><?= Yii::t('app', 'Знайдено <strong>{n}</strong> результат|Знайдено <strong>{n}</strong> результата|Знайдено <strong>{n}</strong> результатів', $search->getTotal()) ?></div>
			</div>
		</div>
		<div class="search-results-list">
            <?php foreach($search->getItems() as $item): ?>
			<article class="search-result">
				<div class="container">
					<div class="column full">
						<div class="index"><?= $item['num'] ?></div>
                        <a href="<?= $item['href'] ?>">
							<h1><?= $item['title'] ?></h1>
							<div class="url"><?= $item['href'] ?></div>
                        </a>
						<div class="excerpt">
							<?= $item['summary'] ?>
						</div>
					</div>
				</div>
			</article>
            <?php endforeach; ?>
		</div>
	</div>
    <?php $this->widget('DamnPager', array('pages' => $search->getPagination())); ?>
</div>