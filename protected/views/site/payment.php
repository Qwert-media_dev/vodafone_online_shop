<?php
/* @var $this SiteController */
/* @var $item Pages */
/* @var $values PaymentPresets[] */

echo CHtml::beginForm(array('site/process', 'lang' => $this->language->code), 'post')
?>
    <div class="container targets">
        <div class="column full">
            <div class="num">
                <label class="radio">
                    <input type="radio" name="target" value="phone" checked="checked"/>
                    <div class="label"><?= Yii::t('app', 'Номер мобільного телефону') ?></div>
                </label>
                <div class="group"><span>+38</span>
                    <div class="select">
                        <?php echo CHtml::dropDownList('code', '', param('codes')); ?>
                    </div>
                    <div class="input">
                        <input type="text" name="num" maxlength="7" tabindex="-1" />
                    </div>
                </div>
            </div>
            <div class="account">
                <label class="radio">
                    <input type="radio" name="target" value="account" />
                    <div class="label"><?= Yii::t('app', 'Новий особовий рахунок МТС') ?></div>
                </label>
                <div class="group">
                    <input type="text" name="account" maxlength="12"  tabindex="-1" />
                </div>
                <p class="signature"><?= Yii::t('app', 'наприклад, 101234567891') ?></p>
            </div>
        </div>
    </div>
    <div class="container amount">
        <div class="column full">
            <label class="label"><?= Yii::t('app', 'Сума поповнення') ?></label>
            <div class="presets">
                <div class="group">
                    <?php foreach($values as $idx => $value): ?>
                    <button data-value="<?= $value->preset ?>" class="btn<?php if($idx == 0){ ?> active<?php } ?>"><?= $value->preset ?></button>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="sum">
                <div class="group">
                    <input data-max="14999" data-min="1" max="14999" min="1" type="number" name="sum" value="<?= count($values) ? $values[0]->preset : '' ?>" maxlength="5" tabindex="1"/><span><?= Yii::t('app', 'грн'); ?></span>
                </div>
                <p class="signature"><?= Yii::t('app', 'Максимум') ?> 14999 <?= Yii::t('app', 'грн'); ?></p>
            </div>
        </div>
    </div>
    <div class="container methods">
        <div class="column full">
            <div class="label"><?= Yii::t('app', 'Платiжна система') ?></div>
            <div class="method">
                <label class="radio pb">
                    <input type="radio" name="method" value="pb" />
                    <div class="label"><img src="/img/payments/pb.png" alt="privat bank"/></div>
                </label>
                <label class="radio portmone">
                    <input type="radio" name="method" value="portmone" />
                    <div class="label"><img src="/img/payments/portmone.png" alt="portmone"/></div>
                </label>
                <label class="radio easypay">
                    <input type="radio" name="method" value="easypay" />
                    <div class="label"><img src="/img/payments/easypay.png" alt="easypay"/></div>
                </label>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="column full">
            <div class="submit">
                <button disabled="disabled" type="submit" class="btn primary"><?= Yii::t('app', 'Далi') ?></button>
            </div>
        </div>
    </div>
<?php echo CHtml::endForm(); ?>

<?= $item->i18n[0]->body; ?>