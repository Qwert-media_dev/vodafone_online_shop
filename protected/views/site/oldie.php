<?php
/* @var $this SiteController */
$urls = [
    'uk' => [
        'chrome' => 'https://www.google.com/intl/uk/chrome/browser/desktop/',
        'ie' => 'http://windows.microsoft.com/uk-ua/internet-explorer/download-ie',
        'firefox' => 'https://www.mozilla.org/uk/firefox/new/',
        'opera' => 'http://www.opera.com/uk'
    ],
    'ru' => [
        'chrome' => 'https://www.google.ru/intl/ru/chrome/browser/desktop/',
        'ie' => 'http://windows.microsoft.com/ru-ru/internet-explorer/download-ie',
        'firefox' => 'https://www.mozilla.org/ru/firefox/new/',
        'opera' => 'http://www.opera.com/ru'
    ],
    'en' => [
        'chrome' => 'https://www.google.co.uk/intl/en/chrome/browser/desktop/',
        'ie' => 'http://windows.microsoft.com/en-gb/internet-explorer/download-ie',
        'firefox' => 'https://www.mozilla.org/en-US/firefox/new/',
        'opera' => 'http://www.opera.com/'
    ],
];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <link rel="stylesheet" href="main.css">
    <title></title>
    <style>
        *{margin:0;padding:0;outline:0}html{background-color:#eee}body{max-width:1300px;width:100%;margin:0 auto}#container{width:100%}.logo-wrapper{max-width:980px;width:100%;margin:0 auto 16px}.logo-wrapper img{margin-left:-41px}#container .main{width:100%;background-color:#FFF}#container .main .text{max-width:980px;width:100%;margin:0 auto 35px;padding-top:29px;font-family:Arial,serif;font-size:22px}#container .main .text>p{padding-left:10px;padding-right:10px;line-height:1.4em}.browser-container{max-width:980px;width:100%;margin:0 auto}.browser{margin-left:10px}.browser li{list-style-type:none;float:left;margin-right:20px;margin-bottom:10px}.browser li a{display:block;padding-top:10px;width:220px;height:60px;border:1px solid #d2d2d2;text-decoration:none}.browser li a img{border:none;float:left;display:block}.browser span{font-family:Arial,serif;padding-left:20px;line-height:15px}span.name{color:#e60000;font-size:16px}span.company{color:#4a4d4e;font-size:11px}img.chrome{padding-left:16px}img.fr,img.ie{padding-left:13px}img.opera{padding-left:16px}
    </style>
</head>
<body>
<div id="container">
    <div class="logo-wrapper">
        <img src="/img/oldie/logo.jpg" alt="">
    </div>
    <div class="main">
        <div class="text">
            <p><?= Yii::t('app', 'На жаль, браузер, яким ви користуєтеся, застарів, і не може нормально відображати сайт. Будь ласка, скачайте будь-який з наступних браузерів:') ?></p>
        </div>
        <div class="browser-container">
            <ul class="browser">
                <li>
                    <a href="<?= $urls[$this->language->code]['chrome'] ?>">
                        <img class="chrome" src="/img/oldie/chrome.jpg" alt="">
                        <span class="name">Chrome</span> <br>
                        <span class="company">Google</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $urls[$this->language->code]['ie'] ?>">
                        <img class="ie" src="/img/oldie/ie.jpg" alt="">
                        <span class="name">Internet Explorer</span> <br>
                        <span class="company">Microsoft</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $urls[$this->language->code]['firefox'] ?>">
                        <img class="fr" src="/img/oldie/firefox.jpg" alt="">
                        <span class="name">Firefox</span> <br>
                        <span class="company">Mozilla</span>
                    </a>
                </li>
                <li>
                    <a href="<?= $urls[$this->language->code]['opera'] ?>">
                        <img class="opera" src="/img/oldie/opera.jpg" alt="">
                        <span class="name">Opera</span> <br>
                        <span class="company">Software ASt</span>
                    </a>
                </li>
            </ul>
        </div>
        <div style="clear: both;display: block;height: 60px"></div>
    </div>
</div>
</body>
</html>
