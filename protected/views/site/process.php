<?php
/* @var $this SiteController */
/* @var $code string */
/* @var $target string */
/* @var $num string */
/* @var $sum string */
/* @var $account string */

echo CHtml::beginForm('https://www.portmone.com.ua/r3/vodafone/', 'post', array('id' => 'payment', 'style' => 'display:none'));
if( $target == 'phone' )
    echo CHtml::hiddenField('phone', sprintf('38%s%s', $code, $num ));
else
    echo CHtml::hiddenField('account', $account);
echo CHtml::hiddenField('lang', $this->language->code);
echo CHtml::hiddenField('amount', $sum);
echo CHtml::endForm();
?>
Loading...
<script>document.getElementById('payment').submit();</script>
