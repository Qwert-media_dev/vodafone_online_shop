<?php
/* @var $this SiteController */
/* @var $tree array */
?>
<div class="site-map-container">
    <?php foreach($tree as $row): ?>
    <div class="map-container-row">
        <?php foreach($row as $item): ?>
        <div class="column">
            <ul class="first-level">
                <li>
                    <?php
                    if( $item['type'] == Pages::TYPE_LINK ) {
                        $url = $item['alias'];
                    } else {
                        $url = sprintf('/%s/%s', $this->language->code, $item['alias']);
                    }
                    echo CHtml::link($item['title'], $url);

                    if( count($item['children']) ) {
                        echo CHtml::openTag('ul', ['class' => 'second-level']);
                        foreach($item['children'] as $second) {
                            echo CHtml::openTag('li');

                            if( $second['type'] == Pages::TYPE_LINK ) {
                                $url = $second['alias'];
                            } else {
                                $url = sprintf('/%s/%s/%s', $this->language->code, $item['alias'], $second['alias']);
                            }
                            echo CHtml::link($second['title'], $url);

                            if( count($second['children']) ) {
                                echo CHtml::openTag('ul', ['class' => 'third-level']);
                                foreach($second['children'] as $third) {
                                    echo CHtml::openTag('li');

                                    if( $third['type'] == Pages::TYPE_LINK ) {
                                        $url = $third['alias'];
                                    } else {
                                        $url = sprintf('/%s/%s/%s/%s', $this->language->code, $item['alias'], $second['alias'], $third['alias']);
                                    }
                                    echo CHtml::link($third['title'], $url);

                                    echo CHtml::closeTag('l');
                                }
                                echo CHtml::closeTag('ul');
                            }

                            echo CHtml::closeTag('li');
                        }
                        echo CHtml::closeTag('ul');
                    }
                    ?>
                    </li>
            </ul>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endforeach; ?>
</div>