<?php
/* @var $item Pages */
/* @var $this SiteController */

if($this->layout == 'main') {
	?>
	<div class="container">
		<div class="row">
			<div class="inner-content-container">
				<?= $item->i18n[0]->body ?>
			</div>
		</div>
	</div>
	<?php
	//echo CHtml::tag('div', ['class' => 'container'], $item->i18n[0]->body);
} else {
	echo $item->i18n[0]->body;
}

if($item->type == Pages::TYPE_MODULE) {

	Yii::import('application.modules.manage.widgets.*');
	if( $item->controller != '' ) {
		$classes = explode(',', $item->controller);
		foreach($classes as $class) {
			$class = trim($class);
			$class = $class . 'Widget';
			if (class_exists($class)) {
				$this->widget($class);
			}
		}
	}

	echo $item->i18n[0]->body2;
}
