<?php

class m151221_144917_create_table_message extends CDbMigration
{
	const TABLE = 'message';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'int not null',
			'language' => 'varchar(16) not null',
			'translation' => 'text not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->addPrimaryKey('pk_'.self::TABLE.'_id_language', self::TABLE, ['id', 'language']);
		$this->addForeignKey('fk_'.self::TABLE.'_id', self::TABLE, 'id', 'source_message', 'id', 'CASCADE', 'RESTRICT');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}