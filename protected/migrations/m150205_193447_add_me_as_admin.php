<?php

class m150205_193447_add_me_as_admin extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->insert('admins', array(
			'name' => 'Sasha',
			'email' => 'sasha.rudenko@gmail.com',
			'password' => '$2y$13$6TaAoTRNMVLQ5e434axMbOEj.NVQp5faWJBdwuPLAgI40icstIbeC'
        ));
	}

	public function safeDown()
	{
		$this->delete('admins', 'email=:email', array(':email' => 'sasha.rudenko@gmail.com'));
	}

}