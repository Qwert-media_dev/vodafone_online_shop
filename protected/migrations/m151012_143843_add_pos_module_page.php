<?php

class m151012_143843_add_pos_module_page extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$langs = Languages::model()->findAll();
		$statuses = array();
		foreach($langs as $lang) {
			$statuses[$lang->code] = 0;
		}

		$page = new Pages();
		$page->type = Pages::TYPE_MODULE;
		$page->title = 'Point of sale';
		$page->sort = 999;
		$page->statuses = CJSON::encode($statuses);
		if( $page->save()) {
			foreach($langs as $lang) {
				$i18n = new PagesI18n();
				$i18n->page_id = $page->id;
				$i18n->lang_id = $lang->id;
				$i18n->title = 'Point of sale';
				$i18n->alias = 'pos';
				$i18n->save();
			}
		}

	}

	public function safeDown()
	{
	}

}