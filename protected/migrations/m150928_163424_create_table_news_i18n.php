<?php

class m150928_163424_create_table_news_i18n extends CDbMigration
{
	const TABLE = 'news_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'lang_id' => 'int not null',
			'news_id' => 'int not null',
			'status' => 'int not null',
			'title' => 'varchar(255) not null',
			'short' => 'text not null',
			'body' => 'text not null'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_news_id', self::TABLE, 'news_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_news_id', self::TABLE, 'news_id', 'news', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}