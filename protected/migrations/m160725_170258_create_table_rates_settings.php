<?php

class m160725_170258_create_table_rates_settings extends CDbMigration
{
    const TABLE = 'rates_settings';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'type' => "enum('private', 'contact', 'business') not null default 'private'",
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}