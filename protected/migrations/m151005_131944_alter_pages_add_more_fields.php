<?php

class m151005_131944_alter_pages_add_more_fields extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'hide_in_menu', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'hide_in_menu');
	}

}