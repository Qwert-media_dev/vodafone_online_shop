<?php

class m160729_175239_alter_services_add_is_deleted extends CDbMigration
{
    const TABLE = 'services';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'is_deleted', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'is_deleted');
    }

}