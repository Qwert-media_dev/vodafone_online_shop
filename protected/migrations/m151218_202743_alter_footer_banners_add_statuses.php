<?php

class m151218_202743_alter_footer_banners_add_statuses extends CDbMigration
{
	const TABLE = 'footer_banner';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'statuses', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'statuses');
	}

}