<?php

class m151204_123700_alter_news_i18n_add_og extends CDbMigration
{
	const TABLE = 'news_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'og_title', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'og_description', 'text not null');
		$this->addColumn(self::TABLE, 'og_image', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'og_image');
		$this->dropColumn(self::TABLE, 'og_description');
		$this->dropColumn(self::TABLE, 'og_title');

	}

}