<?php

class m151116_121219_settings_params extends CDbMigration
{
	const TABLE = 'settings_params';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'thekey' => 'varchar(255) not null',
			'title' => 'varchar(255) not null',
			'type' => "enum('input', 'textarea') not null default 'input'",
			'value_uk' => 'text not null',
			'value_en' => 'text not null',
			'value_ru' => 'text not null',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}