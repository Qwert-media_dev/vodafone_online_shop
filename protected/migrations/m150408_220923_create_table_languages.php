<?php

class m150408_220923_create_table_languages extends CDbMigration
{
    const TABLE = 'languages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'code' => 'varchar(255) not null',
            'title' => 'varchar(255) not null',
            'sort' => 'int not null',
            'is_default' => 'int not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

        $this->insert(self::TABLE, array(
            'code' => 'uk',
            'title' => 'Укр',
            'sort' => 1,
            'is_default' => 1
        ));

        $this->insert(self::TABLE, array(
            'code' => 'en',
            'title' => 'Eng',
            'sort' => 2,
            'is_default' => 1
        ));

	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}