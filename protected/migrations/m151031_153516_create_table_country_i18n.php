<?php

class m151031_153516_create_table_country_i18n extends CDbMigration
{
	const TABLE = 'country_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'lang_id' => 'int not null',
			'country_id' => 'int not null',
			'title' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_country_id', self::TABLE, 'country_id');

		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_country_id', self::TABLE, 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}