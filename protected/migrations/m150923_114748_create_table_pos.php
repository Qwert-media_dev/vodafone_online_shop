<?php

class m150923_114748_create_table_pos extends CDbMigration
{
	const TABLE = 'pos';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'pos_city_id' => 'int not null',
			'sort' => 'int not null',
			'status' => 'int not null',
			'lat' => 'float(10,6) not null',
			'lng' => 'float(10,6) not null',
			'zoom' => 'int not null'
		),'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_pos_city_id', self::TABLE, 'pos_city_id');
		$this->addForeignKey('fk_'.self::TABLE.'_pos_city_id', self::TABLE, 'pos_city_id', 'pos_city', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}