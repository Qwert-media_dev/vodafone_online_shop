<?php

class m160701_164921_alter_front_slider_remove_button_color extends CDbMigration
{
    const TABLE = 'front_slider';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->dropColumn(self::TABLE, 'button_color');
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE, 'button_color', 'varchar(255) not null');
    }

}