<?php

class m160816_161521_alter_table_pages_add_5mins extends CDbMigration
{
    const TABLE = 'pages';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'extra_css', 'longtext not null');
        $this->addColumn(self::TABLE, 'extra_js', 'longtext not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'extra_css');
        $this->dropColumn(self::TABLE, 'extra_js');
    }

}