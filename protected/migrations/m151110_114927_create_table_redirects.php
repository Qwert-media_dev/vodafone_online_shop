<?php

class m151110_114927_create_table_redirects extends CDbMigration
{
	const TABLE = 'redirects';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'url_from' => 'varchar(255) not null',
			'url_to' => 'varchar(255) not null',
			'alarm' => 'int not null'
		), 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}