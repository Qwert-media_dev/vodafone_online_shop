<?php

class m160731_224621_create_table_geo_exclude extends CDbMigration
{
    const TABLE = 'geo_exclude';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'table' => 'varchar(255) not null',
            'table_pk' => 'int not null',
            'region_id' => 'mediumint(8) unsigned NOT NULL',
            'city_id' => 'mediumint(8) unsigned NOT NULL'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_table', self::TABLE, 'table');
        $this->createIndex('idx_'.self::TABLE.'_table_pk', self::TABLE, 'table_pk');
        $this->createIndex('idx_'.self::TABLE.'_region_id', self::TABLE, 'region_id');
        $this->createIndex('idx_'.self::TABLE.'_city_id', self::TABLE, 'city_id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}