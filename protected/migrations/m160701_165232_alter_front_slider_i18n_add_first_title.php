<?php

class m160701_165232_alter_front_slider_i18n_add_first_title extends CDbMigration
{
    const TABLE = 'front_slider_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'first_title', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'first_title');
    }

}