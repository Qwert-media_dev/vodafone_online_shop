<?php

class m151015_120856_alter_languages_add_long_title extends CDbMigration
{
	const TABLE = 'languages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'long_title', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'long_title');
	}

}