<?php

class m160729_150614_alter_rates_i18n_add_more_titles extends CDbMigration
{
    const TABLE = 'rates_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'tab_title', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'tab_title');
    }

}