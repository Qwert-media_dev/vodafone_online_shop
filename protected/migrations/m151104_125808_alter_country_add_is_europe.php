<?php

class m151104_125808_alter_country_add_is_europe extends CDbMigration
{
	const TABLE = 'country';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'is_europe', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'is_europe');
	}

}