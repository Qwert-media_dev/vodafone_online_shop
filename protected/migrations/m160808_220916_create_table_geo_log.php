<?php

class m160808_220916_create_table_geo_log extends CDbMigration
{
    const TABLE = 'geo_log';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'ip' => 'varchar(255) not null',
            'geo' => 'varchar(255) not null',
            'detect' => 'int not null',
            'created_at' => "datetime not null default '0000-00-00 00:00:00'",
            'updated_at' => "datetime not null default '0000-00-00 00:00:00'"
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}