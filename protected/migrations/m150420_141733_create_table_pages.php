<?php

class m150420_141733_create_table_pages extends CDbMigration
{
    const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'status' => 'int not null',
            'created_at' => 'datetime not null',
            'updated_at' => 'datetime not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}