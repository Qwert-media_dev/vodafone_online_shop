<?php

class m160629_112749_create_table_sxgeo_regions extends CDbMigration
{
    const TABLE = 'sxgeo_regions';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'mediumint(8) unsigned NOT NULL',
            'iso' => 'varchar(7) NOT NULL',
            'country' => 'char(2) NOT NULL',
            'name_ru' => 'varchar(128) NOT NULL',
            'name_en' => 'varchar(128) NOT NULL',
            'timezone' => 'varchar(30) NOT NULL',
            'okato' => 'char(4) NOT NULL'
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');

        $this->addPrimaryKey('pk_'.self::TABLE.'_id', self::TABLE, 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}