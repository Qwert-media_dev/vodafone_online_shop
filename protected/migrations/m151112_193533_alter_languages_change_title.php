<?php

class m151112_193533_alter_languages_change_title extends CDbMigration
{
	const TABLE = 'languages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->update(self::TABLE, array('title' => 'Рус'), 'code=:code', array(':code' => 'ru'));
	}

	public function safeDown()
	{
	}

}