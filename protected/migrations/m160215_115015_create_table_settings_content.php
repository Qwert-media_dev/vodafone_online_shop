<?php

class m160215_115015_create_table_settings_content extends CDbMigration
{
	const TABLE = 'settings_content';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'lang_id' => 'int not null',
			'content' => 'text not null',
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');

		$languages = Languages::model()->sorted()->findAll();
		foreach($languages as $language) {
			$this->insert(self::TABLE, [
				'lang_id' => $language->id,
			]);
		}
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}