<?php

class m151113_132049_alter_pages_add_protected extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'protected', 'int not null');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE, 'protected');
	}

}