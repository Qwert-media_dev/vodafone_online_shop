<?php

class m160721_122237_alter_search_log_add_exception extends CDbMigration
{
    const TABLE = 'search_log';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'exception', 'text not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'exception');
    }

}