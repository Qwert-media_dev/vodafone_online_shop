<?php

class m151021_185135_alter_pages_i18n_add_hide_alias extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn(self::TABLE, 'hide_alias', 'int not null');
	}

	public function safeDown()
	{
        $this->dropColumn(self::TABLE, 'hide_alias');
	}

}