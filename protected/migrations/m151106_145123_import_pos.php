<?php

class m151106_145123_import_pos extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->delete('pos');
		$this->delete('pos_city');
		$this->delete('pos_region');
		$this->importRegions();
		$this->importCities();
		$this->importPoints();
	}

	public function safeDown()
	{
	}

	protected function importRegions(){
		$data = CJSON::decode( file_get_contents(__DIR__.'/pos_regions.json') );
		foreach($data as $row) {
			$this->insert('pos_region', array('id' => $row['id']));
			foreach($row['i18n'] as $i18n) {
				$this->insert('pos_region_i18n', array(
					'lang_id' => $i18n['lang_id'],
					'pos_region_id' => $row['id'],
					'title' => $i18n['title']
				));
			}
		}
	}
	protected function importCities(){
		$data = CJSON::decode( file_get_contents(__DIR__.'/pos_cities.json') );
		foreach($data as $row) {
			$this->insert('pos_city', array('id' => $row['id'], 'pos_region_id' => $row['pos_region_id']));
			foreach($row['i18n'] as $i18n) {
				$this->insert('pos_city_i18n', array(
						'lang_id' => $i18n['lang_id'],
						'pos_city_id' => $row['id'],
						'title' => $i18n['title']
				));
			}
		}
	}
	protected function importPoints(){
		$skip = 0;
		$data = CJSON::decode( file_get_contents(__DIR__.'/pos.json') );
		foreach($data as $row) {
			//print_r($row);exit;
			if( !is_null($row['pos_region_id']) && !is_null($row['pos_city_id']) && !is_null($row['lat']) && !is_null($row['lng']) ) {
				$this->insert('pos', array(
						'id' => $row['id'],
						'pos_region_id' => $row['pos_region_id'],
						'pos_city_id' => $row['pos_city_id'],
						'lat' => $row['lat'],
						'lng' => $row['lng'],
						'status' => Pos::STATUS_ACTIVE
				));
				foreach ($row['i18n'] as $i18n) {
					$this->insert('pos_i18n', array(
							'lang_id' => $i18n['lang_id'],
							'pos_id' => $row['id'],
							'title' => (string)$i18n['title'],
							'address' => (string)$i18n['address'],
							'metro' => (string)$i18n['metro'],
							'how_to_find' => (string)$i18n['how_to_find'],
							'work_hours' => (string)$i18n['work_hours']
					));
				}
			} else {
				$skip++;
			}
		}
		echo "\n*** Skipped: {$skip}\n";
	}

}