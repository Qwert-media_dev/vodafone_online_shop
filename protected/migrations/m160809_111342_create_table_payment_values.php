<?php

class m160809_111342_create_table_payment_values extends CDbMigration
{
    const TABLE = 'payment_presets';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'preset' => 'int not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}