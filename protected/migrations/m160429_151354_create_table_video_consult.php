<?php

class m160429_151354_create_table_video_consult extends CDbMigration
{
	const TABLE = 'video_consult';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'lang_id' => 'int not null',
			'url' => 'varchar(255) not null',
			'status' => 'int not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');

		$langs = Languages::model()->findAll();
		foreach($langs as $lang) {
			$this->insert(self::TABLE, [
				'lang_id' => $lang->id
			]);
		}
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}