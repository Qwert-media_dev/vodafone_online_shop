<?php

class m150923_110034_create_table_pos_city extends CDbMigration
{
	const TABLE = 'pos_city';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'pos_region_id' => 'int not null',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_pos_region_id', self::TABLE, 'pos_region_id');
		$this->addForeignKey('fk_'.self::TABLE.'_pos_region_id', self::TABLE, 'pos_region_id', 'pos_region', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}