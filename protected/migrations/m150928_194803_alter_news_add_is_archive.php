<?php

class m150928_194803_alter_news_add_is_archive extends CDbMigration
{
	const TYPE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TYPE, 'is_archive', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TYPE, 'is_archive');
	}

}