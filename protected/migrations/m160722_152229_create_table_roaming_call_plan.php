<?php

class m160722_152229_create_table_roaming_call_plan extends CDbMigration
{
    const TABLE = 'roaming_call_plan';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'country_id' => 'int not null',
            'network_id' => 'int not null',
            'type' => "enum('private', 'business', 'contract') not null default 'private'",
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_country_id', self::TABLE, 'country_id');
        $this->createIndex('idx_'.self::TABLE.'_network_id', self::TABLE, 'network_id');
        $this->addForeignKey('fk_'.self::TABLE.'_country_id', self::TABLE, 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_network_id', self::TABLE, 'network_id', 'network', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}