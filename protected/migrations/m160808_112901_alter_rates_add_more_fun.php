<?php

class m160808_112901_alter_rates_add_more_fun extends CDbMigration
{
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn('rates', 'widget', 'varchar(255) not null');
        $this->addColumn('rates_i18n', 'content_after', 'longtext not null');
    }

    public function safeDown()
    {
        $this->dropColumn('rates_i18n', 'content_after');
        $this->dropColumn('rates', 'widget');
    }

}