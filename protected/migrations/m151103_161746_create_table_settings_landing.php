<?php

class m151103_161746_create_table_settings_landing extends CDbMigration
{
	const TABLE = 'settings_landing';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'id1' => 'int not null',
			'id2' => 'int not null',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->insert(self::TABLE, array('id1' => 0, 'id2' => 0));
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}