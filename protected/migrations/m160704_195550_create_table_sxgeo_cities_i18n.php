<?php

class m160704_195550_create_table_sxgeo_cities_i18n extends CDbMigration
{

    const TABLE = 'sxgeo_cities_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'lang_id' => 'int not null',
            'city_id' => 'mediumint(8) unsigned NOT NULL',
            'name' => 'varchar(255) not null',
            'status' => 'int not null'
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->createIndex('idx_'.self::TABLE.'_city_id', self::TABLE, 'city_id');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}