<?php

class m151207_164556_alter_admins_add_group_id extends CDbMigration
{
	const TABLE = 'admins';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'group_id', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'group_id');
	}

}