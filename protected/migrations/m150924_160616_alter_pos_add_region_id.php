<?php

class m150924_160616_alter_pos_add_region_id extends CDbMigration
{
	const TABLE = 'pos';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'pos_region_id', 'int not null');
		$this->createIndex('idx_'.self::TABLE.'_pos_region_id', self::TABLE, 'pos_region_id');
		$this->addForeignKey('fk_'.self::TABLE.'_pos_region_id', self::TABLE, 'pos_region_id', 'pos_region', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'pos_region_id');
	}

}