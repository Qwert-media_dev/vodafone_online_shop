<?php

class m160720_151706_create_table_rates_geo extends CDbMigration
{
    const TABLE = 'rates_geo';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'rate_id' => 'int not null',
            'region_id' => 'mediumint(8) unsigned NOT NULL',
            'city_id' => 'mediumint(8) unsigned NOT NULL'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');


        $this->createIndex('idx_'.self::TABLE.'_rate_id', self::TABLE, 'rate_id');
        $this->createIndex('idx_'.self::TABLE.'_region_id', self::TABLE, 'region_id');
        $this->createIndex('idx_'.self::TABLE.'_city_id', self::TABLE, 'city_id');
        $this->addForeignKey('fk_'.self::TABLE.'_rate_id', self::TABLE, 'rate_id', 'rates', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}