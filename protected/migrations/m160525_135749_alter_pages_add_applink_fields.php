<?php

class m160525_135749_alter_pages_add_applink_fields extends CDbMigration
{
    const TABLE = 'pages';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'app_name', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'app_store_id', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'app_package', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'app_name');
        $this->dropColumn(self::TABLE, 'app_store_id');
        $this->dropColumn(self::TABLE, 'app_package');
    }

}