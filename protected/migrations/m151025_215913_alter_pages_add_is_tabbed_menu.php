<?php

class m151025_215913_alter_pages_add_is_tabbed_menu extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'is_tabbed_menu', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'is_tabbed_menu');
	}

}