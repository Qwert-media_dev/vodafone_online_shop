<?php

class m151117_205215_alter_logs_add_user_id extends CDbMigration
{
	const TABLE = 'logs';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'user_id', 'int not null');
		$this->createIndex('idx_'.self::TABLE.'_user_id', self::TABLE, 'user_id');
		$this->createIndex('idx_'.self::TABLE.'_email', self::TABLE, 'email');
	}

	public function safeDown()
	{
		$this->dropIndex('idx_'.self::TABLE.'_user_id', self::TABLE);
		$this->dropIndex('idx_'.self::TABLE.'_email', self::TABLE);
	}

}