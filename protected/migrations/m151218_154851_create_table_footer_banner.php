<?php

class m151218_154851_create_table_footer_banner extends CDbMigration
{
	const TABLE = 'footer_banner';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'title' => 'varchar(255) not null',
			'position' => 'int not null',
			'page_id' => 'int not null',
			'is_deleted' => 'int not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}