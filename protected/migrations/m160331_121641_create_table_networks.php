<?php

class m160331_121641_create_table_networks extends CDbMigration
{
    const TABLE = 'network';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'title' => 'varchar(255) not null',
            'sort' => 'int not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}