<?php

class m151216_142900_alter_news_add_vacancy_shock extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('news', 'press','vacancy') not null default 'news'");
	}

	public function safeDown()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('news', 'press') not null default 'news'");
	}

}