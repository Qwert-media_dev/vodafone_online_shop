<?php

class m160715_130338_alter_rates_add_is_delete extends CDbMigration
{
    const TABLE = 'rates';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'is_deleted', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'is_deleted');
    }

}