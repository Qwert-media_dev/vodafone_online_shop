<?php

class m160629_114708_create_table_sxgeo_country extends CDbMigration
{
    const TABLE = 'sxgeo_country';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'tinyint(3) unsigned NOT NULL',
            'iso' => 'char(2) NOT NULL',
            'continent' => 'char(2) NOT NULL',
            'name_ru' => 'varchar(128) NOT NULL',
            'name_en' => 'varchar(128) NOT NULL',
            'lat' => 'decimal(10,5) NOT NULL',
            'lon' => 'decimal(10,5) NOT NULL',
            'timezone' => 'varchar(30) NOT NULL',
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');

        $this->addPrimaryKey('pk_'.self::TABLE.'_id', self::TABLE, 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}