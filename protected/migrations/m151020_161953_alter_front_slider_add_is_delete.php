<?php

class m151020_161953_alter_front_slider_add_is_delete extends CDbMigration
{
    const TABLE = 'front_slider';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'is_deleted', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'is_deleted');
    }


}