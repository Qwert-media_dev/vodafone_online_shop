<?php

class m160729_165835_create_table_services_i18n extends CDbMigration
{
    const TABLE = 'services_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'service_id' => 'int not null',
            'lang_id' => 'int not null',
            'status' => 'int not null',
            'content' => 'longtext not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_service_id', self::TABLE, 'service_id');
        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->addForeignKey('fk_'.self::TABLE.'_service_id', self::TABLE, 'service_id', 'services', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}