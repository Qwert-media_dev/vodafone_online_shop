<?php

class m150928_155602_alter_pos_add_type extends CDbMigration
{
	const TABLE = 'pos';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'type', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'type');
	}

}