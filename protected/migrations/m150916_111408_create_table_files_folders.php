<?php

class m150916_111408_create_table_files_folders extends CDbMigration
{

    const TABLE = 'files_folders';


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'title' => 'varchar(255) not null',
            'sort' => 'int not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}