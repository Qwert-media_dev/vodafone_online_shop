<?php

class m160623_134335_create_alter_front_slider_i18n_add_second_title extends CDbMigration
{
    const TABLE = 'front_slider_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'second_title', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'second_title');
    }

}