<?php

class m150930_173410_alter_pages_add_ml_status extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'status', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'status');
	}
}