<?php

class m160725_154815_alter_rates_add_extra_fields extends CDbMigration
{
    const TABLE = 'rates';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'rate_date', 'date not null');
        $this->addColumn(self::TABLE, 'price', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'sort', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'rate_date');
        $this->dropColumn(self::TABLE, 'price');
        $this->addColumn(self::TABLE, 'sort');
    }

}