<?php

class m151002_132729_alter_pages_add_parent_id_and_title extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'parent_id', 'int not null');
		$this->addColumn(self::TABLE, 'title', 'varchar(255) not null');
		$this->createIndex('idx_'.self::TABLE.'_parent_id', self::TABLE, 'parent_id');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'title');
		$this->dropColumn(self::TABLE, 'parent_id');
	}

}