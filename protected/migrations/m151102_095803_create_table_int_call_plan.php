<?php

class m151102_095803_create_table_int_call_plan extends CDbMigration
{
	const TABLE = 'int_call_plan';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'country_id' => 'int not null',
			'type' => "enum('private', 'business') not null default 'private'",
			'price' => 'float(8,2) not null',
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_country_id', self::TABLE, 'country_id');
		$this->addForeignKey('fk_'.self::TABLE.'_country_id', self::TABLE, 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}