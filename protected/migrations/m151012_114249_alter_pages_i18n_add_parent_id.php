<?php

class m151012_114249_alter_pages_i18n_add_parent_id extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'parent_id', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'parent_id');
	}

}