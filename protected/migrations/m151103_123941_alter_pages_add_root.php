<?php

class m151103_123941_alter_pages_add_root extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'root', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'root');
	}

}