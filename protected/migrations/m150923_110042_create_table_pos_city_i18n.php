<?php

class m150923_110042_create_table_pos_city_i18n extends CDbMigration
{
	const TABLE = 'pos_city_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'lang_id' => 'int not null',
			'pos_city_id' => 'int not null',
			'title' => 'varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_pos_city_id', self::TABLE, 'pos_city_id');

		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_pos_city_id', self::TABLE, 'pos_city_id', 'pos_city', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}