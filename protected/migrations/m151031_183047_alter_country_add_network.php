<?php

class m151031_183047_alter_country_add_network extends CDbMigration
{
	const TABLE = 'country';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'network', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'network');
	}

}