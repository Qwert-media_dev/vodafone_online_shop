<?php

class m151117_131135_create_table_logs extends CDbMigration
{
	const TABLE = 'logs';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'email' => 'varchar(255) not null',
			'table' => 'varchar(255) not null',
			'pk' => 'int not null',
			'message' => 'text not null',
			'ip' => 'int unsigned not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE);
	}

}