<?php

class m151218_164321_create_table_footer_banner_i18n extends CDbMigration
{
	const TABLE = 'footer_banner_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'lang_id' => 'int not null',
			'footer_banner_id' => 'int not null',
			'alt' => 'varchar(255) not null',
			'image' => 'varchar(255) not null',
			'status' => 'int not null',
			'link' => 'varchar(255) not null',
			'in_new_window' => 'varchar(255) not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_footer_banner_id', self::TABLE, 'footer_banner_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_footer_banner_id', self::TABLE, 'footer_banner_id', 'footer_banner', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}