<?php

class m160729_165839_create_table_services_geo extends CDbMigration
{
	const TABLE = 'services_geo';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'service_id' => 'int not null',
			'region_id' => 'mediumint(8) unsigned NOT NULL',
			'city_id' => 'mediumint(8) unsigned NOT NULL'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');


		$this->createIndex('idx_'.self::TABLE.'_service_id', self::TABLE, 'service_id');
		$this->createIndex('idx_'.self::TABLE.'_region_id', self::TABLE, 'region_id');
		$this->createIndex('idx_'.self::TABLE.'_city_id', self::TABLE, 'city_id');
		$this->addForeignKey('fk_'.self::TABLE.'_service_id', self::TABLE, 'service_id', 'services', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}