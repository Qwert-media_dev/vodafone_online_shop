<?php

class m151207_164136_create_table_admin_groups extends CDbMigration
{
	const TABLE = 'admin_groups';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'title' => 'varchar(255) not null',
			'rules' => 'text not null',
			'protected' => 'int not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->insert(self::TABLE, [
			'title' => 'Суперадмины',
			'protected' => 1
		]);
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}