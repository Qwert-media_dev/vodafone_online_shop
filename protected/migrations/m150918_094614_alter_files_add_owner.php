<?php

class m150918_094614_alter_files_add_owner extends CDbMigration
{

    const TABLE = 'files';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn(self::TABLE, 'user_id', 'int not null');
	}

	public function safeDown()
	{
        $this->dropColumn(self::TABLE, 'user_id');
	}

}