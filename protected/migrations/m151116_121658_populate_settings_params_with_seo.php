<?php

class m151116_121658_populate_settings_params_with_seo extends CDbMigration
{
	const TABLE = 'settings_params';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->insert(self::TABLE, [
			'thekey' => 'seo_title',
			'title' => 'SEO Заголовок по умолчанию',
			'type' => 'input'
		]);

		$this->insert(self::TABLE, [
			'thekey' => 'seo_description',
			'title' => 'SEO Описание по умолчанию',
			'type' => 'input'
		]);

	}

	public function safeDown()
	{
		$this->delete(self::TABLE, 'thekey=:thekey', [':thekey' => 'seo_description']);
		$this->delete(self::TABLE, 'thekey=:thekey', [':thekey' => 'seo_title']);
	}

}