<?php

class m150202_234650_create_table_admins extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable('admins', array(
			'id' => 'pk',
			'name' => 'varchar(255) not null',
			'email' => 'varchar(255) not null',
			'password' => 'varchar(255) not null'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable('admins');
	}

}