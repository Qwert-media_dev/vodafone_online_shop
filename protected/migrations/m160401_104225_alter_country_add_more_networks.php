<?php

class m160401_104225_alter_country_add_more_networks extends CDbMigration
{
    const TABLE = 'country';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'network_id_contract', 'int not null');
        $this->addColumn(self::TABLE, 'network_id_prepaid', 'int not null');
        $this->createIndex('idx_'.self::TABLE.'_network_id_contract', self::TABLE, 'network_id_contract');
        $this->createIndex('idx_'.self::TABLE.'_network_id_prepaid', self::TABLE, 'network_id_prepaid');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'network_id_contract');
        $this->dropColumn(self::TABLE, 'network_id_prepaid');
    }

}