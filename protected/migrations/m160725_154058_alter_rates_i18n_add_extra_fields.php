<?php

class m160725_154058_alter_rates_i18n_add_extra_fields extends CDbMigration
{
    const TABLE = 'rates_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'second_title', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'extra_price', 'varchar(255) not null');

        $this->addColumn(self::TABLE, 'pic1', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'pic2', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'pic3', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'pic4', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'second_title');
        $this->dropColumn(self::TABLE, 'extra_price');

        $this->dropColumn(self::TABLE, 'pic1');
        $this->dropColumn(self::TABLE, 'pic2');
        $this->dropColumn(self::TABLE, 'pic3');
        $this->dropColumn(self::TABLE, 'pic4');
    }

}