<?php

class m160311_104447_create_table_feedbacks_emails extends CDbMigration
{
	const TABLE = 'feedbacks_emails';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'email' => 'varchar(255) not null',
            'created_at' => 'datetime not null',
            'updated_at'=> 'datetime not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}

