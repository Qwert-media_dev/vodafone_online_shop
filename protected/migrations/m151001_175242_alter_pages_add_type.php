<?php

class m151001_175242_alter_pages_add_type extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'type', "enum('page','link') not null default 'page'");
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'type');
	}

}