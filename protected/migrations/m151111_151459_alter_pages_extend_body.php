<?php

class m151111_151459_alter_pages_extend_body extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->alterColumn(self::TABLE, 'body', 'longtext not null');
	}

	public function safeDown()
	{
		$this->alterColumn(self::TABLE, 'body', 'text not null');
	}

}