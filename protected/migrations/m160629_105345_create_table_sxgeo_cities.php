<?php

class m160629_105345_create_table_sxgeo_cities extends CDbMigration
{
    const TABLE = 'sxgeo_cities';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'mediumint(8) unsigned NOT NULL',
            'region_id' => 'mediumint(8) unsigned NOT NULL',
            'name_ru' => 'varchar(128) not null',
            'name_en' => 'varchar(128) not null',
            'lat' => 'decimal(10,5) NOT NULL',
            'lon' => 'decimal(10,5) NOT NULL',
            'okato' => 'varchar(20) NOT NULL'
        ], 'ENGINE=MyISAM DEFAULT CHARSET=utf8');

        $this->addPrimaryKey('pk_'.self::TABLE.'_id', self::TABLE, 'id');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}