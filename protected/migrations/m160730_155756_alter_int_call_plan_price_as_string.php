<?php

class m160730_155756_alter_int_call_plan_price_as_string extends CDbMigration
{
    const TABLE = 'int_call_plan';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->alterColumn(self::TABLE, 'price', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->alterColumn(self::TABLE, 'price', 'float(8,2) not null');
    }

}