<?php

class m160726_181204_alter_rates_i18n_add_alias extends CDbMigration
{
    const TABLE = 'rates_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'alias', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'alias');
    }

}