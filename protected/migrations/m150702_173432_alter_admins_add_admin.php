<?php

class m150702_173432_alter_admins_add_admin extends CDbMigration
{

	const TABLE = 'admins';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->insert(self::TABLE, array(
			'name' => 'Admin',
			'email' => 'admin@lelikbolik.com',
			'password' => '$2y$13$RHKVl5wbMj08c8ndzuWYaOJpV/JqT1gaEUv73Ed19Cm3WL2yuVgRm'
		));
	}

	public function safeDown()
	{
		$this->delete(self::TABLE, 'email=:email', array(':email' => 'admin@lelikbolik.com'));
	}
}