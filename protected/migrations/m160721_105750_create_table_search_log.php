<?php

class m160721_105750_create_table_search_log extends CDbMigration
{
    const TABLE = 'search_log';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'query' => 'varchar(255) not null',
            'code' => 'int not null',
            'response' => 'longtext not null',
            'created_at' => "datetime not null default '0000-00-00 00:00:00'",
            'updated_at' => "datetime not null default '0000-00-00 00:00:00'"
        ], 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}