<?php

class m151115_223509_alter_pages_i18n_add_seo extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'seo_title', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'seo_description', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'seo_description');
		$this->dropColumn(self::TABLE, 'seo_title');
	}

}