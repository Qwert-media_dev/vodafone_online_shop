<?php

class m160629_162626_create_table_front_slider_geo extends CDbMigration
{
    const TABLE = 'front_slider_geo';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'front_slider_id' => 'int not null',
            'region_id' => 'mediumint(8) unsigned NOT NULL',
            'city_id' => 'mediumint(8) unsigned NOT NULL'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_front_slider_id', self::TABLE, 'front_slider_id');
        $this->createIndex('idx_'.self::TABLE.'_region_id', self::TABLE, 'region_id');
        $this->createIndex('idx_'.self::TABLE.'_city_id', self::TABLE, 'city_id');
        $this->addForeignKey('fk_'.self::TABLE.'_front_slider_id', self::TABLE, 'front_slider_id', 'front_slider', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}