<?php

class m151025_225431_alter_pages_add_is_deleted extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'is_deleted', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'is_deleted');
	}

}