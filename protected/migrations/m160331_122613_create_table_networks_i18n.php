<?php

class m160331_122613_create_table_networks_i18n extends CDbMigration
{
    const TABLE = 'network_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'lang_id' => 'int not null',
            'network_id' => 'int not null',
            'title' => 'varchar(255) not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->createIndex('idx_'.self::TABLE.'_network_id', self::TABLE, 'network_id');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_network_id', self::TABLE, 'network_id', 'network', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}