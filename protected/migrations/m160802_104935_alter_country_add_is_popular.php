<?php

class m160802_104935_alter_country_add_is_popular extends CDbMigration
{
    const TABLE = 'country';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'is_popular', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'is_popular');
    }

}