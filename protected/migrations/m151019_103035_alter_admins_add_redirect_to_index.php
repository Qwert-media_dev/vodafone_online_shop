<?php

class m151019_103035_alter_admins_add_redirect_to_index extends CDbMigration
{
	const TABLE = 'admins';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn(self::TABLE, 'show_index', 'int not null');
	}

	public function safeDown()
	{
        $this->dropColumn(self::TABLE, 'show_index');
	}

}