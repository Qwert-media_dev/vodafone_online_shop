<?php

class m151002_183542_alter_pages_add_sort extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'sort', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'sort');
	}

}