<?php

class m151012_154907_create_table_front_slider extends CDbMigration
{
	const TABLE = 'front_slider';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'sort' => 'int not null',
			'title' => 'varchar(255) not null',
			'statuses' => 'varchar(255) not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}