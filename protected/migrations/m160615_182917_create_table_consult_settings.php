<?php

class m160615_182917_create_table_consult_settings extends CDbMigration
{
    const TABLE = 'consult_settings';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'type' => "enum('video', 'text') not null default 'video'",
            'statuses' => 'varchar(255) not null',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}