<?php

class m151012_154915_create_table_front_slider_i18n extends CDbMigration
{
	const TABLE = 'front_slider_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
				'lang_id' => 'int not null',
				'front_slider_id' => 'int not null',
				'status' => 'int not null',
				'in_new_window' => 'int not null',
				'title' => 'varchar(255) not null',
				'alt' => 'varchar(255) not null',
				'link' => 'varchar(255) not null',
				'pic1' => 'varchar(255) not null',
				'pic2' => 'varchar(255) not null',
				'pic3' => 'varchar(255) not null',
				'pic4' => 'varchar(255) not null',

		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_front_slider_id', self::TABLE, 'front_slider_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_front_slider_id', self::TABLE, 'front_slider_id', 'front_slider', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}