<?php

class m160701_131524_alter_front_slider_add_colors extends CDbMigration
{
    const TABLE = 'front_slider';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'title_color', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'second_title_color', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'button_color', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'title_color');
        $this->dropColumn(self::TABLE, 'second_title_color');
        $this->dropColumn(self::TABLE, 'button_color');
    }

}