<?php

class m160324_153111_alter_news_add_page_id extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn(self::TABLE, 'page_id', 'int');
        $this->createIndex('idx_'.self::TABLE.'_page_id', self::TABLE, 'page_id');
        $this->addForeignKey('fk_'.self::TABLE.'_page_id', self::TABLE, 'page_id', 'pages', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
        $this->dropColumn(self::TABLE, 'page_id');
	}

}