<?php

class m150923_114756_create_table_pos_i18n extends CDbMigration
{
	const TABLE = 'pos_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
				'lang_id' => 'int not null',
				'pos_id' => 'int not null',
				'title' => 'varchar(255) not null',
				'address' => 'varchar(255) not null',
				'local_name' => 'varchar(255) not null',
				'metro' => 'varchar(255) not null',
				'how_to_find' => 'text not null',
				'work_hours' => 'text not null'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->createIndex('idx_'.self::TABLE.'_pos_id', self::TABLE, 'pos_id');

		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk_'.self::TABLE.'_pos_id', self::TABLE, 'pos_id', 'pos', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}