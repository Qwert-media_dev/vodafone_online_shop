<?php

class m151104_140513_alter_int_call_plan_change_type extends CDbMigration
{
	const TABLE = 'int_call_plan';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('private','business','contract') not null default 'private'");
	}

	public function safeDown()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('private','business') not null default 'private'");
	}

}