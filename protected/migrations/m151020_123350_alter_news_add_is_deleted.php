<?php

class m151020_123350_alter_news_add_is_deleted extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn(self::TABLE, 'is_deleted', 'int not null');
	}

	public function safeDown()
	{
        $this->dropColumn(self::TABLE, 'is_deleted');
	}

}