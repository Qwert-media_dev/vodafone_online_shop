<?php

class m150916_102646_create_table_files extends CDbMigration
{

    const TABLE = 'files';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'original_name' => 'varchar(255) not null',
            'local_name' => 'varchar(255) not null',
            'ext' => 'varchar(255) not null',
            'size' => 'int not null',
            'created_at' => 'datetime not null',
            'updated_at' => 'datetime not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}