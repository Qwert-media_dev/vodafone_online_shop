<?php

class m160715_160133_alter_rates_i18n_add_og extends CDbMigration
{
    const TABLE = 'rates_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'og_title', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'og_description', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'og_image', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'og_image');
        $this->dropColumn(self::TABLE, 'og_description');
        $this->dropColumn(self::TABLE, 'og_title');
    }

}