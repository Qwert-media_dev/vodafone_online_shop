<?php

class m150928_163419_create_table_news extends CDbMigration
{

	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, array(
			'id' => 'pk',
			'type' => "enum('news', 'press') not null default 'news'",
			'news_date' => 'datetime not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}