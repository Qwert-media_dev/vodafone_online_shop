<?php

class m151116_133658_populate_settings_params_with_og extends CDbMigration
{
	const TABLE = 'settings_params';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->insert(self::TABLE, [
			'thekey' => 'og_title',
			'title' => 'Open Graph Заголовок по умолчанию',
			'type' => 'input',
			'value_uk' => 'Vodafone Україна',
			'value_en' => 'Vodafone Ukraine',
			'value_ru' => 'Vodafone Украина',
		]);

		$this->insert(self::TABLE, [
			'thekey' => 'og_description',
			'title' => 'Open Graph Описание по умолчанию',
			'type' => 'input',
			'value_uk' => 'Україна вітає Європу',
			'value_en' => 'Ukraine welcomes Europe',
			'value_ru' => 'Украина приветствует Европу',
		]);

		$this->insert(self::TABLE, [
			'thekey' => 'og_image',
			'title' => 'Open Graph Фото по умолчанию',
			'type' => 'input',
			'value_uk' => '/images/vodafone_open_uk.jpg',
			'value_en' => '/images/vodafone_open_en.jpg',
			'value_ru' => '/images/vodafone_open_ru.jpg',
		]);
	}

	public function safeDown()
	{
		$this->delete(self::TABLE, 'thekey=:thekey', [':thekey' => 'og_image']);
		$this->delete(self::TABLE, 'thekey=:thekey', [':thekey' => 'og_description']);
		$this->delete(self::TABLE, 'thekey=:thekey', [':thekey' => 'og_title']);
	}

}