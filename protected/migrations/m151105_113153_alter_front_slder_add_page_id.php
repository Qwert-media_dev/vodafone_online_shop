<?php

class m151105_113153_alter_front_slder_add_page_id extends CDbMigration
{
	const TABLE = 'front_slider';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'page_id', 'int not null');
		$this->createIndex('idx_'.self::TABLE.'_page_id', self::TABLE, 'page_id');
		$landing = SettingsLanding::model()->find();
		$this->update(self::TABLE, array('page_id' => $landing->id1));
		$this->addForeignKey('fk_'.self::TABLE.'_page_id', self::TABLE, 'page_id', 'pages', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropForeignKey('fk_'.self::TABLE.'_page_id', self::TABLE);
		$this->dropColumn(self::TABLE, 'page_id');
	}

}