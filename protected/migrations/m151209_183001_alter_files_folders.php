<?php

class m151209_183001_alter_files_folders extends CDbMigration
{
	const TABLE = 'files_folders';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'protected', 'int not null');
		$this->insert(self::TABLE, [
			'title' => 'Content Builder',
			'protected' => 1
		]);
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}