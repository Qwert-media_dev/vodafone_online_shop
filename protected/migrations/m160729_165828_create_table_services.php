<?php

class m160729_165828_create_table_services extends CDbMigration
{
    const TABLE = 'services';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'statuses' => 'varchar(255) not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}