<?php

class m151007_130243_alter_pages_add_in_new_window extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'in_new_window', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'in_new_window');
	}

}