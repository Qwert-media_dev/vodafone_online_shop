<?php

class m150503_234216_update_languages extends CDbMigration
{
	const TABLE = 'languages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->update(self::TABLE, array('is_default' => 0), 'code=:code', array(':code' => 'uk'));
        $this->update(self::TABLE, array('is_default' => 1), 'code=:code', array(':code' => 'en'));
	}

	public function safeDown()
	{
        $this->update(self::TABLE, array('is_default' => 1), 'code=:code', array(':code' => 'uk'));
        $this->update(self::TABLE, array('is_default' => 0), 'code=:code', array(':code' => 'en'));
	}

}