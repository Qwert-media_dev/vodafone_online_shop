<?php

class m160817_141518_alter_country_add_visibility extends CDbMigration
{
    const TABLE = 'country';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'private_visibility', 'int not null');
        $this->addColumn(self::TABLE, 'contract_visibility', 'int not null');
        $this->addColumn(self::TABLE, 'business_visibility', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'private_visibility');
        $this->dropColumn(self::TABLE, 'contract_visibility');
        $this->dropColumn(self::TABLE, 'business_visibility');
    }

}