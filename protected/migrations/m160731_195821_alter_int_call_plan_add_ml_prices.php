<?php

class m160731_195821_alter_int_call_plan_add_ml_prices extends CDbMigration
{
    const TABLE = 'int_call_plan';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'price_uk', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'price_en', 'varchar(255) not null');
        $this->addColumn(self::TABLE, 'price_ru', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'price_ru');
        $this->dropColumn(self::TABLE, 'price_en');
        $this->dropColumn(self::TABLE, 'price_uk');
    }

}