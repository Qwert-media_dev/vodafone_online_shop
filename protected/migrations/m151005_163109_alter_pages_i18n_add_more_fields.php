<?php

class m151005_163109_alter_pages_i18n_add_more_fields extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'short_title', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'og_title', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'og_description', 'text not null');
		$this->addColumn(self::TABLE, 'og_image', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'image1', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'image2', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'body2', 'text not null');
		$this->addColumn(self::TABLE, 'redactor', 'text not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'redactor');
		$this->dropColumn(self::TABLE, 'body2');
		$this->dropColumn(self::TABLE, 'image2');
		$this->dropColumn(self::TABLE, 'image1');
		$this->dropColumn(self::TABLE, 'og_image');
		$this->dropColumn(self::TABLE, 'og_description');
		$this->dropColumn(self::TABLE, 'og_title');
		$this->dropColumn(self::TABLE, 'short_title');
	}

}