<?php

class m150916_113133_create_table_files_to_folders extends CDbMigration
{
	const TABLE = 'files_to_folders';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'file_id' => 'int not null',
            'folder_id' => 'int not null',
            'sort' => 'int not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_file_id', self::TABLE, 'file_id');
        $this->createIndex('idx_'.self::TABLE.'_folder_id', self::TABLE, 'folder_id');

        $this->addForeignKey('fk_'.self::TABLE.'_file_id', self::TABLE, 'file_id', 'files', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_folder_id', self::TABLE, 'folder_id', 'files_folders', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}