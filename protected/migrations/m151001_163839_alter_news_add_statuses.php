<?php

class m151001_163839_alter_news_add_statuses extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'statuses', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'statuses');
	}

}