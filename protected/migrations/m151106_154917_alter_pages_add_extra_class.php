<?php

class m151106_154917_alter_pages_add_extra_class extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'extra_class', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'extra_class');
	}

}