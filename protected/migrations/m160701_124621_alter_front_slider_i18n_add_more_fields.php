<?php

class m160701_124621_alter_front_slider_i18n_add_more_fields extends CDbMigration
{
    const TABLE = 'front_slider_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'button_title', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'button_title');
    }

}