<?php

class m160702_182415_alter_sx_tables_add_uk_field extends CDbMigration
{

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
		$this->addColumn('sxgeo_regions', 'name_uk', 'varchar(128) not null');
		$this->addColumn('sxgeo_cities', 'name_uk', 'varchar(128) not null');
    }

    public function safeDown()
    {
        $this->dropColumn('sxgeo_regions', 'name_uk');
        $this->dropColumn('sxgeo_cities', 'name_uk');
    }

}