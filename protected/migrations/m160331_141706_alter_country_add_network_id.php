<?php

class m160331_141706_alter_country_add_network_id extends CDbMigration
{
    const TABLE = 'country';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'network_id', 'int');
        $this->createIndex('idx_'.self::TABLE.'_network_id', self::TABLE, 'network_id');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'network_id');
    }

}