<?php

class m151012_130345_alter_pages_alter_type extends CDbMigration
{
	const TABLE = 'pages';

	public function safeUp()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('page','link','module') not null default 'page'");
	}

	public function safeDown()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('page','link') not null default 'page'");
	}

}