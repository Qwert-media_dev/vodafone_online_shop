<?php

class m160309_195523_create_table_feedbacks extends CDbMigration
{
	const TABLE = 'feedbacks';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'lang_id' => 'int not null',
			'type' => 'varchar(255) not null',
			'category' => 'varchar(255) not null',
			'name' => 'varchar(255) not null',
			'email' => 'varchar(255) not null',
			'code' => 'varchar(255) not null',
			'num' => 'varchar(255) not null',
			'message' => 'text not null',
            'attach' => 'varchar(255) not null',
			'raw_form' => 'text not null',
			'created_at' => 'datetime not null',
			'updated_at' => 'datetime not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

		$this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
		$this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}
