<?php

class m150930_151518_alter_news_add_date_helpers extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->dropColumn(self::TABLE, 'news_date');
		$this->addColumn(self::TABLE, 'news_date', 'date not null');
		$this->addColumn(self::TABLE, 'news_time', 'time not null');
		$this->addColumn(self::TABLE, 'news_dt', 'datetime not null');

		$this->createIndex('idx_'.self::TABLE.'_news_date', self::TABLE, 'news_date');
	}

	public function safeDown()
	{
		$this->dropIndex('idx_'.self::TABLE.'_news_date', self::TABLE);
		$this->dropColumn(self::TABLE, 'news_time');
		$this->dropColumn(self::TABLE, 'news_date');
		$this->addColumn(self::TABLE, 'news_date', 'datetime not null');
	}

}