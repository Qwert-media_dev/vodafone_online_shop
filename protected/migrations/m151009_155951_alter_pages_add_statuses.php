<?php

class m151009_155951_alter_pages_add_statuses extends CDbMigration
{
	const TABLE = 'pages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'statuses', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'statuses');
	}

}