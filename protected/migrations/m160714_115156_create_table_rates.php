<?php

class m160714_115156_create_table_rates extends CDbMigration
{
    const TABLE = 'rates';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'title' => 'varchar(255) not null',
            'statuses' => 'varchar(255) not null',
            'is_archive' => 'int not null',
            'is_active' => 'int not null',
            'is_top' => 'int not null',
            'type' => "enum('private', 'contact', 'business') not null default 'private'",
            'preview' => 'varchar(255) not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}