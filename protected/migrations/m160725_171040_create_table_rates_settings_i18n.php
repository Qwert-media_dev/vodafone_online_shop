<?php

class m160725_171040_create_table_rates_settings_i18n extends CDbMigration
{
    const TABLE = 'rates_settings_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'rates_settings_id' => 'int not null',
            'lang_id' => 'int not null',
            'pic1' => 'varchar(255) not null',
            'pic2' => 'varchar(255) not null',
            'pic3' => 'varchar(255) not null',
            'pic4' => 'varchar(255) not null',
            'og_title' => 'varchar(255) not null',
            'og_description' => 'varchar(255) not null',
            'og_image' => 'varchar(255) not null',
            'title' => 'varchar(255) not null',
            'description' => 'varchar(255) not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_rates_settings_id', self::TABLE, 'rates_settings_id');
        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->addForeignKey('fk_'.self::TABLE.'_rates_settings_id', self::TABLE, 'rates_settings_id', 'rates_settings', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}