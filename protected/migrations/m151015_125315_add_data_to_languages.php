<?php

class m151015_125315_add_data_to_languages extends CDbMigration
{
	const TABLE = 'languages';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->insert(self::TABLE, array(
			'code' => 'ru',
			'title' => 'Rus',
			'sort' => 20,
			'long_title' => 'Русский'
		));
		$this->update(self::TABLE, array('sort' => 10, 'long_title' => 'Українська'), 'code=:code', array(':code' => 'uk'));
		$this->update(self::TABLE, array('sort' => 30, 'long_title' => 'English'), 'code=:code', array(':code' => 'en'));
	}

	public function safeDown()
	{
		$this->delete(self::TABLE, 'code=:code', array(':code' => 'ru'));
	}

}