<?php

class m160714_164311_create_table_rates_advantages extends CDbMigration
{
    const TABLE = 'rates_advantages';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'rate_id' => 'int not null',
            'lang_id' => 'int not null',
            'sort' => 'int not null',
            'type' => "enum('text', 'rich') not null default 'text'",
            'title' => 'varchar(255) not null',
            'icon' => 'varchar(255) not null'
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_rate_id', self::TABLE, 'rate_id');
        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->addForeignKey('fk_'.self::TABLE.'_rate_id', self::TABLE, 'rate_id', 'rates', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}