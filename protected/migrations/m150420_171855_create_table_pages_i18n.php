<?php

class m150420_171855_create_table_pages_i18n extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(self::TABLE, array(
            'id' => 'pk',
            'lang_id' => 'int not null',
            'page_id' => 'int not null',
            'title' => 'varchar(255) not null',
            'alias' => 'varchar(255) not null',
            'body' => 'text not null'
        ), 'ENGINE=InnoDB  DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->createIndex('idx_'.self::TABLE.'_page_id', self::TABLE, 'page_id');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_page_id', self::TABLE, 'page_id', 'pages', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
        $this->dropTable(self::TABLE);
	}

}