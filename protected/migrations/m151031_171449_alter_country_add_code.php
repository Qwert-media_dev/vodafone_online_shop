<?php

class m151031_171449_alter_country_add_code extends CDbMigration
{
	const TABLE = 'country';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'code', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'code');
	}

}