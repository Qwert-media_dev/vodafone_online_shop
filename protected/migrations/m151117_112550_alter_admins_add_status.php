<?php

class m151117_112550_alter_admins_add_status extends CDbMigration
{
	const TABLE = 'admins';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'status', 'int not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'status');
	}

}