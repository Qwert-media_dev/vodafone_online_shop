<?php

class m160616_153417_initial_data_for_consults extends CDbMigration
{

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {

        // video consultant
        $videos = VideoConsult::model()->findAll();
        $model = new ConsultSettings();
        $model->type = 'video';
        $model->save(false);
        $statuses = [];
        foreach( $videos as $video ) {
            $modeli18n = new ConsultSettingsI18n();
            $modeli18n->lang_id = $video->lang_id;
            $modeli18n->consult_id = $model->id;
            $modeli18n->url = $video->url;
            $modeli18n->status = $video->status;
            $modeli18n->save(false);
            $statuses[ $video->lang->code ] = $video->status;
        }
        $model->statuses = CJSON::encode($statuses);
        $model->save(false);

        // text consultant
        $model = new ConsultSettings();
        $model->type = 'text';
        $model->save(false);
        $statuses = [];
        foreach( Languages::model()->findAll() as $lang ) {
            $modeli18n = new ConsultSettingsI18n();
            $modeli18n->lang_id = $lang->id;
            $modeli18n->consult_id = $model->id;
            $modeli18n->save(false);
            $statuses[ $lang->code ] = 0;
        }
        $model->statuses = CJSON::encode($statuses);
        $model->save(false);
	}

    public function safeDown()
    {
        $this->truncateTable('consult_settings_i18n');
        $this->truncateTable('consult_settings');
    }

}