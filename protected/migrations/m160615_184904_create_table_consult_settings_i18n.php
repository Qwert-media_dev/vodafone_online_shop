<?php

class m160615_184904_create_table_consult_settings_i18n extends CDbMigration
{
    const TABLE = 'consult_settings_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'pk',
            'lang_id' => 'int not null',
            'consult_id' => 'int not null',
            'url' => 'varchar(255) not null',
            'status' => 'int not null',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->createIndex('idx_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id');
        $this->createIndex('idx_'.self::TABLE.'_consult_id', self::TABLE, 'consult_id');
        $this->addForeignKey('fk_'.self::TABLE.'_lang_id', self::TABLE, 'lang_id', 'languages', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_'.self::TABLE.'_consult_id', self::TABLE, 'consult_id', 'consult_settings', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE);
    }

}