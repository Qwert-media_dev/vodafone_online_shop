<?php

class m160223_121446_alter_news_add_dialog extends CDbMigration
{
	const TABLE = 'news';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->alterColumn(self::TABLE, 'type', "enum('news', 'press','vacancy', 'dialog') not null default 'news'");
	}

	public function safeDown()
	{
        $this->alterColumn(self::TABLE, 'type', "enum('news', 'press','vacancy') not null default 'news'");
	}

}