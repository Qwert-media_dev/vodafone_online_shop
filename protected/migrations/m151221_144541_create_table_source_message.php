<?php

class m151221_144541_create_table_source_message extends CDbMigration
{
	const TABLE = 'source_message';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->createTable(self::TABLE, [
			'id' => 'pk',
			'category' => 'varchar(32) not null',
			'message' => 'text not null'
		], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
	}

	public function safeDown()
	{
		$this->dropTable(self::TABLE);
	}

}