<?php

class m151022_175119_alter_pages_add_new_type extends CDbMigration
{
	const TABLE = 'pages';
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('page','link','module','not-a-link') not null default 'page'");
	}

	public function safeDown()
	{
		$this->alterColumn(self::TABLE, 'type', "enum('page','link','module') not null default 'page'");
	}

}