<?php

class m160725_173358_populate_rates_settings extends CDbMigration
{

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->insertMultiple('rates_settings', [
            ['id' => 1, 'type' => 'private'],
            ['id' => 2, 'type' => 'contact'],
            ['id' => 3, 'type' => 'business'],
        ]);

        foreach(Languages::model()->findAll() as $lang) {
            $this->insert('rates_settings_i18n', ['rates_settings_id' => 1, 'lang_id' => $lang->id]);
            $this->insert('rates_settings_i18n', ['rates_settings_id' => 2, 'lang_id' => $lang->id]);
            $this->insert('rates_settings_i18n', ['rates_settings_id' => 3, 'lang_id' => $lang->id]);
        }
    }

    public function safeDown()
    {
        $this->truncateTable('rates_settings_i18n');
        $this->truncateTable('rates_settings');
    }

}