<?php

class m160706_124153_alter_sx_tables_add_popup_name_for_cities extends CDbMigration
{
    const TABLE = 'sxgeo_cities_i18n';

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'popup_name', 'varchar(255) not null');
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'popup_name');
    }

}