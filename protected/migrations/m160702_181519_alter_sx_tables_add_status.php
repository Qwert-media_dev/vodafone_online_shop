<?php

class m160702_181519_alter_sx_tables_add_status extends CDbMigration
{

    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
        $this->addColumn('sxgeo_regions', 'status', 'int not null');
        $this->addColumn('sxgeo_cities', 'status', 'int not null');
    }

    public function safeDown()
    {
        $this->dropColumn('sxgeo_regions', 'status');
        $this->dropColumn('sxgeo_cities', 'status');
    }

}