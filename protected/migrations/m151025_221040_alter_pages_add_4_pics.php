<?php

class m151025_221040_alter_pages_add_4_pics extends CDbMigration
{
	const TABLE = 'pages_i18n';

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
		$this->addColumn(self::TABLE, 'pic1', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'pic2', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'pic3', 'varchar(255) not null');
		$this->addColumn(self::TABLE, 'pic4', 'varchar(255) not null');
	}

	public function safeDown()
	{
		$this->dropColumn(self::TABLE, 'pic1');
		$this->dropColumn(self::TABLE, 'pic2');
		$this->dropColumn(self::TABLE, 'pic3');
		$this->dropColumn(self::TABLE, 'pic4');
	}

}