<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 2/4/15
 * Time: 03:01
 */

class ManageController extends CController {

    public $layout = 'manage';
    public $menu = array();
    public $breadcrumbs = array();
    public $languages = array();
    public $useFileAPI = false;

    public function filters() {
        return array(
            'accessControl',
        );
    }


    public function accessRules() {
        return array(
            array('allow',
                'users'=>array('*'),
                'actions'=>array('login'),
            ),
            array('allow',
                //'roles' => array('admin'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }


    public function init() {
        Yii::app()->setComponent('user', Yii::app()->admin);
        Yii::app()->setLanguage('ru');
        Yii::app()->errorHandler->errorAction = '/manage/default/error';
        $this->languages = LangsRegistry::instance()->getItems();

        // checking user auth
        $this->checkAuth();

        $this->checkAccess();
    }

    /**
     * @param $model CActiveRecord
     * @param $attribute string
     * @return string
     */
    public function error($model, $attribute) {
        return $model->getError($attribute) == '' ? '' : ' has-error';
    }

    protected function checkAuth()
    {
	    if( user()->isGuest ) return;

        $c = app()->controller->id;
        $a = app()->controller->action->id;
        if(
	        ( $c == 'user' && $a == 'login' ) ||
	        ( $c == 'default' && ($a == 'index' || is_null($a) ) )
        ) return ;

        $user_id = (int)user()->id;
        if ($user_id < 1) {
            user()->logout();
            $this->redirect('/');
        }

        $user = Admins::model()->findByPk($user_id);
        if (is_null($user) || $user->status == Admins::STATUS_INACTIVE) {
            user()->logout();
            $this->redirect('/');
        }
    }

	protected function checkAccess() {
		if( user()->isGuest ) return;

		$group = user()->group;
		if( is_null($group) ) {
			throw new CHttpException(403, Yii::t('app', 'У вас нет доступа, обратитесь к администратору'));
		}

		if( $group->protected ) return true;

		$rules = CJSON::decode($group->rules);
		if( !count($rules) ) {
			throw new CHttpException(403, Yii::t('app', 'У вас нет доступа, обратитесь к администратору'));
		}

		$access = new Access();
		$access->setRules($rules);
		if( !$access->allow() ) {
			throw new CHttpException(403, Yii::t('app', 'У вас нет доступа, обратитесь к администратору'));
		}
	}

}