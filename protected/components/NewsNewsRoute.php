<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/27/2015
 * Time: 05:08 PM
 */
class NewsNewsRoute extends AbstractModuleRoute
{

	public $rules = array();

	public function __construct()
	{
		$this->rules = array(
			'' => 'news/index',
			'archive' => 'news/archive',
			'<id:\d+>-<alias:[0-9a-z\-]+>' => 'news/show'
		);
	}

	/**
	 * @param $request
	 * @return bool|mixed|string
	 */
	public function getPath($request) {
		$path = false;
		foreach($this->rules as $pattern => $_path) {
			$rule = new CUrlRule($_path, $pattern);
			$compare = $rule->parseUrl( app()->getComponent('urlManager'), app()->getComponent('request'), $request, $request );
			if( $compare !== false ) {
				$path = $compare;
			}
		}

		return $path;
	}

}