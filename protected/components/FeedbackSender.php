<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 3/11/16
 * Time: 12:36
 */
class FeedbackSender
{

    protected $emails = [];
    protected $feedback;
    protected $form;

    public function __construct( $feedback_id )
    {
        $this->emails = FeedbacksEmails::model()->getItems();
        $this->feedback = Feedbacks::model()->findByPk($feedback_id);
        $this->form = CJSON::decode($this->feedback->raw_form);
    }


    public function send() {
        if( !count($this->emails) || is_null($this->feedback) ) return;

        $subject = $this->getSubject();
        $body = $this->getBody();
        foreach($this->emails as $email) {
            Mail::sendFeedback($email, [
                'subject' => $subject,
                'body' => $body,
                'name' => $this->form['name']
            ]);
        }
    }

    protected function getBody() {
        $flat = FeedbackHandler::flatten($this->form);
        $content = CHtml::openTag('ul');
		$labels = $this->attributeLabels();
        foreach($flat as $key => $value) {
            $content .= $this->parseContent($key, $value, $labels);
        }
        if( $this->feedback->attach != '' ) {
            $url = app()->createAbsoluteUrl('site/index').'images/upload/'.$this->feedback->attach;
            $value = CHtml::link($this->feedback->attach, $url);
            $content .= sprintf("<li><b>%s:</b> %s</li>\n", $labels['attach'], $value);
        }
        $content .= CHtml::closeTag('ul');
        return $content;
    }


    protected function parseContent($field, $value, $labels) {
        if( null !== $fixedValue = $this->values($field, $value) ) {
            $value = $fixedValue;
        }
        $paramName = $labels[$field] != '' ? $labels[$field] : ucfirst($field);
        return sprintf("<li><b>%s:</b> %s</li>\n", $paramName, CHtml::encode($value));
    }


    protected function values($field, $value) {
        $data = [
            'type' => [
                'gratitude' => 'Висловити подяку',
                'idea' => 'Подати ідею',
                'appeal' => 'Залишити звернення'
            ],
            'category' => [
                'connection' => 'Якість зв\'язку',
                'service' => 'Якість обслуговування',
                'finance' => 'Тарифікація, фінансові питання',
                'services' => 'Робота послуг',
                'payments' => 'Оплата',
                'loyalty' => 'Програма лояльності',
                'roaming' => 'Роумінг',
                'site' => 'Робота сайту',
                'attention' => 'Увага!*',
                'other' => 'Інше'
            ],
            'connection_signal_type' => [
                1 => 'Стійкий',
                0 => 'Не стійкий'
            ],
            'service_point' => [
                'shop' => 'Магазин',
                'center' => 'Центр обслуговування абонентів',
                'chat' => 'Online Chat'
            ],
            'finance_type' => [
                'prepaid' => 'Передплата',
                'contract' => 'Контракт'
            ],
            'finance_contract_type' => [
                'fiz' => 'Фізична особа',
                'jur' => 'Юридична особа'
            ],
            'services_type' => [
                'goodok' => 'Гудок',
                'messages' => 'Передача повідомлень',
                'content' => 'МТС Клік, контент, підписки',
                'packages' => 'Пакети хвилин, sms, МБ',
                'other' => 'Інші послуги'
            ],
            'payments_type' => [
                'fail' => 'Не надійшов платіж',
                'error' => 'Помилковий платіж'
            ],
            'roaming_signal_type' => [
                1 => 'Стійкий',
                0 => 'Не стійкий'
            ]
        ];

        return $data[$field][$value];
    }

    protected function attributeLabels()
    {
        return array(
			'email' => 'E-mail',
			'code' => 'Код',
			'num' => 'Номер телефона',
			'type' => 'Тип',
			'subject' => 'Тема сообщения',
			'message' => 'Сообщение',
			'category' => 'Тема сообщения',
			'connection_num' => 'Ваші номери телефонів, з якими спостерігаються труднощі',
			'connection_cause' => 'Причина звернення',
			'connection_address' => 'Точна адреса, де виникають проблеми',
			'connection_model' => 'Модель вашого мобільного телефону',
			'connection_services' => 'Якими послугами користуєтесь',
			'connection_see' => 'Що чуєте / бачите на телефонi',
			'connection_when' => 'Дата виникнення проблеми',
			'connection_signal' => 'Рівень сигналу',
			'connection_signal_type' => 'Тип сигналу',
			'service_point' => 'Тип точки обслуговування',
			'service_shop_when' => 'Дата і час зверення',
			'service_shop_address' => 'Адреса магазину',
			'service_shop_name' => 'П.І.Б. працівника, на якого надійшла скарга',
			'service_center_when' => 'Дата і час зверення',
			'service_center_num' => 'Номер, з якого зверталися',
			'service_center_name' => 'Ім\'я/номер працівника',
			'service_chat_when' => 'Дата і час зверення',
			'service_chat_num' => 'Номер, з якого зверталися',
			'service_chat_name' => 'Ім\'я/номер/нік працівника',
			'finance_type' => 'Тип договору',
			'finance_prepaid_code' => 'PUK-код, пароль персонального помічника (IVR-пароль) або кодове слово',
			'finance_contract_type' => 'Тип клієнта',
			'finance_contract_fiz_name' => 'П.І.Б.',
			'finance_contract_fiz_passport' => 'Паспортні дані',
			'finance_contract_fiz_password' => 'Пароль',
			'finance_contract_jur_name' => 'Назва організації',
			'finance_contract_jur_details' => 'Реквізити',
			'finance_contract_jur_password' => 'Пароль',
			'services_type' => 'Тип послуги',
			'payments_type' => 'Тип проблеми',
			'roaming_num' => 'Ваші номери телефонів, з якими спостерігаються труднощі',
			'roaming_country' => 'Країна та місто перебування',
			'roaming_operator' => 'Назва мережі роумінг-оператора',
			'roaming_model' => 'Модель вашого мобільного телефону',
			'roaming_services' => 'Якими послугами користуєтесь',
			'roaming_see' => 'Що чуєте / бачите на телефонi',
			'roaming_when' => 'Дата виникнення проблеми',
			'roaming_signal' => 'Рівень сигналу',
			'roaming_signal_type' => 'Тип сигналу',
            'attach' => 'Вложение'
        );
    }


    protected function getSubject() {
        $subject = '[feedback] ';
        
        if( $this->form['type'] == 'gratitude' ) {
            $subject .= sprintf('Выразить благодарность, тема: %s', CHtml::encode($this->form['subject']));
        } elseif( $this->form['type'] == 'idea' ) {
            $subject .= sprintf('Подать идею, тема: %s', CHtml::encode($this->form['subject']));
        } elseif( $this->form['type'] == 'appeal' ) {
            if( $this->form['category'] == 'connection' ) {
                $subject .= 'Оставить обращение, тема: Качество связи';
            } elseif( $this->form['category'] == 'service' ) {
                if( $this->form['service']['point'] == 'shop' ) {
                    $subject .= 'Оставить обращение, тема: Качество обслуживания (Магазин)';
                } elseif( $this->form['service']['point'] == 'center' ) {
                    $subject .= 'Оставить обращение, тема: Качество обслуживания (ЦОА)';
                } elseif( $this->form['service']['point'] == 'chat' ) {
                    $subject .= 'Оставить обращение, тема: Качество обслуживания (Online chat)';
                }
            } elseif( $this->form['category'] == 'finance' ) {
                if( $this->form['finance']['type'] == 'prepaid' ) {
                    $subject .= 'Оставить обращение, тема: Тарификация, фин. вопросы (Предоплата)';
                } elseif( $this->form['finance']['type'] == 'contract' ) {
                    if( $this->form['finance']['contract']['type'] == 'fiz' ) {
                        $subject .= 'Оставить обращение, тема: Тарификация, фин. вопросы (Контракт/Физ)';
                    } elseif( $this->form['finance']['contract']['type'] == 'jur' ) {
                        $subject .= 'Оставить обращение, тема: Тарификация, фин. вопросы (Контракт/Юр)';
                    }
                }
            } elseif( $this->form['category'] == 'services' ) {
                if( $this->form['services']['type'] == 'goodok' ) {
                    $subject .= 'Оставить обращение, тема: Робота услуг (Гудок)';
                } elseif( $this->form['services']['type'] == 'messages' ) {
                    $subject .= 'Оставить обращение, тема: Робота услуг (Передача сообщений)';
                } elseif( $this->form['services']['type'] == 'content' ) {
                    $subject .= 'Оставить обращение, тема: Робота услуг (МТС Клік, контент, підписки)';
                } elseif( $this->form['services']['type'] == 'packages' ) {
                    $subject .= 'Оставить обращение, тема: Робота услуг (Пакети хвилин, sms, МБ)';
                } elseif( $this->form['services']['type'] == 'other' ) {
                    $subject .= 'Оставить обращение, тема: Робота услуг (Інші послуги)';
                }
            } elseif( $this->form['category'] == 'payments' ) {
                $subject .= 'Оставить обращение, тема: Оплата';
            } elseif( $this->form['category'] == 'loyalty' ) {
                $subject .= 'Оставить обращение, тема: Программа лояльности';
            } elseif( $this->form['category'] == 'roaming' ) {
                $subject .= 'Оставить обращение, тема: Роуминг';
            } elseif( $this->form['category'] == 'site' ) {
                $subject .= 'Оставить обращение, тема: Работа сайта';
            } elseif( $this->form['category'] == 'attention' ) {
                $subject .= 'Оставить обращение, тема: Внимание';
            } elseif( $this->form['category'] == 'other' ) {
                $subject .= 'Оставить обращение, тема: Другие вопросы';
            }
        }

        return $subject;
    }

}
