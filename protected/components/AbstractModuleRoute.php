<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/27/2015
 * Time: 05:51 PM
 */
abstract class AbstractModuleRoute
{
	abstract public function getPath($request);
}