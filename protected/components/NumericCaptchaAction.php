<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 2/12/2016
 * Time: 08:26 PM
 */
class NumericCaptchaAction extends CCaptchaAction
{

	/**
	 * Generates a new verification code.
	 * @return string the generated verification code
	 */
	protected function generateVerifyCode()
	{
		if($this->minLength > $this->maxLength)
			$this->maxLength = $this->minLength;
		if($this->minLength < 3)
			$this->minLength = 3;
		if($this->maxLength > 20)
			$this->maxLength = 20;
		$length = mt_rand($this->minLength,$this->maxLength);

		$dict = '0123456789';
		$code = '';
		for($i = 0; $i < $length; ++$i)
		{
			$code.=$dict[mt_rand(0,9)];
		}

		return $code;
	}

}