<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/28/2015
 * Time: 06:43 PM
 */
class DamnPager extends CLinkPager
{

	const CSS_PREVIOUS_PAGE = 'bwd';
	const CSS_NEXT_PAGE = 'fwd';
	const CSS_SELECTED_PAGE = 'active';

	public $header = '<div class="paginator">';
	public $footer = '</div>';
	public $htmlOptions = array('class' => 'container');
	public $nextPageLabel = '';
	public $prevPageLabel = '';

	/**
	 * Creates the page buttons.
	 * @return array a list of page buttons (in HTML code).
	 */
	protected function createPageButtons() {
		if(($pageCount=$this->getPageCount())<=1)
			return array();

		list($beginPage,$endPage)=$this->getPageRange();
		$currentPage=$this->getCurrentPage(false); // currentPage is calculated in getPageRange()
		$buttons=array();


		// prev page
		if(($page=$currentPage-1)<0)
			$page=0;
		$buttons[]= CHtml::tag('div', array('class' => self::CSS_PREVIOUS_PAGE), $this->createPageButton($this->prevPageLabel,$page,'',$currentPage<=0,false));

		// internal pages
		$middleButons = '';
		for($i=$beginPage;$i<=$endPage;++$i)
			$middleButons.=$this->createPageLinks($i+1,$i,self::CSS_INTERNAL_PAGE,false,$i==$currentPage);

		$buttons[] = $this->createPageCell($middleButons);

		// next page
		if(($page=$currentPage+1)>=$pageCount-1)
			$page=$pageCount-1;
		$buttons[]= CHtml::tag('div', array('class' => self::CSS_NEXT_PAGE), $this->createPageButton($this->nextPageLabel,$page,'',$currentPage>=$pageCount-1,false));

				return $buttons;
	}


	protected function createPageButton($label,$page,$class,$hidden,$selected) {
		$arrowLeft = $arrowRight = '';

		if($class == self::CSS_PREVIOUS_PAGE)
			$arrowLeft = ''; //'&larr; ';

		if($class == self::CSS_NEXT_PAGE)
			$arrowRight = ''; //' &rarr;';

		if($hidden || $selected)
			$class.=' '.($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);

		if(strstr($class, 'inactive') !== false) {
			return '<td class="' . $class . '">'. $arrowLeft . $label . $arrowRight .'</td>';
		} else {
			return '<td class="' . $class . '">'. $arrowLeft . CHtml::link($label, $this->createPageUrl($page), array('class' => $class)) . $arrowRight .'</td>';
		}
	}


	protected function createPageLinks ($label,$page,$class,$hidden,$selected) {
		if($hidden || $selected)
			$class.=' '.($hidden ? self::CSS_HIDDEN_PAGE : self::CSS_SELECTED_PAGE);

		return CHtml::link($label,$this->createPageUrl($page), array('class' => $class));
	}


	protected function createPageCell($content) {
		return CHtml::tag('div', array('class' => 'links'), $content);
	}


	public function run() {
		$this->registerClientScript();
		$buttons=$this->createPageButtons();
		if(empty($buttons))
			return;
		echo $this->header;
		echo CHtml::tag('div', $this->htmlOptions,implode("\n",$buttons));
		echo $this->footer;
	}


}