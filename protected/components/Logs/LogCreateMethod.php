<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 06:05 PM
 */
class LogCreateMethod {

	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'Pages':
				$content = sprintf('Создана страница с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'News':
				$content = sprintf('Создана новость с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosRegion':
				$content = sprintf('Создан регион с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosCity':
				$content = sprintf('Создан город с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Pos':
				$content = sprintf('Создана точка с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Country':
				$content = sprintf('Создана страна с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'IntCallPlan':
				$content = sprintf('Создан тариф с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FrontSlider':
				$content = sprintf('Создан слайдер с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FooterBanner':
				$content = sprintf('Создан баннер для подвала с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Admins':
				$content = sprintf('Создан администратор с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Redirects':
				$content = sprintf('Создана короткая ссылка с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Files':
				$content = sprintf('Загружен файл "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'FilesFolders':
				$content = sprintf('Создана папка "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'Network':
				$content = sprintf('Создана сеть "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'RoamingCallPlan':
				$content = sprintf('Создана ромуинг сеть с параметрами "%s" и ID=%s', $this->title, $this->pk);
                break;
			case 'Rates':
				$content = sprintf('Создан тариф с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Services':
				$content = sprintf('Создана услуга для регионов "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PaymentPresets':
				$content = sprintf('Добавленна сумма "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'FeedbacksEmails':
				$content = sprintf('Создан email "%s" для формы обратной связи с ID=%s', $this->title, $this->pk);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}