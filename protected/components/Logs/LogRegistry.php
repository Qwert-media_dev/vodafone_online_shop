<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 03:37 PM
 */
class LogRegistry
{
	const METHOD_CREATE = 'create';
	const METHOD_UPDATE = 'update';
	const METHOD_DELETE = 'delete';
	const METHOD_RESTORE = 'restore';
	const METHOD_STATUS = 'status';
	const METHOD_IMPORT = 'import';
	const METHOD_RESET = 'reset';

	private static $instance;

	static function instance() {
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	protected function __construct(){}
	private function __clone(){}
	private function __wakeup(){}

	public function log( $method, $table, $pk, $title, $status=null) {
		$class = sprintf('Log%sMethod', ucfirst($method) );
		if( !class_exists($class) ) {
			throw new CHttpException(500, sprintf('Класс %s не найден.'));
		}

		if($method == 'status') {
			$message = new $class($table, $title, $pk, $status);
		} else {
			$message = new $class($table, $title, $pk);
		}

		$log = new Logs();
		$log->user_id = user()->id;
		$log->email = user()->email;
		$log->table = $table;
		$log->pk = $pk;
		$log->message = $message;
		$log->ip = ip2long( app()->request->userHostAddress );
		$log->save(false);
	}

}

