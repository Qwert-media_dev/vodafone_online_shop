<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 08:52 PM
 */
class LogStatusMethod
{
	protected $table;
	protected $title;
	protected $pk;
	protected $status;

	public function __construct($table, $title, $pk, $status)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
		$this->status = $status;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'Pos':
				$content = sprintf('Изменен статус точки с заголовком "%s" и ID=%s в состояние %s', $this->title, $this->pk, param('status')[$this->status]);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}