<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 06:06 PM
 */
class LogUpdateMethod {

	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'Pages':
				$content = sprintf('Изменена страница с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'News':
				$content = sprintf('Изменена новость с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosRegion':
				$content = sprintf('Изменен регион с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosCity':
				$content = sprintf('Изменен город с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Pos':
				$content = sprintf('Изменена точка с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Country':
				$content = sprintf('Изменена страна с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'IntCallPlan':
				$content = sprintf('Изменен тариф с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FrontSlider':
				$content = sprintf('Изменен слайдер с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FooterBanner':
				$content = sprintf('Изменен баннер для подвала с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Admins':
				$content = sprintf('Изменен администратор с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Redirects':
				$content = sprintf('Изменена короткая ссылка с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'SettingsParams':
				$content = sprintf('Изменен параметр с заголовком  "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FilesFolders':
				$content = sprintf('Изменена папка с заголовком  "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'SourceMessage':
				$content = sprintf('Изменен перевод фразы "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Network':
				$content = sprintf('Изменена сеть "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'ConsultSettings':
				$content = sprintf('Изменена настройка "Консультанта" для языка "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'SxgeoCities':
                $content = sprintf('Изменена настройка города "%s" с ID=%s', $this->title, $this->pk);
                break;
            case 'SxgeoRegions':
                $content = sprintf('Изменена настройка области "%s" с ID=%s', $this->title, $this->pk);
                break;
			case 'SettingsContent':
				$content = sprintf('Изменен контен языка "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'RoamingCallPlan':
				$content = sprintf('Изменена роуминг сеть с параметрами "%s" и ID=%s', $this->title, $this->pk);
                break;
			case 'Rates':
				$content = sprintf('Изменен тариф с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'RatesSettings':
				$content = sprintf('Изменена настройка для "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Services':
				$content = sprintf('Изменена услуга для "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PaymentPresets':
				$content = sprintf('Изменена сумма "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'FeedbacksEmails':
				$content = sprintf('Изменен email для формы обратной связи на "%s" с ID=%s', $this->title, $this->pk);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}