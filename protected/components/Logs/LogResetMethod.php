<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/2/2015
 * Time: 08:35 PM
 */
class LogResetMethod
{
	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'ResetCache':
				$content = 'Сброс кеша';
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}