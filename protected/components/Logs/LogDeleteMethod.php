<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 06:22 PM
 */
class LogDeleteMethod
{
	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'Pages':
				$content = sprintf('Удалена страница с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'News':
				$content = sprintf('Удалена нововсть с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosRegion':
				$content = sprintf('Удален регион с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PosCity':
				$content = sprintf('Удален город с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Pos':
				$content = sprintf('Удалена точка с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Country':
				$content = sprintf('Удалена страна с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'IntCallPlan':
				$content = sprintf('Удален тариф с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FrontSlider':
				$content = sprintf('Удален слайдер с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FooterBanner':
				$content = sprintf('Удален баннер для подвала с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Admins':
				$content = sprintf('Удален администратор с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Redirects':
				$content = sprintf('Удалена короткая ссылка с параметрами "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Files':
				$content = sprintf('Удален файл "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'FilesFolders':
				$content = sprintf('Удалена папка "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'SourceMessage':
				$content = sprintf('Удален перевод фразы "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'Network':
				$content = sprintf('Удалена сеть "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'RoamingCallPlan':
				$content = sprintf('Удалена роуминг сеть с параметрами "%s" и ID=%s', $this->title, $this->pk);
                break;
			case 'Rates':
				$content = sprintf('Удален тариф "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Services':
				$content = sprintf('Удалена услуга для "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'PaymentPresets':
				$content = sprintf('Удалена сумма "%s" с ID=%s', $this->title, $this->pk);
				break;
			case 'FeedbacksEmails':
				$content = sprintf('Удален email "%s" для формы обратной связи с ID=%s', $this->title, $this->pk);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}