<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/17/2015
 * Time: 06:29 PM
 */
class LogRestoreMethod
{
	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'Pages':
				$content = sprintf('Восстановлена страница с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'News':
				$content = sprintf('Восстановлена нововсть с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FrontSlider':
				$content = sprintf('Восстановлен слайдер с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'FooterBanner':
				$content = sprintf('Восстановлен баннер для подвала с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Rates':
				$content = sprintf('Восстановлен тариф с заголовком "%s" и ID=%s', $this->title, $this->pk);
				break;
			case 'Services':
				$content = sprintf('Восстановлена услуга для "%s" и ID=%s', $this->title, $this->pk);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}
}