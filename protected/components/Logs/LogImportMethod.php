<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/2/2015
 * Time: 08:31 PM
 */
class LogImportMethod
{
	protected $table;
	protected $title;
	protected $pk;

	public function __construct($table, $title, $pk)
	{
		$this->table = $table;
		$this->title = $title;
		$this->pk = $pk;
	}

	public function __toString()
	{
		switch($this->table) {
			case 'VirtualPoints':
				$content = 'Импортированно покрытие сети';
				break;
			case 'SourceMessage':
				$content = sprintf('Импортировано %s фраз', $this->title);
				break;
			default:
				throw new CHttpException(500, 'Нет обработчика для таблицы '.$this->table);
		}

		return $content;
	}

}