<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/18/2015
 * Time: 08:35 PM
 */
class LangsRegistry
{
	private static $instance;
	private $items = [];

	static function instance() {
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	protected function __construct(){
		$this->items = Languages::model()->sorted()->findAll();
	}

	private function __clone(){}
	private function __wakeup(){}

	public function getItems() {
		return $this->items;
	}

}