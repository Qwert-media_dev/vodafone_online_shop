<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/18/2015
 * Time: 07:33 PM
 */
class CachePurger
{

	protected $table;
	protected $pk;
	protected $languages=[];

	public function __construct($table, $pk)
	{
		$this->table = $table;
		$this->pk = $pk;
		$this->languages = LangsRegistry::instance()->getItems();
	}

	public function process() {
		if( !in_array($_SERVER['HTTP_HOST'], ['vf.isd-group.com'])  ) return ;

		$method = sprintf('process%s', ucfirst($this->table));
		if( method_exists($this, $method) ) {
			$this->$method($this->pk);
		}
	}

	protected function processPages($pk) {
		$paths = [];
		foreach($this->languages as $lang) {
			$paths[ $lang->code ] = sprintf('/%s/%s', $lang->code, $this->findPath($pk, $lang->id) );
		}
		foreach($paths as $request_uri) {
			$this->purge($request_uri);
		}
	}

	protected function processNews($pk) {
		$paths = [];

		$item = News::model()->findByPk($pk);

		foreach($this->languages as $lang) {
			$paths[] = url_to($item->type.'/index', array('lang' => $lang->code ));
			$paths[] = url_to($item->type.'/show', array('lang' => $lang->code, 'alias' => NewsI18n::model()->getAlias($pk, $lang->id), 'id' => $pk));
		}
		foreach($paths as $request_uri) {
			$this->purge($request_uri);
		}
	}

	protected function purge($request_uri) {
		$base_dir = '/run/vf/';
		$request_scheme = 'https';
		$request_method = 'GET';
		$http_host = param('domain');
		$cache_id = md5($request_scheme . $request_method . $http_host . $request_uri);
		$folder_level1 = substr($cache_id, -1, 1);
		$folder_level2 = substr($cache_id, -3, 2);
		$path = $base_dir . $folder_level1 . '/' . $folder_level2 . '/' . $cache_id;
		if (file_exists($path)) {
			@unlink($path);
		}
	}

	protected function findPath($id, $lang_id) {
		$path = array();

		$item = Pages::model()->getById($id, $lang_id);
		if( $item !== null ) {
			$parent_id = $item->parent_id;
			$path[] = $item->i18n[0]->alias;
			if($parent_id > 0) {
				do {
					$item = Pages::model()->getById($parent_id, $lang_id);
					if($item !== null) {
						$parent_id = $item->parent_id;
						$path[] = $item->i18n[0]->alias;
					} else {
						$parent_id = -1;
					}
				}while($parent_id>0);
			}
		}

		$path = array_reverse($path);
		return implode('/', $path);
	}
}