<?php

Yii::import('application.vendor.*');
require_once 'autoload.php';
use GuzzleHttp\Client;

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 5/17/16
 * Time: 15:34
 */
class SettingsService
{
    protected $phone;
    protected $pass = 'AS23XCWE';
    protected $cert;
    protected $key;

    const STATUS_UNSUPPORTED = 0;
    const STATUS_SUPPORTED = 1;
    const STATUS_NOT_DETECTED = 2;
    const STATUS_FAIL = 3;

    public function __construct($phone)
    {
        $this->phone = '38'.$phone;
        if( strlen($this->phone) !=12 ) {
            throw new CHttpException(404, Yii::t('app', 'Не правильный номер телефона'));
        }

        $this->cert = Yii::getPathOfAlias('application').'/cert/file.crt.pem';
        $this->key = Yii::getPathOfAlias('application').'/cert/file.key.pem';
        #e('$this->cert='.$this->cert);
        #e('$this->key='.$this->key);
        if( !file_exists($this->cert) || !file_exists($this->key) ) {
            throw new CHttpException(404, Yii::t('app', 'Нет сертификата или ключа'));
        }
    }


    /**
     * Получаем список доступных настроек
     *
     * @return array
     */
    protected function query() {
        $client = new Client();
        $response = $client->get(
            sprintf('https://80.255.65.86/webcmd/queryService?msisdn=%s&force=true', $this->phone),
            [
                'verify' => false,
                'cert' => [$this->cert, $this->pass],
                'ssl_key' => [$this->key, $this->pass]
            ]);
        return ['code' => $response->getStatusCode(), 'body' => $this->parse($response->getBody())];
    }


    protected function parse($content) {
        $lines = explode("\n", $content);
        foreach ($lines as &$line) {
            $line = trim($line);
        }
        return $lines;
    }

    protected function getStatus($params) {
        if( $params['code'] !== 200 ) {
            return self::STATUS_FAIL;
        } else {
            $hasSettings = false;
            $isNotFound = false;
            foreach ($params['body'] as $param) {
                if( substr($param, 0, 3) == 'id=' ) {
                    $hasSettings = true;
                }
                if( strpos($param, 'Subscriber not found') !== false ) {
                    $isNotFound = true;
                }
            }

            if($isNotFound) {
                return self::STATUS_NOT_DETECTED;
            } elseif( $hasSettings ) {
                return self::STATUS_SUPPORTED;
            } else  {
                return self::STATUS_UNSUPPORTED;
            }
        }
    }

    protected function hasSettings($params) {
        foreach ($params as $param) {
            if( substr($param, 0, 3) == 'id=' ) {
                return true;
            }
        }

        return false;
    }


    public function canRecieve() {
        $response = $this->query();
        //return $this->hasSettings( $this->parse($response) );
        return $this->getStatus($response);
    }


    public function send() {
        $client = new Client();
        $response = $client->get(
            sprintf('https://80.255.65.86/webcmd/sendSettings?msisdn=%s&force=true', $this->phone),
            [
                'verify' => false,
                'cert' => [$this->cert, $this->pass],
                'ssl_key' => [$this->key, $this->pass]
            ]
        );
    }

}