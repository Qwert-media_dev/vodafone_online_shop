<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{

	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public $language;
    public $languages = [];
    public $bodyClass = '';

    public $pageTitle;

    public $og_title = '';
    public $og_description;
    public $og_image;

	public $seo_title;
	public $seo_description;

    public $mainMenu;

	public $widgetContent;
	public $widgetContent2;
	public $footerBanner;
	public $landing;
	public $settings = [];

	public $extraContainerClass;

	public $useGoogleMap = false;
	public $noIndex = false;

	public $isAppLink = false;
	public $appLinkData = [];
	
	public $useVariantSelector = true;
	public $isTv = false;
	
	public $extra_css;
	public $extra_js;

    public function init() {
        $this->languages = LangsRegistry::instance()->getItems();
	    $this->landing = SettingsLanding::model()->find();
	    $this->settings = SettingsParams::model()->getTable();
    }

	/*
    public function render($view, $data = null, $return = false, $options = null) {
        // ************** GENERAL SECTION *************************
        #if( $this->og_description == '' )
        #    $this->og_description = Yii::t('app', '');
        // ************** GENERAL SECTION *************************
        parent::render($view, $data, $return);
    }
	*/

    public function beforeAction($action) {
	    # Redirect section
	    $redirects = Redirects::model()->getTable();
	    $url = '/'.ltrim(app()->request->pathInfo, '/');

	    if( array_key_exists($url, $redirects) ) {
		    $this->redirect( $redirects[$url] );
	    }
	    # /Redirect section

	    $code = $this->_getLang();
        Yii::app()->setLanguage($code);
        $this->language = Languages::model()->findByAttributes( array('code' => $code) );

	    /*
        Yii::import('application.vendor.*');
        require 'autoload.php';
        $detect = new Mobile_Detect;
        if( $detect->version('IE', Mobile_Detect::VERSION_TYPE_FLOAT) <= 8 && !(app()->controller->id == 'site' && app()->controller->action->id) ) {
            $this->redirect(array('site/oldie', 'lang' => $this->language->code));
        }
	    */

        $this->mainMenu = Pages::model()->getActiveTree(0, 0, $this->language->id);
        parent::beforeAction($action);
        return true;
    }

    protected function _getLang() {
        if ( !in_array($_GET['lang'], array('en', 'uk', 'ru')) ) {
            $_GET['lang'] = Yii::app()->params['defaultLang'];
        }

	    return $_GET['lang'];
    }


	public function populateBreadcrumbs(CActiveRecord $item, $use_item=true) {
		$path = array();

		if( $item !== null ) {
			$parent_id = $item->parent_id;
			if( $use_item ) {
				$title = $item->i18n[0]->short_title == '' ? $item->i18n[0]->title : $item->i18n[0]->short_title;
				$path[] = array('title' => $title, 'url' => $item->i18n[0]->alias, 'type' => $item->type);
			}
			if($parent_id > 0) {

				do {
					$item = Pages::model()->getById($parent_id, $this->language->id);
					if($item !== null) {
						$parent_id = $item->parent_id;
						$title = $item->i18n[0]->short_title == '' ? $item->i18n[0]->title : $item->i18n[0]->short_title;
						$path[] = array('title' => $title, 'url' => $item->i18n[0]->alias, 'type' => $item->type);
					} else {
						$parent_id = -1;
					}
				}while($parent_id>0);
			}
		}

		$path = array_reverse($path);
		$tmp = [];
		$tmp[] = $this->language->code;
		foreach($path as $item) {
			if( $item['type'] != Pages::TYPE_LINK ) {
				$tmp[] = $item['url'];
			}

			$url = '/' . implode('/', $tmp);
			if( $item['type'] == Pages::TYPE_LINK )
				$url = $item['url'];

			$this->breadcrumbs[] = array(
				'title' => $item['title'],
				'url' => $url
			);
		}
	}

	public function getOgParam($key) {
		$allowed = ['og_title', 'og_description', 'og_image'];
		if( !in_array($key, $allowed) ) return '';

		if( $this->$key == '' ) {
			return $this->settings[$key][$this->language->code];
		}

        $value = $this->$key;
        if($key == 'og_image') {
            $value = basename($value) != $value ? $value : sprintf('/images/upload/%s', $value); // дефолтное значение лежит приходит с папкой, а остальные только называние файла
        }
        return $value;
	}

	public function getSeoParam($key) {
		$allowed = ['seo_title', 'seo_description'];
		if( !in_array($key, $allowed) ) return '';

		if( $this->$key == '' ) {
			if( $key == 'seo_title' ) {
				$content = [];
				$items = array_reverse($this->breadcrumbs);
				foreach($items as $item) {
					$title = trim($item['title']);
					if( $title != '' )
						$content[] = $title;
				}
				if( count($content) ) {
					$content[] = Yii::t('app', 'Vodafone Україна');
				}
				if( count($content) )
					return implode(' : ', $content);
			}
			return $this->settings[$key][$this->language->code];
		}
		$content = $this->$key;
		if( $key == 'seo_description' ) {
			$content = flattenString($content);
			if( mb_strlen($content, 'utf-8') > 150 ) {
				$content = mb_substr($content, 0, 150, 'utf-8').'...';
			}
		}
		return $content;
	}

	public function setVirtualPathForRegistry($id, $lang_id, $lang_code) {
		$path = Pages::model()->expandPath($id, $lang_id);
		$path = array_map(function($value){
			return URLify::filter($value);
		}, $path);
		#e('[Controller::setVirtualPathForRegistry] $path='.print_r($path, true));
		array_push($path, $lang_code);
		$path = array_reverse($path);
		$url = implode('/', $path);
		PathRegistry::instance()->setSegments($url);
	}

}