<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/3/2015
 * Time: 02:17 PM
 */
class Alts
{
	private static $instance;
	private $items=[];

	static function instance() {
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	protected function __construct(){}
	private function __clone(){}
	private function __wakeup(){}

	public function addItems(array $items) {
		$this->items = $items;
	}

	public function getItem($key) {
		return $this->items[$key];
	}

	public function render() {
		if( count($this->items) < 2 ) return;
		foreach($this->items as $code => $href) {
			echo CHtml::tag('link', [
				'rel' => 'alternate',
				'href' => $href,
				'hreflang' => $code
			]);
		}
	}
}