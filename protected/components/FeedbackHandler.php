<?php

/**
 *
 */
class FeedbackHandler
{

	protected $raw_form;
	protected $form;
	protected $file;
	protected $errors;

	public function __construct()
	{
		$this->raw_form = $_POST;
		$this->form = self::flatten($this->raw_form);
        #e('[flatten] '.print_r($this->form, true));
		$this->file = CUploadedFile::getInstanceByName('attach');
	}

	public function validate() {
        $this->errors = [];
        $hasEmpty = false;
		foreach($this->form as $key => $value) {
            if( $key == 'email' ) {
                $validator = new CEmailValidator();
                if(!$validator->validateValue($value)) {
                    $hasEmpty = true;
                    $this->errors[] = $key;
                }
            } elseif( $value == '' ) {
                $hasEmpty = true;
                $this->errors[] = $key;
            }
        }

        if( $this->file !== null ) {
            $info = pathinfo($this->file->name);
            $maxSize = 1024*1024*12;
            $ext = ['doc','docm','docx','txt','rtf','pdf','ppt','pptx','xls','xlsx','zip','rar','tiff','jpg','jpeg','png','gif','mp3','avi','mov','mp4'];

            if( $this->file->size > $maxSize || !in_array(strtolower($info['extension']), $ext) ) {
                $hasEmpty = true;
                $this->errors[] = 'attach';
            }
        }

        return !$hasEmpty;
	}


    public function getErrors() {
        return $this->errors;
    }

    
    public function save() {
        $scenario = $this->raw_form['code'] == 'null' ? 'null' : 'notnull';
        $model = new Feedbacks($scenario);
        $model->lang_id = app()->getController()->language->id;
        $model->type = $this->raw_form['type'];
        $model->category = $this->raw_form['category'];
        $model->name = $this->raw_form['name'];
        $model->email = $this->raw_form['email'];
        $model->code = $this->raw_form['code'];
        $model->num = $this->raw_form['num'];
        $model->message = $this->raw_form['message'];
        $model->raw_form = CJSON::encode($this->raw_form);

        if( !is_null($this->file) ) {
            $model->attach = $this->file;
            $filename = URLify::filter( FS::hash($this->file->name.'-'.time()) . '-' . $this->file->name, 60, '', true);
            $model->attach->saveAs( param('uploadPath') . $filename );
            $model->attach = $filename;
        }

        $model->save();
        return $model->id;
    }

	public static function flatten($array, $prefix = '') {
	    $result = array();
	    foreach($array as $key=>$value) {
	        if(is_array($value)) {
	            $result = $result + self::flatten($value, $prefix . $key . '_');
	        } else {
	            $result[$prefix.$key] = trim($value);
	        }
	    }
	    return $result;
	}

}
