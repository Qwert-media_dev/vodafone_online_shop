<?php
Yii::import('application.vendor.*');
require('autoload.php');

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/12/16
 * Time: 18:07
 */
class NetworkImporter
{

    protected $files = [
        'coverages' => [
            'uk' => 'https://www.vodafone.ua/3g/uk/coverages.json',
            'en' => 'https://www.vodafone.ua/3g/en/coverages.json',
            'ru' => 'https://www.vodafone.ua/3g/ru/coverages.json'
        ],
        'points' => [
            'uk' => 'https://www.vodafone.ua/3g/uk/points-by-regions.json', //'http://3g.mts.ua/api/ua/points.json',
            'en' => 'https://www.vodafone.ua/3g/en/points-by-regions.json', //'http://3g.mts.ua/api/en/points.json',
            'ru' => 'https://www.vodafone.ua/3g/ru/points-by-regions.json', //'http://3g.mts.ua/api/ru/points.json'
        ],
        'states' => [
            'uk' => 'https://www.vodafone.ua/3g/uk/states.json',
            'en' => 'https://www.vodafone.ua/3g/en/states.json',
            'ru' => 'https://www.vodafone.ua/3g/ru/states.json'
        ]
    ];
    protected $folder;


    public function __construct()
    {
        $this->folder = Yii::getPathOfAlias('application.data').'/';
    }


    public function run() {
        if( !count($this->files) ) {
            //Yii::app()->admin->setFlash('warning', 'Нет списка файлов для импорта!');
            //$this->redirect(array('admin', 'state' => 'warning'));
            return false;
        }

        $newFiles = [];
        foreach($this->files as $key => $links) {
            foreach($links as $lang => $url) {
                $filename = sprintf('%s-%s-%s', $lang, time(), basename($url));
                $newFiles[$key][$lang] = $filename;
                $client = new GuzzleHttp\Client();
                $res = $client->get($url);
                $content = $res->getBody();
                file_put_contents($this->folder . $filename, $content);
            }
        }

        file_put_contents( $this->folder.'package.json', CJSON::encode($newFiles) );
        //Yii::app()->admin->setFlash('success', 'Файлы обновлены.');
        return true;
    }
}