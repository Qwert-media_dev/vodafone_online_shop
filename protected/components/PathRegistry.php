<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/3/2015
 * Time: 01:17 PM
 */
class PathRegistry
{

	private static $instance;
	private $segments;

	static function instance() {
		if( is_null(self::$instance) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

	protected function __construct(){
		$this->segments = $this->parseSegments();
		#e('[PathRegistry::__constructor] '.print_r($this->segments, true));
	}
	private function __clone(){}
	private function __wakeup(){}

	/**
	 * Переинициализация сегментов
	 *
	 * @param $path
	 */
	public function setSegments($path) {
		$this->segments = $this->parseSegments($path);
		#e('[PathRegistry::setSegments] $path='.$path);
		#e('[PathRegistry::setSegments] $this->segments='.print_r($this->segments, true));
	}

	public function getSegments() {
		return $this->segments;
	}

	/**
	 * Превращает ссылку в массив сегментов
	 *
	 * @param string $url
	 * @return array
	 */
	function parseSegments($url = '' ) {
		if(empty($url)) {
			$url = app()->request->getPathInfo();
		}

		// remove params
		if(strpos($url, '?')) {
			$url = substr($url, 0, strpos($url, '?'));
		}

		$url = ltrim($url, '/');
		$url = rtrim($url, '/');
		return explode('/', $url);
	}

	public function isTabbedPage($mainMenu) {
		$isTabbed = false;
		$pos = 0;
		$items = array();
		foreach($mainMenu as $idx => $item) {
			if( $item['alias'] == $this->segments[1] ) {
				$pos = $idx;
			}
		}

		$limit = param('skipBusiness') ? 0 : 1;
		if($pos > $limit) {
			$items = $mainMenu[$pos]['children'];
		} elseif( $this->segments[2] != '' ) {
			foreach($mainMenu[$pos]['children'] as $item) {
				if($item['alias'] == $this->segments[2]) {
					$items = $item['children'];
				}
			}
		}

		foreach($items as $item) {
			if( $item['is_tabbed_menu'] ) {
				$isTabbed = true;
			}
		}

		return $isTabbed;
	}

}