<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	private $cookie_part1;
	private $cookie_part2;
	public $email;
	public $user;

	/**
	 * Auto init
	 */
	public function init() {
		parent::init();
		if($this->getIsGuest()) {
			//$this->autoAuthenticate(); // TODO:
		}
		$this->updateFlash();
	}


	/**
	 * Authenticates a user.
	 */
	public function authenticate() {
		$user = Admins::model()->authorization_by_form($this->username, $this->password);
		if( $user === false )
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		else {

			$this->_id = $user['user_id'];
			$this->setUser($user['model']);
			//$this->username = $user->login;
			$this->cookie_part1 = $user['part1'];
			$this->cookie_part2 = $user['part2'];

			$this->errorCode = self::ERROR_NONE;

			$this->generate_session_id();
		}
		return !$this->errorCode;
	}


	/**
	 * Auto Authenticates a user via cookie
	 */
	public function autoAuthenticate(){
		// check sid param
		$data = explode("-", Yii::app()->session->getSessionID);
		if (count($data) != 3) {
			return false;
		}

		$user = Admins::model()->authorization_by_cookie($data[0], $data[1], $data[2]);
		if($user === false) {
			return false;
		}

		$this->_id = $user->id;
		$this->setUser($user);
		$this->username = $user->login;

		return true;
	}


	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}


	/**
	 * Generate new session ID for auth-ed user
	 */
	public function generate_session_id(){
		$old = Yii::app()->session->toArray();
		Yii::app()->session->clear();
		Yii::app()->session->destroy();

		Yii::app()->session->setSessionID($this->username . "-" . $this->cookie_part1 . "-" . $this->cookie_part2);

		Yii::app()->session->open();
		foreach ($old as $k => $v) {
			Yii::app()->session->add($k, $v);
		}
	}


	public function getUser()
	{
		return $this->user;
	}

	public function setUser(CActiveRecord $user)
	{
		$this->user = $user->attributes;
	}

}
