<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 8/9/16
 * Time: 01:28
 */
class GeoLogger
{

    private static $instance;
    private $params = [];

    static function instance() {
        if( is_null(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
    
    public function addParam($key, $value) {
        $this->params[$key] = $value;
    }
    
    public function pull() {
        if( !count($this->params) ) return;
        $model = new GeoLog();
        $model->attributes = $this->params;
        $model->save(false);
    }
}