<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 6/29/16
 * Time: 20:13
 */
class GeoHelper
{

    private static $instance;
    private $geoData=[];
    
    static function instance() {
        if( is_null(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct(){
        $this->geoData = $this->fetchData();
    }
    private function __clone(){}
    private function __wakeup(){}
    
    
    public function getItems() {
        return $this->geoData;
    }
    
    
    private function fetchData() {
        $rows1 = Yii::app()->db->createCommand()
            ->select("r.id as region_id, r.name_ru as region_name, r.id as city_id, r.name_ru as city_name")
            ->from('sxgeo_regions r')
            ->where('r.country=:country', array(':country'=>'UA'))
            ->queryAll();
        foreach($rows1 as &$value) {
            $value['city_id'] = 0;
            $value['city_name'] = '';
        }


        $rows = Yii::app()->db->createCommand()
            ->select('r.id as region_id, r.name_ru as region_name, c.id as city_id, c.name_ru as city_name')
            ->from('sxgeo_regions r')
            ->join('sxgeo_cities c', 'c.region_id=r.id')
            ->where('r.country=:country', array(':country'=>'UA'))
            ->order('region_name, city_name')
            ->queryAll();
        return array_merge($rows1, $rows);
    }


    public function listData() {
        $data = [];

        foreach($this->geoData as $row) {
            if( $row['city_id'] == 0 ) continue;
            $data[$row['region_id'].'|'.$row['region_name']][] = $row;
        }

        return $data;
    }

    public static function detect($ip) {
        $file = Yii::getPathOfAlias('application').'/data/SxGeoCity.dat';
        if( file_exists($file) && !in_array($ip, ['127.0.0.1', '::1']) ) {
            $SxGeo = new SxGeo($file);
            $resp = $SxGeo->getCityFull($ip);
            if( $resp['country']['iso'] == 'UA' ) {
                return [
                    $resp['region']['id'],
                    $resp['city']['id']
                ];
            }
        }

        $defaultRegion = SxgeoRegions::model()->findByAttributes(['iso' => 'UA-30']); // Kiev

        return [$defaultRegion->id,0];
    }

    protected function toMap() {
        $data = [];

        foreach($this->geoData as $row) {
            $data[$row['region_id'].'|'.$row['city_id']] = $row;
        }

        return $data;
    }


    public function getBannerGeo($front_slider_id) {
        $titles = array(); 
        $map = $this->toMap();
        $items = FrontSliderGeo::model()->findAllByAttributes(array('front_slider_id' => $front_slider_id));
        foreach($items as $item) {
            $key = $item->region_id.'|'.$item->city_id;
            if( $key == '0|0' ) return 'Вся Украина';
            if( $item->city_id == 0 ) {
                $titles[] = $map[ $key ]['region_name'];
            } else {
                $titles[] = $map[ $key ]['city_name'];
            }
        }
        return implode(', ', $titles);
    }


    public function getRateGeo($rate_id) {
        $titles = array();
        $map = $this->toMap();
        $items = RatesGeo::model()->findAllByAttributes(array('rate_id' => $rate_id));
        foreach($items as $item) {
            $key = $item->region_id.'|'.$item->city_id;
            if( $key == '0|0' ) return 'Вся Украина';
            if( $item->city_id == 0 ) {
                $titles[] = $map[ $key ]['region_name'];
            } else {
                $titles[] = $map[ $key ]['city_name'];
            }
        }
        return implode(', ', $titles);
    }


    public function getServiceGeo($service_id) {
        $titles = array();
        $map = $this->toMap();
        $items = ServicesGeo::model()->findAllByAttributes(array('service_id' => $service_id));
        foreach($items as $item) {
            $key = $item->region_id.'|'.$item->city_id;
            if( $key == '0|0' ) return 'Вся Украина';
            if( $item->city_id == 0 ) {
                $titles[] = $map[ $key ]['region_name'];
            } else {
                $titles[] = $map[ $key ]['city_name'];
            }
        }
        return implode(', ', $titles);
    }


    public static function getActiveItemsForMenu($lang_id) {
        $list = [];

        // список активных областей
        $criteria = new CDbCriteria();
        $criteria->select = 't.*, i18n.name as external_name';
        $criteria->condition = 't.country=:country AND i18n.status=:status AND i18n.lang_id=:lang_id';
        $criteria->params = [':country' => 'UA', ':status' => SxgeoRegions::STATUS_ACTIVE, ':lang_id' => $lang_id];
        $criteria->join = 'join sxgeo_regions_i18n i18n ON i18n.region_iso=t.iso';
        $criteria->order = 'i18n.name';
        $items = SxgeoRegions::model()->findAll($criteria);
        foreach($items as $item) {
            $list[ $item->id.'_0' ] = [
                'name' => $item->external_name,
                'nested' => []
            ];
        }

        // список активных городов
        $criteria = new CDbCriteria();
        $criteria->select = 't.region_id, i18n.*';
        $criteria->condition = 'i18n.status=:status AND i18n.lang_id=:lang_id';
        $criteria->params = [':status' => SxgeoCities::STATUS_ACTIVE, ':lang_id' => $lang_id];
        $criteria->join = 'JOIN sxgeo_cities_i18n i18n on i18n.city_id=t.id';
        $criteria->order = 'i18n.name';
        $items = SxgeoCities::model()->findAll($criteria);
        foreach($items as $item) {
            $key = $item->region_id.'_0';
            if( array_key_exists($key, $list) ) {
                $key2 = $item->region_id.'_'.$item->city_id;
                $list[$key]['nested'][$key2] = [
                    'name' => $item->name,
                    'nested' => []
                ];
            } else {
                $key = $item->region_id.'_'.$item->city_id;
                $list[$key] = [
                    'name' => $item->name,
                    'nested' => []
                ];
            }
        }

        return $list;
    }
}