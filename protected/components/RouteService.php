<?php

/**
 * Class RouteService
 * По некоторым параметрам пытаемся определить куда форвардить запрос
 *
 *
 */
class RouteService
{
	protected $url;
	protected $controller;
	protected $stop;

	public function __construct( $url, $controller, $stop )
	{
		$this->url = $url;
		$this->controller = $controller;
		$this->stop = $stop;
	}

	public function getForward() {
		list(, $module_url) = explode($this->stop, $this->url);
		$module_url = ltrim($module_url, '/');

		list($controller, $type) = explode('_', $this->controller);

		$class = ucfirst($controller).ucfirst($type).'Route';
		if( !class_exists($class) ) {
			throw new CHttpException(500, sprintf('Class %s not found.', $class));
		}

		$obj = new $class;
		$forward = $obj->getPath($module_url);
		if($forward === false) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}
		return $forward;
	}


}