<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 2/4/15
 * Time: 03:07
 */

class AdminUser extends CWebUser {

    // Store model to not repeat query.
    private $_model;


    public function getName(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->name;
    }


    public function getEmail(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->email;
    }

    public function getGroup(){
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->group;
    }

    public function getShow_Index() {
        $user = $this->loadUser(Yii::app()->user->id);
        return $user->show_index;
    }


    // Load user model.
    protected function loadUser($id=null) {
        if($this->_model===null) {
            if($id!==null)
                $this->_model = Admins::model()->with('group')->findByPk($id);
        }
        return $this->_model;
    }

}