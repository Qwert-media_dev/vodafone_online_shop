<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 06:27 PM
 */
abstract class AbstractRule
{

	protected $controller;
	protected $action;

	public function __construct($controller, $action)
	{
		$this->controller = $controller;
		$this->action = $action;
	}

	abstract public function checkAccess();
	abstract public function getInfo();

}
