<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 09:09 PM
 */
class Access
{
	protected $controller;
	protected $action;
	protected $rules = [];

	public function __construct($controller=null, $action=null)
	{
		$this->controller = $controller;
		if( is_null($this->controller) ) {
			$this->controller = app()->controller->id;
		}

		$this->action = $action;
		if( is_null($this->action) ) {
			$this->action = app()->controller->action->id;
		}
	}

	public function setRules(array $rules) {
		$this->rules = $rules;
	}

	public function getRules() {
		$rules = [];
		$files = CFileHelper::findFiles( Yii::getPathOfAlias('application.components.Rbac.Rules'), ['fileTypes' => ['php']] );
		foreach($files as $file) {
			$file = basename($file);
			$file = substr($file, 0, -4);
			if( class_exists($file) ) {
				$rule = new $file($this->controller, $this->action);
				$rules[$file] = $rule->getInfo();
			}
		}
		return $rules;
	}

	protected function commonRules() {
		return [
			//['controller' => 'user', 'action' => 'login'],
			//['controller' => 'user', 'action' => 'logout'],
			['controller' => 'user', 'action' => null],
			['controller' => 'default', 'action' => null],
			//['controller' => 'default', 'action' => 'index'],
			['controller' => 'default', 'action' => 'error']
		];
	}

	public function allow() {
		foreach( $this->commonRules() as $pair ) {
			if( $pair['controller'] == $this->controller && $pair['action'] == $this->action ) {
				return true;
			}
		}

		if( !count($this->rules) ) {
			throw new CHttpException(500, Yii::t('app', 'Не установлены правила авторизации'));
		}

		$allow = false;
		foreach($this->rules as $rule) {
			if( class_exists($rule) ) {
				$obj = new $rule($this->controller, $this->action);
				$allow = $allow || $obj->checkAccess();
			}
		}

		return $allow;
	}

	public function visible($controller) {
		foreach( $this->commonRules() as $pair ) {
			if( $pair['controller'] == $controller ) {
				return true;
			}
		}

		$visible = false;
		foreach($this->rules as $rule) {
			if( class_exists($rule) ) {
				$obj = new $rule($controller, '*');
				$visible = $visible || $obj->checkAccess();
			}
		}

		return $visible;
	}

}

