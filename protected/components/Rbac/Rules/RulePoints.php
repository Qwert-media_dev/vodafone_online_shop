<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 07:32 PM
 */
class RulePoints extends AbstractRule
{

	public function checkAccess() {
		$able = $this->controller == 'region';
		$able = $able || $this->controller == 'city';
		$able = $able || $this->controller == 'point';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление регионами, городами и точками продаж';
	}

}