<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 07:48 PM
 */
class RulePages extends AbstractRule
{

	public function checkAccess()
	{
		$able = $this->controller == 'pages';
		$able = $able || ($this->controller == 'frontSlider' && $this->action == 'upload');
		$able = $able || ($this->controller == 'files' && $this->action == 'export');
		return $able;
	}

	public function getInfo()
	{
		return 'Управление контентом и меню';
	}

}