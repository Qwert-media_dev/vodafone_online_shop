<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 08:26 PM
 */
class RuleNews extends AbstractRule
{

	public function checkAccess()
	{
		$able = $this->controller == 'news';
		$able = $able || ($this->controller == 'frontSlider' && $this->action == 'upload');
		$able = $able || ($this->controller == 'files' && $this->action == 'export');
		return $able;
	}

	public function getInfo()
	{
		return 'Управление новостями';
	}

}