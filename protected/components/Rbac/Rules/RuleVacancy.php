<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/16/2015
 * Time: 04:44 PM
 */
class RuleVacancy extends AbstractRule
{

	public function checkAccess()
	{
		$able = $this->controller == 'vacancy';
		$able = $able || ($this->controller == 'frontSlider' && $this->action == 'upload');
		$able = $able || ($this->controller == 'files' && $this->action == 'export');
		return $able;
	}

	public function getInfo()
	{
		return 'Управление вакансиями';
	}

}