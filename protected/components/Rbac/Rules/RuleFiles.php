<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/9/2015
 * Time: 06:03 PM
 */
class RuleFiles extends AbstractRule
{

	public function checkAccess()
	{
		$able = $this->controller == 'files';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление файлами';
	}

}