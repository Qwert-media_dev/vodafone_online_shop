<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 08:30 PM
 */
class RuleSettings extends AbstractRule
{

	public function checkAccess() {
		$able = $this->controller == 'cache';
		$able = $able || $this->controller == 'importPoint';
		$able = $able || $this->controller == 'logs';
		$able = $able || $this->controller == 'redirects';
		$able = $able || $this->controller == 'importPoints';
		$able = $able || $this->controller == 'settingsLanding';
		$able = $able || $this->controller == 'settingsParams';
		$able = $able || $this->controller == 'translations';
		$able = $able || $this->controller == 'settingsContent';
		$able = $able || $this->controller == 'сonsult';
		$able = $able || $this->controller == 'paymentPresets';
		$able = $able || $this->controller == 'feedbacksEmails';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление различными настройками';
	}

}