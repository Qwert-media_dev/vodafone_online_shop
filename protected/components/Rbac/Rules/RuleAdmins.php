<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 08:36 PM
 */
class RuleAdmins extends AbstractRule
{

	public function checkAccess() {
		$able = $this->controller == 'admins';
		$able = $able || $this->controller == 'adminGroups';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление доступом';
	}

}