<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 08:39 PM
 */
class RuleBanners extends AbstractRule
{

	public function checkAccess() {
		$able = $this->controller == 'frontSlider';
		$able = $able || $this->controller == 'footerBanner';
		$able = $able || $this->controller == 'sxgeoRegions';
		$able = $able || $this->controller == 'sxgeoCities';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление баннерами';
	}

}