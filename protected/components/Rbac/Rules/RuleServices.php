<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/29/16
 * Time: 21:10
 */
class RuleServices extends AbstractRule
{

    public function checkAccess()
    {
        $able = $this->controller == 'services';
        $able = $able || ($this->controller == 'frontSlider' && $this->action == 'upload');
        $able = $able || ($this->controller == 'files' && $this->action == 'export');
        return $able;
    }

    public function getInfo()
    {
        return 'Управление услугами';
    }

}