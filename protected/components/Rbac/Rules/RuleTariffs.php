<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/7/2015
 * Time: 08:41 PM
 */
class RuleTariffs extends AbstractRule
{

	public function checkAccess() {
		$able = $this->controller == 'country';
		$able = $able || $this->controller == 'intCallPlan';
		return $able;
	}

	public function getInfo()
	{
		return 'Управление тарифами';
	}

}