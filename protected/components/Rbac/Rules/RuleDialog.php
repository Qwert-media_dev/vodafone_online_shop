<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 2/23/16
 * Time: 18:58
 */
class RuleDialog extends AbstractRule
{
    public function checkAccess()
    {
        $able = $this->controller == 'dialog';
        $able = $able || ($this->controller == 'frontSlider' && $this->action == 'upload');
        $able = $able || ($this->controller == 'files' && $this->action == 'export');
        return $able;
    }

    public function getInfo()
    {
        return 'Управление диалогами';
    }
}