<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/5/16
 * Time: 15:30
 */
class GeoLocationsWidget extends CWidget 
{

    public function run() {
        $geoMark = (string)app()->request->cookies['section'];
        $items = GeoHelper::getActiveItemsForMenu( $this->getController()->language->id );
        $this->render('geo-location', compact('items', 'geoMark'));
    }
}