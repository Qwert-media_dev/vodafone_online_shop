<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 4/29/2016
 * Time: 08:27 PM
 */
class ContactsWidget extends CWidget
{

    public function run()
    {
        $video = ConsultSettings::model()->getItem('video', $this->controller->language->id);
        $text = ConsultSettings::model()->getItem('text', $this->controller->language->id);
        $this->render('contacts', compact('video', 'text'));
    }
}