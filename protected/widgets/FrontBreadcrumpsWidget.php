<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/14/2015
 * Time: 10:55 PM
 */
class FrontBreadcrumpsWidget extends CWidget
{

	public $breadcrumbs;

	public function run() {
		$this->render('front-breadcrumps');
	}
}