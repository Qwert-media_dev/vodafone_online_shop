<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/30/16
 * Time: 17:55
 */
class RatesTabWidget extends CWidget
{
    /**
     * @var Rates
     */
    public $current;
    
    public function run() {
        $type = app()->request->getQuery('type');
        $items = Rates::model()->getActiveItems( $type, $this->getController()->language->id, getGeo() );
        $this->render('rates-tab', compact('type', 'items'));
    }
}