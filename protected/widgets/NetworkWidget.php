<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 2/5/2016
 * Time: 01:09 PM
 */
class NetworkWidget extends CWidget
{

	public function run() {
		$this->getController()->useGoogleMap = true;
		$this->getController()->extraContainerClass .= ' network';

		$folder = Yii::getPathOfAlias('application.data').'/';
		$files = CJSON::decode( file_get_contents($folder.'package.json') );
		$points = CJSON::decode( file_get_contents($folder.$files['points'][$this->getController()->language->code]) );
		$temp = CJSON::decode( file_get_contents($folder.$files['states'][$this->getController()->language->code]) );
		$states = [];
		foreach($temp as $t) {
			$states[$t['id']] = $t;
		}
		$this->render('network', compact('item', 'points', 'states'));
	}
}