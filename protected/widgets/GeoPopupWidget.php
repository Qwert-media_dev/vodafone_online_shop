<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/4/16
 * Time: 14:48
 */
class GeoPopupWidget extends CWidget
{

    public function run() {
        $geoMark = (string)app()->request->cookies['section'];
        if( $geoMark === null || $geoMark == '' ) {
            $geo = GeoHelper::detect(getRealIP());
            if ($geo[1] == 0) {
                $name = SxgeoRegions::model()->getName($geo[0], $this->getController()->language->id);
            } else {
                $name = SxgeoCities::model()->getName($geo[0], $geo[1], $this->getController()->language->id);
            }
        }
        $items = GeoHelper::getActiveItemsForMenu( $this->getController()->language->id );
        $this->render('geo-popup', compact('items', 'geoMark', 'name', 'geo'));
    }
}