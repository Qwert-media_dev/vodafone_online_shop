<?php
/* @var $this SettingsWidget */

class SettingsWidget extends CWidget
{
	public function run() {
		$this->getController()->extraContainerClass .= ' settings';
		$content = SettingsContent::model()->findByAttributes(['lang_id' => $this->controller->language->id]);
		$this->render('settings', compact('content'));
	}
}