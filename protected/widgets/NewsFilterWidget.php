<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/28/2015
 * Time: 02:44 PM
 */
class NewsFilterWidget extends CWidget
{

	public $is_archive;
	public $type;
    public $page_id;

	public function run() {
        $data = News::model()->getActiveDates($this->type, $this->controller->language->id, $this->is_archive, $this->page_id);

		$months = Yii::app()->locale->getMonthNames('wide', true);
		$filter = is_array($_GET['filter']) ? $_GET['filter'] : array();
		if( array_key_exists('year', $filter) ) {
			$year = (int)$filter['year'];
			if( !in_array($year, array_keys($data)) ) $year = date('Y');

			$month = (int)$filter['month'];
			if( !in_array($month, range(1,12)) ) $month = date('n');
			$dateTitle = sprintf('%s %s', $months[$month], $year);
			$filtered = true;
		} else {
			$dateTitle = Yii::t('app', 'Всі');
			$filtered = false;
		}

		$action = $this->is_archive ? 'archive' : 'index';

		$this->render('news-filter', compact('data', 'dateTitle', 'months', 'month', 'year', 'action', 'filtered'));
	}
}