<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/2/2015
 * Time: 07:08 PM
 */
class RoamingPrivateContractWidget extends CWidget
{
    public function run()
    {
        $networks = Network::model()->getFlat($this->controller->language->id);
        $items = RoamingCallPlan::model()->getItems(RoamingCallPlan::TYPE_CONTRACT, $this->controller->language->id );
        $this->render('roaming-private-contract', compact('items', 'networks'));
    }
}