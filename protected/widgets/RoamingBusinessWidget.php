<?php
/* @var $this RoamingBusinessWidget */

class RoamingBusinessWidget extends CWidget
{
    public function run()
    {
		$networks = Network::model()->getFlat( $this->controller->language->id );
        $items = RoamingCallPlan::model()->getItems(RoamingCallPlan::TYPE_BUSINESS, $this->controller->language->id );
        $this->render('roaming-business', compact('items', 'networks'));
    }
}