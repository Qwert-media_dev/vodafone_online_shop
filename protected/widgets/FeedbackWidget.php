<?php

class FeedbackWidget extends CWidget
{

    public function run() {
        $this->getController()->extraContainerClass = 'feedback';
        $this->render('feedback');
    }
}
