<?php
/* @var $this SettingsWidget */
/* @var $content SettingsContent */
?>
<div class="row">
    <div class="column full">
        <hr class="wide">
    </div>
</div>
<div class="block settings-block-container">
	<div class="container">
		<div class="column full">
			<div class="variants-selector tabs-switch"><a href="#settings-auto" class="active"><?= Yii::t('settings', 'Автоматичнi') ?></a><a href="#settings-manual"><?= Yii::t('settings', 'Ручнi') ?></a></div>
			<h1 class="page-title"><?= Yii::t('settings', 'Налаштування 3G') ?></h1>
		</div>
	</div>
	<div class="container">
		<div class="column full settings-content">
			<div id="settings-auto">
				<div class="important"><?= Yii::t('settings', 'Усi поля обов\'язкові для заповнення поля.') ?></div>
				<?= CHtml::beginForm(['network/settings', 'lang' => $this->controller->language->code], 'post'); ?>
					<div class="num">
						<div class="label"><?= Yii::t('settings', 'Номер Вашого мобільного телефону') ?></div>
						<div class="group"><span>+38</span>
							<div class="select">
                                <?= CHtml::dropDownList('code', '', param('codes'), ['class' => 'code']) ?>
							</div>
							<div class="input">
                                <?= CHtml::textField('num', '', ['maxlength' => 7]) ?>
							</div>
						</div>
					</div>
					<div class="captcha">
						<div class="label"><?= Yii::t('settings', 'Номер на картинцi') ?></div>
						<div class="group">
                            <?php
                            $this->widget('CCaptcha', array('captchaAction' => 'site/captcha'));
                            echo CHtml::textField('captcha', '', ['maxlength' => 4])
                            ?>
						</div>
					</div>
					<div class="submit">
						<button type="submit" class="btn primary"><?= Yii::t('settings', 'Надiслати налаштування') ?></button>
					</div>
					<div class="response">
						<button type="reset" class="close">x</button>
						<div class="content"></div>
					</div>
				<?= CHtml::endForm() ?>
				<div class="agreement">
					<p>
						<?= Yii::t('settings', 'Натискаючи кнопку «Відправити», Ви погоджуєтесь на обробку Ваших персональних даних та / або передачу її третім особам для цілей, пов\'язаних з її заповненням, а також маркетингових цілей.') ?> <a class="trigger" href="#expand"><?= Yii::t('settings', 'Детальніше') ?></a>
					</p>
					<div style="display: none" class="details">
						<h3><?= Yii::t('settings', 'Угода') ?></h3>
						<p><?= Yii::t('settings', 'Заповненням цієї реєстраційної форми Абонент надає свою згоду на обробку його персональних даних та/або передачу їх третім особам для цілей, пов\'язаних з їх зберіганням, а також маркетингових цілей (включаючи, але не обмежуючись, отримання винагород та подарунків по акціям та програмам лояльності ПрАТ «MTC УКРАЇНА», отримання рекламної інформації від ПрАТ «MTC УКРАЇНА» та/або партнерів з використанням вказаних даних).') ?></p>
						<p><?= Yii::t('settings', 'Заповненням цієї реєстраційної форми Абонент надає згоду на обробку своїх персональних даних з використанням засобів автоматизації та/або без таких ПрАТ «МТС УКРАЇНА» самостійно та/або розпорядником бази персональних даних за дорученням ПрАТ «МТС УКРАЇНА», а також погоджуюся, що ПрАТ «МТС Україна» може передавати мої персональні дані третім особам відповідно до закону або змісту цієї реєстраційної форми.') ?></p>
						<p><?= Yii::t('settings', 'Підтверджую, що заповнивши цю реєстраційну форму я проінформований про внесення моїх персональних до бази персональних даних ПрАТ «МТС Україна», а також про свої права та осіб, яким передаються мої персональні дані відповідно до Закону України «Про захист персональних даних».') ?></p>
					</div>
				</div>
			</div>
			<div id="settings-manual" style="display: none;">
				<?= $content->content ?>
			</div>
		</div>
	</div>
</div>
