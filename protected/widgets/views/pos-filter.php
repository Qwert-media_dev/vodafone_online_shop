<?php
/* @var $regions PosRegion[] */
/* @var $cities array */
?>

<?php echo CHtml::beginForm(array('pos/index', 'lang' => $this->controller->language->code), 'get', array('id' => 'filter')); ?>
<div class="filter">
	<div class="region">
		<label><?= Yii::t('app', 'Регіон') ?></label>
		<div class="select">
			<?php
            echo CHtml::dropDownList(
                    'filter[region]',
                    $_GET['filter']['region'],
                    CHtml::listData($regions, 'id', 'i18n.0.title'),
                    array('class' => 'filter-trigger', 'empty' => Yii::t('app', 'Всі'), 'id' => 'region')
            );
            ?>
		</div>
	</div>
	<div class="city">
		<label><?= Yii::t('app', 'Місто') ?></label>
		<div class="select">
			<?php
            echo CHtml::dropDownList(
                    'filter[city]',
                    $_GET['filter']['city'],
                    $cities,
                    array('class' => 'filter-trigger', 'empty' => Yii::t('app', 'Всі'), 'id' => 'city')
            );
            ?>
		</div>
	</div>
</div>
<?php echo CHtml::endForm(); ?>
<script>
	$(function(){
		$('.filter-trigger').change(function(){
            var region = $('#region option:selected').val();
            if( region == '' ) {
                top.location.href = $('#filter').attr('action');
            } else {
                $('#filter').submit();
            }
		});
	});
</script>
