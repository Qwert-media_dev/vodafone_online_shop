<?php
/* @var $this FeedbackWidget */
?>
<?= CHtml::beginForm(['site/feedback', 'lang' => $this->getController()->language->code], 'post', ['id' => 'ff', 'enctype' => 'multipart/form-data']) ?>
  <div class="container head-container">
	<div class="row">
	  <div class="column third">
		<label><?= Yii::t('feedback', 'Ваше ім`я') ?></label>
		<div class="group" data-find="name">
		  <input type="text" name="name"/>
		</div>
		<p class="signature"><?= Yii::t('feedback', 'Вкажіть ваші прізвище, ім\'я та по-батькові') ?></p>
	  </div>
	  <div class="column third">
		<label><?= Yii::t('feedback', 'Ваша email адреса') ?></label>
		<div class="group" data-find="email">
		  <input type="text" name="email" value=""/>
		</div>
		<p class="signature"><?= Yii::t('feedback', 'Введiть свою електронну адресу.') ?></p>
	  </div>
	  <div class="column third">
		<label><?= Yii::t('feedback', 'Ваш номер телефону') ?></label>
		<div class="group num" data-find="num"><span>+38</span>
		  <div class="select">
			<select name="code">
				<?php foreach (param('codes') as $code): ?>
			  	<option value="<?= $code ?>"><?= $code ?></option>
				<?php endforeach; ?>
                <option value="null"><?= Yii::t('feedback', 'не абонент') ?></option>
			</select>
		  </div>
		  <div class="input">
			<input type="text" name="num" maxlength="7"/>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <hr/>
  <div class="container form-container">
	<div class="row">
	  <div class="column half">
		<label><?= Yii::t('feedback', 'Вид звернення') ?></label>
		<div class="group">
		  <div class="select">
			<select name="type" data-toggle=".category-selector">
			  <option value="gratitude"><?= Yii::t('feedback', 'Висловити подяку') ?></option>
			  <option value="idea"><?= Yii::t('feedback', 'Подати ідею') ?></option>
			  <option value="appeal"><?= Yii::t('feedback', 'Залишити звернення') ?></option>
			</select>
		  </div>
		</div>
	  </div>
	  <fieldset class="column half category-selector target-gratitude target-idea">
		<label><?= Yii::t('feedback', 'Тема листа') ?></label>
		<div class="group" data-find="subject">
		  <input type="text" name="subject"/>
		</div>
	  </fieldset>
	  <fieldset class="column half category-selector target-appeal">
		<label><?= Yii::t('feedback', 'Тема зверення') ?></label>
		<div class="group">
		  <div class="select">
			<select name="category" data-toggle=".category-container">
			  <option value="connection"><?= Yii::t('feedback', 'Якість зв\'язку') ?></option>
			  <option value="service"><?= Yii::t('feedback', 'Якість обслуговування') ?></option>
			  <option value="finance"><?= Yii::t('feedback', 'Тарифікація, фінансові питання') ?></option>
			  <option value="services"><?= Yii::t('feedback', 'Робота послуг') ?></option>
			  <option value="payments"><?= Yii::t('feedback', 'Оплата') ?></option>
			  <option value="loyalty"><?= Yii::t('feedback', 'Програма лояльності') ?></option>
			  <option value="roaming"><?= Yii::t('feedback', 'Роумінг') ?></option>
			  <option value="site"><?= Yii::t('feedback', 'Робота сайту') ?></option>
			  <option value="attention"><?= Yii::t('feedback', 'Увага!*') ?></option>
			  <option value="other"><?= Yii::t('feedback', 'Інше') ?></option>
			</select>
		  </div>
		</div>
	  </fieldset>
	</div>
  </div>
  <div class="container form-container">
	<fieldset class="category-container target-connection">
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Ваші номери телефонів, з якими спостерігаються труднощі') ?></label>
		  <div class="group" data-find="connection_num">
			<input type="text" name="connection[num]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Введіть номери, з яких ви телефонували, через кому. Наприклад, +380991234567, +380667654321') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Причина звернення') ?></label>
		  <div class="group" data-find="connection_cause">
			<input type="text" name="connection[cause]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть причину звернення. Наприклад, "Не вдається здійснити дзвінок"') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Точна адреса, де виникають проблеми') ?></label>
		  <div class="group" data-find="connection_address">
			<input type="text" name="connection[address]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть область, район, назву населеного пункту, вулиці, номер будинку, квартиру, поверх') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Модель вашого мобільного телефону') ?></label>
		  <div class="group" data-find="connection_model">
			<input type="text" name="connection[model]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть модель вашого мобільного телефону. Наприклад, iPhone 4s') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Якими послугами користуєтесь') ?></label>
		  <div class="group" data-find="connection_services">
			<input type="text" name="connection[services]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть типи послуг. Наприклад, звінки, SMS, доступ до інтернету тощо.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Що чуєте / бачите на телефонi') ?></label>
		  <div class="group" data-find="connection_see">
			<input type="text" name="connection[see]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Надайте опис якомога детальніше.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Дата виникнення проблеми') ?></label>
		  <div class="group" data-find="connection_when">
			<input type="text" name="connection[when]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть, як давно спостерігаються проблеми.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Рівень сигналу') ?></label>
		  <div class="group" data-find="connection_signal">
			<input type="text" name="connection[signal]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть кількість поділок сигналу.') ?></p>
		</div>
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Тип сигналу') ?></label>
		  <div class="group">
			<div class="select">
			  <select name="connection[signal_type]">
				<option value="1"><?= Yii::t('feedback', 'Стійкий') ?></option>
				<option value="0"><?= Yii::t('feedback', 'Не стійкий') ?></option>
			  </select>
			</div>
		  </div>
		</div>
	  </div>
	</fieldset>
	<fieldset class="category-container target-service">
	  <div class="row">
		<div class="column full">
		  <label><strong><?= Yii::t('feedback', 'Тип точки обслуговування') ?></strong></label>
		</div>
	  </div>
	  <div class="row">
		<div class="column third">
		  <label class="radio">
			<input type="radio" name="service[point]" value="shop" data-toggle=".subcategory-service" checked="checked"/>
			<div class="label"><?= Yii::t('feedback', 'Магазин') ?></div>
		  </label>
		</div>
		<div class="column third">
		  <label class="radio">
			<input type="radio" name="service[point]" value="center" data-toggle=".subcategory-service"/>
			<div class="label"><?= Yii::t('feedback', 'Центр обслуговування абонентів') ?></div>
		  </label>
		</div>
		<div class="column third">
		  <label class="radio">
			<input type="radio" name="service[point]" value="chat" data-toggle=".subcategory-service"/>
			<div class="label"><?= Yii::t('feedback', 'Online Chat') ?></div>
		  </label>
		</div>
	  </div>
	  <fieldset class="subcategory-service target-shop">
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Дата і час зверення') ?></label>
			<div class="group" data-find="service_shop_when">
			  <input type="text" name="service[shop][when]"/>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Адреса магазину') ?></label>
			<div class="group" data-find="service_shop_address">
			  <input type="text" name="service[shop][address]"/>
			</div>
			<p class="signature"><?= Yii::t('feedback', 'Вкажіть область, район, назву населеного пункту, вулиці, номер будинку') ?></p>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'П.І.Б. працівника, на якого надійшла скарга') ?></label>
			<div class="group" data-find="service_shop_name">
			  <input type="text" name="service[shop][name]"/>
			</div>
		  </div>
		</div>
	  </fieldset>
	  <fieldset class="subcategory-service target-center">
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Дата і час зверення') ?></label>
			<div class="group" data-find="service_center_when">
			  <input type="text" name="service[center][when]"/>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Номер, з якого зверталися') ?></label>
			<div class="group" data-find="service_center_num">
			  <input type="text" name="service[center][num]"/>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Ім\'я/номер працівника') ?></label>
			<div class="group" data-find="service_center_name">
			  <input type="text" name="service[center][name]"/>
			</div>
		  </div>
		</div>
	  </fieldset>
	  <fieldset class="subcategory-service target-chat">
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Дата і час зверення') ?></label>
			<div class="group" data-find="service_chat_when">
			  <input type="text" name="service[chat][when]"/>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Номер, з якого зверталися') ?></label>
			<div class="group" data-find="service_chat_num">
			  <input type="text" name="service[chat][num]"/>
			</div>
		  </div>
		</div>
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'Ім\'я/номер/нік працівника') ?></label>
			<div class="group" data-find="service_chat_name">
			  <input type="text" name="service[chat][name]"/>
			</div>
		  </div>
		</div>
	  </fieldset>
	</fieldset>
	<fieldset class="category-container target-finance">
	  <div class="row">
		<div class="column full">
		  <label><strong><?= Yii::t('feedback', 'Тип договору') ?></strong></label>
		</div>
	  </div>
	  <div class="row">
		<div class="column third">
		  <label class="radio">
			<input type="radio" name="finance[type]" value="prepaid" data-toggle=".subcategory-finance" checked="checked"/>
			<div class="label"><?= Yii::t('feedback', 'Передплата') ?></div>
		  </label>
		</div>
		<div class="column third">
		  <label class="radio">
			<input type="radio" name="finance[type]" value="contract" data-toggle=".subcategory-finance"/>
			<div class="label"><?= Yii::t('feedback', 'Контракт') ?></div>
		  </label>
		</div>
	  </div>
	  <fieldset class="subcategory-finance target-prepaid">
		<div class="row">
		  <div class="column full">
			<label><?= Yii::t('feedback', 'PUK-код, пароль персонального помічника (IVR-пароль) або кодове слово') ?></label>
			<div class="group" data-find="finance_prepaid_code">
			  <input type="text" name="finance[prepaid][code]"/>
			</div>
		  </div>
		</div>
	  </fieldset>
	  <fieldset class="subcategory-finance target-contract">
		<div class="row">
		  <div class="column full">
			<label><strong><?= Yii::t('feedback', 'Тип клієнта') ?></strong></label>
		  </div>
		</div>
		<div class="row">
		  <div class="column third">
			<label class="radio">
			  <input type="radio" name="finance[contract][type]" value="fiz" data-toggle=".subcategory-finance-contract" checked="checked"/>
			  <div class="label"><?= Yii::t('feedback', 'Фізична особа') ?></div>
			</label>
		  </div>
		  <div class="column third">
			<label class="radio">
			  <input type="radio" name="finance[contract][type]" value="jur" data-toggle=".subcategory-finance-contract"/>
			  <div class="label"><?= Yii::t('feedback', 'Юридична особа') ?></div>
			</label>
		  </div>
		</div>
		<fieldset class="subcategory-finance-contract target-fiz">
		  <div class="row">
			<div class="column full">
			  <label>П.І.Б.</label>
			  <div class="group" data-find="finance_contract_fiz_name">
				<input type="text" name="finance[contract][fiz][name]"/>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="column full">
			  <label><?= Yii::t('feedback', 'Паспортні дані') ?></label>
			  <div class="group" data-find="finance_contract_fiz_passport">
				<input type="text" name="finance[contract][fiz][passport]"/>
			  </div>
			  <p class="signature"><?= Yii::t('feedback', 'Серія, номер паспорту, ким та коли виданий') ?></p>
			</div>
		  </div>
		  <div class="row">
			<div class="column full">
			  <label><?= Yii::t('feedback', 'Пароль') ?></label>
			  <div class="group" data-find="finance_contract_fiz_password">
				<input type="password" name="finance[contract][fiz][password]"/>
			  </div>
			</div>
		  </div>
		</fieldset>
		<fieldset class="subcategory-finance-contract target-jur">
		  <div class="row">
			<div class="column full">
			  <label><?= Yii::t('feedback', 'Назва організації') ?></label>
			  <div class="group" data-find="finance_contract_jur_name">
				<input type="text" name="finance[contract][jur][name]"/>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="column full">
			  <label><?= Yii::t('feedback', 'Реквізити') ?></label>
			  <div class="group" data-find="finance_contract_jur_details">
				<input type="text" name="finance[contract][jur][details]"/>
			  </div>
			</div>
		  </div>
		  <div class="row">
			<div class="column full">
			  <label><?= Yii::t('feedback', 'Пароль') ?></label>
			  <div class="group" data-find="finance_contract_jur_password">
				<input type="password" name="finance[contract][jur][password]"/>
			  </div>
			</div>
		  </div>
		</fieldset>
	  </fieldset>
	</fieldset>
	<fieldset class="category-container target-services">
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Тип послуги') ?></label>
		  <div class="group">
			<div class="select">
			  <select name="services[type]">
				<option value="goodok"><?= Yii::t('feedback', 'Гудок') ?></option>
				<option value="messages"><?= Yii::t('feedback', 'Передача повідомлень') ?></option>
				<option value="content"><?= Yii::t('feedback', 'МТС Клік, контент, підписки') ?></option>
				<option value="packages"><?= Yii::t('feedback', 'Пакети хвилин, sms, МБ') ?></option>
				<option value="other"><?= Yii::t('feedback', 'Інші послуги') ?></option>
			  </select>
			</div>
		  </div>
		</div>
	  </div>
	</fieldset>
	<fieldset class="category-container target-payments">
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Тип проблеми') ?></label>
		  <div class="group">
			<div class="select">
			  <select name="payments[type]">
				<option value="fail"><?= Yii::t('feedback', 'Не надійшов платіж') ?></option>
				<option value="error"><?= Yii::t('feedback', 'Помилковий платіж') ?></option>
			  </select>
			</div>
		  </div>
		</div>
	  </div>
	</fieldset>
	<!--include feedback/appeal/loyalty-->
	<fieldset class="category-container target-roaming">
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Ваші номери телефонів, з якими спостерігаються труднощі') ?></label>
		  <div class="group" data-find="roaming_num">
			<input type="text" name="roaming[num]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Введіть номери, з яких ви телефонували, через кому. Наприклад, +380991234567, +380667654321') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Країна та місто перебування') ?></label>
		  <div class="group" data-find="roaming_country">
			<input type="text" name="roaming[country]"/>
		  </div>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Назва мережі роумінг-оператора') ?></label>
		  <div class="group" data-find="roaming_operator">
			<input type="text" name="roaming[operator]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть назву мережі роумінг-оператора, що відобажається на екрані телефону') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Модель вашого мобільного телефону') ?></label>
		  <div class="group" data-find="roaming_model">
			<input type="text" name="roaming[model]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть модель вашого мобільного телефону. Наприклад, iPhone 4s') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Якими послугами користуєтесь') ?></label>
		  <div class="group" data-find="roaming_services">
			<input type="text" name="roaming[services]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть типи послуг. Наприклад, звінки, SMS, доступ до інтернету тощо.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column full">
		  <label><?= Yii::t('feedback', 'Що чуєте / бачите на телефонi') ?></label>
		  <div class="group" data-find="roaming_see">
			<input type="text" name="roaming[see]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Надайте опис якомога детальніше.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Дата виникнення проблеми') ?></label>
		  <div class="group" data-find="roaming_when">
			<input type="text" name="roaming[when]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть, як давно спостерігаються проблеми.') ?></p>
		</div>
	  </div>
	  <div class="row">
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Рівень сигналу') ?></label>
		  <div class="group" data-find="roaming_signal">
			<input type="text" name="roaming[signal]"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вкажіть кількість поділок сигналу.') ?></p>
		</div>
		<div class="column half">
		  <label><?= Yii::t('feedback', 'Тип сигналу') ?></label>
		  <div class="group">
			<div class="select">
			  <select name="roaming[signal_type]">
				<option value="1"><?= Yii::t('feedback', 'Стійкий') ?></option>4
				<option value="0"><?= Yii::t('feedback', 'Не стійкий') ?></option>
			  </select>
			</div>
		  </div>
		</div>
	  </div>
	</fieldset>
	<!--include feedback/appeal/site-->
	<fieldset class="category-container target-attention">
	  <div class="alert alert-important"><?= Yii::t('feedback', '*У випадку різкого погіршення якості зв’язку/ раптові труднощі з вхідними/вихідними дзвінками та смс/складнощі при поповненні рахунку просимо вибирати розділ «Увага/Внимание/Attention».') ?></div>
	</fieldset>
	<!--include feedback/appeal/other-->
  </div>
  <div class="container form-container">
	<div class="row">
	  <div class="column full">
		<label><?= Yii::t('feedback', 'Текст листа') ?></label>
		<div class="group" data-find="message">
		  <textarea rows="7" name="message"></textarea>
		</div>
	  </div>
	</div>
	<div class="row">
	  <div class="column full">
		  <div class="group file-group" data-find="attach">
			  <input type="file" name="attach"/>
		  </div>
		  <p class="signature"><?= Yii::t('feedback', 'Вага файлу не може перевищувати 12 МБ<br>Дозволені наступні формати: .doc, .docm, .docx, .txt, .rtf, .pdf, .ppt, .pptx, .xls, .xlsx, .zip, .rar, .tiff, .jpg, .png, .gif, .mp3, .avi, .mov, .mp4') ?></p>
	  </div>
	</div>
	<div class="row">
	  <div class="column full">
		<div class="submit">
		  <button disabled="disabled" class="btn primary"><?= Yii::t('feedback', 'Надіслати') ?></button>
			<div class="loader"><img src="/img/loader.gif"/></div>
		</div>
	  </div>
	</div>
  </div>
	<div class="container response-container">
		<div class="response">
		  <button type="reset" class="close">x</button>
		  <div class="content"></div>
		</div>
  	</div>
<?= CHtml::endForm() ?>
