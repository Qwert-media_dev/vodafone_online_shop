<?php
/* @var $type string */
/* @var $has_archive boolean */
/* @var $is_archive boolean */
/* @var $has_private boolean */
/* @var $has_contract boolean */
/* @var $has_archive boolean */
/* @var $code string */
?>
<div class="select-type">
    <?php if($has_private): ?>
    <a href="<?= url_to('rates/index', ['lang' => $code, 'type' => 'private']) ?>" <?php if($type == 'private' && !$is_archive){ ?>class="active"<?php } ?>><?= Yii::t('app', 'Передоплата') ?></a>
    <?php endif; ?>

    <?php if($has_contract): ?>
    <a href="<?= url_to('rates/index', ['lang' => $code, 'type' => 'contract']) ?>" <?php if($type == 'contract' && !$is_archive){ ?>class="active"<?php } ?>><?= Yii::t('app', 'Контракт') ?></a>
    <?php endif; ?>

    <?php if($has_archive): ?>
        <a href="<?= url_to('rates/archive', ['lang' => $code, 'type' => $type]) ?>" <?php if($is_archive){ ?> class="active"<?php } ?>><?= Yii::t('app', 'Архів') ?></a>
    <?php endif; ?>
</div>