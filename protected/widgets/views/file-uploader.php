<?php
/* @var $fieldKey string */
/* @var $field string */
/* @var $model CActiveRecord */
/* @var $form CActiveForm */
/* @var $this FileUploaderWidget */
/* @var $lang_id integer */
?>

<div class="form-group group-<?= $fieldKey ?> <?php echo $this->controller->error($model, $field) ?>">
    <?php
    echo $form->labelEx($model, $field, array('class' => 'control-label'));
    $class = get_class($model);
    $name = is_null($lang_id) ? sprintf('%s[%s]', $class, $field)  : sprintf('%s[%d][%s]', $class, $lang_id, $field);
    echo $form->hiddenField($model, $field, array('class'=>'form-control image-src', 'id' => $fieldKey, 'name' => $name));
    ?>

    <div class="uploader <?= $fieldKey ?>" <?php if ($model->$field != '') echo ' style="display:none"'; ?>>
        <div class="js-fileapi-wrapper">
            <input type="file" name="files[]"/>
        </div>
        <div data-fileapi="active.show" class="progress">
            <div data-fileapi="progress" class="progress__bar"></div>
        </div>
    </div>

    <div class="preview">
        <?php if ($model->$field != ''): ?>
            <img src="/images/upload/<?= $model->$field ?>" class="img-rounded" height="100"/><br>
            <a href="#delete" class="remove-preview btn btn-danger btn-xs" data-root="group-<?= $fieldKey ?>">
                Удалить
            </a>
        <?php endif; ?>
    </div>

    <div class="help-block"><?= $this->help ?></div>
</div>

<script type="text/javascript">
    $(function(){
        $('.<?= $fieldKey ?>').fileapi({
            url: '<?= $this->url ?>',
            dataType: 'json',
            data: {field: "<?= $field ?>"},
            autoUpload: true,
            accept: 'image/*',
            multiple: false,
            maxSize: FileAPI.MB * 10, // max file size
            onFileComplete: function (e, o) {
                console.log('onFileComplete (<?= $fieldKey ?>)', o.result);
                var data = o.result;
                if(data.status == 'ok') {
                    $('#<?= $fieldKey ?>').val(data.location);
                    $('.<?= $fieldKey ?>').hide();
                    var content = '<img src="/images/upload/' + data.location + '" class="img-rounded" height="100" /><br>'+
                                  '<a href="#delete" class="remove-preview btn btn-danger btn-xs" data-root="group-<?= $fieldKey ?>">Удалить</a>';
                    $('.group-<?= $fieldKey ?> .preview').html(content).show();
                } else {
                    alert(data.msg);
                }
            }
        });

        $(document).on('click', '.group-<?= $fieldKey ?> .remove-preview', function(e){
        //$('.group-<?= $fieldKey ?> .remove-preview').click(function(e){
            e.preventDefault();
            var root = $(this).data('root');
            if(confirm('Удалить ?')) {
                $('.'+root+' .image-src').val('');
                $('.'+root+' .preview').html('').hide();
                $('.'+root+' .uploader').show();
            }
        });
    });
</script>