<?php
/* @var $selector string */
/* @var $fileSelectUrl string */

Yii::app()->clientScript->registerCssFile('http://fonts.googleapis.com/css?family=Open+Sans:300,600,800');
Yii::app()->clientScript->registerCssFile('/js/ContentBuilder/assets/vodafone/content.css');
Yii::app()->clientScript->registerCssFile('/js/ContentBuilder/scripts/contentbuilder.css');

Yii::app()->clientScript->registerScriptFile('/js/ContentBuilder/scripts/contentbuilder.js', CClientScript::POS_END);
Yii::app()->clientScript->registerScriptFile('/js/ContentBuilder/scripts/saveimages.js', CClientScript::POS_END);
?>

<script>
    $(function(){
        $("<?= $selector ?>").contentbuilder({
            zoom: 1,
            fileselect: '<?= $fileSelectUrl ?>',
            snippetFile: '/js/ContentBuilder/assets/vodafone/snippets.html',
            toolbar: 'left'
        });
    });
</script>