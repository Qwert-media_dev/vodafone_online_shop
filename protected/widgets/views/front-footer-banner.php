<?php
/* @var $this FrontFooterBannerWidget */
/* @var $items FooterBanner[] */
/* @var $class string */
?>

<?php if( count($items) ): ?>
<div class="banners<?= $class ?>">
	<?php foreach($items as $item): ?>
    <?php $label = URLify::filter($item->id.'-'.$item->title); ?>
	<div class="banner">
		<?php if($item->i18n[0]->link != ''): ?>
			<?php
				$params = [
                    'class' => 'cnt',
                    'alt' => $item->i18n[0]->alt,
                    'style' => 'background-image: url(/images/upload/'. $item->i18n[0]->image. ')',
                    'data-label' => $label
                ];
				if($item->i18n[0]->in_new_window) {
					$params['target'] = '_blank';
				}
				echo CHtml::link('', $item->i18n[0]->link, $params);
			?>
		<?php else: ?>
			<div class="cnt" style="background-image: url(/images/upload/<?= $item->i18n[0]->image ?>)"></div>
	    <?php endif; ?>
        <?php if( param('ga') != '' ): ?>
            <script>
                $(function(){
                    ga('send', 'event', 'footerbanner', 'view', '<?php echo $label; ?>');
                });
            </script>
        <?php endif; ?>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php if( param('ga') != '' ): ?>
    <script>
        $(function(){
            $('a.cnt').click(function (e) {
                ga('send', 'event', 'footerbanner', 'click', $(this).data('label'));
                return true;
            });
        });
    </script>
<?php endif; ?>