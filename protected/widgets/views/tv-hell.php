<div class="tv-packages-wrapper">
    <div class="tv-packages tv-container">
        <div class="row clearfix">
            <div class="container">
                <div class="column full">
                    <p>Послуга Vodafone TV надає своїм абонентам можливість дивитися популярні телеканали, в тому числі в HD-якості в режимі реального часу, фільми з каталогу і слухати радіостанції без плати за інтернет трафік.</p>
                    <p>Перелік доступних відео, телеканалів і радіостанцій регулярно оновлюється.</p>
                </div>
            </div>
        </div>
        <div class="tv-package-list">

            <div class="tv-package tv-package-1">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Ефір</h3>
                                <p>11 грн / 7 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>41 телеканал</li>
                        <li>3 з яких в HD якості</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

            <div class="tv-package tv-package-2">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Футбол</h3>
                                <p>11 грн / 7 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>4 футбольні телеканали</li>
                        <li>Прямі трансляції ліг та чемпіонатів</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

            <div class="tv-package tv-package-3">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Кіно</h3>
                                <p>21 грн / 30 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>Більш ніж 3 000 фільмів</li>
                        <li>Серіалів та ТВ-шоу</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

            <div class="tv-package tv-package-4">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Дитячий</h3>
                                <p>7,5 грн/7 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>11 телеканалів</li>
                        <li>Дитячий кiнозал</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

            <div class="tv-package tv-package-5">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Пізнавальний</h3>
                                <p>11 грн / 7 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>12 телеканалів</li>
                        <li>Пізнавальні кінозали</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

            <div class="tv-package tv-package-6">
                <div class="tv-package-inner">
                    <div class="tv-package-header">
                        <div class="tv-package-header-inner">
                            <div class="tv-package-icon"></div>
                            <div class="tv-package-title">
                                <h3>Спорт Плюс</h3>
                                <p>7,5 грн / 30 днів</p>
                            </div>
                        </div>
                    </div>
                    <ul>
                        <li>12 телеканалів</li>
                        <li>Кiнозал Top Gear</li>
                    </ul>
                    <a class="tv-more" href="#">Детальнiше</a>
                </div>
            </div>

        </div>
    </div>
    <div class="tv-access">
        <video loop="loop" preload style="background-image: url(https://daks2k3a4ib2z.cloudfront.net/57583762be901fd64403eefa/575852a3d08d27f76cb52636_bg-poster-00001.jpg);">
            <source src="https://daks2k3a4ib2z.cloudfront.net/57583762be901fd64403eefa/575852a3d08d27f76cb52636_bg-transcode.webm">
            <source src="https://daks2k3a4ib2z.cloudfront.net/57583762be901fd64403eefa/575852a3d08d27f76cb52636_bg-transcode.mp4">
        </video>
        <div class="tv-video-top">
            <svg height="100" width="100%">
                <polygon points="0,0 0,100 2600,0" style="fill:#eeeeee;"></polygon>
            </svg>
        </div>
        <div class="tv-video-bottom">
            <svg height="100" width="2600">
                <polygon points="0,0 0,100 2600,0" style="fill:#eeeeee;"></polygon>
            </svg>
        </div>
        <div class="tv-stores-container tv-container">
            <div class="tv-stores-wrapper">
                <div class="tv-stores-inner">
                    <div class="tv-stores-text">
                        <div class="tv-stores">
                            <h2>Доступ</h2>
                            <p>Встанови додаток на смартфон або планшет та дивись <br/>на&nbsp;трьох пристроях без додаткової плати</p>
                            <p><a target="_blank" href="https://itunes.apple.com/ua/app/vodafone-tv/id1067569042?l=ru&ls=1&mt=8"><img src="/images/appstore.png"/></a><a target="_blank" href="https://play.google.com/store/apps/details?id=ua.vodafone.tv"><img src="/images/gplay.png"/></a></p>
                            <a class="tv-more" href="#">Детальнiше</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tv-functions-wrapper">
        <div class="tv-functions tv-container">
            <h2>Перегляд</h2>
            <p>Користувачам доступні сучасні функції керування переглядом</p>
            <div class="tv-functions-list">
                <div class="tv-function tv-function-1">
                    <div class="tv-function-icon"></div>
                    <p>ТВ-запис<br/> на 7 днів</p>
                </div>
                <div class="tv-function tv-function-2">
                    <div class="tv-function-icon"></div>
                    <p>ТВ-пауза <br/>прямого ефіру</p>
                </div>
                <div class="tv-function tv-function-3">
                    <div class="tv-function-icon"></div>
                    <p>Батьківський <br/>контроль</p>
                </div>
                <div class="tv-function tv-function-4">
                    <div class="tv-function-icon"></div>
                    <p>Звукові <br/>доріжки/субтитри</p>
                </div>
                <div class="tv-function tv-function-5">
                    <div class="tv-function-icon"></div>
                    <p>Програми <br/>за темами</p>
                </div>
            </div>
            <div class="tv-functions-descr">
                <div class="tv-descr">
                    <h3>ТВ-запис на 7 днів</h3>
                    <p>При необхідності обмежуйте доступ до телеканалів і фільмів для дітей, використовуючи функцію «Батьківський контроль». Пароль до налаштувань цієї послуги Ви отримаєте в SMS при реєстрації.</p>
                    <p>Натисніть на сторінці телеканалу або фільму на значок «замку» - його колір зміниться на червоний і перегляд цього контенту буде можливий тільки після введення пароля «Батьківського контролю».</p>
                    <p>Щоб зняти блокування з телеканалу або фільму - повторно натисніть на значок «Батьківського контролю» і введіть пароль. Також це можна зробити в розділі меню програми «Батьківський контроль».</p>
                </div>
                <div class="tv-descr">
                    <h3>ТВ-пауза прямого ефіру</h3>
                    <p>При необхідності обмежуйте доступ до телеканалів і фільмів для дітей, використовуючи функцію «Батьківський контроль». Пароль до налаштувань цієї послуги Ви отримаєте в SMS при реєстрації.</p>
                    <p>Щоб зняти блокування з телеканалу або фільму - повторно натисніть на значок «Батьківського контролю» і введіть пароль. Також це можна зробити в розділі меню програми «Батьківський контроль».</p>
                </div>
                <div class="tv-descr">
                    <h3>Батьківський контроль</h3>
                    <p>При необхідності обмежуйте доступ до телеканалів і фільмів для дітей, використовуючи функцію «Батьківський контроль». Пароль до налаштувань цієї послуги Ви отримаєте в SMS при реєстрації.</p>
                </div>
                <div class="tv-descr">
                    <h3>Звукові доріжки/субтитри</h3>
                    <p>При необхідності обмежуйте доступ до телеканалів і фільмів для дітей, використовуючи функцію «Батьківський контроль». Пароль до налаштувань цієї послуги Ви отримаєте в SMS при реєстрації.</p>
                    <p>Натисніть на сторінці телеканалу або фільму на значок «замку» - його колір зміниться на червоний і перегляд цього контенту буде можливий тільки після введення пароля «Батьківського контролю».</p>
                    <p>Щоб зняти блокування з телеканалу або фільму - повторно натисніть на значок «Батьківського контролю» і введіть пароль. Також це можна зробити в розділі меню програми «Батьківський контроль».</p>
                </div>
                <div class="tv-descr">
                    <h3>Програми за темами</h3>
                    <p>При необхідності обмежуйте доступ до телеканалів і фільмів для дітей, використовуючи функцію «Батьківський контроль». Пароль до налаштувань цієї послуги Ви отримаєте в SMS при реєстрації.</p>
                    <p>Натисніть на сторінці телеканалу або фільму на значок «замку» - його колір зміниться на червоний і перегляд цього контенту буде можливий тільки після введення пароля «Батьківського контролю».</p>
                    <p>Щоб зняти блокування з телеканалу або фільму - повторно натисніть на значок «Батьківського контролю» і введіть пароль. Також це можна зробити в розділі меню програми «Батьківський контроль».</p>
                </div>
            </div>
        </div>
    </div>
</div>