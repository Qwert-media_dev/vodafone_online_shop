<?php
/* @var $this ContactsWidget */
/* @var $video ConsultSettings */
/* @var $text ConsultSettings */
?>

<?php if ($video !== null || $text !== null): ?>
    <div id="consult" class="consult">
        <div>
            <button class="btn show-buttons"><?= Yii::t('consultant', 'Онлайн консультант'); ?></button>
        </div>
        <div>
            <?php if ($text !== null): ?>
                <a class="btn btn-show-text" target="_blank"
                   href="<?= $text->i18n[0]->url ?>"><?= Yii::t('consultant', 'Текстовий-чат') ?></a>
            <?php endif; ?>
            <?php if ($video !== null): ?>
                <button class="btn btn-show-video"><?= Yii::t('consultant', 'Вiдео-чат') ?></button>
            <?php endif; ?>
        </div>
    </div>
    <?php /* if($text !== null): ?>
<div class="consult-iframe consult-iframe-text">
    <div class="catcher"></div>
    <iframe data-src="<?= $text->i18n[0]->url ?>" scrolling="no" frameborder="0"></iframe>
</div>
<?php endif; */ ?>
    <?php if ($video !== null): ?>
        <div class="consult-iframe consult-iframe-video">
            <div class="catcher"></div>
            <div class="iframe-holder">
                <iframe data-src="<?= $video->i18n[0]->url ?>" scrolling="no" frameborder="0"></iframe>
                <button class="close"></button>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>