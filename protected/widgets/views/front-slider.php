<?php
/* @var $this FrontSliderWidget */
/* @var $sliders FrontSlider[] */
/* @var $isBusiness boolean */
?>
    <div class="mainpage-slider">
        <div class="slides">
            <?php foreach ($sliders as $idx => $slider): ?>
                <?php
                $tag = $slider->i18n[0]->link == '' ? 'div' : 'a';
                $class = ['slide'];
                if ($idx == 0) $class[] = 'active';

                $label = URLify::filter($slider->id . '-' . $slider->title);
                $params = ['class' => implode(' ', $class)];
                if ($tag == 'a') {
                    if ($slider->i18n[0]->in_new_window)
                        $params['target'] = '_blank';
                    $params['href'] = $slider->i18n[0]->link;
                    $params['data-label'] = $label;
                }
                echo CHtml::openTag($tag, $params);
                ?>
                <picture>
                    <!--[if IE 9]>
                    <video style="display: none;"><![endif]-->
                    <source srcset="/images/upload/<?= $slider->i18n[0]->pic1 ?>" media="(min-width: 1601px)"/>
                    <source srcset="/images/upload/<?= $slider->i18n[0]->pic2 ?>" media="(min-width: 980px)"/>
                    <source srcset="/images/upload/<?= $slider->i18n[0]->pic3 ?>" media="(min-width: 660px)"/>
                    <!--[if IE 9]></video><![endif]-->
                    <img srcset="/images/upload/<?= $slider->i18n[0]->pic4 ?>"/>
                </picture>
                <div class="container">
                    <div class="title <?= $slider->title_color ?>"><?= $slider->i18n[0]->first_title ?></div>
                    <div class="subtitle <?= $slider->second_title_color ?>"><?= $slider->i18n[0]->second_title ?></div>
                    <?php if ($slider->i18n[0]->button_title != '') { ?><span
                        class="btn<?php if($isBusiness) echo ' business'; ?>"><?= $slider->i18n[0]->button_title ?></span><?php } ?>
                </div>
            <?= CHtml::closeTag($tag) ?>
            <?php if (param('ga') != ''): ?>
                <script>
                    $(function () {
                        ga('send', 'event', 'frontslider', 'view', '<?php echo $label; ?>');
                    });
                </script>
            <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="controls">
            <ul>
                <?php foreach ($sliders as $idx => $slider): ?>
                    <li <?php if ($idx == 0) { ?>class="active"<?php } ?>>
                        <button><?= $slider->i18n[0]->title ?></button>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="mobile-controls">
            <button class="bwd"></button>
            <button class="fwd"></button>
        </div>
    </div>
<?php if (param('ga') != ''): ?>
    <script>
        $(function () {
            $('a.slide').click(function (e) {
                ga('send', 'event', 'frontslider', 'click', $(this).data('label'));
                return true;
            });
        });
    </script>
<?php endif; ?>