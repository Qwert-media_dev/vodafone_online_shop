<?php
/* @var $items RoamingCallPlan[] */
/* @var $networks array */
?>
<hr class="extrawide">
<div class=" countries">
    <div class="accordion">
        <div class="panel">
            <div class="title">
                <div class="container">
                    <div
                        class="column full"><?= Yii::t('app', 'Країни Vodafone в Європі,  в яких діє послуга «Роумінг, як вдома»') ?></div>
                </div>
            </div>
            <div class="content" style="display: none;">
                <?php
                include 'filter-countries.php'
                ?>
                <div class="container">
                    <div class="column full">
                        <div class="">
                            <table class="striped">
                                <thead>
                                <tr>
                                    <td><?= Yii::t('app', 'Країна') ?></td>
                                    <td><?= Yii::t('app', 'Код країни') ?></td>
                                    <td><?= Yii::t('app', 'Мережа') ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <colgroup>
                                    <col>
                                    <col>
                                    <col>
                                </colgroup>
                                <tbody>
                                <?php foreach ($items as $item): ?>
                                    <tr>
                                        <th><?= $item->title ?></th>
                                        <td><?= $item->code ?></td>
                                        <td><?= $networks[$item->network_id] ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="extrawide">