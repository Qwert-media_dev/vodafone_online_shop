<?php
/* @var $this IntCallPrivatePrepaidSimpleWidget */
/* @var $countries Country[] */
$hasPopular = false;
?>
<hr class="extrawide">
<div class="countries">
	<div class="accordion">
		<div class="panel">
			<div class="title">
				<div class="container">
					<div class="column full"><?= Yii::t('app', 'Країни Vodafone та інші країни світу для дзвінків, SMS&MMS в межах пакетних послуг') ?></div>
				</div>
			</div>
			<div class="content" style="display: none;">
				<div class="container">
					<div class="column full">
						<p><?= Yii::t('app', 'Полный перечень стран и направлений для звонков за границу, SMS&MMS в рамках пакета услуг, как по Украине в тарифах Vodafone Red S, M, L, а также для звонков 3 грн/мин – Vodafone Red XS и Vodafone Light.') ?></p>
					</div>
				</div>
				<?php
					include 'filter-countries.php'
				?>
				<div class="container">
					<div class="column full">
                        <table class="striped">
                            <thead>
                            <tr>
                                <td style="width:75%"><?= Yii::t('app', 'Країна') ?></td>
                                <td style="width:25%"><?= Yii::t('app', 'Код країни') ?></td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <colgroup>
                                <col>
                                <col>
                            </colgroup>
                            <tbody>
                            <?php foreach($countries as $item): ?>
                                <?php
                                if($item->is_popular) {
                                    $hasPopular = true;
                                    continue;
                                }
                                ?>
                                <tr>
                                    <th><?= $item->i18n[0]->title ?></th>
                                    <td><?= $item->code ?></td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
					</div>
				</div>
                <?php if($hasPopular): ?>
                    <div class="container">
                        <div class="column full">
                            <p><?= Yii::t('app', 'Дополнительные популярные направления для звонков и SMS&MMS:') ?></p>
                        </div>
                    </div>
                    <div class="container">
                        <div class="column full">
                            <table class="striped">
                                <thead>
                                <tr>
                                    <td style="width:75%"><?= Yii::t('app', 'Країна') ?></td>
                                    <td style="width:25%"><?= Yii::t('app', 'Код країни') ?></td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <colgroup>
                                    <col>
                                    <col>
                                </colgroup>
                                <tbody>
                                <?php foreach($countries as $item): ?>
                                    <?php
                                    if(!$item->is_popular) {
                                        continue;
                                    }
                                    ?>
                                    <tr>
                                        <th><?= $item->i18n[0]->title ?></th>
                                        <td><?= $item->code ?></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
			</div>
		</div>
	</div>
</div>
<hr class="extrawide">

