<?php
/* @var $this ActiveRateItemWidget */
/* @var $item Rates */
/* @var $advantages1 RatesAdvantages[] */
/* @var $advantages2 RatesAdvantages[] */
/* @var $action string */
?>
<div class="item">
    <a href="<?= url_to('rates/'.$action, ['lang' => $this->getController()->language->code, 'alias' => $item->i18n[0]->alias, 'type' => $item->type] ) ?>" style="background-image: url('/images/upload/<?= $item->preview ?>')" class="item-img">
        <div class="item-title"><?php
            echo $item->i18n[0]->title;
            if( $item->i18n[0]->second_title != '' )
                echo ' <span>', $item->i18n[0]->second_title, '</span>';
            ?></div>
        <?php if($item->is_top): ?>
        <div class="item-recomend"><?= Yii::t('app', 'Рекомендуємо') ?></div>
        <?php endif; ?>
    </a>
    <div class="item-content">
        <div class="item-features">
            <ul>
                <?php foreach ($advantages1 as $advantage): ?>
                <li>
                    <div style="background-image:url('/images/upload/<?= $advantage->icon ?>')" class="icon"></div><?= $advantage->title ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="item-advantages">
            <div class="title"><?= Yii::t('app', 'Переваги:') ?></div>
            <ul>
                <?php foreach ($advantages2 as $advantage): ?>
                <li> <?= $advantage->title ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="item-bottom">
            <div class="item-price">
                <span><?= $item->price ?></span><?= Yii::t('app', 'грн') ?>
                <?php if($item->i18n[0]->extra_price != ''): ?>
                    <small><?= $item->i18n[0]->extra_price ?></small>
                <?php endif; ?>
            </div>
            <a href="<?= url_to('rates/'.$action, ['lang' => $this->getController()->language->code, 'alias' => $item->i18n[0]->alias, 'type' => $item->type] ) ?>" class="btn"><?= Yii::t('app', 'Дiзнатися бiльше') ?></a>
        </div>
    </div>
</div>