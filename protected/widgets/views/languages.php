<?php
/* @var $this LanguagesWidget */
?>
<ul <?php if($this->righted) echo ' class="righted"'; ?>>
	<?php foreach($this->controller->languages as $lang): ?>
	<li <?php if($this->controller->language->id == $lang->id) echo ' class="active"';  ?>>
		<?php
        $url = $this->fixUrl($lang->code);
        if( app()->controller->id == 'site' && app()->controller->action->id == 'page' ) {
            $page = Alts::instance()->getItem($lang->code);
            if( !is_null($page) )
                $url = $page;
        }
        echo CHtml::link( $this->short ? $lang->title : $lang->long_title, $url );
        ?>
	</li>
	<?php endforeach; ?>
</ul>