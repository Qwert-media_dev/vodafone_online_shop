<?php
/* @var $this GeoLocationsWidget */
/* @var $geoMark string */
/* @var $items array */
?>
<div class="locations">
    <div class="wrap">
        <ul>
            <li><span><?= Yii::t('geopopup', 'Україна') ?></span></li>
            <?php foreach($items as $id => $item): ?>
            <li <?php if( $id == $geoMark ) echo ' class="active"'; ?>>
                <a href="#" data-id="<?= $id ?>"><?= $item['name'] ?></a>
                <?php if(count($item['nested'])): ?>
                    <ul>
                        <?php foreach($item['nested'] as $id2 => $second){ ?>
                            <li <?php if( $id2 == $geoMark ) echo ' class="active"'; ?>><a href="#" data-id="<?= $id2 ?>"><?= $second['name'] ?></a></li>
                        <?php } ?>
                    </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <button class="close">Close</button>
</div>