<?php
/* @var $this GeoConfirmWidget */
/* @var $name string */
/* @var $geo array */
/* @var $geoMark string */
?>
<div class="confirm <?php if( !$geoMark ) echo ' show'; ?>">
    <div>
        <p><?= Yii::t('geopopup', 'Ви дійсно знаходитесь в {region}', ['{region}' => $name]) ?>?</p>
        <p>
            <span class="yes" data-id="<?= implode('_', $geo) ?>"><?= Yii::t('geopopup', 'Так, все вірно') ?></span>
            <span class="no"><?= Yii::t('geopopup', 'Ні, змінити регіон') ?></span>
        </p>
    </div>
</div>