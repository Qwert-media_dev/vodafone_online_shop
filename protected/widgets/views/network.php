<div class="row">
	<div class="map-title">
		<div class="container">
			<div class="column full">
				<h1><?= Yii::t('network', 'Наша мережа') ?></h1>
				<div class="map-buttons">
					<button class="btn_listing"><?= Yii::t('network', 'Перелік') ?></button>
					<div class="map-coverages" data-data="<?= url_to('network/load', array('lang' => $this->getController()->language->code, 'key' => 'coverages')) ?>"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="map-wrapper">
		<div class="map-search"><input type="text" placeholder="<?= Yii::t('network', 'Шукати мiсто...') ?>"></div>
		<div class="map"></div>
		<div class="map-listing">
			<div class="container">
				<div class="column full">
					<button class="close_listing"></button>
					<div class="filter">
						<div class="legend" style="display:none">
							<div
								data-data="<?= url_to('network/load', array('lang' => $this->getController()->language->code, 'key' => 'points')) ?>"
								class="legend-3g"><img
									src="/img/map-marker-3g.png"/><?= Yii::t('app', 'точки доступу 3G до 42 Мбiт/сек') ?> </div>
						</div>
						<div class="selector state">
							<label><?= Yii::t('app', 'Регіон') ?>:</label>
							<div class="select">
								<select data-data="<?= url_to('network/load', array('lang' => $this->getController()->language->code, 'key' => 'states')) ?>"></select>
							</div>
						</div>
					</div>
					<div class="points">
						<?php foreach ($points as $state => $region): ?>
							<div data-state="<?= $state ?>" class="list">
								<h2><?= Yii::t('app', 'Спробуйте швидкісний Інтернет <em>3G Vodafone</em> до <em>42Мбіт/с</em> у') ?>
									<strong><?= $states[$state]['t_title2'] ?></strong></h2>
								<div class="scrollable">
									<?php foreach ($region['groups'] as $group): ?>
										<?php if (strlen($group['title']) > 0): ?>
											<div class="district collapsed">
											<strong><?= $group['title'] ?></strong>
											<div class="collapse">
										<?php endif; ?>
										<ol>

											<?php
											$items = $group['items'];
											//ksort($items);
											foreach($items as $row): ?>
												<li><a href="#"
													   data-point="<?= $row['id'] ?>"><?= $row['description'] ?></a>
												</li>
											<?php endforeach; ?>
										</ol>
										<?php if (strlen($group['title']) > 0): ?>
											</div></div>
										<?php endif; ?>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>