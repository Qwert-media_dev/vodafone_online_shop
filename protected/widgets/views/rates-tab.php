<?php
/* @var $this RatesTabWidget */
/* @var $items Rates[] */
/* @var $type string*/

if(count($items)) {
?>
<div class="select-type">
    <?php
    foreach($items as $item) {
        $params = [];
        if( $this->current->id == $item->id ) {
            $params['class'] = 'active';
        }
        echo CHtml::link(
            $item->i18n[0]->tab_title,
            [
                'rates/show',
                'lang' => $this->getController()->language->code,
                'type' => $type,
                'alias' => $item->i18n[0]->alias
            ],
            $params
        );
    }
    ?>

</div>
<?php } ?>