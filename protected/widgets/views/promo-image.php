<?php
/* @var $this PromoImageWidget */
?>
<picture>
    <!--[if IE 9]><video style="display: none;"><![endif]-->
	<source srcset="/images/upload/<?= $this->pic1 ?>" media="(min-width: 1601px)"/>
	<source srcset="/images/upload/<?= $this->pic2 ?>" media="(min-width: 980px)"/>
	<source srcset="/images/upload/<?= $this->pic3 ?>" media="(min-width: 660px)"/>
    <!--[if IE 9]></video><![endif]-->
	<img srcset="/images/upload/<?= $this->pic4 ?>"/>
</picture>