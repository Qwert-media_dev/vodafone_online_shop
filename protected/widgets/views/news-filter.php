<?php
/* @var $this NewsFilterWidget */
/* @var $data array */
/* @var $months array */
/* @var $dateTitle string */
/* @var $month integer */
/* @var $year integer */
/* @var $action string */
/* @var $filtered boolean */
$controller = app()->controller->id;
$action = app()->controller->action->id;
?>
<div class="select-type">
    <?php
    if( $controller == News::TYPE_DIALOG ) {
        $link1title = Yii::t('app', 'Приватним клієнтам');
        $link2title = Yii::t('app', 'Для бізнесу');
        $link1 = url_to('dialog/index', ['lang' => $this->controller->language->code, 'page' => 'private']);
        $link2 = url_to('dialog/index', ['lang' => $this->controller->language->code, 'page' => 'bussines']);
        $params1 = $_GET['page'] == 'private' ? ['class' => 'active'] : [];
        $params2 = $_GET['page'] == 'bussines' ? ['class' => 'active'] : [];
    } else {
        $link1title = $controller == News::TYPE_VACANCY ? Yii::t('app', 'Вакансії') : Yii::t('app', 'Новини');
        $link2title = Yii::t('app', 'Архів');
        $link1 = url_to($controller.'/index', ['lang' => $this->controller->language->code]);
        $link2 = url_to($controller.'/archive', ['lang' => $this->controller->language->code]);
        $params1 = in_array($action, ['index', 'show']) ? ['class' => 'active'] : [];
        $params2 = in_array($action, ['archive', 'archiveshow']) ? ['class' => 'active'] : [];
    }

    echo CHtml::link( $link1title, $link1, $params1 );
    echo CHtml::link( $link2title, $link2, $params2 );
    ?>
</div>
<h1 class="page-title"><?= $this->controller->pageTitle ?></h1>
<button class="period-filter"><span><?= $dateTitle ?></span></button>
<div class="calendar">
	<div class="years">
        <a href="<?= url_to($controller.'/index', ['lang' => $this->controller->language->code]) ?>" class="year<?php if(is_null($year)) echo ' active'; ?>" data-val="0"><?= Yii::t('app', 'Всі') ?></a>
        <?php foreach($data as $_year => $dummy ): ?>
		<a href="#" class="year<?php if( $year == $_year ) echo ' active'; ?>" data-val="<?= $_year ?>"><?= $_year ?></a>
        <?php endforeach; ?>
	</div>
	<div class="months">
        <?php
        foreach(range(1,12) as $_month){
            $class = ['month'];
            if( $filtered && $month == $_month ) {
                $class[] = 'active';
            }

            if( $filtered && $data[$year][$_month] != 1 ) {
                $class[] = 'disabled';
            }

            if( !$filtered) {
                $class[] = 'disabled';
            }

        ?>
		<a href="#" class="<?php echo implode(' ', $class) ?>" data-val="<?= $_month ?>"><?= $months[$_month] ?></a>
        <?php } ?>
		<div class="buttons">
            <button class="close"><span><?= Yii::t('app', 'Закрити') ?></span></button>
        </div>
	</div>
    <?php
    $formAction = [$controller.'/'.$action, 'lang' => $this->controller->language->code];
    if( $controller == News::TYPE_DIALOG ) {
        $formAction = array_merge($formAction, ['page' => $_GET['page']]);
    }
    echo CHtml::beginForm($formAction, 'get', ['id' => 'filter']);
    echo CHtml::hiddenField('filter[year]', '', ['id' => 'year']);
    echo CHtml::hiddenField('filter[month]', '', ['id' => 'month']);
    echo CHtml::endForm();
    ?>
</div>

<script>
    var cal = <?= CJSON::encode($data) ?>;
    $(function(){
        $('a.year').click(function(e){
            var year = $(this).data('val');

            if( year != 0 ) {
                e.preventDefault();
                setActiveYear(year);
                $('a.month').removeClass('active').removeClass('disabled');
                for(var i =1; i < 13; i++) {
                    var selector = '.month[data-val='+i+']';
                    if(cal[year][i] != 1) {
                        $(selector).addClass('disabled');
                    }
                }
            }
        });

        $('a.month').click(function(e){
            e.preventDefault();
            if( $(this).hasClass('disabled') ) return;
            if( !$('a.year.active').length ) return;

            var year = $('a.year.active').data('val');
            var month = $(this).data('val');
            setActiveMonth(month);

            $('#year').val( year );
            $('#month').val( month );
            $('#filter').submit();
        });

        <?php if(!is_null($year)): ?>
        setActiveYear(<?= $year ?>);
        <?php endif; ?>

        <?php if(!is_null($month)): ?>
        setActiveMonth(<?= $month ?>);
        <?php endif; ?>
    });

    function setActiveYear(year) {
        $('a.year').removeClass('active');
        $('a.year[data-val='+year+']').addClass('active');
    }

    function setActiveMonth(month) {
        $('a.month').removeClass('active');
        $('a.month[data-val='+month+']').addClass('active');
    }
</script>