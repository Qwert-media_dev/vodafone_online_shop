<?php
/* @var $this GeoPopupWidget */
/* @var $items array */
/* @var $geoMark string */
/* @var $name string */
/* @var $geo array */
?>
<li class="geo <?php if( !$geoMark ) echo ' confirm'; ?>">
    <span class="icon"></span>
    <?php if( !$geoMark ): ?>
    <div class="confirm">
        <div>
            <p><?= Yii::t('geopopup', 'Ви дійсно знаходитесь в {region}', ['{region}' => $name]) ?>?</p>
            <p>
                <span class="yes" data-id="<?= implode('_', $geo) ?>"><?= Yii::t('geopopup', 'Так, все вірно') ?></span>
                <span class="no"><?= Yii::t('geopopup', 'Ні, змінити регіон') ?></span>
            </p>
        </div>
    </div>
    <?php endif; ?>
    <div class="choose">
        <ul class="list">
            <li class="title"><?= Yii::t('geopopup', 'Україна') ?></li>
            <?php foreach($items as $id => $item): ?>
            <li class="region <?php if( $id == $geoMark ) echo ' active'; ?>">
                <a href="#" data-id="<?= $id ?>"><?= $item['name'] ?></a>
                <?php if(count($item['nested'])): ?>
                    <ul>
                        <?php foreach($item['nested'] as $id2 => $second){ ?>
                        <li <?php if( $id2 == $geoMark ) echo ' class="active"'; ?>><a href="#" data-id="<?= $id2 ?>"><?= $second['name'] ?></a></li>
                        <?php } ?>
                    </ul>
                <?php endif; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</li>
<li class="separator">&nbsp;</li>

<script>
    $(function(){
        $('.locations a, .confirm .yes, .geo .choose a').click(function (e) {
           e.preventDefault();
            $.post('<?= url_to('site/setgeo') ?>', {id: $(this).data('id')}, function (resp) {
                var uri = window.location.toString().split('#');
                window.location = uri[0];
            }, 'text');
        });
    });

</script>