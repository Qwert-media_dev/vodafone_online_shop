<?php
/* @var $this FrontBreadcrumpsWidget*/
$index = 0;
$current = array_pop($this->breadcrumbs);
?>
<nav class="bread-crumps">
	<ol itemscope itemtype="http://schema.org/BreadcrumbList">
		<li class="home" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="<?= url_to('site/index', array('lang' => $this->controller->language->code)) ?>">
				<span itemprop="name">Home</span>
			</a>
			<meta itemprop="position" content="1" />
		</li>
		<?php foreach( $this->breadcrumbs as $index => $item ): ?>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="<?= $item['url'] ?>">
				<span itemprop="name"><?= $item['title'] ?></span>
			</a>
			<meta itemprop="position" content="<?= $index + 2 ?>" />
		</li>
		<?php endforeach; ?>
		<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
			<strong><?= $current['title'] ?></strong>
			<meta itemprop="position" content="<?= $index + 2 ?>" />
		</li>
	</ol>
</nav>