<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/2/2015
 * Time: 07:13 PM
 */
class IntCallPrivatePrepaidSimpleWidget extends CWidget
{
	public function run() {
		$countries = Country::model()->getFromNetwork( $this->controller->language->id );
		$this->render('int-call-private-prepaid-simple', compact('countries'));
	}
}