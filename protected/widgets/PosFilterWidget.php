<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/4/2015
 * Time: 10:12 PM
 */
class PosFilterWidget extends CWidget
{

	public function run() {
		$regions = PosRegion::model()->getItems($this->controller->language->id);
		$cities = array();
		$region_id = (int)$_GET['filter']['region'];
		if($region_id > 0) {
			$cities = CHtml::listData(PosCity::model()->getItemsByRegion( $region_id, $this->controller->language->id ), 'id', 'i18n.0.title');
		}
		$this->render('pos-filter', compact('regions', 'cities'));
	}
}