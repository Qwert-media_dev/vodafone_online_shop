<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/2/2015
 * Time: 09:32 PM
 */
class NewsShareWidget extends CWidget
{

	public $model;

	public function run() {
		$lang_code = $this->controller->language->code;
		$title = CHtml::encode( empty( $this->model->i18n[0]->og_title ) ? $this->model->i18n[0]->title : $this->model->i18n[0]->og_title );
		$description = CHtml::encode( empty( $this->model->i18n[0]->og_description ) ? $this->model->i18n[0]->short : $this->model->i18n[0]->og_description );
		$image = empty( $this->model->i18n[0]->og_image ) ? $this->controller->settings['og_image'][$lang_code] : $this->model->i18n[0]->og_image;
		if( $image != '' ) {
			$image = sprintf('%s%s', app()->request->getHostInfo(), $image);
		}
		$url = sprintf('%s/%s', app()->request->getHostInfo(), cleanupPathInfo(Yii::app()->request->getPathInfo()));
		$this->render('news-share', compact('title', 'description', 'image', 'url'));
	}
}