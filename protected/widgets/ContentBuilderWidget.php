<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 21.10.15
 * Time: 16:41
 */

class ContentBuilderWidget extends CWidget {

    public $selector;
    public $fileSelectUrl;

    public function run() {
        $this->render('content-builder', array('selector' => $this->selector, 'fileSelectUrl' => $this->fileSelectUrl));
    }
}