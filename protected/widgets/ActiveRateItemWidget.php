<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/27/16
 * Time: 16:06
 */
class ActiveRateItemWidget extends CWidget
{

    /**
     * @var Rates
     */
    public $item;

    public $is_archive = false;

    public function run() {
        $action = $this->is_archive ? 'archiveshow' : 'show';
        $advantages1 = RatesAdvantages::model()->getItems($this->item->id, 'rich', $this->getController()->language->id);
        $advantages2 = RatesAdvantages::model()->getItems($this->item->id, 'text', $this->getController()->language->id);
        $this->render('active-rate-item', ['item' => $this->item, 'advantages1' => $advantages1, 'advantages2' => $advantages2, 'action' => $action]);
    }
}