<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 8/4/16
 * Time: 16:13
 */
class TvHellWidget extends CWidget 
{

    public function run() {
        $this->controller->isTv = true;
        $this->controller->extraContainerClass = 'fullwidth';
        $this->render('tv-hell');
    }
}