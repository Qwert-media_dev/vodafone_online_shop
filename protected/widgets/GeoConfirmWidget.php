<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/5/16
 * Time: 15:20
 */
class GeoConfirmWidget extends CWidget
{

    public function run() {
        $geoMark = (string)app()->request->cookies['section'];
        $geo = getGeo();
        if( $geo[1] == 0 ) {
            $name = SxgeoRegions::model()->getName($geo[0], $this->getController()->language->id);
        } else {
            $name = SxgeoCities::model()->getName($geo[0], $geo[1], $this->getController()->language->id);
        }
        $this->render('geo-confirm', compact('geo', 'region', 'geoMark', 'name'));
    }
}