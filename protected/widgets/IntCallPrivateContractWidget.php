<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/4/2015
 * Time: 04:16 PM
 */
class IntCallPrivateContractWidget extends CWidget
{
	public function run() {
		$countries = Country::model()->getFromNetwork( $this->controller->language->id, 'contract_visibility' );
		$items = IntCallPlan::model()->getItems( IntCallPlan::TYPE_CONTRACT, $this->controller->language->id  );
		$this->render('int-call-private-contract', compact('countries', 'items'));
	}
}