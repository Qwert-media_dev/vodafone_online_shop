<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/15/2015
 * Time: 02:19 PM
 */
class LanguagesWidget extends CWidget
{

	/**
	 * @var bool если true используется title, иначе long_title
	 */
	public $short = false;

	/**
	 * @var bool метка для UL класса
	 */
	public $righted = false;

	public function run(){
		$this->render('languages');
	}

	public function fixUrl($lang_code) {
		list($url,) = explode('?', app()->request->requestUri);
		$url = cleanupPathInfo($url);
		$url = str_replace('//', '/', $url);
		if( app()->controller->id == 'site' && app()->controller->action->id == 'index' ) {
			return '/'.$lang_code;
		} else {
			$segments = explode('/', $url);
			$segments[1] = $lang_code;
			return implode('/', $segments);
		}
	}
}