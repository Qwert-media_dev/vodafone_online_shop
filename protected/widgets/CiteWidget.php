<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 10.07.15
 * Time: 17:49
 */

class CiteWidget extends CWidget {

    public function run() {
        if (function_exists('curl_init')) {
            $ch = curl_init();

            $data = array('method' => 'getQuote', 'key' => rand(1, 99999), 'format' => 'html');

            curl_setopt($ch, CURLOPT_URL, 'http://www.forismatic.com/api/1.0/');
            //curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

            $res = curl_exec($ch);

            curl_close($ch);
        }
        $this->render('cite', array('cite' => substr($res, 0, -1)));
    }

}