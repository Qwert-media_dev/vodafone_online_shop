<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/2/2015
 * Time: 06:28 PM
 */
class RoamingPrivatePrepaidWidget extends CWidget
{
    public function run()
    {
        $networks = Network::model()->getFlat($this->controller->language->id);
        $items = RoamingCallPlan::model()->getItems(RoamingCallPlan::TYPE_PRIVATE, $this->controller->language->id );
        $this->render('roaming-private-prepaid', compact('items', 'networks'));
    }
}