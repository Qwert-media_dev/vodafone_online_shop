<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/3/2015
 * Time: 02:22 PM
 */
class FrontSliderWidget extends CWidget
{

    public $page_id;

    public function run()
    {
        $geo = getGeo();
        $sliders = FrontSlider::model()->getItems($this->page_id, $this->controller->language->id, $geo);
        $isBusiness = $this->page_id == $this->controller->landing->id2;
        $this->render('front-slider', compact('sliders', 'isBusiness'));
    }

}