<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/28/16
 * Time: 14:49
 */
class RatesMenuWidget extends CWidget
{

    public function run() {
        $geo = getGeo();
        $type = $_GET['type'];
        $is_archive = app()->controller->action->id == 'archive' || app()->controller->action->id == 'archiveshow';
        $has_private = Rates::model()->hasRates( 'private', $this->controller->language->id, $geo );
        $has_contract = Rates::model()->hasRates( 'contract', $this->controller->language->id, $geo );
        $has_archive = Rates::model()->hasArchive( $type, $this->controller->language->id, $geo );
        $this->render('rates-menu', [
            'type' => $type,
            'code' => $this->controller->language->code,
            'is_archive' => $is_archive,
            'has_private' => $has_private,
            'has_contract' => $has_contract,
            'has_archive' => $has_archive
        ]);
    }
    
    
}