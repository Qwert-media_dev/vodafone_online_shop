<?php

/**
 * Пробую унифицировать адский момент с подключением fileapi
 */
class FileUploaderWidget extends CWidget
{

	/**
	 * @var string поле + код языка или просто название поля для не мультиязычных полей
	 */
	public $fieldKey;

	/**
	 * @var string поле модели, к которой подключается виджет загрузки
	 */
	public $field;

	/**
	 * @var CActiveRecord экземпляр CActiveRecord
	 */
	public $model;

	/**
	 * @var CActiveForm экзмепляр CActvieForm
	 */
	public $form;

	/**
	 * @var string подсказка для поля
	 */
	public $help;

	/**
	 * @var integer ID языка для мультиязычных полей
	 */
	public $lang_id;

	/**
	 * @var string Ссылка обрабатывающая загрузку
	 */
	public $url;

	public function run() {
		$this->render('file-uploader', array('fieldKey' => $this->fieldKey, 'field' => $this->field, 'model' => $this->model, 'form' => $this->form, 'lang_id' => $this->lang_id));
	}
}