<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/2/2015
 * Time: 07:13 PM
 */
class IntCallPrivatePrepaidWidget extends CWidget
{
	public function run() {
		$countries = Country::model()->getFromNetwork( $this->controller->language->id, 'private_visibility' );
		$items = IntCallPlan::model()->getItems( IntCallPlan::TYPE_PRIVATE, $this->controller->language->id  );
		$this->render('int-call-private-prepaid', compact('countries', 'items'));
	}
}