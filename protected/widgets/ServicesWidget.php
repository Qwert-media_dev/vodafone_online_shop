<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/29/16
 * Time: 22:04
 */
class ServicesWidget extends CWidget {

    public function run() {
        $item = Services::model()->getItem(getGeo(), $this->controller->language->id);
        $this->render('services', compact('item'));
    }
}