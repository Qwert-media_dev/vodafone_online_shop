<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/19/2015
 * Time: 12:20 AM
 */
class FrontFooterBannerWidget extends CWidget
{
	public $page_id;

	public function run() {
		$items = FooterBanner::model()->getSet($this->page_id, $this->controller->language->id);

		switch(count($items)) {
			case 1:
				$class = ' one';
				break;
			case 2:
				$class = ' two';
				break;
			default:
				$class = '';
		}

		$this->render('front-footer-banner', compact('items', 'class'));
	}
}