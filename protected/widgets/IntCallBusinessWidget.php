<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/2/2015
 * Time: 08:01 PM
 */
class IntCallBusinessWidget extends CWidget
{
	public function run() {
		$countries = Country::model()->getFromNetwork( $this->controller->language->id, 'business_visibility');
		$items = IntCallPlan::model()->getItems( IntCallPlan::TYPE_BUSINESS, $this->controller->language->id  );
		$this->render('int-call-business', compact('countries', 'items'));
	}
}