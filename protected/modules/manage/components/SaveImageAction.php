<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 9/28/2015
 * Time: 10:21 PM
 */
class SaveImageAction extends CAction
{

    public function run() {
        header('Cache-Control: no-cache, must-revalidate');
        //Specify url path
        $path = param('uploadPath');
        $webPath = '/images/upload/';

        //Read image
        $count = $_REQUEST['count'];
        $b64str = $_REQUEST['hidimg-' . $count];
        $imgname = $_REQUEST['hidname-' . $count];
        $imgtype = $_REQUEST['hidtype-' . $count];

        //Generate random file name here
        if($imgtype == 'png'){
            $image = $imgname . '-' . base_convert(rand(),10,36) . '.png';
            $original_name = sprintf('%s.%s', $imgname, 'png');
        } else {
            $image = $imgname . '-' . base_convert(rand(),10,36) . '.jpg';
	        $original_name = sprintf('%s.%s', $imgname, 'jpg');
        }

        //Save image
        $success = file_put_contents($path . $image, base64_decode($b64str));
        if ($success === FALSE) {

            if (!file_exists($path)) {
                echo "<html><body onload=\"alert('Saving image to folder failed. Folder ".$path." not exists.')\"></body></html>";
            } else {
                echo "<html><body onload=\"alert('Saving image to folder failed. Please check write permission on " .$path. "')\"></body></html>";
            }

        } else {
            $folder = FilesFolders::model()->findByAttributes(['protected' => 1]);
            if( !is_null($folder) ) {
                $file = new Files();
                $file->original_name = $original_name;
                $file->local_name = $image;
                $file->ext = $imgtype;
                $file->size = filesize(param('uploadPath') . $image);
                $file->user_id = user()->id;
                if($file->save()) {
                    $connect = new FilesToFolders();
                    $connect->file_id = $file->id;
                    $connect->folder_id = $folder->id;
                    $connect->save();
                }
            }

            //Replace image src with the new saved file
            echo "<html><body onload=\"parent.document.getElementById('img-" . $count . "').setAttribute('src','" . $webPath . $image . "');  parent.document.getElementById('img-" . $count . "').removeAttribute('id') \"></body></html>";
        }
    }
}