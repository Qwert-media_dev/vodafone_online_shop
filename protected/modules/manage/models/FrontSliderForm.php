<?php

/**
 * Class FrontSliderForm
 * Валидатор баннеров для главной страницы
 */
class FrontSliderForm extends CFormModel
{

	public $image;

	public function rules()
	{
		return array(
			array('image', 'required', 'on' => array('pic1', 'pic2', 'pic3', 'pic4', 'image')),
			array('image', 'file', 'types' => 'png, jpg, jpeg, gif', 'allowEmpty' => false, 'on' => array('pic1', 'pic2', 'pic3', 'pic4', 'image', 'og_image')),

			array('image', 'checkDim', 'on' => 'pic1', 'width' => 2600, 'height' => 506),
			array('image', 'checkDim', 'on' => 'pic2', 'width' => 1600, 'height' => 506),
			array('image', 'checkDim', 'on' => 'pic3', 'width' => 980, 'height' => 360),
			array('image', 'checkDim', 'on' => 'pic4', 'width' => 660, 'height' => 500),

			array('image', 'checkDim', 'on' => 'image', 'width' => 620, 'height' => 240),
			array('image', 'checkDim', 'on' => 'og_image', 'width' => 1200, 'height' => 630),
		);
	}


	public function checkDim($attribute, $params)
	{
		if(!$this->hasErrors()) {
			list($width, $height, $type, $dummy1, $mime) = getimagesize($this->image->tempName);
			if( $width != $params['width'] || $height != $params['height'] ) {
				$this->addError('image', sprintf('Не верный размер файла (%sx%s)', $width, $height));
			}
		}
	}

}