<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/18/16
 * Time: 18:23
 */

class PhotoForm extends CFormModel
{

    public $file;

    public function rules()
    {
        return array(
            array('file', 'required'),
            array('file', 'file', 'types' => 'jpg, jpeg, png', 'allowEmpty' => false),
            array('file', 'isImage')
        );
    }


    public function isImage($attribute, $params) {

        if ($this->file instanceof CUploadedFile ) {
            $info = getimagesize( $this->file->tempName );
            if( !in_array($info[2], [IMAGETYPE_JPEG, IMAGETYPE_JPEG2000, IMAGETYPE_PNG]) ) {
                $this->addError('file', 'Не верный формат файла' );
                $this->file = '';
            }
        }
    }


}