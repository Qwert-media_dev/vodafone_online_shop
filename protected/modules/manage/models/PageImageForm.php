<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/6/2015
 * Time: 03:40 PM
 */
class PageImageForm extends  CFormModel
{

	public $image;

	public function rules()
	{
		return array(
			array('image', 'required'),
			array('image', 'file', 'types' => 'png, jpg, jpeg', 'allowEmpty' => false),
		);

	}
}