<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/8/2015
 * Time: 05:40 PM
 */
class ManageMenuWidget extends CWidget
{

    public $ulClass = 'nav navbar-nav';

    /**
     * @var AdminGroups
     */
    protected $group;
    protected $rules;

    public function init()
    {
        $this->group = user()->group;
        $this->rules = CJSON::decode($this->group->rules);
    }

    public function run()
    {
        $controller = $this->controller->id;
        $items = $this->getItems();
        $this->render('manage-menu', compact('controller', 'items'));
    }

    protected function getItems()
    {
        return [
            ['controller' => ['default'], 'url' => url_to('manage/default/index'), 'title' => '<span class="glyphicon glyphicon-home"></span>', 'visible' => $this->visible('default')],
            ['controller' => ['pages'], 'url' => url_to('manage/page/pages/tree'), 'title' => 'Страницы', 'visible' => $this->visible('pages')],
            ['controller' => ['news', 'press', 'vacancy', 'dialog', 'rates', 'services'], 'url' => '#', 'title' => 'Компания <span class="caret"></span>', 'items' => [
                ['controller' => ['news'], 'url' => url_to('manage/company/news/admin'), 'title' => 'Новости', 'visible' => $this->visible('news')],
                //['controller' => ['press'], 'url' => url_to('manage/company/press/admin'), 'title' => 'Пресс-центр', 'visible' => $this->visible('press')],
                ['controller' => ['vacancy'], 'url' => url_to('manage/company/vacancy/admin'), 'title' => 'Вакансии', 'visible' => $this->visible('vacancy')],
                ['controller' => ['dialog'], 'url' => url_to('manage/company/dialog/admin'), 'title' => 'Диалог', 'visible' => $this->visible('dialog')],
                ['controller' => ['rates'], 'url' => url_to('manage/company/rates/admin'), 'title' => 'Тарифы', 'visible' => $this->visible('rates')],
                ['controller' => ['services'], 'url' => url_to('manage/company/services/admin'), 'title' => 'Услуги', 'visible' => $this->visible('services')],
            ]],
            ['controller' => ['files'], 'url' => url_to('manage/files/admin'), 'title' => 'Файлы', 'visible' => $this->visible('files')],
            ['controller' => ['region', 'city', 'point'], 'url' => '#', 'title' => 'Точки продаж <span class="caret"></span>', 'items' => [
                ['controller' => ['point'], 'url' => url_to('manage/pos/point/admin'), 'title' => 'Точки продаж', 'visible' => $this->visible('point')],
                [],
                ['controller' => ['region'], 'url' => url_to('manage/pos/region/admin'), 'title' => 'Регионы', 'visible' => $this->visible('region')],
                ['controller' => ['city'], 'url' => url_to('manage/pos/city/admin'), 'title' => 'Города', 'visible' => $this->visible('city')]
            ]],
            ['controller' => ['country', 'intCallPlan', 'network', 'roamingCallPlan'], 'url' => '#', 'title' => 'Тарифы <span class="caret"></span>', 'items' => [
                ['controller' => ['country'], 'url' => url_to('manage/tariff/country/admin'), 'title' => 'Страны', 'visible' => $this->visible('country')],
                ['controller' => ['intCallPlan'], 'url' => url_to('manage/tariff/intCallPlan/admin'), 'title' => 'Международные звонки', 'visible' => $this->visible('intCallPlan')],
                ['controller' => ['roamingCallPlan'], 'url' => url_to('manage/tariff/roamingCallPlan/admin'), 'title' => 'Роуминг', 'visible' => $this->visible('roamingCallPlan')],
                ['controller' => ['network'], 'url' => url_to('manage/tariff/network/admin'), 'title' => 'Сеть', 'visible' => $this->visible('network')],

            ]],
            ['controller' => ['settingsLanding', 'redirects', 'importPoints', 'settingsParams', 'logs', 'cache', 'consult', 'paymentPresets', 'feedbacksEmails'], 'url' => '#', 'title' => 'Настройки <span class="caret"></span>', 'items' => [
                ['controller' => ['settingsLanding'], 'url' => url_to('manage/settings/settingsLanding/admin'), 'title' => 'Лендинговые страницы', 'eval' => 'return user()->email == param("adminEmail");', 'visible' => $this->visible('settingsLanding')],
                ['controller' => ['redirects'], 'url' => url_to('manage/settings/redirects/admin'), 'title' => 'Короткие ссылки', 'visible' => $this->visible('redirects')],
                ['controller' => ['importPoints'], 'url' => url_to('manage/settings/importPoints/admin'), 'title' => 'Импорт покрытия', 'visible' => $this->visible('importPoints')],
                ['controller' => ['settingsParams'], 'url' => url_to('manage/settings/settingsParams/admin'), 'title' => 'Различные настройки', 'visible' => $this->visible('settingsParams')],
                ['controller' => ['logs'], 'url' => url_to('manage/settings/logs/admin'), 'title' => 'Лог', 'visible' => $this->visible('logs')],
                ['controller' => ['cache'], 'url' => url_to('manage/settings/cache/admin'), 'title' => 'Кеш', 'visible' => $this->visible('cache')],
                ['controller' => ['translations'], 'url' => url_to('manage/settings/translations/admin'), 'title' => 'Переводы', 'visible' => $this->visible('translations')],
                ['controller' => ['consult'], 'url' => url_to('manage/settings/consult/admin'), 'title' => 'Консультант', 'visible' => $this->visible('consult')],
                ['controller' => ['paymentPresets'], 'url' => url_to('manage/settings/paymentPresets/admin'), 'title' => 'Пополнение счета (суммы)', 'visible' => $this->visible('paymentPresets')],
                ['controller' => ['feedbacksEmails'], 'url' => url_to('manage/settings/feedbacksEmails/admin'), 'title' => 'Адресаты обратной связи', 'visible' => $this->visible('feedbacksEmails')]
            ]],
            ['controller' => ['frontSlider', 'footerBanner'], 'url' => '#', 'title' => 'Баннеры <span class="caret"></span>', 'items' => [
                ['controller' => ['frontSlider'], 'url' => url_to('manage/banners/frontSlider/admin'), 'title' => 'Слайдеры', 'visible' => $this->visible('frontSlider')],
                ['controller' => ['footerBanner'], 'url' => url_to('manage/banners/footerBanner/admin'), 'title' => 'Баннеры в подвале', 'visible' => $this->visible('footerBanner')],
                [],
                ['controller' => ['sxgeoRegions'], 'url' => url_to('manage/banners/sxgeoRegions/admin'), 'title' => 'Области', 'visible' => $this->visible('sxgeoRegions')],
                ['controller' => ['sxgeoCities'], 'url' => url_to('manage/banners/sxgeoCities/admin'), 'title' => 'Города', 'visible' => $this->visible('sxgeoCities')],
            ]],
            ['controller' => ['admins', 'adminGroups'], 'url' => '#', 'title' => 'Доступы <span class="caret"></span>', 'items' => [
                ['controller' => ['admins'], 'url' => url_to('manage/admins/admin'), 'title' => 'Пользователи', 'visible' => $this->visible('admins')],
                ['controller' => ['adminGroups'], 'url' => url_to('manage/adminGroups/admin'), 'title' => 'Группы', 'visible' => $this->visible('adminGroups')]
            ]],
            ['controller' => ['phones'], 'url' => url_to('manage/phones/phones'), 'title' => 'Телефоны', 'visible' => $this->visible('phones')],
            ['controller' => ['orders'], 'url' => url_to('manage/orders/orders'), 'title' => 'Заявки', 'visible' => $this->visible('orders')],
        ];
    }


    protected function visible($controller)
    {
        if ($this->group->protected) return true;
        $access = new Access();
        $access->setRules($this->rules);
        return $access->visible($controller);
    }
}