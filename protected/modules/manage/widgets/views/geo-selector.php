<?php
/* @var $this GeoSelectorWidget */
/* @var $geoData array */
/* @var $geo array */
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Географическая привязка
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-6">
                        <p class="help-block">Выбери регион для отображения баннера</p>
                    </div>
                    <div class="col-md-6">
                        <p class="help-block pull-right">Нажми на область для отобржения городов</p>
                    </div>
                </div>

                <ul class="list-unstyled" id="regions">
                    <li>
                        <?= CHtml::checkBox('Geo[]', in_array('0_0', $geo), array('value' => '0_0', 'id' => '0_0', 'data-root'=> 'yes')) ?>
                        Весь мир (кроме Украины). Актуально для Тарифы, Услуги, Баннеры
                    </li>
                    <?php foreach($geoData as $regionData => $rows): ?>
                        <?php $region = explode('|', $regionData) ?>
                        <li>
                            <?= CHtml::checkBox('Geo[]', in_array($region[0].'_0', $geo), array('value' => $region[0].'_0', 'id' => $region[0].'_0', 'data-region' => 'yes', 'data-region-id' => $region[0])) ?>
                            <a href="#" class="city-switch"><?= $region[1] ?></a>
                            <ul style="list-style: none;display:none">
                                <?php foreach($rows as $row): ?>
                                    <li>
                                        <?= CHtml::checkBox('Geo[]', in_array($region[0].'_'.$row['city_id'], $geo), array('value' => $region[0].'_'.$row['city_id'], 'id' => $region[0].'_'.$row['city_id'], 'data-city' => 'yes', 'data-region-id' => $region[0])) ?>
                                        <?= $row['city_name'] ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <script>
                    $(function(){
                        $('.city-switch').click(function (e) {
                            e.preventDefault();
                            $(this).next('ul').toggle();
                        });

                        $('#regions input[type=checkbox]').change(function() {
                            var id = $(this).attr('id');
                            var isRoot = $(this).data('root') == 'yes';
                            var isRegion = $(this).data('region') == 'yes';
                            var isCity = $(this).data('city') == 'yes';
                            var isChecked = $(this).is(':checked');
                            var region_id = $(this).data('region-id');

                            if( isRoot && isChecked ) {
                                $('#regions input[type=checkbox]').each(function() {
                                    if( $(this).attr('id') != id ) {
                                        $(this).prop('checked', false);
                                    }
                                });
                            }

                            // сбрасываем города в области
                            if( isRegion && isChecked ) {
                                $('#regions input[type=checkbox]').each(function(){
                                    if( $(this).data('city') == 'yes' && $(this).data('region-id') == region_id ) {
                                        $(this).prop('checked', false);
                                    }
                                });
                            }

                            // сбрасываем область если выбран город
                            if( isCity && isChecked ) {
                                $('#regions input[type=checkbox]').each(function(){
                                    if( $(this).data('region') == 'yes' && $(this).data('region-id') == region_id ) {
                                        $(this).prop('checked', false);
                                    }
                                });
                            }
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>