<?php
/* @var $this ManageMenuWidget */
/* @var $controller string */
/* @var $items array */

echo CHtml::openTag('ul', ['class' => $this->ulClass]);

foreach($items as $item) {
	if( count($item['items']) ) {
		$cnt = 0;
		$content = '';
		foreach($item['items'] as $row) {
			if( empty($row) ) {
				$content .= '<li role="separator" class="divider"></li>';
			} else {
				$visible = $row['visible'];
				if($row['eval'] != '') {
					$visible = $visible && eval($row['eval']);
				}
				if($visible) {
					$content .= CHtml::openTag('li', in_array($controller, $row['controller']) ? ['class' => 'active'] : []);
					$content .= CHtml::link($row['title'], $row['url']);
					$content .= CHtml::closeTag('li');
					$cnt++;
				}
			}
		}

		if($cnt > 0) {
			echo CHtml::openTag('li', in_array($controller, $item['controller']) ? ['class' => 'dropdown active'] : ['class' => 'dropdown']);
			echo CHtml::link($item['title'], $item['url'], [
				'class' => 'dropdown-toggle',
				'data-toggle' => 'dropdown',
				'role' => 'button',
				'aria-haspopup' => 'true',
				'aria-expanded' => 'false']);
			echo CHtml::openTag('ul', ['class' => 'dropdown-menu']);
			echo $content;
			echo CHtml::closeTag('ul');
			echo CHtml::closeTag('li');
		}
	} else {
		$visible = $item['visible'];
		if($item['eval'] != '') {
			$visible = $visible && eval($item['eval']);
		}
		if( $visible ) {
			echo CHtml::openTag('li', in_array($controller, $item['controller']) ? ['class' => 'active'] : []);
			echo CHtml::link($item['title'], $item['url']);
			echo CHtml::closeTag('li');
		}
	}
}

echo CHtml::closeTag('ul');