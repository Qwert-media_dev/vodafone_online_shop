<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/20/16
 * Time: 18:45
 */
class GeoExcludeSelectorWidget extends CWidget
{
    public $geoData;
    public $geo;
    
    public function run() {
        $this->render('geo-exclude-selector', ['geoData' => $this->geoData, 'geo' => $this->geo]);
    }
}