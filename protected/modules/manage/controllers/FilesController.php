<?php

class FilesController extends ManageController
{

	public $images = ['jpg', 'jpeg', 'gif', 'png'];

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + deletefolder', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(
					'admin','delete','bulk', 'download', 'createfolder', 'export', 'deletefolder', 'sortfolders',
					'addtofolder', 'removefromfolder', 'getfolders', 'getmenu'
				),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        if( file_exists( param('uploadPath').$model->local_name ) && $model->local_name != '' ) {
            unlink( param('uploadPath').$model->local_name );
        }
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->local_name );
        $model->delete();
        app()->user->setFlash('success', 'Файл удален');

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$items = Files::model()->getLast();
        $extentions = Files::model()->getExts();
        $folder_id = (int)$_GET['folder_id'];
		$this->render('admin', compact('items', 'extentions', 'folder_id'));
	}

    public function actionBulk() {
        $resp = array('status' => 'fail');
        $return = array();
        $folder_id = (int)$_GET['folder_id'];

        $file = CUploadedFile::getInstanceByName('file');
        $max = 1024*1024*12;
        if( !is_null($file) && $file->size <= $max ) {
            $filename = URLify::filter( FS::hash(time()) . '_' . $file->name, param('urlifyFileLength'), '', true);
            $file->saveAs( param('uploadPath') . $filename);

            $item = new Files();
            $item->original_name = $file->name;
            $item->local_name = $filename;
            $item->ext = pathinfo($file->name, PATHINFO_EXTENSION);
            $item->size = filesize(param('uploadPath') . $filename);
            $item->user_id = user()->id;

            if( $item->save() ) {

                if( $folder_id ) {
                    $ftf = new FilesToFolders();
                    $ftf->file_id = $item->id;
                    $ftf->folder_id = $folder_id;
                    $ftf->save();
                }

                LogRegistry::instance()->log( LogRegistry::METHOD_CREATE, get_class($item), $item->id, $item->local_name );
                $return = array(
                    'row' => $this->renderPartial('_row-file', compact('item', 'folder_id'), true),
                    'id' => $item->id
                );
            } else {
                $resp['reason'] = $file->getError();
                $resp['file'] = $file->name;
            }
        } else {
            $resp['reason'] = 'no-data';
        }

        if(sizeof($return)) {
            $resp = array('status' => 'ok', 'item' => $return);
        }

        echo CJSON::encode($resp);
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Files the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Files::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


    public function actionDownload($id) {
        $item = $this->loadModel($id);
        app()->request->sendFile(
            $item->original_name,
            file_get_contents(param('uploadPath').$item->local_name),
            'application/octet-stream'
        );
    }

    public function actionCreatefolder() {
        $resp = array('status' => 'fail');

        if( array_key_exists('title', $_POST) ) {
            $id = (int)$_POST['id'];
            $title = trim($_POST['title']);

            if( $id > 0 ) {
                $item = FilesFolders::model()->findByPk($id);
	            if( $item->protected ) {
		            echo CJSON::encode($resp);
		            Yii::app()->end();
	            }
                $old = true;
            } else {
                $item = new FilesFolders();
                $old = false;
            }
            $item->title = $title;
            if( $item->save() ) {

	            $method = $old ? LogRegistry::METHOD_UPDATE : LogRegistry::METHOD_CREATE;
	            LogRegistry::instance()->log( $method, get_class($item), $item->id, $item->title );

                $resp = array('status' => 'ok', 'row' => $this->renderPartial('_row-folder', compact('item'), true));
                if($old) {
                    $resp['old'] = $old;
                    $resp['id'] = $item->id;
                }
            }

        }

        echo CJSON::encode($resp);
    }

    public function actionExport() {
        //$this->layout = ' ';
        $items = Files::model()->getLast();
	    $categories = FilesFolders::model()->sorted()->findAll();
        $this->renderPartial('export', compact('items', 'categories'));
    }

    public function actionDeletefolder() {
        $resp = array('status' => 'fail');
        $id = (int)$_POST['id'];
        $folder = FilesFolders::model()->findByPk($id);
        if( !is_null($folder) && !$folder->protected ) {
	        LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($folder), $folder->id, $folder->title );
            $folder->delete();
            $resp = array('status' => 'ok');
        }

        echo CJSON::encode($resp);
    }


    public function actionSortfolders() {
        $resp = array('status' => 'fail');

        if( array_key_exists('items', $_POST) ) {
            $items = $_POST['items'];
            $sort = 10;
            foreach($items as $item) {
                list(,$id) = explode('_', $item);
                $folder = FilesFolders::model()->findByPk($id);
                $folder->sort = $sort;
                $folder->save();
                $sort+=10;
            }
            $resp = array('status' => 'ok');
        }

        echo CJSON::encode($resp);
    }

    public function actionAddtofolder() {
        $resp = array('msg' => '');

        $file_id = (int)$_POST['file_id'];
        $folder_id = (int)$_GET['folder_id'];
        if( $file_id > 0 && $folder_id > 0 ) {
            if( FilesToFolders::model()->countByAttributes(array('file_id' => $file_id, 'folder_id' => $folder_id)) ) {
                $resp = array('msg' => 'Файл уже есть в этой папке', 'class' => 'warning');
            } else {
                $item = new FilesToFolders();
                $item->file_id = $file_id;
                $item->folder_id = $folder_id;
                $item->save();
                $resp = array('msg' => 'Файл добавлен в папку', 'class' => 'success');
            }
        }

        echo CJSON::encode($resp);
    }

    public function actionRemovefromfolder($id, $folder_id) {
        FilesToFolders::model()->deleteAllByAttributes(array('file_id' => $id, 'folder_id' => $folder_id));
        user()->setFlash('success', 'Файл исключен из папки.');
        $this->redirect(array('admin', 'folder_id' => $folder_id));
    }


    public function actionGetfolders() {
        $items = FilesFolders::model()->sorted()->findAll();
        $this->renderPartial('_folders-dropdown', compact('items'));
    }

    public function actionGetmenu() {
        $items = FilesFolders::model()->sorted()->findAll();
        $folder_id = (int)$_GET['folder_id'];
        $this->renderPartial('_folders-menu', compact('items', 'folder_id'));
    }

}
