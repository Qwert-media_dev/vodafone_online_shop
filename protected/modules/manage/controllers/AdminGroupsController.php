<?php

class AdminGroupsController extends ManageController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new AdminGroups;
		$class = get_class($model);

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$rules = count($form['rules']) ? CJSON::encode($form['rules']) : '';
			$model->title = $form['title'];
			$model->rules = $rules;
			if($model->validate()) {
				$model->save();
				$this->redirect(array('admin'));
			}
		}

		$rules = (new Access())->getRules();
		$selected = [];
		$this->render('create', compact('model', 'rules', 'selected'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$class = get_class($model);

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$rules = count($form['rules']) ? CJSON::encode($form['rules']) : '';
			$model->title = $form['title'];
			$model->rules = $rules;
			if($model->validate()) {
				$model->save();
				$this->redirect(array('admin'));
			}
		}

		$rules = (new Access())->getRules();
		$selected = CJSON::decode($model->rules);
		$this->render('update', compact('model', 'rules', 'selected'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AdminGroups('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AdminGroups']))
			$model->attributes=$_GET['AdminGroups'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AdminGroups the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AdminGroups::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404, 'The requested page does not exist.');

		if($model->protected)
			throw new CHttpException(403, 'Forbidden.');

		return $model;
	}

}
