<?php

class DefaultController extends ManageController
{

    public function actionIndex()
    {
		$ip1 = getRealIP();
        $ip2 = $_SERVER['REMOTE_ADDR'];
        $geo = GeoHelper::detect( getRealIP() );
        $this->render('index', compact('ip1', 'ip2', 'geo'));
    }

    public function actionError()
    {
        $error = Yii::app()->errorHandler->error;
        $this->render('error', compact('error'));
    }


    public function actionUpload() {
        $resp = array('status' => 'fail', 'msg' => 'init');

        if (in_array($_POST['field'], array('pic1', 'pic2', 'pic3', 'pic4', 'og_image'))) {
            $model = new FrontSliderForm($_POST['field']);
            $model->image = CUploadedFile::getInstanceByName('files[0]');
            if ($model->validate()) {
                $filename = URLify::filter($model->image->name, param('urlifyFileLength'), '', true);
                $model->image->saveAs(param('uploadPath') . $filename);
                $resp = array(
                    'status' => 'ok',
                    'location' => $filename,
                );
            } else {
                $resp['msg'] = $model->getError('image');
                $resp['field'] = $_POST['field'];
            }
        }

        echo CJSON::encode($resp);
    }
}