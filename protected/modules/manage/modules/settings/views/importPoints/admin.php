<?php
/* @var $this ImportPointsController */

$this->breadcrumbs=array(
	'Импорт покрытия',
);
?>
<h1>Импорт покрытия</h1>

<?php if( $_GET['state'] == 'warning' ): ?>
    <div class="alert alert-warning" role="alert">Нет списка файлов для импорта!</div>
<?php endif; ?>

<?php if( $_GET['state'] == 'success' ): ?>
    <div class="alert alert-success" role="alert">Файлы обновлены.</div>
<?php endif; ?>

<?php echo CHtml::link('Импорт', array('import'), array('class' => 'btn btn-success')); ?>
