<?php
/* @var $this FeedbacksEmailsController */
/* @var $model FeedbacksEmails */

$this->breadcrumbs=array(
	'Feedbacks Emails'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список адресатов', 'url'=>array('admin')),
);
?>

<h1>Добавление</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>