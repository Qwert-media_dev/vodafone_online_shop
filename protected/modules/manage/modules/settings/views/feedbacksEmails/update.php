<?php
/* @var $this FeedbacksEmailsController */
/* @var $model FeedbacksEmails */

$this->breadcrumbs=array(
	'Feedbacks Emails'=>array('admin'),
	$model->id=>array('update','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Список email', 'url'=>array('create')),
	array('label'=>'Список адресатов', 'url'=>array('admin')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>