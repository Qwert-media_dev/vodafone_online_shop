<?php
/* @var $this FeedbacksEmailsController */
/* @var $model FeedbacksEmails */

$this->breadcrumbs=array(
	'Feedbacks Emails'=>array('фвьшт'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Добавить Email', 'url'=>array('create')),
);

?>

<h1>Список адресатов</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feedbacks-emails-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'enableSorting' => false,
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
		'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		'email',
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
		),
	),
));
