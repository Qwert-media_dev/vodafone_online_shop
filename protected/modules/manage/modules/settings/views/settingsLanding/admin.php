<?php
/* @var $this SettingsLandingController */
/* @var $model SettingsLanding */
/* @var $pages Pages[] */

$this->breadcrumbs=array(
	'Лендинговые страницы'=>array('admin'),
	'Настройки',
);

?>

<h1>Лендинговые страницы</h1>

<?php if( user()->hasFlash('success') ): ?>
    <div class="alert alert-success" role="alert"><?php echo user()->getFlash('success'); ?></div>
<?php endif; ?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'settings-landing-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Необходимо выбрать разделы <mark>Частным клиентам</mark> и <mark>Для Бизнеса</mark> </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model,'id1', array('class' => 'control-label')); ?>
        <?php echo $form->dropDownList($model,'id1', CHtml::listData($pages, 'id', 'title'), array('class' => 'form-control', 'style' => 'width:250px;', 'empty' => '-Выберите раздел-')); ?>
        <?php echo $form->error($model,'id1', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'id2', array('class' => 'control-label')); ?>
        <?php echo $form->dropDownList($model,'id2', CHtml::listData($pages, 'id', 'title'), array('class' => 'form-control', 'style' => 'width:250px;', 'empty' => '-Выберите раздел-')); ?>
        <?php echo $form->error($model,'id2', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->