<?php
/* @var $this RedirectsController */
/* @var $model Redirects */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'redirects-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>
    <p class="bg-success" style="padding:15px;">В качестве ссылки необходимо использовать выделенную часть <kbd style="padding: 5px;">http://vodafone.ua<mark>/prepaid-roaming</mark></kbd></p>

	<div class="form-group <?php if(isset($errors['short'])) echo ' has-error'; ?>">
		<?php echo $form->labelEx($model,'url_from', array('class' => 'control-label')); ?>
		<?php echo $form->textField($model,'url_from', array('class'=>'form-control', 'placeholder' => '/short-url')); ?>
		<?php echo $form->error($model,'url_from', array('class' => 'help-block')); ?>
	</div>

	<div class="form-group <?php if(isset($errors['short'])) echo ' has-error'; ?>">
		<?php echo $form->labelEx($model,'url_to', array('class' => 'control-label')); ?>
		<?php echo $form->textField($model,'url_to', array('class'=>'form-control', 'placeholder' => '/real/long/url')); ?>
		<?php echo $form->error($model,'url_to', array('class' => 'help-block')); ?>
	</div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->