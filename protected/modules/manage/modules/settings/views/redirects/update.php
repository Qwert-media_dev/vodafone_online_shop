<?php
/* @var $this RedirectsController */
/* @var $model Redirects */

$this->breadcrumbs=array(
	'Короткие ссылки'=>array('admin'),
	$model->id=>array('update','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить ссылку', 'url'=>array('create')),
	array('label'=>'Список ссылок', 'url'=>array('admin')),
);
?>

<h1>Редактирование ссылки</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>