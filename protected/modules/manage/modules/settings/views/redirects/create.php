<?php
/* @var $this RedirectsController */
/* @var $model Redirects */

$this->breadcrumbs=array(
	'Короткие ссылки'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список ссылок', 'url'=>array('admin')),
);
?>

<h1>Создание ссылки</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>