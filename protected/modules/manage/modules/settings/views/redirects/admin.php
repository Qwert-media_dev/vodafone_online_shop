<?php
/* @var $this RedirectsController */
/* @var $model Redirects */

$this->breadcrumbs=array(
	'Короткие ссылки'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить ссылку', 'url'=>array('create')),
);

?>

<h1>Короткие ссылки</h1>



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'redirects-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
    'enableSorting' => false,
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		'url_from',
		'url_to',
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
			'template' => '{update} {delete}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					)
				),
			)
		),
	),
));
