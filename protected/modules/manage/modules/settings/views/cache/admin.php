<?php
/* @var $this CacheController */

$this->breadcrumbs=array(
	'Сброс кеша',
);
?>
<h1>Сброс кеша</h1>

<?php if( $_GET['state'] == 'warning' ): ?>
	<div class="alert alert-warning" role="alert">На тестовом сервере кеша нет :) </div>
<?php endif; ?>

<?php if( $_GET['state'] == 'success' ): ?>
	<div class="alert alert-success" role="alert">Кеш сброшен.</div>
<?php endif; ?>

<?php if( $_GET['state'] == 'success-404' ): ?>
    <div class="alert alert-success" role="alert">Страница 404 обновленна.</div>
<?php endif; ?>

<p>
    <?php echo CHtml::link('Сбросить кеш', array('clean'), array('class' => 'btn btn-success')); ?>
</p>

<p>
    <a href="/404.<?= rand() ?>" id="test" class="btn btn-success" data-loading-text="Обновляю...">Обновить 404</a>
</p>


<script>
    var $btn;

    $(function(){
       $('#test').click(function(e){
           e.preventDefault();
           $btn = $(this).button('loading')
           $.ajax({
               url: $(this).attr('href'),
               type: 'get',
               success: function(resp) {
                   fail();
               },
               error: function(jqXHR, textStatus, errorThrown) {
                   if(jqXHR.status == 404) {
                       sendContent(jqXHR.responseText);
                   } else {
                       fail();
                   }
               }
           });
       });
    });

    function fail() {
        $btn.button('reset')
        alert('Не получилось обновить 404 страницу.');
    }

    function sendContent(content) {
        $.post('<?= url_to('manage/settings/cache/notfound') ?>', {content: content}, function(resp){
            if(resp.status == 'ok') {
                top.location.href = resp.redirect;
            } else {
                fail();
            }
        }, 'json');
    }
</script>