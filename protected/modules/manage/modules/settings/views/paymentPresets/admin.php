<?php
/* @var $this PaymentPresetsController */
/* @var $model PaymentPresets */

$this->breadcrumbs = array(
    'Payment Presets' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Добавить сумму', 'url' => array('create')),
);

?>

<h1>Список сумм</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'payment-presets-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter'=>$model,
    'columns' => array(
        'preset',
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 300, 'align' => 'right'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
)); ?>
