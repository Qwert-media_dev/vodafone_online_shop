<?php
/* @var $this PaymentPresetsController */
/* @var $model PaymentPresets */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'payment-presets-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'preset', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model, 'preset', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'preset', array('class' => 'help-block')); ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group buttons" style="margin-top:24px">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->