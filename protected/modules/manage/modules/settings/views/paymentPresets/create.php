<?php
/* @var $this PaymentPresetsController */
/* @var $model PaymentPresets */

$this->breadcrumbs=array(
	'Payment Presets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Список сумм', 'url'=>array('admin')),
);
?>

<h1>Добавить сумму</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>