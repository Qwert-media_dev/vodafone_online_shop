<?php
/* @var $this PaymentPresetsController */
/* @var $model PaymentPresets */

$this->breadcrumbs=array(
	'Payment Presets'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Добавить сумму', 'url'=>array('create')),
	array('label'=>'Список сумм', 'url'=>array('admin')),
);
?>

<h1>Редактирование суммы</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>