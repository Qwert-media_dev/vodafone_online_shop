<?php
/* @var $this TranslationsController */
/* @var $model SourceMessage */
/* @var $categories array */

$this->breadcrumbs=array(
	'Переводы',
);
?>
<h1>Переводы</h1>

<?php if( $_GET['state'] == 'success' && $_GET['new'] > 0 ): ?>
    <div class="alert alert-success" role="alert">Добавлено новых фраз: <?= (int)$_GET['new'] ?> шт.</div>
<?php endif; ?>

<?php if( $_GET['state'] == 'success' && $_GET['new'] == 0 ): ?>
    <div class="alert alert-warning" role="alert">Новых фраз не найдено.</div>
<?php endif; ?>


<a href="<?= url_to('manage/settings/translations/scan') ?>" class="btn btn-success">Импорт</a>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'redirects-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'enableSorting' => false,
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'filter'=>$model,
    'columns'=>array(
        array(
            'name' => 'message',
            'type' => 'raw',
            'value' => '$this->grid->owner->details($data)',
            'filter' => false
        ),
        array(
            'name' => 'category',
            'value' => '$data->category',
            'filter' => CHtml::activeDropDownList($model, 'category', $categories, ['class' => 'form-controll', 'empty' => ''])
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));