<?php
/* @var $this TranslationsController */
/* @var $model SourceMessage */
/* @var $class string */

$this->breadcrumbs=array(
	'Переводы'=>array('admin'),
	$model->id=>array('update','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список фраз', 'url'=>array('admin')),
);
?>


<h1>Редактирование фразы</h1>

<?php $this->renderPartial('_form', compact('model', 'class')); ?>
