<?php
/* @var $this TranslationsController */
/* @var $model SourceMessage */
/* @var $form CActiveForm */

?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'redirects-form',
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
	)); ?>

	<h4 class="bg-succes1s" style="padding:15px;">Оригинал: <br><kbd style="padding: 5px; margin: 5px 0; display: block;"><?= CHtml::encode($model->message) ?></kbd></h4>

	<?php foreach($model->messages as $message): ?>
	<div class="form-group">
		<?php echo CHtml::label('Перевод', 'translation_'.$message->language, array('class' => 'control-label')); ?>

		<div class="input-group input-group-lg">
			<span class="input-group-addon" id="sizing-addon-<?= $message->language ?>"><img src="/images/manage/<?= $message->language ?>.png"></span>
			<?php echo CHtml::textField($class.'[translation]['.$message->language.']', $message->translation, array('class'=>'form-control', 'id' => 'translation_'.$message->language, 'aria-describedby' => 'sizing-addon-'.$message->language)); ?>
		</div>

		<?php //echo $form->error($model,'url_from', array('class' => 'help-block')); ?>
	</div>
	<?php endforeach; ?>

	<div class="form-group buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->