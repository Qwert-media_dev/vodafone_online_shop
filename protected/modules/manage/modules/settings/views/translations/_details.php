<?php
/* @var $model SourceMessage */
?>
<?= CHtml::encode($model->message) ?><br>
<a class="btn btn-link" role="button" data-toggle="collapse" href="#collapse_<?= $model->id ?>" aria-expanded="false" aria-controls="collapse_<?= $model->id ?>">
	Переводы
</a>
<div class="collapse" id="collapse_<?= $model->id ?>">
	<div class="well">
		<dl class="dl-horizontal details">
			<?php foreach($model->messages as $message): ?>
			<dt><?= CHtml::encode($message->language) ?></dt>
			<dd><?= CHtml::encode($message->translation) ?></dd>
			<?php endforeach; ?>
		</dl>
	</div>
</div>