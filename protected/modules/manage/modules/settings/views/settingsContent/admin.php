<?php
/* @var $this SettingsContentController */
/* @var $model SettingsContent */

$this->breadcrumbs=array(
	'Settings Contents'=>array('admin'),
	'Manage',
);

?>

<h1>Контент ручных настроек</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'settings-content-grid',
	'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
        array(
            'name' => 'Язык',
            'value' => '$data->lang->long_title'
        ),
		//'content',
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 300, 'align' => 'right'),
			'template' => '{update}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
			)
		),
	),
));

