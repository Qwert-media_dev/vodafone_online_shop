<?php
/* @var $this SettingsContentController */
/* @var $model SettingsContent */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-content-form',
    'action' => array('save'),
	'enableAjaxValidation'=>false,
));
echo $form->hiddenField($model, 'id');
?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="form-group group-content">
		<?php echo $form->labelEx($model,'content', array('class' => 'control-label')); ?>
		<?php echo $form->hiddenField($model,'content', array('id' => 'content')); ?>

        <div id="contentarea" class="container inner-content-container editor">
            <?php echo $model->content; ?>
        </div>

        <?php echo $form->error($model,'content', array('class' => 'help-block')); ?>
	</div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php $this->widget('ContentBuilderWidget', array('selector' => '#contentarea', 'fileSelectUrl' => url_to('manage/files/export') )); ?>

<script>
    var inProgress = false;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';
    var saveImageURL = '<?= url_to('manage/page/pages/saveimage') ?>';
    var $btn;

    $(function(){
        $('#settings-content-form').submit(function (e) {
            e.preventDefault();
            var $form = $(this);

            if( !inProgress ) {
                inProgress = true;
                showLoader();
                $btn = $('.btn-save').button('loading');

                // reset errors and error messages
                $('.has-error').removeClass('has-error');
                $('a.error').removeClass('error');
                $('.help-block').each(function () {
                    $(this).text('');
                });

                saveImages(function () {
                    populateBody();
                    savePage($form);
                });
            }
        });
    });


    function savePage($form) {
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: $form.attr('method'),
            success: function(resp) {
                if( resp.status == 'ok' ) {
                    top.location.href = resp.redirect;
                } else {
                    $btn.button('reset');
                    for(var i in resp.errors) {
                        $('.group-'+i).find('.help-block').text(resp.errors[i]);
                        $('.group-'+i).addClass('has-error');
                    }
                }
                hideLoader();
                inProgress = false;
            },
            error: function() {
                hideLoader();
                inProgress = false;
            }
        });
    }

    function populateBody() {
        $('#content').val( $('#contentarea').data('contentbuilder').html() );
    }


    /**
     * Сохраняем бинарники на сервер и вызываем колбек дальше по цепочке
     *
     * @param callback callable function
     */
    function saveImages(callback) {
        $("#contentarea").saveimages({
            handler: saveImageURL,
            onComplete: function () {
                callback();
            }
        });
        $("#contentarea").data('saveimages').save();
    }
</script>
