<?php
/* @var $this SettingsContentController */
/* @var $model SettingsContent */

$this->breadcrumbs=array(
	'Settings Contents'=>array('admin'),
	'Update',
);

$this->menu=array(
	array('label'=>'Языковые версии', 'url'=>array('admin')),
);
?>

<h1>Редактирование <?php echo $model->lang->title; ?> версии</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>