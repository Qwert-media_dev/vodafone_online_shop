<?php
/* @var $this LogsController */
/* @var $model Logs */

$this->breadcrumbs=array(
	'Логи'=>array('admin'),
	'Список',
);

?>

<h1>Список действий</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'logs-grid',
	'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
    'enableSorting' => false,
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		'email',
		'message',
        array(
            'name' => 'ip',
            'value' => 'long2ip($data->ip)'
        ),
        array(
            'name' => 'created_at',
            'htmlOptions' => array('nowrap' => 'nowrap')
        ),
	),
));
