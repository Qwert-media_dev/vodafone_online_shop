<?php
/* @var $this SettingsParamsController */
/* @var $model SettingsParams */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'settings-params-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));

switch($model->type) {
    case 'input':
        $field = 'textField';
        break;
    case 'textarea':
        $field = 'textArea';
        break;
    default:
        $field = 'textField';
}
?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php foreach($this->languages as $language): ?>

	<div class="form-group <?php if(isset($errors['value_'.$language->code])) echo ' has-error'; ?>">
		<?php echo $form->labelEx($model,'value_'.$language->code, array('class' => 'control-label')); ?>
		<?php echo $form->$field($model,'value_'.$language->code, array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'value_'.$language->code, array('class' => 'help-block')); ?>
	</div>

    <?php endforeach; ?>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->