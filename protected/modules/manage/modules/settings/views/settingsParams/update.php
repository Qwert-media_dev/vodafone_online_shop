<?php
/* @var $this SettingsParamsController */
/* @var $model SettingsParams */

$this->breadcrumbs=array(
	'Различные настройки'=>array('admin'),
	$model->title=>array('update','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Список параметров', 'url'=>array('admin')),
);
?>

<h1>Редактирование "<?php echo $model->title; ?>"</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>