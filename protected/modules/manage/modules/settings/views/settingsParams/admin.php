<?php
/* @var $this SettingsParamsController */
/* @var $model SettingsParams */

$this->breadcrumbs=array(
	'Различные настройки'=>array('admin'),
	'Список',
);
?>

<h1>Список параметров</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'settings-params-grid',
	'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
    'enableSorting' => false,
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
			'template' => '{update} {delete}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					)
				),
			)
		),
	),
));

