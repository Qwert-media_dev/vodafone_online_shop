<?php
/* @var $this ConsultController */
/* @var $model ConsultSettings */
/* @var $multiErrors array */
/* @var $i18nModel ConsultSettingsI18n[] */


$this->breadcrumbs = array(
    'Consults' => array('admin'),
    $model->id => array('update', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Список параметров', 'url' => array('admin')),
);
?>

<h1>Редактирование (<?= $model->type ?>)</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'langs', 'errors')); ?>