<?php
/* @var $this ConsultController */
/* @var $model ConsultSettings */

$this->breadcrumbs = array(
    'Consults' => array('index'),
    'Manage',
);

?>

    <h1>Настройки консультанта</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'video-consult-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'columns' => array(
        array(
            'name' => 'type',
            'value' => '$data->type'
        ),
        array(
            'name' => 'status',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),

            )
        ),
    ),
));

