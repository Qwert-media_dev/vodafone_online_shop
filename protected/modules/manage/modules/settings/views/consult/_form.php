<?php
/* @var $this ConsultController */
/* @var $model ConsultSettings */
/* @var $form CActiveForm */
/* @var $multiErrors array */
/* @var $i18nModel ConsultSettingsI18n[] */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'video-consult-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
echo CHtml::hiddenField($class.'[dummy]', 1);
?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-9">
            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach($this->languages as $lang): ?>
                        <li>
                            <a href="#<?=$lang->code?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
                                <?=$lang->title?>
                                <?php if( count($multiErrors[$lang->code]) ): ?>
                                    <span class="label label-danger"> <span class="glyphicon glyphicon-warning-sign"></span> Ошибка</span>
                                <?php endif; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors();?>

                            <br/>

                            <div class="row">
                                <div class="col-md-10">
                                    <div class="form-group<?php if(isset($errors['url'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'url', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'url',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][url]')); ?>
                                        <?php echo $form->error($i18nModel[$lang->id],'url', array('class' => 'help-block')); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save')); ?>
            </div>
        </div>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->