<?php

class ConsultController extends ManageController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'update'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = array();
        foreach ($model->i18n as $item) {
            $i18nModel[$item->lang_id] = $item;
        }

        if (isset($_POST[$class])) {

            $form = $_POST[$class];
            $i18n = $_POST[$class.'I18n'];
            $model->attributes = $form;
            $validate = $model->validate();

            $statuses = array();
            foreach($this->languages as $lang) {
                $currentStatus = (int)$i18n[$lang->id]['status'];
                $statuses[$lang->code] = $currentStatus;
                $i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;

                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->url = $i18n[$lang->id]['url'];
                $i18nModel[$lang->id]->status = $currentStatus;

                $validateMulti = $i18nModel[$lang->id]->validate();
                $validate = $validate && $validateMulti;

                if( !$validateMulti ) {
                    $multiErrors[ $lang->code ][] = $i18nModel[$lang->id]->getErrors();
                }
            }

            if ($validate) {
                $model->statuses = CJSON::encode($statuses);
                $model->save();

                foreach($this->languages as $lang) {
                    $i18nModel[$lang->id]->save();
                }

                $title = '';
                LogRegistry::instance()->log(LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $title);
                $this->redirect(array('admin'));
            } else {
                $errors = array();
                if(sizeof($model->getErrors())) {
                    foreach($model->getErrors() as $field => $data) {
                        if(sizeof($data))
                            $errors[$field] = $data[0];
                    }
                }

                $langs = array();
                foreach( $this->languages as $lang ) {
                    if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
                        foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
                            if(sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);

                $output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
            }
        }

        $this->render('update', compact('model', 'class', 'i18nModel', 'multiErrors', 'langs', 'errors'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new ConsultSettings('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['ConsultSettings']))
            $model->attributes = $_GET['ConsultSettings'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ConsultSettings the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = ConsultSettings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


}
