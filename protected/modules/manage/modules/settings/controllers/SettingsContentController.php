<?php

class SettingsContentController extends ManageController
{

	protected static $class = 'SettingsContent';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'update', 'save'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$this->render('update', compact('model'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new SettingsContent('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SettingsContent']))
			$model->attributes=$_GET['SettingsContent'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SettingsContent the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SettingsContent::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}



	public function actionSave() {
		$output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

		if(isset($_POST[self::$class]))
		{
			$form = $_POST[self::$class];

			if( array_key_exists('id', $form) && (int)$form['id'] > 0 ) {
				$model = SettingsContent::model()->findByPk($form['id']);
			} else {
				throw new CHttpException(500, Yii::t('app', 'Нет возможности создавать новые записи.'));
			}

			$model->content = $form['content'];

			if($validate = $model->validate()) {

				$model->save();

				LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $model->lang->title );

				$params = array('id' => $model->id);
				$redirect = url_to('manage/settings/settingsContent/update', $params);
				if( user()->show_index ) {
					$redirect = url_to('manage/settings/settingsContent/admin');
				}

				$output = array('status' => 'ok', 'redirect' => $redirect);
			} else {
				$errors = array();
				if(sizeof($model->getErrors())) {
					foreach($model->getErrors() as $field => $data) {
						if(sizeof($data))
							$errors[$field] = $data[0];
					}
				}

				$output = array('status' => 'fail', 'errors' => $errors);
			}
		}

		echo CJSON::encode($output);
	}

}
