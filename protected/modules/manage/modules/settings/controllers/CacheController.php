<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/20/2015
 * Time: 07:33 PM
 */
class CacheController extends ManageController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'clean', 'notfound'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionAdmin()
	{
		$this->render('admin');
	}


	public function actionClean() {
		if( in_array($_SERVER['HTTP_HOST'], ['v.mts-ukraine.com', 'vodafone.dev', 'localhost']) )  {
			user()->setFlash('warning', 'На тестовом сервере кеша нет :) ');
			LogRegistry::instance()->log( LogRegistry::METHOD_RESET, 'ResetCache', 0, '' );
			$this->redirect(array('admin', 'state' => 'warning'));
		}

		foreach( param('cache') as $path ) {
			if (is_dir($path)) {
				$objects = scandir($path);
				foreach ($objects as $object) {
					if ($object != '.' && $object != '..') {
						$objectPath = "{$path}/{$object}";
						FS::rmDir($objectPath);
					}
				}
			}
		}
		user()->setFlash('success', 'Кеш сброшен.');
		LogRegistry::instance()->log( LogRegistry::METHOD_RESET, 'ResetCache', 0, '' );
		$this->redirect(array('admin', 'state' => 'success'));
	}


	public function actionNotfound() {
		$resp = ['status' => 'fail'];

		if( array_key_exists('content', $_POST) ) {
			$path = realpath( Yii::getPathOfAlias('application').'/..' ).'/404.html';
			file_put_contents($path, $_POST['content']);
			$resp = ['status' => 'ok', 'redirect' => url_to('manage/settings/cache/admin', ['state' => 'success-404'])];
		}

		echo CJSON::encode($resp);
	}
}