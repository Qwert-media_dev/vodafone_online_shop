<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/21/2015
 * Time: 08:20 PM
 */
class TranslationsController extends ManageController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin', 'scan', 'delete', 'update'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAdmin() {
		$model=new SourceMessage('search');
		$class = get_class($model);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET[$class]))
			$model->attributes=$_GET[$class];
		$categories = SourceMessage::model()->getCategories();
		$this->render('admin', compact('model', 'categories'));
	}


	public function actionScan() {
		$new = 0;
		$messages = $this->scanFiles();
		$messages = $this->normalizeDict($messages);
		$vocabulary = $this->getVocabulary();
		foreach( $messages as $category => $phrases ) {
			foreach($phrases as $message) {
				if( $message == '' ) continue;
				$source = SourceMessage::model()->findByAttributes(['category' => $category, 'message' => $message]);
				if( is_null($source) ) {
					$source = new SourceMessage();
					$source->category = $category;
					$source->message = $message;
					$source->save(false);
					$new++;
				}
				foreach($this->languages as $language) {
					$translated = Message::model()->findByAttributes(['id' => $source->id, 'language' => $language->code]);
					if( is_null($translated) ) {
						$translation = isset($vocabulary[$category][$language->code]) ? $vocabulary[$category][$language->code][$message] : '';
						$translated = new Message();
						$translated->id = $source->id;
						$translated->language = $language->code;
						$translated->translation = $translation;
						$translated->save(false);
					}
				}
			}
		}

		LogRegistry::instance()->log( LogRegistry::METHOD_IMPORT, 'SourceMessage', 0, $new );
		$this->redirect(['admin', 'state' => 'success', 'new' => $new]);
	}


	public function actionDelete($id) {
		$model = $this->loadModel($id);
		$title = sprintf('%s", Категория="%s', $model->message, $model->category);
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $title );
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionUpdate($id) {
		$model = $this->loadModel($id);
		$class = get_class($model);

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			foreach($form['translation'] as $language => $translation) {
				Message::model()->updateAll(['translation' => $translation], 'id=:id AND language=:language', [':id' => $model->id, ':language' => $language]);
			}

			LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $model->message );
			$this->redirect(array('admin'));
		}

		$this->render('update', compact('model', 'class'));
	}

	protected function details(SourceMessage $model) {
		$this->renderPartial('_details', compact('model'));
	}

	/**
	 * @param $id integer
	 * @return SourceMessage
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SourceMessage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	protected  function getVocabulary() {
		$voc = [];
		foreach($this->languages as $lang) {
			$files=CFileHelper::findFiles(YiiBase::getPathOfAlias('application.messages.'.$lang->code), ['fileTypes' => ['php']]);
			foreach($files as $file) {
				$category = substr(basename($file), 0, -4);
				$voc[$category][$lang->code] = require($file);
			}
		}
		return $voc;
	}

	protected function scanFiles() {
		$options = [
			'fileTypes' => ['php'],
			'exclude' => [
				'.svn',
				'.gitignore',
				'yiilite.php',
				'yiit.php',
				'/i18n/data',
				'/messages',
				'/modules',
				'/vendors',
				'/web/js',
			]
		];
		$files=CFileHelper::findFiles(YiiBase::getPathOfAlias('application'),$options);

		$translator='Yii::t';

		$messages = array();
		foreach($files as $file) {
			$messages = array_merge_recursive($messages, $this->extractMessages($file,$translator));
		}
		return $messages;
	}

	protected function normalizeDict($dict) {
		$temp = [];

		foreach ($dict as $category => $items) {
			$items = array_unique($items);
			sort($items);
			$temp[$category ] = $items;
		}

		return $temp;
	}

	protected function extractMessages($fileName,$translator)
	{
		$subject=file_get_contents($fileName);
		$messages=array();
		if(!is_array($translator))
			$translator=array($translator);

		foreach ($translator as $currentTranslator)
		{
			$n=preg_match_all('/\b'.$currentTranslator.'\s*\(\s*(\'[\w.]*?(?<!\.)\'|"[\w.]*?(?<!\.)")\s*,\s*(\'.*?(?<!\\\\)\'|".*?(?<!\\\\)")\s*[,\)]/s',$subject,$matches,PREG_SET_ORDER);

			for($i=0;$i<$n;++$i)
			{
				if(($pos=strpos($matches[$i][1],'.'))!==false)
					$category=substr($matches[$i][1],$pos+1,-1);
				else
					$category=substr($matches[$i][1],1,-1);
				$message=$matches[$i][2];
				$messages[$category][]=eval("return $message;");  // use eval to eliminate quote escape
			}
		}
		return $messages;
	}

}