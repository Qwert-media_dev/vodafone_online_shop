<?php

class SettingsLandingController extends ManageController
{


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
				'accessControl', // perform access control for CRUD operations
				'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(

				array('allow', // allow admin user to perform 'admin' and 'delete' actions
						'actions' => array('admin'),
						'users' => array('@'),
				),
				array('deny',  // deny all users
						'users' => array('*'),
				),
		);
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model = SettingsLanding::model()->find();
		if ($model === null)
			throw new CHttpException(500, 'Нет данных настроек');

		$class = get_class($model);
		if( array_key_exists($class, $_POST) ) {
			$model->id1 = (int)$_POST[$class]['id1'];
			$model->id2 = (int)$_POST[$class]['id2'];
			$model->save();
			user()->setFlash('success', 'Параметры сохранены.');
			$this->redirect(array('admin'));
		}

		$pages = Pages::model()->getFirstLevel();
		$this->render('admin', compact('model', 'pages'));
	}

}
