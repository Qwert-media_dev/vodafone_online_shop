<?php

class ImportPointsController extends ManageController
{


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'import'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

//	public function beforeAction($action) {
//		$this->folder = Yii::getPathOfAlias('application.data').'/';
//        parent::beforeAction($action);
//        return true;
//    }

    public function actionAdmin()
    {
        $this->render('admin');
    }


    public function actionImport()
    {
        $importer = new NetworkImporter();
        if($importer->run()) {
            Yii::app()->admin->setFlash('success', 'Файлы обновлены.');
            LogRegistry::instance()->log(LogRegistry::METHOD_IMPORT, 'VirtualPoints', 0, '');
            $this->redirect(array('admin', 'state' => 'success'));
        } else {
            Yii::app()->admin->setFlash('warning', 'Нет списка файлов для импорта!');
            $this->redirect(array('admin', 'state' => 'warning'));
        }
    }
}