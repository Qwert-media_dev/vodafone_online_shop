<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/7/2015
 * Time: 12:09 PM
 */
class OrdersModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'orders.models.*',
			'oreders.components.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			$this->layoutPath = Yii::getPathOfAlias('manage.views.layouts');
			$this->layout = 'вы';
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}