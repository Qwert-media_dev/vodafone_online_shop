<?php
/* @var $this DialogController */
/* @var $model News */
/* @var $class string */
/* @var $i18nModel NewsI18n[] */
/* @var $codes array */
/* @var $pages Pages[] */

$this->breadcrumbs=array(
	'Диалоги'=>array('admin'),
	$model->id=>array('update','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить диалог', 'url'=>array('create')),
	array('label'=>'Список диалогов', 'url'=>array('admin')),
	//array('label'=>'Архив', 'url'=>array('archive')),
    array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'codes', 'pages')); ?>