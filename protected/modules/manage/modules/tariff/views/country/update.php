<?php
/* @var $this CountryController */
/* @var $model Country */
/* @var $class string */
/* @var $i18nModel CountryI18n[] */
/* @var $multiErrors array */
/* @var $networks Network[] */

$this->breadcrumbs = array(
    'Страны' => array('admin'),
    $model->id => array('update', 'id' => $model->id),
    'Редактирование',
);

$this->menu = array(
    array('label' => 'Добавить страну', 'url' => array('create')),
    array('label' => 'Список стран', 'url' => array('admin')),
);
?>

    <h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'networks')); ?>