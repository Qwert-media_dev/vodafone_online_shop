<?php
/* @var $this CountryController */
/* @var $model Country */
/* @var $class string */

$this->breadcrumbs = array(
    'Страны' => array('admiin'),
    'Управление',
);

$this->menu = array(
    array('label' => 'Добавить страну', 'url' => array('create')),
);
?>

    <h1>Список стран</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'country-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'enableSorting' => false,
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'filter' => $model,
    'columns' => array(
        //'i18n.0.title',
        array(
            'name' => 'i18n.0.title',
            'type' => 'raw',
            'filter' => CHtml::textField($class . '[title]', $_GET[$class]['title'], array('class' => 'form-control')),
        ),
        array(
            'name' => 'code',
            'type' => 'raw',
            'filter' => CHtml::textField($class . '[code]', $_GET[$class]['code'], array('class' => 'form-control')),
        ),
        array(
            'name' => 'network',
            'type' => 'raw',
            'value' => 'param("network")[$data->network]',
            'filter' => CHtml::dropDownList($class . '[network]', $_GET[$class]['network'], param("network"), array('empty' => ' ', 'class' => 'form-control')),
        ),
        array(
            'name' => 'is_europe',
            'type' => 'raw',
            'value' => 'param("europe")[$data->is_europe]',
            'filter' => CHtml::dropDownList($class . '[is_europe]', $_GET[$class]['is_europe'], param("europe"), array('empty' => ' ', 'class' => 'form-control')),
        ),
        array(
            'name' => 'is_popular',
            'type' => 'raw',
            'value' => 'param("popular")[$data->is_popular]',
            'filter' => CHtml::dropDownList($class . '[is_popular]', $_GET[$class]['is_popular'], param("popular"), array('empty' => ' ', 'class' => 'form-control')),
        ),
        array(
            'header' => 'Видимость (Препейд/Контракт/Бизнес)',
            'value' => '$this->grid->owner->displayVisibility($data)'
        ),
        /*
        array(
            'header' => 'Сеть (Бизнес/Контракт/Препейд)',
            'value' => '$this->grid->owner->displayNetworks($data)'
        ),
        */
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 300, 'align' => 'right'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
