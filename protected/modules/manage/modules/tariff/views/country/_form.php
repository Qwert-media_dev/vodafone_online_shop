<?php
/* @var $this CountryController */
/* @var $model Country */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel CountryI18n[] */
/* @var $multiErrors array */
/* @var $networks Network[] */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'country-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-9">
            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach($this->languages as $lang): ?>
                        <li>
                            <a href="#<?=$lang->code?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
                                <?=$lang->title?>
                                <?php if( count($multiErrors[$lang->code]) ): ?>
                                    <span class="label label-danger"> <span class="glyphicon glyphicon-warning-sign"></span> Ошибка</span>
                                <?php endif; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors();?>

                            <br/>

                            <div class="form-group<?php if(isset($errors['title'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
                                <?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]')); ?>
                                <?php echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                            </div>

                        </div>
                    <?php endforeach; ?>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group <?php echo  $this->error($model, 'code') ?>">
                <?php echo $form->labelEx($model,'code', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'code', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'code', array('class' => 'help-block')); ?>
            </div>

            <?php /*
            <div class="form-group">
                <?php echo $form->labelEx($model,'network_id', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'network_id', CHtml::listData($networks, 'id', 'i18n.0.title'), array('class' => 'form-control', 'empty' => 'Выберите сеть')); ?>
                <?php echo $form->error($model,'network_id', array('class' => 'help-block')); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model,'network_id_contract', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'network_id_contract', CHtml::listData($networks, 'id', 'i18n.0.title'), array('class' => 'form-control', 'empty' => 'Выберите сеть')); ?>
                <?php echo $form->error($model,'network_id_contract', array('class' => 'help-block')); ?>
            </div>

            <div class="form-group">
                <?php echo $form->labelEx($model,'network_id_prepaid', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'network_id_prepaid', CHtml::listData($networks, 'id', 'i18n.0.title'), array('class' => 'form-control', 'empty' => 'Выберите сеть')); ?>
                <?php echo $form->error($model,'network_id_prepaid', array('class' => 'help-block')); ?>
            </div>
            */ ?>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'network'));
                echo $form->checkBox($model,'network', array('id' => 'network', 'value' => 1));
                echo $model->getAttributeLabel('network');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_europe'));
                echo $form->checkBox($model,'is_europe', array('id' => 'is_europe', 'value' => 1));
                echo $model->getAttributeLabel('is_europe');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_popular'));
                echo $form->checkBox($model,'is_popular', array('id' => 'is_popular', 'value' => 1));
                echo $model->getAttributeLabel('is_popular');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="well">
                <label for="">Видимость</label>
                <div class="checkbox">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'private_visibility'));
                    echo $form->checkBox($model,'private_visibility', array('id' => 'private_visibility', 'value' => 1));
                    echo $model->getAttributeLabel('private_visibility');
                    echo CHtml::closeTag('label');
                    ?>
                </div>

                <div class="checkbox">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'contract_visibility'));
                    echo $form->checkBox($model,'contract_visibility', array('id' => 'contract_visibility', 'value' => 1));
                    echo $model->getAttributeLabel('contract_visibility');
                    echo CHtml::closeTag('label');
                    ?>
                </div>

                <div class="checkbox">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'business_visibility'));
                    echo $form->checkBox($model,'business_visibility', array('id' => 'business_visibility', 'value' => 1));
                    echo $model->getAttributeLabel('business_visibility');
                    echo CHtml::closeTag('label');
                    ?>
                </div>
            </div>
            
            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->