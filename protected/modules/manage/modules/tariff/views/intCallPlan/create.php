<?php
/* @var $this IntCallPlanController */
/* @var $model IntCallPlan */
/* @var $countries Country[] */

$this->breadcrumbs=array(
	'Междунарожные звонки'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список тарифов', 'url'=>array('admin')),
);
?>

<h1>Добавить стоимость</h1>

<?php $this->renderPartial('_form', compact('model', 'countries')); ?>