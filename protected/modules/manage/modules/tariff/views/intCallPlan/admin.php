<?php
/* @var $this IntCallPlanController */
/* @var $model IntCallPlan */
/* @var $class string */
/* @var $countries Country[] */

$this->breadcrumbs = array(
    'Международные звонки' => array('index'),
    'Список',
);

$this->menu = array(
    array('label' => 'Добавить стоимость', 'url' => array('create')),
);

?>

    <h1>Международные тарифы</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'int-call-plan-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'filter'=>$model,
    'columns' => array(
        array(
            'header' => 'Страна',
            'value' => '$data->country->i18n[0]->title',
            'type' => 'raw',
            'filter' => CHtml::dropDownList($class . '[country_id]', $_GET[$class]['country_id'], CHtml::listData($countries, 'id', 'i18n.0.title'), array('class' => 'form-control', 'empty' => '')),
        ),
        array(
            'name' => 'type',
            'type' => 'raw',
            'filter' => CHtml::dropDownList($class . '[type]', $_GET[$class]['type'], param("plan_type"), array('class' => 'form-control', 'empty' => '')),
            'value' => 'param("plan_type")[$data->type]'
        ),
		[
            'header' => 'Тариф (Укр/Рус/Анг)',
            'type' => 'raw',
            'value' => '$data->price_uk."<br>".$data->price_ru."<br>".$data->price_en'
        ],
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 300, 'align' => 'right'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
