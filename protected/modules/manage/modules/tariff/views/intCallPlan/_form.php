<?php
/* @var $this IntCallPlanController */
/* @var $model IntCallPlan */
/* @var $form CActiveForm */
/* @var $countries Country[] */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'int-call-plan-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group <?php echo  $this->error($model, 'price_uk') ?>">
                        <?php echo $form->labelEx($model,'price_uk', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'price_uk', array('class'=>'form-control')); ?>
                        <?php echo $form->error($model,'price_uk', array('class' => 'help-block')); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php echo  $this->error($model, 'price_ru') ?>">
                        <?php echo $form->labelEx($model,'price_ru', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'price_ru', array('class'=>'form-control')); ?>
                        <?php echo $form->error($model,'price_ru', array('class' => 'help-block')); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group <?php echo  $this->error($model, 'price_en') ?>">
                        <?php echo $form->labelEx($model,'price_en', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'price_en', array('class'=>'form-control')); ?>
                        <?php echo $form->error($model,'price_en', array('class' => 'help-block')); ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div class="form-group <?php echo  $this->error($model, 'country_id') ?>">
                <?php echo $form->labelEx($model,'country_id', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'country_id', CHtml::listData($countries, 'id', 'i18n.0.title'), array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'country_id', array('class' => 'help-block')); ?>
            </div>

            <div class="form-group <?php echo  $this->error($model, 'type') ?>">
                <?php echo $form->labelEx($model,'type', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'type', param('plan_type'), array('class'=>'form-control')); ?>
                <?php echo $form->error($model,'type', array('class' => 'help-block')); ?>
            </div>

            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>



<?php $this->endWidget(); ?>

</div><!-- form -->