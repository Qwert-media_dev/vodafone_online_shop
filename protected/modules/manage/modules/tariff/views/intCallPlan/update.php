<?php
/* @var $this IntCallPlanController */
/* @var $model IntCallPlan */
/* @var $countries Country[] */

$this->breadcrumbs=array(
	'Международные тарифы'=>array('admin'),
	$model->id=>array('edit','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить стоимость', 'url'=>array('create')),
	array('label'=>'Список тарифов', 'url'=>array('admin')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model','countries')); ?>