<?php
/* @var $this NetworkController */
/* @var $model Network */
/* @var $class string */
/* @var $i18nModel NetworkI18n[] */
/* @var $multiErrors array */

$this->breadcrumbs=array(
	'Сети'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список сетей', 'url'=>array('admin')),
);
?>

<h1>Добавить сеть</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors')); ?>