<?php
/* @var $this NetworkController */
/* @var $model Network */

$this->breadcrumbs=array(
	'Сети'=>array('admin'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Добавить сеть', 'url'=>array('create')),
);
?>

<h1>Список сетей</h1>


<?php
Yii::import('zii.widgets.grid.CGridView');
$this->widget('PointsGridView', array(
	'id'=>'pos-city-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		'title',
		array(
			'class'=>'CButtonColumn',
			'htmlOptions' => array('width' => 300, 'align' => 'right'),
			'template' => '{update} {delete}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					)
				),
			)
		),
	),
));
