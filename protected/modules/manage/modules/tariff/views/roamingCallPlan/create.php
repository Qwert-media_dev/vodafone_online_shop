<?php
/* @var $this RoamingCallPlanController */
/* @var $model RoamingCallPlan */
/* @var $countries Country[] */
/* @var $networks Network[] */

$this->breadcrumbs = array(
    'Roaming Call Plans' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'Список сетей', 'url' => array('admin')),
);
?>

<h1>Создание</h1>

<?php $this->renderPartial('_form', compact('model', 'countries', 'networks')); ?>