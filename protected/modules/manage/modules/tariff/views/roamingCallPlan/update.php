<?php
/* @var $this RoamingCallPlanController */
/* @var $model RoamingCallPlan */
/* @var $countries Country[] */
/* @var $networks Network[] */

$this->breadcrumbs = array(
    'Roaming Call Plans' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Добавить сеть', 'url' => array('create')),
    array('label' => 'Список сетей', 'url' => array('admin')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'countries', 'networks')); ?>