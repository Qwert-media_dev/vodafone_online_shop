<?php
/* @var $this RoamingCallPlanController */
/* @var $model RoamingCallPlan */

$this->breadcrumbs = array(
    'Roaming Call Plans' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'Добавить сеть', 'url' => array('create')),
);

?>

<h1>Сети в роуминге</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'roaming-call-plan-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'enableSorting' => false,
    //'filter' => $model,
    'columns' => array(
        'country.i18n.0.title',
        'network.i18n.0.title',
        array(
            'name' => 'type',
            'type' => 'raw',
            'value' => 'param("plan_type")[$data->type]'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 300, 'align' => 'right'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
