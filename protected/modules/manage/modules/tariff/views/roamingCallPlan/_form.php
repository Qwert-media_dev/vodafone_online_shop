<?php
/* @var $this RoamingCallPlanController */
/* @var $model RoamingCallPlan */
/* @var $form CActiveForm */
/* @var $countries Country[] */
/* @var $networks Network[] */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'roaming-call-plan-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>
    
    <div class="form-group <?php if (isset($errors['country_id'])) echo ' has-error'; ?>">
        <?php echo $form->labelEx($model, 'country_id', array('class' => 'control-label')); ?>
        <?php echo $form->dropDownList($model, 'country_id', CHtml::listData($countries, 'id', 'i18n.0.title'), array('class' => 'form-control', 'style' => 'width:250px;', 'empty' => '-Выберите страну-')); ?>
        <?php echo $form->error($model, 'country_id', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group <?php if (isset($errors['network_id'])) echo ' has-error'; ?>">
        <?php echo $form->labelEx($model, 'network_id', array('class' => 'control-label')); ?>
        <?php echo $form->dropDownList($model, 'network_id', CHtml::listData($networks, 'id', 'i18n.0.title'), array('class' => 'form-control', 'style' => 'width:250px;', 'empty' => '-Выберите сеть-')); ?>
        <?php echo $form->error($model, 'network_id', array('class' => 'help-block')); ?>
    </div>
    
    <div class="form-group <?php echo  $this->error($model, 'type') ?>">
        <?php echo $form->labelEx($model,'type', array('class' => 'control-label')); ?>
        <?php echo $form->dropDownList($model,'type', param('plan_type'), array('class'=>'form-control', 'style' => 'width:250px;')); ?>
        <?php echo $form->error($model,'type', array('class' => 'help-block')); ?>
    </div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->