<?php

class NetworkController extends ManageController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Network();
        $class = get_class($model);
        $i18nModel = array();
        foreach ($this->languages as $lang) {
            $i18nModel[$lang->id] = new NetworkI18n();
        }

        if (isset($_POST[$class])) {
            $form = $_POST[$class];
            $i18n = $_POST[$class . 'I18n'];

            $model->sort = (int)$form['sort'];
            $validate = $model->validate();

            $title = '';
            foreach ($this->languages as $lang) {
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];

                if ($lang->id == param('defaultLangId')) {
                    $title = $i18n[$lang->id]['title'];
                }

                $validateMulti = $i18nModel[$lang->id]->validate();
                $validate = $validate && $validateMulti;

                if (!$validateMulti) {
                    $multiErrors[$lang->code][] = $i18nModel[$lang->id]->getErrors();
                }
            }

            $model->title = $title;

            if ($validate) {

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->network_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                LogRegistry::instance()->log(LogRegistry::METHOD_CREATE, get_class($model), $model->id, $title);
                user()->setFlash('success', 'Сохранено');

                $redirect = array('update', 'id' => $model->id);
                if (user()->show_index) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            }
        }

        $this->render('create', compact('model', 'class', 'i18nModel', 'multiErrors'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = array();
        foreach ($model->i18n as $item) {
            $i18nModel[$item->lang_id] = $item;
        }

        if (isset($_POST[$class])) {
            $form = $_POST[$class];
            $i18n = $_POST[$class . 'I18n'];

            $model->sort = (int)$form['sort'];
            $validate = $model->validate();

            $title = '';
            foreach ($this->languages as $lang) {
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];

                if ($lang->id == param('defaultLangId')) {
                    $title = $i18n[$lang->id]['title'];
                }

                $validateMulti = $i18nModel[$lang->id]->validate();
                $validate = $validate && $validateMulti;

                if (!$validateMulti) {
                    $multiErrors[$lang->code][] = $i18nModel[$lang->id]->getErrors();
                }
            }

            $model->title = $title;
            if ($validate) {

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->network_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                LogRegistry::instance()->log(LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $title);
                user()->setFlash('success', 'Сохранено');

                $redirect = array('update', 'id' => $model->id);
                if (user()->show_index) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            }
        }

        $this->render('update', compact('model', 'class', 'i18nModel', 'multiErrors'));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        LogRegistry::instance()->log(LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->i18n[0]->title);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Network('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Network']))
            $model->attributes = $_GET['Network'];

        $this->render('admin', compact('model'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return PosCity the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Network::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


}
