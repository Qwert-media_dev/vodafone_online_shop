<?php

class CountryController extends ManageController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Country;
        $class = get_class($model);
        $i18nModel = array();
        foreach ($this->languages as $lang) {
            $i18nModel[$lang->id] = new CountryI18n();
        }

        if (isset($_POST[$class])) {
            $form = $_POST[$class];
            $i18n = $_POST[$class . 'I18n'];

            $model->code = $form['code'];
            $model->network = (int)$form['network'];
            $model->is_europe = (int)$form['is_europe'];
            $model->is_popular = (int)$form['is_popular'];
            $model->network_id = (int)$form['network_id'];
            $model->network_id_contract = (int)$form['network_id_contract'];
            $model->network_id_prepaid = (int)$form['network_id_prepaid'];
            $validate = $model->validate();

            $title = '';
            foreach ($this->languages as $lang) {
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];

                if ($lang->id == param('defaultLangId')) {
                    $title = $i18n[$lang->id]['title'];
                }

                $validateMulti = $i18nModel[$lang->id]->validate();
                $validate = $validate && $validateMulti;

                if (!$validateMulti) {
                    $multiErrors[$lang->code][] = $i18nModel[$lang->id]->getErrors();
                }
            }

            if ($validate) {

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->country_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                LogRegistry::instance()->log(LogRegistry::METHOD_CREATE, get_class($model), $model->id, $title);
                user()->setFlash('success', 'Сохранено');

                $redirect = array('update', 'id' => $model->id);
                if (user()->show_index) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            }
        }

        $networks = Network::model()->getItems( param('defaultLangId') );
        $this->render('create', compact('model', 'class', 'i18nModel', 'multiErrors', 'networks'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = array();
        foreach ($model->i18n as $item) {
            $i18nModel[$item->lang_id] = $item;
        }

        if (isset($_POST[$class])) {
            $form = $_POST[$class];
            $i18n = $_POST[$class . 'I18n'];

            $model->code = $form['code'];
            $model->network = (int)$form['network'];
            $model->is_europe = (int)$form['is_europe'];
            $model->is_popular = (int)$form['is_popular'];
            $model->network_id = (int)$form['network_id'];
            $model->network_id_contract = (int)$form['network_id_contract'];
            $model->network_id_prepaid = (int)$form['network_id_prepaid'];
            $model->private_visibility = (int)$form['private_visibility'];
            $model->contract_visibility = (int)$form['contract_visibility'];
            $model->business_visibility = (int)$form['business_visibility'];

            $validate = $model->validate();

            $title = '';
            foreach ($this->languages as $lang) {
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];

                if ($lang->id == param('defaultLangId')) {
                    $title = $i18n[$lang->id]['title'];
                }

                $validateMulti = $i18nModel[$lang->id]->validate();
                $validate = $validate && $validateMulti;

                if (!$validateMulti) {
                    $multiErrors[$lang->code][] = $i18nModel[$lang->id]->getErrors();
                }
            }

            if ($validate) {

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->country_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                LogRegistry::instance()->log(LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $title);
                user()->setFlash('success', 'Сохранено');

                $redirect = array('update', 'id' => $model->id);
                if (user()->show_index) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            }
        }

        $networks = Network::model()->getItems( param('defaultLangId') );
        $this->render('update', compact('model', 'class', 'i18nModel', 'multiErrors', 'networks'));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        LogRegistry::instance()->log(LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->i18n[0]->title);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Country('search');
        $class = get_class($model);
        $model->unsetAttributes();  // clear any default values
        if (array_key_exists($class, $_GET)) {
            $model->attributes = $_GET[$class];
        }

        $this->render('admin', compact('model', 'class'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Country the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Country::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    
    public function displayNetworks(Country $model) {
        $output = [];
        $output[] = $model->network_id > 0 ? $model->net->title : '-';
        $output[] = $model->network_id_contract > 0 ? $model->net_contract->title : '-';
        $output[] = $model->network_id_prepaid > 0 ? $model->net_prepaid->title : '-';
        return implode(' / ', $output);
    }

    public function displayVisibility(Country $model) {
        $output = [];
        $output[] = $model->private_visibility > 0 ? '+' : '-';
        $output[] = $model->contract_visibility > 0 ? '+' : '-';
        $output[] = $model->business_visibility > 0 ? '+' : '-';
        return implode(' / ', $output);
    }
}
