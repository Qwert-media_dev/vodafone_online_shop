<?php

class IntCallPlanController extends ManageController
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new IntCallPlan;
		$class = get_class($model);

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$model->country_id = (int)$form['country_id'];
			$model->type = $form['type'];
			//$model->price = $form['price'];
			$model->price_uk = $form['price_uk'];
			$model->price_en = $form['price_en'];
			$model->price_ru = $form['price_ru'];
			if($model->validate()) {
				$model->save();

				$title = sprintf('Страна=%s, тип=%s', $model->country->i18n[0]->title, param('plan_type')[$model->type]);
				LogRegistry::instance()->log( LogRegistry::METHOD_CREATE, get_class($model), $model->id, $title );

				$redirect = array('update', 'id' => $model->id);
				if(user()->show_index) {
					$redirect = array('admin');
				}

				$this->redirect($redirect);
			}
		}

		$countries = Country::model()->getNotFromNetwork(param('defaultLangId'));
		$this->render('create', compact('model', 'countries'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$class = get_class($model);

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$model->country_id = (int)$form['country_id'];
			$model->type = $form['type'];
			//$model->price = $form['price'];
			$model->price_uk = $form['price_uk'];
			$model->price_en = $form['price_en'];
			$model->price_ru = $form['price_ru'];
			if($model->validate()) {
				$model->save();

				$title = sprintf('Страна=%s, тип=%s', $model->country->i18n[0]->title, param('plan_type')[$model->type]);
				LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $title );

				$redirect = array('update', 'id' => $model->id);
				if(user()->show_index) {
					$redirect = array('admin');
				}

				$this->redirect($redirect);
			}
		}

		$countries = Country::model()->getNotFromNetwork(param('defaultLangId'));
		$this->render('update', compact('model', 'countries'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$title = sprintf('Страна=%s, тип=%s', $model->country->i18n[0]->title, param('plan_type')[$model->type]);
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $title );
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new IntCallPlan('search');
        $class = get_class($model);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['IntCallPlan']))
			$model->attributes=$_GET['IntCallPlan'];

        $ids = IntCallPlan::model()->getCountryID();
        $countries = Country::model()->getItems( param('defaultLangId'), $ids );
		$this->render('admin', compact('model', 'class', 'countries'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return IntCallPlan the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=IntCallPlan::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}
