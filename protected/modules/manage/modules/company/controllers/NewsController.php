<?php

class NewsController extends ManageController
{

	const TYPE = 'news';
	protected static $class = 'News';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'save', 'saveimage', 'archive', 'trash', 'restore'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actions()
	{
		return array(
			'saveimage'=>'application.modules.manage.components.SaveImageAction',
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new News;
		$class = get_class($model);

		$i18nModel = array();
		foreach($this->languages as $lang) {
			$i18nModel[$lang->id] = new NewsI18n();
		}

		$model->news_date = date('Y-m-d');
		$model->news_time = date('H:i');
		$codes = Languages::model()->getCodes($this->languages);
		$this->render('create', compact('model', 'class', 'i18nModel', 'codes'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$class = get_class($model);

		$i18nModel = array();
		foreach($model->i18n as $item) {
			$i18nModel[$item->lang_id] = $item;
		}

		$codes = Languages::model()->getCodes($this->languages);
		$this->render('update', compact('model', 'class', 'i18nModel', 'codes'));
	}


	public function actionSave() {
		$output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

		if(isset($_POST[self::$class]))
		{
			$form = $_POST[self::$class];
			$i18nModel = array();

			if( array_key_exists('id', $form) && (int)$form['id'] > 0 ) {
				$isNew = false;
				$model = News::model()->findByPk($form['id']);
				foreach($model->i18n as $item) {
					$i18nModel[ $item->lang_id ] = $item;
				}
			} else {
				$isNew = true;
				$model = new News();
				foreach($this->languages as $lang) {
					$i18nModel[ $lang->id ] = new NewsI18n;
				}
			}

			$i18n = $_POST[self::$class.'I18n'];
			$model->type = self::TYPE;
			$model->news_date = $form['news_date'];
			$model->news_time = $form['news_time'];
			$model->is_archive = (int)$form['is_archive'];

			$defaultTitle = '';
			$statuses = array();
			$validate = $model->validate();
			foreach($this->languages as $lang) {
				$currentStatus = (int)$i18n[$lang->id]['status'];
				$statuses[$lang->code] = $currentStatus;
				$i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;
				$i18nModel[$lang->id]->lang_id = $lang->id;
				$i18nModel[$lang->id]->status = $currentStatus;
				$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
				$i18nModel[$lang->id]->short = $i18n[$lang->id]['short'];
				$i18nModel[$lang->id]->body = $i18n[$lang->id]['body'];
				$i18nModel[$lang->id]->og_title = $i18n[$lang->id]['og_title'];
				$i18nModel[$lang->id]->og_description = $i18n[$lang->id]['og_description'];
				$i18nModel[$lang->id]->og_image = $i18n[$lang->id]['og_image'];

				$res = $i18nModel[$lang->id]->validate();
				$validate = $validate && $res;

				if( $defaultTitle == '' || ($lang->id == param('defaultLangId') && trim($i18n[$lang->id]['title']) != '') ) {
					$defaultTitle = trim($i18n[$lang->id]['title']);
				}
			}

			if($validate) {

				if( empty($defaultTitle) ) {
					$defaultTitle = '[Без заголовка]';
				}
				$model->title = $defaultTitle;
				$model->statuses = CJSON::encode($statuses);

				$model->save();

				foreach($this->languages as $lang) {
					$i18nModel[$lang->id]->news_id = $model->id;
					$i18nModel[$lang->id]->save();
				}

				$method = $isNew ? LogRegistry::METHOD_CREATE : LogRegistry::METHOD_UPDATE;
				LogRegistry::instance()->log( $method, get_class($model), $model->id, $model->title );

				(new CachePurger( strtolower(get_class($model)), $model->id ))->process();

				$params = array('id' => $model->id);
				if(array_key_exists('lang', $_POST) && $_POST['lang'] != '')
					$params['lang'] = $_POST['lang'];
				$redirect = url_to('manage/company/news/update', $params);
                if( user()->show_index ) {
                    $redirect = url_to('manage/company/news/admin');
                }

                $output = array('status' => 'ok', 'redirect' => $redirect);
			} else {
				$errors = array();
				if(sizeof($model->getErrors())) {
					foreach($model->getErrors() as $field => $data) {
						if(sizeof($data))
							$errors[$field] = $data[0];
					}
				}

				$langs = array();
				foreach( $this->languages as $lang ) {
					if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
						foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
							if(sizeof($data)) {
								$errors[$field . '_' . $lang->code] = $data[0];
								$langs[] = $lang->code;
							}
						}
					}
				}

				$langs = array_unique($langs);

				$output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
			}
		}

		echo CJSON::encode($output);
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        $model->is_deleted = 1;
        $model->save();
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->title );
		(new CachePurger( strtolower(get_class($model)), $model->id ))->process();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}



	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$provider = News::model()->getTable(self::TYPE, 0);
		$this->render('admin',compact('provider'));
	}

	public function actionArchive()
	{
		$provider = News::model()->getTable(self::TYPE, 1);
		$this->render('archive',compact('provider'));
	}

    public function actionTrash()
    {
        $provider = News::model()->getTable(self::TYPE, null, 1);
        $this->render('trash',compact('provider'));
    }


    public function actionRestore($id) {
        $model = $this->loadModel($id);
        $model->is_deleted = 0;
        $model->save();
	    LogRegistry::instance()->log( LogRegistry::METHOD_RESTORE, get_class($model), $model->id, $model->title );
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('trash'));
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


}
