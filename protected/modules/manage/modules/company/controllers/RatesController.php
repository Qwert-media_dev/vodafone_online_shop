<?php

class RatesController extends ManageController
{

    protected static $class = 'Rates';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'save', 'saveimage', 'archive', 'trash', 'restore', 'upload', 'clone'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'settings', 'edit'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
            'saveimage' => 'application.modules.manage.components.SaveImageAction',
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Rates;
        $class = get_class($model);

        $i18nModel = array();
        foreach ($this->languages as $lang) {
            $i18nModel[$lang->id] = new RatesI18n();
        }

        $codes = Languages::model()->getCodes($this->languages);
        $advantages1 = [];
        $advantages2 = [];
        $geoData = GeoHelper::instance()->listData();
        $geo = [];
        $geoExclude = [];
        $this->render('create', compact('model', 'class', 'i18nModel', 'codes', 'advantages1', 'advantages2', 'geo', 'geoData', 'geoExclude'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);

        $i18nModel = array();
        foreach ($model->i18n as $item) {
            $i18nModel[$item->lang_id] = $item;
        }

        $codes = Languages::model()->getCodes($this->languages);
        $advantages1 = RatesAdvantages::model()->getByRate($model->id, 'rich');
        $advantages2 = RatesAdvantages::model()->getByRate($model->id);
        $geoData = GeoHelper::instance()->listData();
        $geo = RatesGeo::model()->getItems($model->id);
        $geoExclude = GeoExclude::model()->getItems( $model->tableName(), $model->id);
        $this->render('update', compact('model', 'class', 'i18nModel', 'codes', 'advantages1', 'advantages2', 'geo', 'geoData', 'geoExclude'));
    }


    public function actionSave()
    {
        $output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

        if (isset($_POST[self::$class])) {
            #if( $_COOKIE['sasha'] == 'exit' ) {
            #    dump($_POST);
            #    exit;
            #}
            $form = $_POST[self::$class];
            $geo = $_POST['Geo'];
            $geoExclude = $_POST['GeoExclude'];
            $i18nModel = array();

            if (array_key_exists('id', $form) && (int)$form['id'] > 0) {
                $isNew = false;
                $model = Rates::model()->findByPk($form['id']);
                foreach ($model->i18n as $item) {
                    $i18nModel[$item->lang_id] = $item;
                }
            } else {
                $isNew = true;
                $model = new Rates();
                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id] = new RatesI18n;
                }
            }

            $i18n = $_POST[self::$class . 'I18n'];
            $advantages1 = $_POST['advantages1'];
            $advantages2 = $_POST['advantages2'];

            $model->is_archive = (int)$form['is_archive'];
            $model->is_active = (int)$form['is_active'];
            $model->is_top = (int)$form['is_top'];
            $model->type = $form['type'];
            $model->preview = $form['preview'];
            $model->price = $form['price'];
            $model->rate_date = $form['rate_date'];
            $model->sort = (int)$form['sort'];
            if( user()->email == param('adminEmail') ) {
                $model->widget = $form['widget'];
            }

            $defaultTitle = '';
            $statuses = array();
            $validate = $model->validate();
            foreach ($this->languages as $lang) {
                $currentStatus = (int)$i18n[$lang->id]['status'];
                $statuses[$lang->code] = $currentStatus;
                $i18nModel[$lang->id]->scenario = 'save_' . $currentStatus;
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->status = $currentStatus;
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
                $i18nModel[$lang->id]->caption = $i18n[$lang->id]['caption'];
                $i18nModel[$lang->id]->legal = $i18n[$lang->id]['legal'];
                $i18nModel[$lang->id]->content = $i18n[$lang->id]['content'];
                $i18nModel[$lang->id]->content_after = $i18n[$lang->id]['content_after'];
                $i18nModel[$lang->id]->og_title = $i18n[$lang->id]['og_title'];
                $i18nModel[$lang->id]->og_description = $i18n[$lang->id]['og_description'];
                $i18nModel[$lang->id]->og_image = $i18n[$lang->id]['og_image'];
                $i18nModel[$lang->id]->second_title = $i18n[$lang->id]['second_title'];
                $i18nModel[$lang->id]->extra_price = $i18n[$lang->id]['extra_price'];
                $i18nModel[$lang->id]->pic1 = $i18n[$lang->id]['pic1'];
                $i18nModel[$lang->id]->pic2 = $i18n[$lang->id]['pic2'];
                $i18nModel[$lang->id]->pic3 = $i18n[$lang->id]['pic3'];
                $i18nModel[$lang->id]->pic4 = $i18n[$lang->id]['pic4'];
                $i18nModel[$lang->id]->tab_title = $i18n[$lang->id]['tab_title'];

                $i18nModel[$lang->id]->alias = URLify::filter($i18n[$lang->id]['alias']);
                if( $i18nModel[$lang->id]->alias == '' ) {
                    $i18nModel[$lang->id]->alias = URLify::filter($i18n[$lang->id]['title']);
                }

                $res = $i18nModel[$lang->id]->validate();
                $validate = $validate && $res;

                if ($defaultTitle == '' || ($lang->id == param('defaultLangId') && trim($i18n[$lang->id]['title']) != '')) {
                    $defaultTitle = trim($i18n[$lang->id]['title']);
                }
            }

            if ($validate) {

                if (empty($defaultTitle)) {
                    $defaultTitle = '[Без заголовка]';
                }
                $model->title = $defaultTitle;
                $model->statuses = CJSON::encode($statuses);

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->rate_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                RatesAdvantages::model()->populate($model->id, $advantages1, 'rich');
                RatesAdvantages::model()->populate($model->id, $advantages2);
                RatesGeo::model()->populate($model->id, $geo);
                GeoExclude::model()->populate($model->tableName(), $model->id, $geoExclude);

                $method = $isNew ? LogRegistry::METHOD_CREATE : LogRegistry::METHOD_UPDATE;
                LogRegistry::instance()->log($method, get_class($model), $model->id, $model->title);

                (new CachePurger(strtolower(get_class($model)), $model->id))->process();

                $params = array('id' => $model->id);
                if (array_key_exists('lang', $_POST) && $_POST['lang'] != '')
                    $params['lang'] = $_POST['lang'];
                $redirect = url_to('manage/company/rates/update', $params);
                if (user()->show_index) {
                    $redirect = url_to('manage/company/rates/admin');
                }

                $output = array('status' => 'ok', 'redirect' => $redirect);
            } else {
                $errors = array();
                if (sizeof($model->getErrors())) {
                    foreach ($model->getErrors() as $field => $data) {
                        if (sizeof($data))
                            $errors[$field] = $data[0];
                    }
                }

                $langs = array();
                foreach ($this->languages as $lang) {
                    if (sizeof($i18nModel[$lang->id]->getErrors())) {
                        foreach ($i18nModel[$lang->id]->getErrors() as $field => $data) {
                            if (sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);

                $output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
            }
        }

        echo CJSON::encode($output);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        $model->save();
        LogRegistry::instance()->log(LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->title);
        (new CachePurger(strtolower(get_class($model)), $model->id))->process();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $provider = Rates::model()->getTable(0);
        $this->render('admin', compact('provider'));
    }

    public function actionArchive()
    {
        $provider = Rates::model()->getTable(1);
        $this->render('archive', compact('provider'));
    }

    public function actionTrash()
    {
        $provider = Rates::model()->getTable( null, 1);
        $this->render('trash', compact('provider'));
    }


    public function actionRestore($id)
    {
        $model = $this->loadModel($id);
        $model->is_deleted = 0;
        $model->save();
        LogRegistry::instance()->log(LogRegistry::METHOD_RESTORE, get_class($model), $model->id, $model->title);
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('trash'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return News the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Rates::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


    public function actionUpload()
    {
        $resp = array('status' => 'fail', 'msg' => 'init');

        $type = $_POST['type'];
        if (in_array($type, array('photo', 'icon'))) {
            $class = ucfirst($type) . 'Form';
            $model = new $class;
            $model->file = CUploadedFile::getInstanceByName('files[0]');
            if ($model->validate()) {
                $filename = URLify::filter(FS::hash($model->file->name . '-' . time()) . '-' . $model->file->name, 60, '', true);
                $model->file->saveAs(param('uploadPath') . $filename);
                $resp = array(
                    'status' => 'ok',
                    'location' => $filename
                );
            } else {
                $resp['msg'] = $model->getError('file');
            }
        }

        echo CJSON::encode($resp);
    }


    public function actionClone($id) {
        $model = Rates::model()->with('i18n', 'advantages', 'geo')->findByPk($id);
        if( $model === null ) {
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
        }

        $newModel = clone $model;
        $newModel->isNewRecord = true;
        unset($newModel->id);
        $newModel->save(false);
        foreach($newModel->i18n as $i18n) {
            $i18n->isNewRecord = true;
            unset($i18n->id);
            $i18n->rate_id = $newModel->id;
            $i18n->save(false);
        }

        foreach($newModel->advantages as $advantage) {
            $advantage->isNewRecord = true;
            unset($advantage->id);
            $advantage->rate_id = $newModel->id;
            $advantage->save(false);
        }

        foreach($newModel->geo as $geo) {
            $geo->isNewRecord = true;
            unset($geo->id);
            $geo->rate_id = $newModel->id;
            $geo->save(false);
        }

        foreach($newModel->geoExclude as $geo) {
            $geo->isNewRecord = true;
            unset($geo->id);
            $geo->table_pk = $newModel->id;
            $geo->save(false);
        }
        
        $this->redirect(array('admin'));
    }

    
    public function actionSettings() {
        $model = new RatesSettings('search');
        $model->unsetAttributes();
        $this->render('settings', compact('model'));
    }


    public function actionEdit($id) {
        $model = RatesSettings::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        $class = get_class($model);
        $i18nModel = RatesSettingsI18n::model()->getItems($model->id);
        foreach($this->languages as $lang) {
            if( !array_key_exists($lang->id, $i18nModel) ) {
                $i18nModel[$lang->id] = new RatesSettingsI18n();
            }
        }

        $langs = array();
        if(isset($_POST[$class.'I18n'])) {
            $i18n = $_POST[$class.'I18n'];
            $validate = true;

            foreach($this->languages as $lang) {
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->pic1 = $i18n[$lang->id]['pic1'];
                $i18nModel[$lang->id]->pic2 = $i18n[$lang->id]['pic2'];
                $i18nModel[$lang->id]->pic3 = $i18n[$lang->id]['pic3'];
                $i18nModel[$lang->id]->pic4 = $i18n[$lang->id]['pic4'];
                $i18nModel[$lang->id]->og_title = $i18n[$lang->id]['og_title'];
                $i18nModel[$lang->id]->og_description = $i18n[$lang->id]['og_description'];
                $i18nModel[$lang->id]->og_image = $i18n[$lang->id]['og_image'];
                $i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
                $i18nModel[$lang->id]->description = $i18n[$lang->id]['description'];

                $res = $i18nModel[$lang->id]->validate();
                $validate = $validate && $res;
            }

            if ($validate) {

                LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, param('plan_type')[$model->type] );

                foreach($this->languages as $lang) {
                    $i18nModel[$lang->id]->rates_settings_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                $redirect = array('edit', 'id' => $model->id);
                if( user()->show_index ) {
                    $redirect = array('settings');
                }

                $this->redirect($redirect);
            } else {
                foreach( $this->languages as $lang ) {
                    if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
                        foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
                            if(sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);
            }
        }
        
        $this->render('settings-update', compact('model', 'i18nModel', 'class', 'langs'));
    }
}
