<?php

class ServicesController extends ManageController
{

    protected static $class = 'Services';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'save', 'saveimage', 'trash', 'restore', 'clone'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
            'saveimage' => 'application.modules.manage.components.SaveImageAction',
        );
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Services;
        $class = get_class($model);

        $i18nModel = array();
        foreach ($this->languages as $lang) {
            $i18nModel[$lang->id] = new ServicesI18n();
        }

        $codes = Languages::model()->getCodes($this->languages);
        $geoData = GeoHelper::instance()->listData();
        $geo = [];
        $geoExclude = [];
        $this->render('create', compact('model', 'class', 'i18nModel', 'codes', 'geo', 'geoData', 'geoExclude'));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);

        $i18nModel = array();
        foreach ($model->i18n as $item) {
            $i18nModel[$item->lang_id] = $item;
        }

        $codes = Languages::model()->getCodes($this->languages);
        $geoData = GeoHelper::instance()->listData();
        $geo = ServicesGeo::model()->getItems($model->id);
        $geoExclude = GeoExclude::model()->getItems( $model->tableName(), $model->id);
        $this->render('update', compact('model', 'class', 'i18nModel', 'codes', 'geo', 'geoData', 'geoExclude'));
    }


    public function actionSave()
    {
        $output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

        if (isset($_POST[self::$class])) {
            $form = $_POST[self::$class];
            $geo = $_POST['Geo'];
            $geoExclude = $_POST['GeoExclude'];
            $i18nModel = array();

            if (array_key_exists('id', $form) && (int)$form['id'] > 0) {
                $isNew = false;
                $model = Services::model()->findByPk($form['id']);
                foreach ($model->i18n as $item) {
                    $i18nModel[$item->lang_id] = $item;
                }
            } else {
                $isNew = true;
                $model = new Services();
                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id] = new ServicesI18n;
                }
            }

            $i18n = $_POST[self::$class . 'I18n'];

            $statuses = array();
            $validate = $model->validate();
            foreach ($this->languages as $lang) {
                $currentStatus = (int)$i18n[$lang->id]['status'];
                $statuses[$lang->code] = $currentStatus;
                $i18nModel[$lang->id]->scenario = 'save_' . $currentStatus;
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->status = $currentStatus;
                $i18nModel[$lang->id]->content = $i18n[$lang->id]['content'];

                $res = $i18nModel[$lang->id]->validate();
                $validate = $validate && $res;

            }

            if ($validate) {

                $model->statuses = CJSON::encode($statuses);

                $model->save();

                foreach ($this->languages as $lang) {
                    $i18nModel[$lang->id]->service_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                ServicesGeo::model()->populate($model->id, $geo);
                GeoExclude::model()->populate($model->tableName(), $model->id, $geoExclude);

                $method = $isNew ? LogRegistry::METHOD_CREATE : LogRegistry::METHOD_UPDATE;
                LogRegistry::instance()->log($method, get_class($model), $model->id, GeoHelper::instance()->getServiceGeo($model->id));

                //(new CachePurger(strtolower(get_class($model)), $model->id))->process();

                $params = array('id' => $model->id);
                if (array_key_exists('lang', $_POST) && $_POST['lang'] != '')
                    $params['lang'] = $_POST['lang'];
                $redirect = url_to('manage/company/services/update', $params);
                if (user()->show_index) {
                    $redirect = url_to('manage/company/services/admin');
                }

                $output = array('status' => 'ok', 'redirect' => $redirect);
            } else {
                $errors = array();
                if (sizeof($model->getErrors())) {
                    foreach ($model->getErrors() as $field => $data) {
                        if (sizeof($data))
                            $errors[$field] = $data[0];
                    }
                }

                $langs = array();
                foreach ($this->languages as $lang) {
                    if (sizeof($i18nModel[$lang->id]->getErrors())) {
                        foreach ($i18nModel[$lang->id]->getErrors() as $field => $data) {
                            if (sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);

                $output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
            }
        }

        echo CJSON::encode($output);
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->is_deleted = 1;
        $model->save();
        LogRegistry::instance()->log(LogRegistry::METHOD_DELETE, get_class($model), $model->id, GeoHelper::instance()->getServiceGeo($model->id));
        (new CachePurger(strtolower(get_class($model)), $model->id))->process();
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $provider = Services::model()->getTable();
        $this->render('admin', compact('provider'));
    }
    

    public function actionTrash()
    {
        $provider = Services::model()->getTable(1);
        $this->render('trash', compact('provider'));
    }


    public function actionRestore($id)
    {
        $model = $this->loadModel($id);
        $model->is_deleted = 0;
        $model->save();
        LogRegistry::instance()->log(LogRegistry::METHOD_RESTORE, get_class($model), $model->id, GeoHelper::instance()->getServiceGeo($model->id));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('trash'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return News the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Services::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    

    public function actionClone($id) {
        $model = Services::model()->with('i18n', 'geo')->findByPk($id);
        if( $model === null ) {
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
        }

        $newModel = clone $model;
        $newModel->isNewRecord = true;
        unset($newModel->id);
        $newModel->save(false);
        foreach($newModel->i18n as $i18n) {
            $i18n->isNewRecord = true;
            unset($i18n->id);
            $i18n->service_id = $newModel->id;
            $i18n->save(false);
        }

        foreach($newModel->geo as $geo) {
            $geo->isNewRecord = true;
            unset($geo->id);
            $geo->service_id = $newModel->id;
            $geo->save(false);
        }
        
        $this->redirect(array('admin'));
    }

}
