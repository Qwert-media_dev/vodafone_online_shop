<?php
/* @var $this RatesController */
/* @var $model Rates */
/* @var $class string */
/* @var $i18nModel RatesI18n[] */
/* @var $codes array */
/* @var $advantages1 array */
/* @var $advantages2 array */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */

$this->breadcrumbs = array(
    'Тарифы' => array('admin'),
    'Добавление',
);

$this->menu = array(
    array('label' => 'Список тарифов', 'url' => array('admin')),
    array('label' => 'Архив', 'url' => array('archive')),
    array('label' => 'Корзина', 'url' => array('trash')),
    array('label' => '<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings'), 'encodeLabel' => false, 'itemOptions' => ['class' => 'pull-right']),
);
?>

<h1>Создание тарифа</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'codes', 'advantages1', 'advantages2', 'geo', 'geoData', 'geoExclude')); ?>