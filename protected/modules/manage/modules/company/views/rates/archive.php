<?php
/* @var $this RatesController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Тарифы'=>array('admin'),
    'Список архивных тарифов',
);

$this->menu=array(
    array('label'=>'Добавить тариф', 'url'=>array('create')),
    array('label'=>'Список тарифов', 'url'=>array('admin')),
    array('label'=>'Корзина', 'url'=>array('trash')),
    array('label' => '<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings'), 'encodeLabel' => false, 'itemOptions' => ['class' => 'pull-right']),
);

?>

    <script>
        $(function(){
            $( "#date" ).datepicker({
                showAnim: "show",
                dateFormat: "yy-mm-dd"
            });
        });
    </script>

    <h1>Список архивных тарифов</h1>

    <br>
    <hr>
<?php echo CHtml::beginForm(array('archive'), 'get',array('class' => 'form-inline')); ?>
    <div class="form-group">
        <label for="title">Заголовок</label>
        <?php echo CHtml::textField('filter[title]', $_GET['filter']['title'], array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Заголовок')); ?>
    </div>
    <div class="form-group">
        <label for="date">Дата</label>
        <?php echo CHtml::textField('filter[date]', $_GET['filter']['date'], array('class' => 'form-control', 'id' => 'date', 'placeholder' => 'Дата')); ?>
    </div>
    <button type="submit" class="btn btn-default">Искать</button>
<?= CHtml::link('Сбросить', array('admin')); ?>
<?php echo CHtml::endForm(); ?>
    <hr>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'rate-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        [
            'name' => 'type',
            'value' => 'Rates::model()->getTypes()[$data->type]'
        ],
        [
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ],
        array(
            'header' => 'Geo',
            'type' => 'raw',
            'value' => 'GeoHelper::instance()->getRateGeo($data->id)'
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
