<?php
/* @var $this RatesController */
/* @var $model RatesSettings */

$this->breadcrumbs = array(
    'Тарифы' => array('admin'),
    'Список тарифов',
);

$this->menu = array(
    array('label' => 'Добавить тариф', 'url' => array('create')),
    array('label' => 'Список тарифов', 'url' => array('admin')),
    array('label' => 'Архив', 'url' => array('archive')),
    array('label' => 'Корзина', 'url' => array('trash')),
);

?>

<h1>Настройки</h1>

<br>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'rates-grid',
    'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        [
            'name' => 'type',
            'value' => 'Rates::model()->getTypes()[$data->type]'
        ],
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 280, 'align' => 'center'),
            'template' => '{edit}',
            'buttons' => array(
                'edit' => array(
                    'imageUrl' => false,
                    'label' => 'Редактировать',
                    'url' => 'url_to("manage/company/rates/edit", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
            )
        ),
    ),
));
