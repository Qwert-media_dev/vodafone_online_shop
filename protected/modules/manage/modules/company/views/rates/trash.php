<?php
/* @var $this RatesController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Тарифы'=>array('admin'),
    'Корзина',
);

$this->menu=array(
    array('label'=>'Добавить тариф', 'url'=>array('create')),
    array('label'=>'Список тарифов', 'url'=>array('admin')),
    array('label'=>'Архив', 'url'=>array('archive')),
    array('label' => '<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings'), 'encodeLabel' => false, 'itemOptions' => ['class' => 'pull-right']),
);

?>

<h1>Список удаленных тарифов</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'rate-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        [
            'name' => 'type',
            'value' => 'Rates::model()->getTypes()[$data->type]'
        ],
        [
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ],
        array(
            'header' => 'Geo',
            'type' => 'raw',
            'value' => 'GeoHelper::instance()->getRateGeo($data->id)'
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{restore}',
            'buttons' => array(
                'restore' => array(
                    'label' => 'Восстановить',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("manage/company/rates/restore", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-success btn-xs',
                        'confirm' => 'Вы уверены ?'
                    )
                ),
            )
        ),
    ),
));
