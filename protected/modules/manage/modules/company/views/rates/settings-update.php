<?php
/* @var $this RatesController */
/* @var $model RatesSettings */
/* @var $class string */
/* @var $i18nModel RatesSettingsI18n[] */
/* @var $form CActiveForm */

$modelErrors = $model->getErrors();


$this->breadcrumbs = array(
    'Тарифы' => array('admin'),
    $model->id => array('update', 'id' => $model->id),
    'Редактирование',
);

$this->menu = array(
    array('label' => 'Добавить тариф', 'url' => array('create')),
    array('label' => 'Список тарифов', 'url' => array('admin')),
    array('label' => 'Архив', 'url' => array('archive')),
    array('label' => 'Корзина', 'url' => array('trash')),
    array('label' => '<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings'), 'encodeLabel' => false, 'itemOptions' => ['class' => 'pull-right']),
);
?>

<h1>Редактирование</h1>

<script>
    window.FileAPI = {
        debug: true, // debug mode
        staticPath: '/js/manage/jquery.fileapi/FileAPI/' // path to *.swf
    };
</script>
<script type="text/javascript" src="/js/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
<script type="text/javascript" src="/js/jquery.fileapi/jquery.fileapi.min.js"></script>

<div class="form">

    <?php

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'rates-form',
        'action' => array('edit', 'id' => $model->id),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
    if (!$model->getIsNewRecord()) {
        echo $form->hiddenField($model, 'id');
    }

    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-md-9">


            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach ($this->languages as $lang): ?>
                        <li>
                            <a href="#<?= $lang->code ?>" class="<?= $lang->code ?>" aria-controls="<?= $lang->code ?>"
                               role="tab" data-toggle="tab">
                                <?= $lang->title ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach ($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?= $lang->code ?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors(); ?>

                            <br/>

                            <div class="form-group group-title_<?= $lang->code ?> <?php if (isset($errors['title'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id], 'title', array('class' => 'control-label')); ?>
                                <?php echo $form->textField($i18nModel[$lang->id], 'title', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][title]', 'id' => 'title_' . $lang->code)); ?>
                                <div class="help-block"></div>
                            </div>
                            <div class="form-group group-description_<?= $lang->code ?> <?php if (isset($errors['description'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id], 'description', array('class' => 'control-label')); ?>
                                <?php echo $form->textField($i18nModel[$lang->id], 'description', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][description]', 'id' => 'description_' . $lang->code)); ?>
                                <div class="help-block"></div>
                            </div>

                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $lang->code ?>" aria-expanded="true" aria-controls="collapseOne">
                                                Настройки
                                                <span class="caret"></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $lang->code ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">


                                            <div class="form-group group-og_title_<?= $lang->code ?> <?php if (isset($errors['og_title'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_title', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_title', array('class' => 'form-control counter', 'name' => $class . 'I18n[' . $lang->id . '][og_title]', 'maxlength' => 70)); ?>
                                                    <div class="input-group-addon">
                                                        <div class="badge">0</div>
                                                    </div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>

                                            <div class="form-group group-og_description_<?= $lang->code ?> <?php if (isset($errors['og_description'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_description', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_description', array('class' => 'form-control counter', 'name' => $class . 'I18n[' . $lang->id . '][og_description]', 'rows' => 3, 'maxlength' => 150)); ?>
                                                    <div class="input-group-addon">
                                                        <div class="badge">0</div>
                                                    </div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>


                                            <div class="form-group group-og_image_<?= $lang->code ?> <?php echo $this->error($i18nModel[$lang->id], 'og_image') ?> wel2l">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_image', array('class' => 'control-label')); ?>

                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_image', array('class' => 'form-control image-src', 'id' => 'og_image_' . $lang->code, 'name' => $class . 'I18n[' . $lang->id . '][og_image]')); ?>
                                                    <div class="input-group-addon">
                                                        <a href="#" data-field="og_image" data-lang="<?= $lang->code ?>"
                                                           class="btn btn-default btn-xs open-filemanager">
                                                            <span class="glyphicon glyphicon-picture"></span>
                                                        </a>
                                                    </div>
                                                </div>


                                            </div>


                                            <hr>

                                            <div class="panel panel-default vis-promo">
                                                <div class="panel-heading">Промо картинки</div>
                                                <div class="panel-body">

                                                    <?php
                                                    $uploadUrl = url_to('manage/default/upload');
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic1', 'fieldKey' => 'pic1_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '2600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic2', 'fieldKey' => 'pic2_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '1600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic3', 'fieldKey' => 'pic3_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '980x360 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic4', 'fieldKey' => 'pic4_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '660x500 jpg,gif,png', 'url' => $uploadUrl));
                                                    ?>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

        </div>

        <div class="col-md-3">


            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
            </div>

        </div>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->

<div class="modal fade" id="file-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Файловый менеджер</h4>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" data-type="" id="pageBrowse" data-lang="" width="100%"
                        height="500"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script>
    //var languages = <?php echo CJSON::encode($codes); ?>;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';

    $(function () {

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>

        $('.open-filemanager').click(function (e) {
            e.preventDefault();
            $('#pageBrowse').attr('data-field', $(this).data('field')).attr('data-lang', $(this).data('lang'));
            $('#pageBrowse').attr('src', cbFilesURL).load(function () {
                $('#file-modal').modal({});
            });
        });
    });
</script>