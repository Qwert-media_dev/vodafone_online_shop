<?php
/* @var $this RatesController */
/* @var $model Rates */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel RatesI18n[] */
/* @var $codes array */
/* @var $advantages1 array */
/* @var $advantages2 array */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */

$modelErrors = $model->getErrors();
?>

<style type="text/css">
    .sortable{ list-style-type: none; margin: 10px 0 0 0; padding: 0; width: 100%; }
    .sortable>li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; /*height: 68px;*/ }
    .sortable>li>span { position: absolute; margin-left: -1.3em; }

    .adv1 label,
    .help-block{
        font-size: 12px;
        font-weight: bold;
    }

</style>

<script>
    window.FileAPI = {
        debug: true, // debug mode
        staticPath: '/js/manage/jquery.fileapi/FileAPI/' // path to *.swf
    };
</script>
<script type="text/javascript" src="/js/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
<script type="text/javascript" src="/js/jquery.fileapi/jquery.fileapi.min.js"></script>

<div class="form">

    <?php

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'rates-form',
        'action' => array('save'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
    if (!$model->getIsNewRecord()) {
        echo $form->hiddenField($model, 'id');
    }

    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-md-9">


            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach ($this->languages as $lang): ?>
                        <li>
                            <a href="#<?= $lang->code ?>" class="<?= $lang->code ?>" aria-controls="<?= $lang->code ?>"
                               role="tab" data-toggle="tab">
                                <?= $lang->title ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach ($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?= $lang->code ?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors(); ?>

                            <br/>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group group-title_<?= $lang->code ?> <?php if (isset($errors['title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'title', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][title]', 'id' => 'title_' . $lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group group-second_title_<?= $lang->code ?> <?php if (isset($errors['second_title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'second_title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'second_title', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][second_title]', 'id' => 'second_title_' . $lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group group-tab_title_<?= $lang->code ?> <?php if (isset($errors['tab_title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'tab_title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'tab_title', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][tab_title]', 'id' => 'tab_title_' . $lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_' . $lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id], 'status', array('id' => 'status_' . $lang->code, 'value' => 1, 'name' => $class . 'I18n[' . $lang->id . '][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group group-caption_<?= $lang->code ?> <?php if (isset($errors['caption'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'caption', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'caption', array('class' => 'form-control rte', 'name' => $class . 'I18n[' . $lang->id . '][caption]', 'id' => 'caption_' . $lang->code)); ?>
                                        <?php echo $form->error($i18nModel[$lang->id], 'caption', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group group-extra_price_<?= $lang->code ?> <?php if (isset($errors['extra_price'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'extra_price', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'extra_price', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][extra_price]', 'id' => 'extra_price_' . $lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group group-alias_<?= $lang->code ?> <?php if (isset($errors['alias'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id], 'alias', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id], 'alias', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][alias]', 'id' => 'alias_' . $lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $lang->code ?>" aria-expanded="true" aria-controls="collapseOne">
                                                Настройки
                                                <span class="caret"></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $lang->code ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">


                                            <div class="form-group group-og_title_<?= $lang->code ?> <?php if (isset($errors['og_title'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_title', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_title', array('class' => 'form-control counter', 'name' => $class . 'I18n[' . $lang->id . '][og_title]', 'maxlength' => 70)); ?>
                                                    <div class="input-group-addon">
                                                        <div class="badge">0</div>
                                                    </div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>

                                            <div class="form-group group-og_description_<?= $lang->code ?> <?php if (isset($errors['og_description'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_description', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_description', array('class' => 'form-control counter', 'name' => $class . 'I18n[' . $lang->id . '][og_description]', 'rows' => 3, 'maxlength' => 150)); ?>
                                                    <div class="input-group-addon">
                                                        <div class="badge">0</div>
                                                    </div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>


                                            <div class="form-group group-og_image_<?= $lang->code ?> <?php echo $this->error($i18nModel[$lang->id], 'og_image') ?> wel2l">
                                                <?php echo $form->labelEx($i18nModel[$lang->id], 'og_image', array('class' => 'control-label')); ?>

                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_image', array('class' => 'form-control image-src', 'id' => 'og_image_' . $lang->code, 'name' => $class . 'I18n[' . $lang->id . '][og_image]')); ?>
                                                    <div class="input-group-addon">
                                                        <a href="#" data-field="og_image" data-lang="<?= $lang->code ?>"
                                                           class="btn btn-default btn-xs open-filemanager">
                                                            <span class="glyphicon glyphicon-picture"></span>
                                                        </a>
                                                    </div>
                                                </div>


                                            </div>


                                            <hr>

                                            <div class="panel panel-default vis-promo">
                                                <div class="panel-heading">Промо картинки</div>
                                                <div class="panel-body">

                                                    <?php
                                                    $uploadUrl = url_to('manage/default/upload');
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic1', 'fieldKey' => 'pic1_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '2600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic2', 'fieldKey' => 'pic2_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '1600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic3', 'fieldKey' => 'pic3_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '980x360 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic4', 'fieldKey' => 'pic4_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '660x500 jpg,gif,png', 'url' => $uploadUrl));
                                                    ?>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group group-legal_<?= $lang->code ?> <?php if (isset($errors['legal'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id], 'legal', array('class' => 'control-label')); ?>
                                <?php echo $form->textArea($i18nModel[$lang->id], 'legal', array('class' => 'form-control rte', 'name' => $class . 'I18n[' . $lang->id . '][legal]', 'id' => 'legal_' . $lang->code)); ?>
                                <?php echo $form->error($i18nModel[$lang->id], 'legal', array('class' => 'help-block')); ?>
                                <div class="help-block"></div>
                            </div>

                            <div
                                class="form-group group-content_<?= $lang->code ?> <?php if (isset($errors['content'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id], 'content', array('class' => 'control-label')); ?>
                                <?php echo $form->hiddenField($i18nModel[$lang->id], 'content', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][content]', 'id' => 'content_' . $lang->code)); ?>

                                <div id="contentarea_<?= $lang->code ?>"
                                     class="container inner-content-container editor">
                                    <?php echo $i18nModel[$lang->id]->content; ?>
                                </div>

                                <?php //echo $form->error($i18nModel[$lang->id],'content', array('class' => 'help-block')); ?>
                                <div class="help-block"></div>
                            </div>


                            <div class="form-group group-content_after_<?= $lang->code ?> <?php if(isset($errors['content_after'])) echo ' has-error'; ?>">
                                <div class="row">
                                    <div class="col-md-3">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'content_after', array('class' => 'control-label')); ?>
                                    </div>
                                    <div class="col-md-1">
                                        <?php echo ' ', CHtml::tag('span', ['class' => 'glyphicon glyphicon-question-sign', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Используется если на странице есть виджет', 'style' => 'display:inline;']); ?>
                                    </div>
                                </div>

                                <?php echo $form->hiddenField($i18nModel[$lang->id],'content_after',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][content_after]', 'id' => 'content_after_'.$lang->code)); ?>
                                <div id="contentarea2_<?= $lang->code ?>" class="container inner-content-container editor">
                                    <?php echo $i18nModel[$lang->id]->content_after; ?>
                                </div>

                                <div class="help-block"></div>
                            </div>


                            <div class="panel-group" id="accordionAdv1<?= ucfirst($lang->code) ?>" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingAdv1<?= ucfirst($lang->code) ?>">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordionAdv1<?= ucfirst($lang->code) ?>" href="#collapseAdv1<?= ucfirst($lang->code) ?>" aria-expanded="false" aria-controls="collapseAdv1<?= ucfirst($lang->code) ?>">
                                                Приемущества 1
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseAdv1<?= ucfirst($lang->code) ?>" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingAdv1<?= ucfirst($lang->code) ?>">
                                        <div class="panel-body">

                                            <a href="#" class="btn btn-success add-adv1" data-lang="<?= $lang->id ?>">Добавить</a>

                                            <ul id="adv1<?= $lang->id ?>" class="adv1 sortable">
                                                <?php
                                                foreach($advantages1 as $id => $list) {
                                                    foreach($list as $lang_id => $data) {
                                                        if($lang_id == $lang->id ) {
                                                            ?>
                                                            <li class="well" id="<?= $id ?>">
                                                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                                                <a href="#" class="btn btn-danger btn-xs delete-adv1 pull-right">
                                                                    <div class="glyphicon glyphicon-remove"></div>
                                                                </a>

                                                                <div class="row">
                                                                    <div class="col-md-8">
                                                                        <label>Описание</label>
                                                                        <input type="text" name="advantages1[<?= $id ?>][<?= $lang_id ?>][title]"  class="form-control input-sm content title not-empty" value="<?= CHtml::encode($data['title']) ?>">
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label>Иконка</label>

                                                                        <input type="hidden" name="advantages1[<?= $id ?>][<?= $lang_id ?>][icon]" value="<?= CHtml::encode($data['icon']) ?>" class="icon not-empty">

                                                                        <div class="uploader" <?php if($data['icon'] != ''){ ?>style="display: none"<?php } ?>>
                                                                            <div class="js-fileapi-wrapper">
                                                                                <input type="file" name="files[]" class="btn btn-xs" />
                                                                            </div>
                                                                            <div data-fileapi="active.show" class="progress">
                                                                                <div data-fileapi="progress" class="progress__bar"></div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="preview">
                                                                            <?php if($data['icon'] != ''): ?>
                                                                                <img src="/images/upload/<?= $data['icon'] ?>" class="img-rounded img-thumbnail" /><br>
                                                                                <a href="#delete" class="remove-advantage-icon btn btn-danger btn-xs" data-id="<?= $id ?>" style="margin-top: 5px">Удалить фото</a>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                        <script>$(function(){bindCover('<?= $id ?>', "icon");});</script>
                                                                    </div>
                                                                    <div class="col-md-1"></div>
                                                                </div>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-group" id="accordionAdv2<?= ucfirst($lang->code) ?>" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingAdv2<?= ucfirst($lang->code) ?>">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordionAdv2<?= ucfirst($lang->code) ?>" href="#collapseAdv2<?= ucfirst($lang->code) ?>" aria-expanded="false" aria-controls="collapseAdv2<?= ucfirst($lang->code) ?>">
                                                Приемущества 2
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapseAdv2<?= ucfirst($lang->code) ?>" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingAdv2<?= ucfirst($lang->code) ?>">
                                        <div class="panel-body">

                                            <a href="#" class="btn btn-success add-adv2" data-lang="<?= $lang->id ?>">Добавить</a>

                                            <ul id="adv2<?= $lang->id ?>" class="adv2 sortable">
                                                <?php
                                                foreach($advantages2 as $id => $list) {
                                                    foreach($list as $lang_id => $data) {
                                                        if($lang_id == $lang->id ) {
                                                            ?>
                                                            <li class="well" id="<?= $id ?>">
                                                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                                                <a href="#" class="btn btn-danger btn-xs delete-adv2 pull-right">
                                                                    <div class="glyphicon glyphicon-remove"></div>
                                                                </a>

                                                                <label>Описание</label>
                                                                <input type="text" name="advantages2[<?= $id ?>][<?= $lang_id ?>][title]"  class="form-control input-sm content title not-empty" value="<?= CHtml::encode($data['title']) ?>">
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php endforeach; ?>
                </div>

            </div>


            <hr/>

            <?php $this->widget('GeoSelectorWidget', ['geo' => $geo, 'geoData' => $geoData]); ?>
            
            <?php $this->widget('GeoExcludeSelectorWidget', ['geo' => $geoExclude, 'geoData' => $geoData]); ?>

        </div>

        <div class="col-md-3 right-column">

            <div class="form-group <?php echo  $this->error($model, 'type') ?>">
                <?php echo $form->labelEx($model,'type', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'type', $model->getTypes(), array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'type', array('class' => 'help-block')); ?>
            </div>

            <div class="form-group group-date <?php echo  $this->error($model, 'rate_date') ?>">
                <?php echo $form->labelEx($model,'rate_date', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'rate_date',array('class'=>'form-control', 'id' => 'rate_date')); ?>
                <?php //echo $form->error($model,'date', array('class' => 'help-block')); ?>
                <div class="help-block"></div>
            </div>

            <div class="form-group <?php echo  $this->error($model, 'price') ?>">
                <?php echo $form->labelEx($model,'price', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'price', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'price', array('class' => 'help-block')); ?>
            </div>


            <div class="form-group <?php echo  $this->error($model, 'sort') ?>">
                <?php echo $form->labelEx($model,'sort', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'sort', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'sort', array('class' => 'help-block')); ?>
            </div>


            <?php if( user()->email == param('adminEmail') ): ?>
            <div class="form-group <?php echo  $this->error($model, 'widget') ?>">
                <?php echo $form->labelEx($model,'widget', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'widget', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'widget', array('class' => 'help-block')); ?>
            </div>
            <?php endif; ?>


            <div class="form-group group-preview <?php echo  $this->error($model, 'preview') ?> well">
                <?php echo $form->labelEx($model,'preview', array('class' => 'control-label')); ?>

                <?php echo $form->hiddenField($model, 'preview', array('class'=>'form-control', 'id' => 'preview')); ?>

                <div class="uploader preview" <?php if($model->preview != '') echo ' style="display:none"'; ?>>
                    <div class="js-fileapi-wrapper">
                        <input type="file" name="files[]" />
                    </div>
                    <div data-fileapi="active.show" class="progress">
                        <div data-fileapi="progress" class="progress__bar"></div>
                    </div>
                </div>
                <div class="preview-preview">
                    <?php if($model->preview != '' ): ?>
                        <img src="/images/upload/<?= $model->preview ?>" class="img-rounded" width="180" />
                        <a href="#delete" class="remove-preview-preview btn btn-danger btn-xs pull-right" style="margin-top: 5px">Удалить</a>
                    <?php endif; ?>
                </div>

                <?php echo $form->error($model, 'preview', array('class' => 'help-block')); ?>
            </div>

            <script>
                $(function() {
                    $('.preview').fileapi({
                        url: '<?php echo Yii::app()->createUrl('manage/company/rates/upload'); ?>',
                        dataType: 'json',
                        data: {type: "photo"},
                        autoUpload: true,
                        accept: 'image/*',
                        multiple: false,
                        maxSize: FileAPI.MB * 10, // max file size
                        onFileComplete: function (e, o) {
                            console.log('onFileComplete', o.result);
                            var data = o.result;
                            if(data.status == 'ok') {
                                $('#preview').val(data.location);
                                $('.preview').hide();
                                $('.preview-preview').html('<img src="/images/upload/' + data.location + '" class="img-rounded" width="180" /><a href="#delete" class="remove-preview-preview btn btn-danger btn-xs pull-right">Удалить</a>').show();
                            } else {
                                alert(data.msg);
                            }
                        }
                    });

                    $(document).on('click', '.remove-preview-preview', function (e) {
                        e.preventDefault();
                        if(confirm('Удалить ?')) {
                            $('#preview').val('');
                            $('.preview-preview').html('').hide();
                            $('.preview').show();
                        }
                    });
                });
            </script>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_archive'));
                echo $form->checkBox($model, 'is_archive', array('id' => 'is_archive', 'value' => 1));
                echo $model->getAttributeLabel('is_archive');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_active'));
                echo $form->checkBox($model, 'is_active', array('id' => 'is_active', 'value' => 1));
                echo $model->getAttributeLabel('is_active');
                echo ' ', CHtml::tag('span', ['class' => 'glyphicon glyphicon-question-sign', 'data-toggle' => 'tooltip', 'data-placement' => 'right', 'title' => 'Работает в паре с опцией Архивная']);
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_top'));
                echo $form->checkBox($model, 'is_top', array('id' => 'is_top', 'value' => 1));
                echo $model->getAttributeLabel('is_top');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
            </div>

        </div>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->

<div class="modal fade" id="file-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Файловый менеджер</h4>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" data-type="" id="pageBrowse" data-lang="" width="100%"
                        height="500"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<div id="advantages1-template" style="display: none;">
    <li class="well">
        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
        <a href="#" class="btn btn-danger btn-xs delete-adv1 pull-right">
            <div class="glyphicon glyphicon-remove"></div>
        </a>

        <div class="row">
            <div class="col-md-8">
                <label for="">Описание</label>
                <input type="text" name="" value="" class="form-control input-sm content title not-empty">
            </div>
            <div class="col-md-3">
                <label>Иконка</label>

                <input type="hidden" name="" value="" class="icon not-empty">

                <div class="uploader">
                    <div class="js-fileapi-wrapper">
                        <input type="file" name="files[]" class="btn btn-xs" />
                    </div>
                    <div data-fileapi="active.show" class="progress">
                        <div data-fileapi="progress" class="progress__bar"></div>
                    </div>
                </div>
                <div class="preview">
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>

    </li>
</div>


<div id="advantages2-template" style="display: none;">
    <li class="well">
        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
        <a href="#" class="btn btn-danger btn-xs delete-adv2 pull-right">
            <div class="glyphicon glyphicon-remove"></div>
        </a>

        <label for="">Описание</label>
        <input type="text" name="" value="" class="form-control input-sm content title not-empty">

    </li>
</div>

<?php
foreach ($this->languages as $lang) {
    $this->widget('ContentBuilderWidget', array('selector' => '#contentarea_' . $lang->code, 'fileSelectUrl' => url_to('manage/files/export')));
    $this->widget('ContentBuilderWidget', array('selector' => '#contentarea2_' . $lang->code, 'fileSelectUrl' => url_to('manage/files/export')));
}
?>


<script>
    var fired = 0;
    var inProgress = false;
    var languages = <?php echo CJSON::encode($codes); ?>;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';
    var $btn;

    $(function () {

        $( ".sortable" ).sortable({
            handle: '.ui-icon-arrowthick-2-n-s',
            helper: "clone"
        });

        $( "#rate_date" ).datepicker({
            showAnim: "show",
            dateFormat: "yy-mm-dd"
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>


        $('.add-adv1').click(function(e){
            e.preventDefault();
            var lang = $(this).data('lang');
            var id = getRandomId();
            var clone = $('#advantages1-template li').eq(0).clone();
            clone.attr('id', id);
            clone.find('.title').attr('name', 'advantages1['+id+']['+lang+'][title]');
            clone.find('.icon').attr('name', 'advantages1['+id+']['+lang+'][icon]');
            clone.find('.sort').attr('name', 'advantages1['+id+']['+lang+'][sort]');
            clone.appendTo('#adv1'+lang);
            bindCover(id, 'icon');
        });


        $(document).on('click', '.delete-adv1', function(e){
            e.preventDefault();
            if( confirm('Удалить ?') ) {
                $(this).parent().remove();
            }
        });

        $(document).on('click', '.remove-advantage-icon', function(e){
            e.preventDefault();
            if( confirm('Удалить ?') ) {
                var id = $(this).data('id');
                $('#' + id + ' .preview').html('');
                $('#' + id + ' .icon').val('');
                $('#' + id + ' .uploader').show();
            }
        });


        $('.add-adv2').click(function(e){
            e.preventDefault();
            var lang = $(this).data('lang');
            var id = getRandomId();
            var clone = $('#advantages2-template li').eq(0).clone();
            clone.attr('id', id);
            clone.find('.title').attr('name', 'advantages2['+id+']['+lang+'][title]');
            clone.appendTo('#adv2'+lang);
            bindCover(id, 'icon');
        });


        $(document).on('click', '.delete-adv2', function(e){
            e.preventDefault();
            if( confirm('Удалить ?') ) {
                $(this).parent().remove();
            }
        });

        $('#rates-form').submit(function (e) {
            e.preventDefault();

            fired = 0;
            log('submitted');
            if (!inProgress) {
                if( hasEmptyAdvantages() ) {
                    alert('Заполните все преимущества!');
                    return false;
                }
                inProgress = true;
                showLoader();
                $btn = $('.btn-save').button('loading');

                // reset errors and error messages
                $('.has-error').removeClass('has-error');
                $('a.error').removeClass('error');
                $('.help-block').each(function () {
                    $(this).text('');
                });

                saveImages(function () {
                    populateBody();
                    savePage();
                });
            } else {
                log('already in progress');
            }

        });

        $('.open-filemanager').click(function (e) {
            e.preventDefault();
            $('#pageBrowse').attr('data-field', $(this).data('field')).attr('data-lang', $(this).data('lang'));
            $('#pageBrowse').attr('src', cbFilesURL).load(function () {
                $('#file-modal').modal({});
            });
        });
    });
    
    
    function hasEmptyAdvantages() {
        var isEmpty = false;
        $('#rates-form .not-empty').each(function(){
            var value = $(this).val();
            if( $.trim(value) == '' ) {
                isEmpty = true;
            }
        });
        return isEmpty;
    }


    /**
     * Сохраняем бинарники на сервер и вызываем колбек дальше по цепочке
     *
     * @param callback callable function
     */
    function saveImages(callback) {
        log('saveImage');
        for (var i in languages) {
            $("#contentarea_" + languages[i]).saveimages({
                handler: '<?= url_to('manage/company/rates/saveimage') ?>',
                onComplete: function () {
                    fired++;
                    if (fired == languages.length) {
                        callback();
                    }
                }
            });
            $("#contentarea_" + languages[i]).data('saveimages').save();
        }
    }

    function savePage() {
        log('saving page');
        var $form = $('#rates-form');
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: $form.attr('method'),
            success: function (resp) {
                log('success');
                if (resp.status == 'ok') {
                    top.location.href = resp.redirect;
                } else {
                    $btn.button('reset');
                    for (var i in resp.errors) {
                        console.log(i);
                        $('.group-' + i).find('.help-block').text(resp.errors[i]);
                        $('.group-' + i).addClass('has-error');
                    }
                    for (var i in resp.langs) {
                        $('a.' + resp.langs[i]).addClass('error');
                    }
                }
                hideLoader();
                inProgress = false;
            },
            error: function () {
                log('error');
                hideLoader();
                inProgress = false;
            }
        });
    }

    function populateBody() {
        log('populate body');
        for (var i in languages) {
            $('#content_' + languages[i]).val($('#contentarea_' + languages[i]).data('contentbuilder').html());
            $('#content_after_' + languages[i]).val($('#contentarea2_' + languages[i]).data('contentbuilder').html());
        }
    }

    function bindCover(id, type) {
        $('#' + id + ' .uploader').fileapi({
            url: '<?php echo Yii::app()->createUrl('manage/company/rates/upload'); ?>',
            dataType: 'json',
            data: {type: type},
            autoUpload: true,
            accept: 'image/*',
            multiple: false,
            maxSize: FileAPI.MB * 10, // max file size
            onFileComplete: function (e, o) {
                console.log('onFileComplete', o.result);
                var data = o.result;
                if (data.status == 'ok') {
                    var content = '<img src="/images/upload/' + data.location + '" class="img-rounded img-thumbnail" /><br>' +
                        '<a href="#delete" class="remove-advantage-icon btn btn-danger btn-xs" data-id="'+id+'" style="margin-top: 5px">Удалить фото</a>';
                    $('#' + id + ' .preview').html(content);
                    $('#' + id + ' .icon').val(data.location);
                    $('#' + id + ' .uploader').hide();
                } else {
                    alert(data.msg);
                }
            }
        });
    }
</script>