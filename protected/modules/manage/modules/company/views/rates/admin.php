<?php
/* @var $this RatesController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs = array(
    'Тарифы' => array('admin'),
    'Список тарифов',
);

$this->menu = array(
    array('label' => 'Добавить тариф', 'url' => array('create')),
    array('label' => 'Архив', 'url' => array('archive')),
    array('label' => 'Корзина', 'url' => array('trash')),
    array('label' => '<span class="glyphicon glyphicon-cog"></span> Настройки', 'url'=>array('settings'), 'encodeLabel' => false, 'itemOptions' => ['class' => 'pull-right']),
);

?>


    <h1>Список тарифов</h1>

    <br>
    <hr>
<?php echo CHtml::beginForm(array('admin'), 'get', array('class' => 'form-inline')); ?>
    <div class="form-group">
        <label for="title">Заголовок</label>
        <?php echo CHtml::textField('filter[title]', $_GET['filter']['title'], array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Заголовок')); ?>
    </div>

    <button type="submit" class="btn btn-default">Искать</button>
<?= CHtml::link('Сбросить', array('admin')); ?>
<?php echo CHtml::endForm(); ?>
    <hr>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'rates-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        [
            'name' => 'type',
            'value' => 'Rates::model()->getTypes()[$data->type]'
        ],
        [
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ],
        array(
            'header' => 'Geo',
            'type' => 'raw',
            'value' => 'GeoHelper::instance()->getRateGeo($data->id)'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 280, 'align' => 'center'),
            'template' => '{clone} {update} {delete}',
            'buttons' => array(
                'clone' => array(
                    'imageUrl' => false,
                    'label' => '<span class="glyphicon glyphicon-duplicate"></span>',
                    'url' => 'url_to("manage/company/rates/clone", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-default btn-xs'
                    )
                ),
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
