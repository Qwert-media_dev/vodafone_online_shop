<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel NewsI18n[] */
/* @var $codes array */

$modelErrors = $model->getErrors();
Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/clockpicker/dist/jquery-clockpicker.min.js', CClientScript::POS_END);
Yii::app()->getClientScript()->registerCssFile(Yii::app()->baseUrl.'/js/clockpicker/dist/jquery-clockpicker.min.css');
?>

<div class="form">

<?php

$form = $this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'action' => array('save'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
if( !$model->getIsNewRecord() ) {
	echo $form->hiddenField($model, 'id');
}

?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php //echo $form->errorSummary($model); ?>


	<div class="row">
		<div class="col-md-9">


			<div role="tabpanel">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="myTab">
					<?php foreach($this->languages as $lang): ?>
						<li>
							<a href="#<?=$lang->code?>" class="<?= $lang->code ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
								<?=$lang->title?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<?php foreach($this->languages as $lang): ?>
						<div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
							<?php $errors = $i18nModel[$lang->id]->getErrors();?>

							<br/>

							<div class="row">
								<div class="col-md-9"><div class="form-group group-title_<?= $lang->code ?> <?php if(isset($errors['title'])) echo ' has-error'; ?>">
										<?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
										<?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]', 'id' => 'title_'.$lang->code)); ?>
										<?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
										<div class="help-block"></div>
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
								</div>
							</div>

                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $lang->code ?>" aria-expanded="true" aria-controls="collapseOne">
                                                Share
                                                <span class="caret"></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $lang->code ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">


                                            <div class="form-group group-og_title_<?= $lang->code ?> <?php if(isset($errors['og_title'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id],'og_title', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id],'og_title',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][og_title]', 'maxlength' => 70)); ?>
                                                    <div class="input-group-addon"><div class="badge">0</div></div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>

                                            <div class="form-group group-og_description_<?= $lang->code ?> <?php if(isset($errors['og_description'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id],'og_description', array('class' => 'control-label')); ?>
                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id],'og_description',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][og_description]', 'rows' => 3, 'maxlength' => 150)); ?>
                                                    <div class="input-group-addon"><div class="badge">0</div></div>
                                                </div>
                                                <div class="help-block"></div>
                                            </div>


                                            <div class="form-group group-og_image_<?= $lang->code ?> <?php echo  $this->error($i18nModel[$lang->id], 'og_image') ?> wel2l">
                                                <?php echo $form->labelEx($i18nModel[$lang->id],'og_image', array('class' => 'control-label')); ?>

                                                <div class="input-group">
                                                    <?php echo $form->textField($i18nModel[$lang->id], 'og_image', array('class'=>'form-control image-src', 'id' => 'og_image_'.$lang->code, 'name' => $class.'I18n['.$lang->id.'][og_image]')); ?>
                                                    <div class="input-group-addon">
                                                        <a href="#" data-field="og_image" data-lang="<?= $lang->code ?>" class="btn btn-default btn-xs open-filemanager">
                                                            <span class="glyphicon glyphicon-picture"></span>
                                                        </a>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

							<div class="form-group group-short_<?= $lang->code ?> <?php if(isset($errors['short'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'short', array('class' => 'control-label')); ?>
								<?php echo $form->textArea($i18nModel[$lang->id],'short',array('class'=>'form-control rte', 'name' => $class.'I18n['.$lang->id.'][short]', 'id' => 'short_'.$lang->code)); ?>
								<?php echo $form->error($i18nModel[$lang->id],'short', array('class' => 'help-block')); ?>
								<div class="help-block"></div>
							</div>

							<div class="form-group group-body_<?= $lang->code ?> <?php if(isset($errors['body'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'body', array('class' => 'control-label')); ?>
								<?php echo $form->hiddenField($i18nModel[$lang->id],'body',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][body]', 'id' => 'body_'.$lang->code)); ?>

								<div id="contentarea_<?= $lang->code ?>" class="container inner-content-container editor">
									<?php echo $i18nModel[$lang->id]->body; ?>
								</div>

								<?php //echo $form->error($i18nModel[$lang->id],'body', array('class' => 'help-block')); ?>
								<div class="help-block"></div>
							</div>


						</div>
					<?php endforeach; ?>
				</div>

			</div>


			<hr/>

		</div>

		<div class="col-md-3">


			<div class="form-group group-date <?php echo  $this->error($model, 'news_date') ?>">
				<?php echo $form->labelEx($model,'news_date', array('class' => 'control-label')); ?>
				<?php echo $form->textField($model,'news_date',array('class'=>'form-control', 'id' => 'news_date')); ?>
				<?php //echo $form->error($model,'date', array('class' => 'help-block')); ?>
				<div class="help-block"></div>
			</div>

			<div class="form-group group-time <?php echo  $this->error($model, 'news_time') ?>">
				<?php echo $form->labelEx($model,'news_time', array('class' => 'control-label')); ?>
				<?php echo $form->textField($model,'news_time',array('class'=>'form-control', 'id' => 'news_time')); ?>
				<?php //echo $form->error($model,'time', array('class' => 'help-block')); ?>
				<div class="help-block"></div>
			</div>


            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'is_archive'));
                echo $form->checkBox($model,'is_archive', array('id' => 'is_archive', 'value' => 1));
                echo $model->getAttributeLabel('is_archive');
                echo CHtml::closeTag('label');
                ?>
            </div>

			<div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
			</div>

		</div>
	</div>



<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="modal fade" id="file-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Файловый менеджер</h4>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" data-type="" id="pageBrowse" data-lang="" width="100%" height="500"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
foreach($this->languages as $lang) {
    $this->widget('ContentBuilderWidget', array('selector' => '#contentarea_'.$lang->code, 'fileSelectUrl' => url_to('manage/files/export') ));
}
?>


<script>
	var fired = 0;
	var inProgress = false;
	var languages = <?php echo CJSON::encode($codes); ?>;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';
    var $btn;

	$(function(){

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>

		$( "#news_date" ).datepicker({
			showAnim: "show",
			dateFormat: "yy-mm-dd"
		});

		$('#news_time').clockpicker({
			placement: 'bottom',
			align: 'left',
			donetext: '<span>Установить время</span>'
		});

		$('#news-form').submit(function(e){
			e.preventDefault();

			fired = 0;
			log('submitted');
			if( !inProgress ) {
				inProgress = true;
				showLoader();
                $btn = $('.btn-save').button('loading');

				// reset errors and error messages
				$('.has-error').removeClass('has-error');
				$('a.error').removeClass('error');
				$('.help-block').each(function () {
					$(this).text('');
				});

				saveImages(function () {
					populateBody();
					savePage();
				});
			} else {
				log('already in progress');
			}

		});

        $('.open-filemanager').click(function(e){
            e.preventDefault();
            $('#pageBrowse').attr('data-field', $(this).data('field')).attr('data-lang', $(this).data('lang'));
            $('#pageBrowse').attr('src', cbFilesURL).load(function () {
                $('#file-modal').modal({});
            });
        });
	});


	/**
	 * Сохраняем бинарники на сервер и вызываем колбек дальше по цепочке
	 *
	 * @param callback callable function
	 */
	function saveImages(callback) {
		log('saveImage');
		for(var i in languages) {
			$("#contentarea_"+languages[i]).saveimages({
				handler: '<?= url_to('manage/company/vacancy/saveimage') ?>',
				onComplete: function () {
					fired++;
					if( fired == languages.length ) {
						callback();
					}
				}
			});
			$("#contentarea_"+languages[i]).data('saveimages').save();
		}
	}

	function savePage() {
		log('saving page');
		var $form = $('#news-form');
		$.ajax({
			url: $form.attr('action'),
			data: $form.serialize(),
			dataType: 'json',
			type: $form.attr('method'),
			success: function(resp) {
				log('success');
				if( resp.status == 'ok' ) {
					top.location.href = resp.redirect;
				} else {
                    $btn.button('reset');
					for(var i in resp.errors) {
						console.log(i);
						$('.group-'+i).find('.help-block').text(resp.errors[i]);
						$('.group-'+i).addClass('has-error');
					}
					for(var i in resp.langs) {
						$('a.'+resp.langs[i]).addClass('error');
					}
				}
				hideLoader();
				inProgress = false;
			},
			error: function() {
				log('error');
				hideLoader();
				inProgress = false;
			}
		});
	}

	function populateBody() {
		log('populate body');
		for(var i in languages) {
			$('#body_'+languages[i]).val( $('#contentarea_'+languages[i]).data('contentbuilder').html() );
		}
	}
</script>