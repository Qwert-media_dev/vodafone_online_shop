<?php
/* @var $this NewsController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Вакансии'=>array('admin'),
    'Корзина',
);

$this->menu=array(
    array('label'=>'Добавить вакансию', 'url'=>array('create')),
    array('label'=>'Список вакансий', 'url'=>array('admin')),
    array('label'=>'Архив', 'url'=>array('archive')),
);

?>

    <h1>Список удаленных вакансий</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'news-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        array(
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ),
        'news_dt',
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{restore}',
            'buttons' => array(
                'restore' => array(
                    'label' => 'Восстановить',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("manage/company/vacancy/restore", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-success btn-xs',
                        'confirm' => 'Вы уверены ?'
                    )
                ),
            )
        ),
    ),
));
