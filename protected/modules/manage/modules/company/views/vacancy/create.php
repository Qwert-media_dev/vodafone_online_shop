<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $class string */
/* @var $i18nModel NewsI18n[] */
/* @var $codes array */

$this->breadcrumbs=array(
	'Вакансии'=>array('admin'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список вакансий', 'url'=>array('admin')),
	array('label'=>'Архив', 'url'=>array('archive')),
    array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Создание вакансии</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'codes')); ?>