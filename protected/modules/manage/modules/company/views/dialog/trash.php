<?php
/* @var $this DialogController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Диалоги'=>array('admin'),
    'Корзина',
);

$this->menu=array(
    array('label'=>'Добавить диалог', 'url'=>array('create')),
    array('label'=>'Список диалогов', 'url'=>array('admin')),
    //array('label'=>'Архив', 'url'=>array('archive')),
);

?>

<h1>Список удаленных диалогов</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'news-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        array(
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ),
        'news_dt',
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{restore}',
            'buttons' => array(
                'restore' => array(
                    'label' => 'Восстановить',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("manage/company/dialog/restore", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-success btn-xs',
                        'confirm' => 'Вы уверены ?'
                    )
                ),
            )
        ),
    ),
));
