<?php
/* @var $this ЫукмшсуыController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Услуги' => array('admin'),
    'Корзина',
);

$this->menu=array(
    array('label'=>'Добавить услугу', 'url'=>array('create')),
    array('label'=>'Список услуг', 'url'=>array('admin')),
);

?>

<h1>Список удаленных услуг</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'services-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        [
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ],
        array(
            'header' => 'Geo',
            'type' => 'raw',
            'value' => 'GeoHelper::instance()->getServiceGeo($data->id)'
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{restore}',
            'buttons' => array(
                'restore' => array(
                    'label' => 'Восстановить',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("manage/company/services/restore", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-success btn-xs',
                        'confirm' => 'Вы уверены ?'
                    )
                ),
            )
        ),
    ),
));
