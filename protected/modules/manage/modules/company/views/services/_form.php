<?php
/* @var $this ServicesController */
/* @var $model Services */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel ServicesI18n[] */
/* @var $codes array */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */

$modelErrors = $model->getErrors();
?>

<div class="form">

    <?php

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'services-form',
        'action' => array('save'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
    if (!$model->getIsNewRecord()) {
        echo $form->hiddenField($model, 'id');
    }
    echo CHtml::hiddenField($class.'[dummy]', 1);
    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-md-9">


            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach ($this->languages as $lang): ?>
                        <li>
                            <a href="#<?= $lang->code ?>" class="<?= $lang->code ?>" aria-controls="<?= $lang->code ?>"
                               role="tab" data-toggle="tab">
                                <?= $lang->title ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach ($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?= $lang->code ?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors(); ?>

                            <br/>

                            <div class="checkbox" style="margin-top: 30px;">
                                <?php
                                echo CHtml::openTag('label', array('for' => 'status_' . $lang->code));
                                echo $form->checkBox($i18nModel[$lang->id], 'status', array('id' => 'status_' . $lang->code, 'value' => 1, 'name' => $class . 'I18n[' . $lang->id . '][status]'));
                                echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                echo CHtml::closeTag('label');
                                ?>
                            </div>

                            <div
                                class="form-group group-content_<?= $lang->code ?> <?php if (isset($errors['content'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id], 'content', array('class' => 'control-label')); ?>
                                <?php echo $form->hiddenField($i18nModel[$lang->id], 'content', array('class' => 'form-control', 'name' => $class . 'I18n[' . $lang->id . '][content]', 'id' => 'content_' . $lang->code)); ?>

                                <div id="contentarea_<?= $lang->code ?>"
                                     class="container inner-content-container editor">
                                    <?php echo $i18nModel[$lang->id]->content; ?>
                                </div>

                                <?php //echo $form->error($i18nModel[$lang->id],'content', array('class' => 'help-block')); ?>
                                <div class="help-block"></div>
                            </div>

                        </div>
                    <?php endforeach; ?>
                </div>

            </div>


            <hr/>

            <?php $this->widget('GeoSelectorWidget', ['geo' => $geo, 'geoData' => $geoData]); ?>

            <?php $this->widget('GeoExcludeSelectorWidget', ['geo' => $geoExclude, 'geoData' => $geoData]); ?>

        </div>

        <div class="col-md-3 right-column">

            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
            </div>

        </div>
    </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->


<?php
foreach ($this->languages as $lang) {
    $this->widget('ContentBuilderWidget', array('selector' => '#contentarea_' . $lang->code, 'fileSelectUrl' => url_to('manage/files/export')));
}
?>


<script>
    var fired = 0;
    var inProgress = false;
    var languages = <?php echo CJSON::encode($codes); ?>;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';
    var $btn;

    $(function () {

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>


        $('#services-form').submit(function (e) {
            e.preventDefault();

            fired = 0;
            log('submitted');
            if (!inProgress) {

                inProgress = true;
                showLoader();
                $btn = $('.btn-save').button('loading');

                // reset errors and error messages
                $('.has-error').removeClass('has-error');
                $('a.error').removeClass('error');
                $('.help-block').each(function () {
                    $(this).text('');
                });

                saveImages(function () {
                    populateBody();
                    savePage();
                });
            } else {
                log('already in progress');
            }

        });

    });
    


    /**
     * Сохраняем бинарники на сервер и вызываем колбек дальше по цепочке
     *
     * @param callback callable function
     */
    function saveImages(callback) {
        log('saveImage');
        for (var i in languages) {
            $("#contentarea_" + languages[i]).saveimages({
                handler: '<?= url_to('manage/company/services/saveimage') ?>',
                onComplete: function () {
                    fired++;
                    if (fired == languages.length) {
                        callback();
                    }
                }
            });
            $("#contentarea_" + languages[i]).data('saveimages').save();
        }
    }

    function savePage() {
        log('saving page');
        var $form = $('#services-form');
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: $form.attr('method'),
            success: function (resp) {
                log('success');
                if (resp.status == 'ok') {
                    top.location.href = resp.redirect;
                } else {
                    $btn.button('reset');
                    for (var i in resp.errors) {
                        console.log(i);
                        $('.group-' + i).find('.help-block').text(resp.errors[i]);
                        $('.group-' + i).addClass('has-error');
                    }
                    for (var i in resp.langs) {
                        $('a.' + resp.langs[i]).addClass('error');
                    }
                }
                hideLoader();
                inProgress = false;
            },
            error: function () {
                log('error');
                hideLoader();
                inProgress = false;
            }
        });
    }

    function populateBody() {
        log('populate body');
        for (var i in languages) {
            $('#content_' + languages[i]).val($('#contentarea_' + languages[i]).data('contentbuilder').html());
        }
    }
</script>