<?php
/* @var $this ServicesController */
/* @var $model Services */
/* @var $class string */
/* @var $i18nModel ServicesI18n[] */
/* @var $codes array */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */

$this->breadcrumbs = array(
    'Услуги' => array('admin'),
    'Создание',
);

$this->menu = array(
    array('label' => 'Список услуг', 'url' => array('admin')),
    array('label' => 'Корзина', 'url' => array('trash')),
);
?>

<h1>Создание услуги</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'codes', 'geo', 'geoData', 'geoExclude')); ?>