<?php
/* @var $this ServicesController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs = array(
    'Услуги' => array('admin'),
    'Список услуг',
);

$this->menu = array(
    array('label' => 'Добавить услугу', 'url' => array('create')),
    array('label' => 'Корзина', 'url' => array('trash')),
)

?>


<h1>Список услуг</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'services-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        [
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ],
        array(
            'header' => 'Geo',
            'type' => 'raw',
            'value' => 'GeoHelper::instance()->getServiceGeo($data->id)'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 280, 'align' => 'center'),
            'template' => '{clone} {update} {delete}',
            'buttons' => array(
                'clone' => array(
                    'imageUrl' => false,
                    'label' => '<span class="glyphicon glyphicon-duplicate"></span>',
                    'url' => 'url_to("manage/company/services/clone", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-default btn-xs'
                    )
                ),
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
        ),
    ),
));
