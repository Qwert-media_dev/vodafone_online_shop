<?php
/* @var $this ServicesController */
/* @var $model Services */
/* @var $class string */
/* @var $i18nModel ServicesI18n[] */
/* @var $codes array */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */


$this->breadcrumbs = array(
    'Услуги' => array('admin'),
    $model->id => array('update', 'id' => $model->id),
    'Редактирование',
);

$this->menu = array(
    array('label' => 'Добавить услугу', 'url' => array('create')),
    array('label' => 'Список услуг', 'url' => array('admin')),
    array('label' => 'Корзина', 'url' => array('trash')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'codes', 'geo', 'geoData', 'geoExclude')); ?>