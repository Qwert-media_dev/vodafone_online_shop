<?php
/* @var $item Pages */
/* @var $level integer */
?>
<tr class="item<?= $item->id ?>">
	<td>
		<div class="shift" style="width: <?= $level*40 ?>px"></div>

		<?php if($item->type == Pages::TYPE_LINK): ?>
			<span class="glyphicon glyphicon-link"></span>
		<?php elseif($item->type == Pages::TYPE_MODULE): ?>
			<span class="glyphicon glyphicon-cog"></span>
		<?php elseif($item->type == Pages::TYPE_NOT_A_LINK): ?>
			<span class="glyphicon glyphicon-random"></span>
		<?php else: ?>
			<span class="glyphicon glyphicon-file"></span>
		<?php endif;?>

		<?= $item->title ?>

		<?php if( Pages::model()->countByAttributes(['parent_id' => $item->id]) ): ?>
			<a href="<?= url_to('manage/page/pages/more', ['parent_id' => $item->id, 'level' => $level]) ?>"class="load-more"><span class="glyphicon glyphicon-chevron-down"></span></a>
		<?php endif; ?>

		<?php if($item->in_new_window && $item->type == Pages::TYPE_LINK ): ?>
			<span class="glyphicon glyphicon-new-window"></span>
		<?php endif; ?>
	</td>
	<td>
		<?php if($item->is_tabbed_menu): ?>
			<span class="glyphicon glyphicon-option-horizontal"></span>
		<?php endif; ?>
	</td>
	<td><?= displayStatuses($item->statuses) ?></td>
	<td align="right">
		<?php
		$editLink = /*$item['type'] == Pages::TYPE_LINK ? array('link/update', 'id' => $item['id']) :*/ array('update', 'id' => $item->id);
		echo CHtml::link('Редактировать', $editLink, array('class' => 'btn btn-info btn-xs')), ' ';

		if( !($item->type == Pages::TYPE_MODULE || $item->protected) ) {
			echo CHtml::link(
				'Удалить',
				array('delete', 'id' => $item->id),
				array(
					'class' => 'btn btn-danger btn-xs delete',
					'data-loading-text' => 'Загрузка...',
					'data-childs' => url_to('manage/page/pages/childs', array('id' => $item->id))
				)
			);
		}
		?>
	</td>
</tr>