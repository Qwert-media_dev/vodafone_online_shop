<?php
/* @var $this PagesController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
	'Страницы'=>array('tree'),
	'Корзина',
);

$this->menu=array(
	array('label'=>'Добавить страницу', 'url'=>array('create')),
	array('label'=>'Список страниц', 'url'=>array('tree')),
);

?>

<h1>Список удаленных страниц</h1>


	<div class="modal fade" id="parentModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">Куда будем восстанавливать ?</h4>
				</div>
				<div class="modal-body">

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
					<button type="button" class="btn btn-success confirm-restore">Да, я хочу восстановить!</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pages-grid',
	'dataProvider' => $provider,
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter' => $model,
	'columns' => array(
		'title',
		array(
			'name' => 'statuses',
			'type' => 'raw',
			'value' => 'displayStatuses($data->statuses)'
		),
		'updated_at',
		array(
			'class'=>'CButtonColumn',
			'htmlOptions' => array('width' => 240, 'align' => 'center'),
			'template' => '{restore}',
			'buttons' => array(
				'restore' => array(
					'label' => 'Восстановить',
					'imageUrl' => false,
					'url' => 'Yii::app()->createUrl("manage/page/pages/restore", array("id" => $data->id))',
					'options' => array(
						'class' => 'btn btn-success btn-xs restore',
						//'confirm' => 'Вы уверены ?',
						'data-loading-text' => 'Загрузка...',
					)
				),
			)
		),
	),
));

?>

<script>
	var $btn;
	var $modal = $('#parentModal');
	$(function(){
		$('.restore').click(function(e){
			e.preventDefault();
			var self = this;
			$btn = $(this).button('loading');
			$.get('<?= url_to('manage/page/pages/structure') ?>', function(resp){
				$modal.attr('data-href', $(self).attr('href') );
				$modal.find('.modal-body').html(resp.content);
				$modal.modal({});
				$btn.button('reset');
			}, 'json');
		});

		$modal.on('hidden.bs.modal', function (e) {
			$modal.removeAttr('data-href');
			$btn.button('reset');
		});

		$('.confirm-restore').click(function(e){
			e.preventDefault();
			$.post( $modal.data('href'), {parent_id: $('#parent_id').val()}, function(resp){
				if(resp.status == 'ok') {
					top.location.reload(true);
				} else {
					alert('WARNING! Системная ошибка');
				}
			}, 'json' );
		});
	});
</script>
