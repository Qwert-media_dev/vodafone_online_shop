<?php
/* @var $this PagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Страницы',
);

$this->menu=array(
	//array('label'=>'Добавить страницу', 'url'=>array('create')),
	//array('label'=>'Список страниц', 'url'=>array('index')),
);
?>

<h1>Страницы</h1>

<div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        Создать
        <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a href="<?= url_to('manage/page/pages/create'); ?>">Контентная</a></li>
        <li><a href="<?= url_to('manage/page/link/create'); ?>">Ссылочная</a></li>
    </ul>
</div>

<link href="/js/fancytree/skin-bootstrap-n/ui.fancytree.css" rel="stylesheet" type="text/css" class="skinswitcher">

<script src="/js/fancytree/jquery.fancytree.js" type="text/javascript"></script>
<script src="/js/fancytree/jquery.fancytree.dnd.js" type="text/javascript"></script>
<script src="/js/fancytree/jquery.fancytree.edit.js" type="text/javascript"></script>
<script src="/js/fancytree/jquery.fancytree.glyph.js" type="text/javascript"></script>
<script src="/js/fancytree/jquery.fancytree.table.js" type="text/javascript"></script>
<script src="/js/fancytree/jquery.fancytree.wide.js" type="text/javascript"></script>

<script src="//cdn.jsdelivr.net/jquery.ui-contextmenu/1.8.2/jquery.ui-contextmenu.min.js" type="text/javascript"></script>

<style type="text/css">
	.ui-menu {
		width: 100px;
		font-size: 63%;
		z-index: 3; /* over ext-wide titles */
	}
</style>

<script>
	glyph_opts = {
		map: {
			doc: "glyphicon glyphicon-file",
			docOpen: "glyphicon glyphicon-file",
			checkbox: "glyphicon glyphicon-unchecked",
			checkboxSelected: "glyphicon glyphicon-check",
			checkboxUnknown: "glyphicon glyphicon-share",
			dragHelper: "glyphicon glyphicon-play",
			dropMarker: "glyphicon glyphicon-arrow-right",
			error: "glyphicon glyphicon-warning-sign",
			expanderClosed: "glyphicon glyphicon-plus-sign",
			expanderLazy: "glyphicon glyphicon-plus-sign",  // glyphicon-expand
			expanderOpen: "glyphicon glyphicon-minus-sign",  // glyphicon-collapse-down
			folder: "glyphicon glyphicon-folder-close",
			folderOpen: "glyphicon glyphicon-folder-open",
			loading: "glyphicon glyphicon-refresh"
		}
	};

	$(function(){
		$("#tree").fancytree({
			extensions: ["dnd", "edit", "glyph", "wide"],
			checkbox: false,
			debugLevel: 0,
			dnd: {
				focusOnClick: true,
				dragStart: function(node, data) { return true; },
				dragEnter: function(node, data) { return false; },
				dragDrop: function(node, data) { data.otherNode.copyTo(node, data.hitMode); }
			},
			glyph: glyph_opts,
			selectMode: 2,
			source: {url: "<?= url_to('manage/page/pages/getlevel') ?>"},
			toggleEffect: { effect: "drop", options: {direction: "left"}, duration: 400 },
			wide: {
				iconWidth: "1em",     // Adjust this if @fancy-icon-width != "16px"
				iconSpacing: "0.5em", // Adjust this if @fancy-icon-spacing != "3px"
				levelOfs: "1.5em"     // Adjust this if ul padding != "16px"
			},
			iconClass: function(event, data){
				// if( data.node.isFolder() ) {
				// 	return "glyphicon glyphicon-book";
				// }
			},
			activate: function(event, data) {
				//alert('activate ' + data.node.key);
			},
			lazyLoad: function(event, data) {
				var node = data.node;
				data.result = {url: "<?= url_to('manage/page/pages/getlevel') ?>", data: {key: node.key}};
			}
		});

		var path = '<?= url_to('manage/page/pages/go') ?>';
		$("#tree").contextmenu({
			delegate: "span.fancytree-title",
			menu: [
				{title: "Create", cmd: "create", uiIcon: "ui-icon-pencil" },
				{title: "Edit", cmd: "edit", uiIcon: "ui-icon-pencil" },
				{title: "Delete", cmd: "delete", uiIcon: "ui-icon-trash"}
			],
			beforeOpen: function(event, ui) {
				var node = $.ui.fancytree.getNode(ui.target);
				node.setActive();
			},
			select: function(event, ui) {
				var node = $.ui.fancytree.getNode(ui.target);
				top.location.href = path + '?cmd=' + ui.cmd + '&key=' + node.key;
			}
		});
	});
</script>


<div id="tree" class="panel-body fancytree-colorize-hover"></div>