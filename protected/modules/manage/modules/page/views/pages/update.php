<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $menu array */
/* @var $class string */

$this->breadcrumbs=array(
	'Страницы'=>array('tree'),
	$model->i18n[0]->title,
	'Редактирование',
);

$this->menu=array(
        array('label'=>'Добавить страницу', 'url'=>array('create')),
        array('label'=>'Список страниц', 'url'=>array('tree')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'i18nModel', 'class', 'codes', 'menu')); ?>