<?php
/* @var $flat array */
/* @var $main Pages */
?>

<p class="alert alert-danger"><b>Вы пытаетесь удалить следующие страницы.</b></p>

<ul class="table table-hover">
	<li><?= $main->title ?></li>
<?php foreach($flat as $item): ?>
	<li style="padding-left: <?= ($item['level']*25)+20 ?>px">
		<?= $item['title'] ?>
	</li>
<?php endforeach; ?>
</ul>

<p  class="alert alert-info">Удаленные страницы можно будет найти в <a href="<?= url_to('manage/page/pages/trash') ?>"><b>Корзине</b></a></p>