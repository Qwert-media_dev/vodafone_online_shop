<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $i18nModel PagesI18n[] */
/* @var $form CActiveForm */
/* @var $codes array */
/* @var $menu array */
/* @var $class string */

$modelErrors = $model->getErrors();
?>


<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'pages-form',
        'action' => array('save'),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));

    echo $form->hiddenField($model, 'dummy', array('value' => 1));
    echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
    if( !$model->getIsNewRecord() ) {
        echo $form->hiddenField($model, 'id');
    }

    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php echo $form->errorSummary($model, '<div class="alert alert-success" role="alert">', '</div>'); ?>

    <div class="row">
        <div class="col-md-9">


            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach($this->languages as $lang): ?>
                        <li><a href="#<?=$lang->code?>" class="<?= $lang->code ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab"><?=$lang->title?></a></li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors();?>

                            <br/>

                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group group-title_<?= $lang->code ?> <?php if(isset($errors['title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]', 'id' => 'title_'.$lang->code)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
                                </div>
                            </div>


                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $lang->code ?>" aria-expanded="true" aria-controls="collapseOne">
                                                Настройки
                                                <span class="caret"></span>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse<?= $lang->code ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">

                                            <div class="form-group vis-short group-short_title_<?= $lang->code ?> <?php if(isset($errors['short_title'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id],'short_title', array('class' => 'control-label')); ?>
                                                <?php echo $form->textField($i18nModel[$lang->id],'short_title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][short_title]', 'id' => 'short_title_'.$lang->code)); ?>
                                                <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                                <div class="help-block"></div>
                                            </div>


                                            <div class="form-group group-alias_<?= $lang->code ?> <?php if(isset($errors['alias'])) echo ' has-error'; ?>">
                                                <?php echo $form->labelEx($i18nModel[$lang->id],'alias', array('class' => 'control-label')); ?>
                                                <?php echo $form->textField($i18nModel[$lang->id],'alias',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][alias]', 'rows' => 3)); ?>
                                                <?php //echo $form->error($i18nModel[$lang->id],'alias', array('class' => 'help-block')); ?>
                                                <div class="help-block"></div>
                                            </div>


                                            <div class="panel panel-default vis-seo">
                                                <div class="panel-heading">SEO</div>
                                                <div class="panel-body">

                                                    <div class="form-group group-seo_title_<?= $lang->code ?> <?php if(isset($errors['seo_title'])) echo ' has-error'; ?>">
                                                        <?php echo $form->labelEx($i18nModel[$lang->id],'seo_title', array('class' => 'control-label')); ?>
                                                        <div class="input-group">
                                                            <?php echo $form->textField($i18nModel[$lang->id],'seo_title',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][seo_title]', 'maxlength' => 70)); ?>
                                                            <div class="input-group-addon"><div class="badge">0</div></div>
                                                        </div>
                                                        <div class="help-block"></div>
                                                    </div>

                                                    <div class="form-group group-seo_description_<?= $lang->code ?> <?php if(isset($errors['seo_description'])) echo ' has-error'; ?>">
                                                        <?php echo $form->labelEx($i18nModel[$lang->id],'seo_description', array('class' => 'control-label')); ?>
                                                        <div class="input-group">
                                                            <?php echo $form->textField($i18nModel[$lang->id],'seo_description',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][seo_description]', 'rows' => 3, 'maxlength' => 150)); ?>
                                                            <div class="input-group-addon"><div class="badge">0</div></div>
                                                        </div>
                                                        <div class="help-block"></div>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="panel panel-default vis-og">
                                                <div class="panel-heading">Share</div>
                                                <div class="panel-body">

                                                    <div class="form-group group-og_title_<?= $lang->code ?> <?php if(isset($errors['og_title'])) echo ' has-error'; ?>">
                                                        <?php echo $form->labelEx($i18nModel[$lang->id],'og_title', array('class' => 'control-label')); ?>
                                                        <div class="input-group">
                                                            <?php echo $form->textField($i18nModel[$lang->id],'og_title',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][og_title]', 'maxlength' => 70)); ?>
                                                            <div class="input-group-addon"><div class="badge">0</div></div>
                                                        </div>
                                                        <div class="help-block"></div>
                                                    </div>

                                                    <div class="form-group group-og_description_<?= $lang->code ?> <?php if(isset($errors['og_description'])) echo ' has-error'; ?>">
                                                        <?php echo $form->labelEx($i18nModel[$lang->id],'og_description', array('class' => 'control-label')); ?>
                                                        <div class="input-group">
                                                            <?php echo $form->textField($i18nModel[$lang->id],'og_description',array('class'=>'form-control counter', 'name' => $class.'I18n['.$lang->id.'][og_description]', 'rows' => 3, 'maxlength' => 150)); ?>
                                                            <div class="input-group-addon"><div class="badge">0</div></div>
                                                        </div>
                                                        <div class="help-block"></div>
                                                    </div>


                                                    <div class="form-group group-og_image_<?= $lang->code ?> <?php echo  $this->error($i18nModel[$lang->id], 'og_image') ?> wel2l">
                                                        <?php
                                                        $uploadUrl = url_to('manage/page/pages/upload');
                                                        $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'og_image', 'fieldKey' => 'og_image_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '1200x630 jpg,gif,png', 'url' => $uploadUrl));
                                                        ?>
                                                        <!--div class="input-group">
                                                            <?php //echo $form->textField($i18nModel[$lang->id], 'og_image', array('class'=>'form-control image-src', 'id' => 'og_image_'.$lang->code, 'name' => $class.'I18n['.$lang->id.'][og_image]')); ?>
                                                            <div class="input-group-addon">
                                                                <a href="#" data-field="og_image" data-lang="<?= $lang->code ?>" class="btn btn-default btn-xs open-filemanager">
                                                                    <span class="glyphicon glyphicon-picture"></span>
                                                                </a>
                                                            </div>
                                                        </div-->


                                                    </div>

                                                </div>
                                            </div>

                                            <hr>

                                            <div class="panel panel-default vis-promo">
                                                <div class="panel-heading">Промо картинки</div>
                                                <div class="panel-body">

                                                    <?php
                                                    $uploadUrl = url_to('manage/default/upload');
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic1', 'fieldKey' => 'pic1_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '2600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic2', 'fieldKey' => 'pic2_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '1600x506 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic3', 'fieldKey' => 'pic3_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '980x360 jpg,gif,png', 'url' => $uploadUrl));
                                                    $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic4', 'fieldKey' => 'pic4_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '660x500 jpg,gif,png', 'url' => $uploadUrl));
                                                    ?>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group vis-body group-body_<?= $lang->code ?> <?php if(isset($errors['body'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id],'body', array('class' => 'control-label')); ?>
                                <?php echo $form->hiddenField($i18nModel[$lang->id],'body',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][body]', 'id' => 'body_'.$lang->code)); ?>

                                <div id="contentarea_<?= $lang->code ?>" class="container inner-content-container editor">
                                    <?php echo $i18nModel[$lang->id]->body; ?>
                                </div>

                                <div class="help-block"></div>
                            </div>

                            <?php if($model->type == Pages::TYPE_MODULE): ?>
                            <hr>
                                <div class="well">
                                    <h1 class="text-center">Модульный блок</h1>
                                </div>

                            <div class="form-group group-body2_<?= $lang->code ?> <?php if(isset($errors['body2'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id],'body2', array('class' => 'control-label')); ?>
                                <?php echo $form->hiddenField($i18nModel[$lang->id],'body2',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][body2]', 'id' => 'body2_'.$lang->code)); ?>

                                <div id="contentarea2_<?= $lang->code ?>" class="container inner-content-container editor">
                                    <?php echo $i18nModel[$lang->id]->body2; ?>
                                </div>

                                <div class="help-block"></div>
                            </div>
                            <?php endif; ?>

                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

            <hr/>

        </div>

        <div class="col-md-3 right-column" style="background-color: white">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOptions" aria-expanded="false" aria-controls="collapseOne" class="collapsed">
                Настройки cтраницы
                <span class="caret"></span>
            </a>
            <div id="collapseOptions" class="collapse">
                <div class="form-group">
                    <?php
                    echo $form->labelEx($model, 'parent_id');
                    $parent_id  = $model->parent_id;
                    if( $model->isNewRecord == 'create' && $_GET['parent_id'] > 0 ) {
                        $parent_id = $_GET['parent_id'];
                    }
                    echo getMenuSelect($class, $menu, $parent_id);
                    ?>
                </div>

                <div class="form-group group-date <?php echo  $this->error($model, 'sort') ?>">
                    <?php echo $form->labelEx($model,'sort', array('class' => 'control-label')); ?>
                    <?php echo $form->textField($model,'sort',array('class'=>'form-control', 'id' => 'sort')); ?>
                    <div class="help-block"></div>
                </div>

                <?php if($model->type == Pages::TYPE_MODULE): ?>
                    <?php echo $form->hiddenField($model, 'type'); ?>
                <?php else: ?>
                    <div class="well">
                        <label for="">Тип страницы</label>

                        <div class="radio">
                            <?php
                            echo CHtml::openTag('label', array('for' => 'type_'.Pages::TYPE_PAGE));
                            echo CHtml::radioButton($class.'[type]', $model->type == Pages::TYPE_PAGE, array('value' => Pages::TYPE_PAGE, 'id' => 'type_'.Pages::TYPE_PAGE, 'class' => 'type-trigger'));
                            echo param('page_types')[Pages::TYPE_PAGE];
                            echo CHtml::closeTag('label');
                            ?>
                        </div>

                        <div class="radio">
                            <?php
                            echo CHtml::openTag('label', array('for' => 'type_'.Pages::TYPE_LINK));
                            echo CHtml::radioButton($class.'[type]', $model->type == Pages::TYPE_LINK, array('value' => Pages::TYPE_LINK, 'id' => 'type_'.Pages::TYPE_LINK, 'class' => 'type-trigger'));
                            echo param('page_types')[Pages::TYPE_LINK];
                            echo CHtml::closeTag('label');
                            ?>
                        </div>

                        <div class="radio">
                            <?php
                            echo CHtml::openTag('label', array('for' => 'type_'.Pages::TYPE_NOT_A_LINK));
                            echo CHtml::radioButton($class.'[type]', $model->type == Pages::TYPE_NOT_A_LINK, array('value' => Pages::TYPE_NOT_A_LINK, 'id' => 'type_'.Pages::TYPE_NOT_A_LINK, 'class' => 'type-trigger'));
                            echo param('page_types')[Pages::TYPE_NOT_A_LINK];
                            echo CHtml::closeTag('label');
                            ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="checkbox vis-new-window" style="display: none;">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'in_new_window'));
                    echo $form->checkBox($model,'in_new_window', array('id' => 'in_new_window', 'value' => 1));
                    echo $model->getAttributeLabel('in_new_window');
                    echo CHtml::closeTag('label');
                    ?>
                </div>

                <div class="checkbox">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'hide_in_menu'));
                    echo $form->checkBox($model,'hide_in_menu', array('id' => 'hide_in_menu', 'value' => 1));
                    echo $model->getAttributeLabel('hide_in_menu');
                    echo CHtml::closeTag('label');
                    ?>
                </div>

                <div class="checkbox">
                    <?php
                    echo CHtml::openTag('label', array('for' => 'is_tabbed_menu'));
                    echo $form->checkBox($model,'is_tabbed_menu', array('id' => 'is_tabbed_menu', 'value' => 1));
                    echo $model->getAttributeLabel('is_tabbed_menu');
                    echo CHtml::closeTag('label');
                    ?>
                </div>

                <?php if( user()->email == param('adminEmail') ): ?>
                    <div class="form-group <?php echo  $this->error($model, 'controller') ?>">
                        <?php echo $form->labelEx($model,'controller', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'controller',array('class'=>'form-control', 'id' => 'controller')); ?>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group <?php echo  $this->error($model, 'extra_class') ?>">
                        <?php echo $form->labelEx($model,'extra_class', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'extra_class',array('class'=>'form-control', 'id' => 'extra_class')); ?>
                        <div class="help-block"></div>
                    </div>

                    <div class="checkbox">
                        <?php
                        echo CHtml::openTag('label', array('for' => 'protected'));
                        echo $form->checkBox($model,'protected', array('id' => 'protected', 'value' => 1));
                        echo $model->getAttributeLabel('protected');
                        echo CHtml::closeTag('label');
                        ?>
                    </div>


                    <hr>
                    <div class="form-group <?php echo  $this->error($model, 'app_name') ?>">
                        <?php echo $form->labelEx($model,'app_name', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'app_name',array('class'=>'form-control', 'id' => 'app_name')); ?>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group <?php echo  $this->error($model, 'app_store_id') ?>">
                        <?php echo $form->labelEx($model,'app_store_id', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'app_store_id',array('class'=>'form-control', 'id' => 'app_store_id')); ?>
                        <div class="help-block"></div>
                    </div>

                    <div class="form-group <?php echo  $this->error($model, 'app_package') ?>">
                        <?php echo $form->labelEx($model,'app_package', array('class' => 'control-label')); ?>
                        <?php echo $form->textField($model,'app_package',array('class'=>'form-control', 'id' => 'app_package')); ?>
                        <div class="help-block"></div>
                    </div>
                <?php endif; ?>

                <hr>
                <div class="form-group <?php echo  $this->error($model, 'extra_css') ?>">
                    <?php echo $form->labelEx($model,'extra_css', array('class' => 'control-label')); ?>
                    <?php echo $form->textArea($model,'extra_css',array('class'=>'form-control', 'id' => 'extra_css', 'rows' => 5)); ?>
                    <div class="help-block"></div>
                </div>

                <div class="form-group <?php echo  $this->error($model, 'extra_js') ?>">
                    <?php echo $form->labelEx($model,'extra_js', array('class' => 'control-label')); ?>
                    <?php echo $form->textArea($model,'extra_js',array('class'=>'form-control', 'id' => 'extra_js', 'rows' => 5)); ?>
                    <div class="help-block"></div>
                </div>
            </div>
            
            
            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
            </div>

        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->



<div class="modal fade" id="file-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Файловый менеджер</h4>
            </div>
            <div class="modal-body">
                <iframe src="" frameborder="0" data-type="" id="pageBrowse" data-lang="" width="100%" height="500"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
foreach($this->languages as $lang) {
    $this->widget('ContentBuilderWidget', array('selector' => '#contentarea_'.$lang->code, 'fileSelectUrl' => url_to('manage/files/export') ));
    if( $model->type == Pages::TYPE_MODULE )
        $this->widget('ContentBuilderWidget', array('selector' => '#contentarea2_'.$lang->code, 'fileSelectUrl' => url_to('manage/files/export') ));
}
?>

<script>
    var fired = 0;
    var ratio = <?= $model->type == Pages::TYPE_MODULE ? 2 : 1 ?>;
    var inProgress = false;
    var languages = <?php echo CJSON::encode($codes); ?>;
    var cbFilesURL = '<?php echo url_to('manage/files/export'); ?>';
    var saveImageURL = '<?= url_to('manage/page/pages/saveimage') ?>';
    var $btn;

    $(function() {

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>

        $('.type-trigger').on('change', function(){
            switch( $(this).val() ) {
                case 'link':
                    //$('.vis-seo, .vis-body, .vis-promo').hide();
                    $('.vis-seo, .vis-promo, .vis-og').hide();
                    $('.vis-short').show();
                    $('.vis-new-window').show();
                    break;
                case 'page':
                    $('.vis-seo, .vis-body, .vis-promo, .vis-short, .vis-og').show();
                    $('.vis-new-window').hide();
                    break;
                case 'not-a-link':
                    $('.vis-seo, .vis-body, .vis-promo, .vis-short, .vis-new-window, .vis-og').hide();
                    break;
            }
        });
        $('.type-trigger:checked').trigger('change');

        $('.open-filemanager').click(function(e){
            e.preventDefault();
            $('#pageBrowse').attr('data-field', $(this).data('field')).attr('data-lang', $(this).data('lang'));
            $('#pageBrowse').attr('src', cbFilesURL).load(function () {
                $('#file-modal').modal({});
            });
        });

        $('#pages-form').submit(function(e){
            e.preventDefault();
            var $form = $(this);
            fired = 0;
            if( !inProgress ) {
                inProgress = true;
                showLoader();
                $btn = $('.btn-save').button('loading');

                // reset errors and error messages
                $('.has-error').removeClass('has-error');
                $('a.error').removeClass('error');
                $('.help-block').each(function () {
                    $(this).text('');
                });

                saveImages(function () {
                    populateBody();
                    savePage($form);
                });
            }

        });

    });


    function savePage($form) {
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: $form.attr('method'),
            success: function(resp) {
                if( resp.status == 'ok' ) {
                    top.location.href = resp.redirect;
                } else {
                    $btn.button('reset');
                    for(var i in resp.errors) {
                        $('.group-'+i).find('.help-block').text(resp.errors[i]);
                        $('.group-'+i).addClass('has-error');
                    }
                    for(var i in resp.langs) {
                        $('a.'+resp.langs[i]).addClass('error');
                    }
                }
                hideLoader();
                inProgress = false;
            },
            error: function() {
                hideLoader();
                inProgress = false;
            }
        });
    }

    function populateBody() {
        for(var i in languages) {
            $('#body_'+languages[i]).val( $('#contentarea_'+languages[i]).data('contentbuilder').html() );
            <?php if($model->type == Pages::TYPE_MODULE): ?>
            $('#body2_'+languages[i]).val( $('#contentarea2_'+languages[i]).data('contentbuilder').html() );
            <?php endif; ?>
        }
    }


    /**
     * Сохраняем бинарники на сервер и вызываем колбек дальше по цепочке
     *
     * @param callback callable function
     */
    function saveImages(callback) {
        for(var i in languages) {
            $("#contentarea_"+languages[i]).saveimages({
                handler: saveImageURL,
                onComplete: function () {
                    fired++;
                    if( fired == languages.length*ratio ) {
                        callback();
                    }
                }
            });
            $("#contentarea_"+languages[i]).data('saveimages').save();
            <?php if($model->type == Pages::TYPE_MODULE): ?>
            $("#contentarea2_"+languages[i]).saveimages({
                handler: saveImageURL,
                onComplete: function () {
                    fired++;
                    if( fired == languages.length*ratio ) {
                        callback();
                    }
                }
            });
            $("#contentarea2_"+languages[i]).data('saveimages').save();
            <?php endif; ?>
        }
    }
</script>
