<?php
/* @var $items array */
/* @var $class string */
?>

<div class="form-horizontal">
	<div class="form-group">
		<label class="col-sm-5 control-label">Родительская страница</label>
		<div class="col-sm-7">
			<?php
			echo getMenuSelect($class, $items);
			?>
		</div>
	</div>
</div>