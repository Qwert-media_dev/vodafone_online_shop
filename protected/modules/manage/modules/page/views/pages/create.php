<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $menu array */
/* @var $class string */

$this->breadcrumbs=array(
	'Страницы'=>array('tree'),
	'Добавить страницу',
);

$this->menu=array(
        array('label'=>'Список страниц', 'url'=>array('tree')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Добавить страницу</h1>

<?php $this->renderPartial('_form', compact('model', 'i18nModel', 'class', 'codes', 'menu')); ?>