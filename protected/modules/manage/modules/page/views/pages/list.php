<?php
/* @var $this PagesController */
/* @var $items Pages[] */

$this->breadcrumbs=array(
	'Страницы'=>array('admin'),
	'Управление страницами',
);

$this->menu=array(
        array('label'=>'Добавить страницу', 'url'=>array('create')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);

?>

<div class="modal fade" id="deleteModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Удаление</h4>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				<button type="button" class="btn btn-danger confirm-delete">Все равно удалить</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<h1>Страницы</h1>


<br><br>

<table class="table table-striped table-hover">
	<thead>
	<tr>
		<th>Название</th>
		<th></th>
		<th>Статус публикации</th>
		<th></th>
	</tr>
	</thead>
	<?php
	$level = 0;
	foreach($items as $item) {
		$this->renderPartial('list-item', compact('item', 'level'));
	}
	?>
</table>



<script>
	var $btn;
	var $modal = $('#deleteModal');
	$(function(){

		$(document).on('click', '.load-more', function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			var self = this;
			$.get(href, function(resp){
				if( resp.rows ) {
					$(resp.item).after(resp.content);
				}
				$(self).remove();
			}, 'json');
		});

		$('.delete').click(function(e){
			e.preventDefault();
			var self = this;
			$btn = $(this).button('loading');
			$.get($(this).data('childs'), function(resp){
				$modal.attr('data-href', $(self).attr('href') );
				$modal.find('.modal-body').html(resp.content);
				$modal.modal({});
			}, 'json');
		});

		$modal.on('hidden.bs.modal', function (e) {
			$modal.removeAttr('data-href');
			$btn.button('reset');
		});

		$('.confirm-delete', $modal).click(function(e){
			e.preventDefault();
			if( confirm('Вы действительно хотите удалить \nэту и все связанные с ней страницы?') ) {
				top.location.href = $modal.data('href');
			}
		});
	});
</script>