<?php
/* @var $items array */

$this->menu=array(
        array('label'=>'Добавить страницу', 'url'=>array('create')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);

?>


<div class="modal fade" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Удаление</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-danger confirm-delete">Все равно удалить</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<h1>Страницы</h1>

<br><br>

<table class="table table-striped table-hover">
    <thead>
	<tr>
		<th>Название</th>
		<th></th>
		<th>Статус публикации</th>
		<th></th>
	</tr>
    </thead>
<?php foreach($items as $item): ?>
	<tr>
		<td>
			<div class="shift" style="width: <?= $item['level']*40 ?>px"></div>

            <?php if($item['type'] == Pages::TYPE_LINK): ?>
                <span class="glyphicon glyphicon-link"></span>
            <?php elseif($item['type'] == Pages::TYPE_MODULE): ?>
                <span class="glyphicon glyphicon-cog"></span>
            <?php elseif($item['type'] == Pages::TYPE_NOT_A_LINK): ?>
                <span class="glyphicon glyphicon-random"></span>
            <?php else: ?>
                <span class="glyphicon glyphicon-file"></span>
            <?php endif;?>

			<?= $item['title'] ?>

            <?php if($item['in_new_window'] && $item['type'] == Pages::TYPE_LINK ): ?>
            <span class="glyphicon glyphicon-new-window"></span>
            <?php endif; ?>
		</td>
		<td>
            <?php if($item['is_tabbed_menu']): ?>
            <span class="glyphicon glyphicon-option-horizontal"></span>
            <?php endif; ?>
        </td>
		<td><?= displayStatuses($item['statuses']) ?></td>
		<td align="right">
			<?php
            $editLink = /*$item['type'] == Pages::TYPE_LINK ? array('link/update', 'id' => $item['id']) :*/ array('update', 'id' => $item['id']);
			echo CHtml::link('Редактировать', $editLink, array('class' => 'btn btn-info btn-xs')), ' ';

            if( !($item['type'] == Pages::TYPE_MODULE || $item['protected']) ) {
                echo CHtml::link(
                    'Удалить',
                    array('delete', 'id' => $item['id']),
                    array(
                        'class' => 'btn btn-danger btn-xs delete',
                        'data-loading-text' => 'Загрузка...',
                        'data-childs' => url_to('manage/page/pages/childs', array('id' => $item['id']))
                    )
                );
            }
			?>
		</td>
	</tr>	
<?php endforeach; ?>
</table>


<script>
    var $btn;
    var $modal = $('#deleteModal');
    $(function(){
        $('.delete').click(function(e){
            e.preventDefault();
            var self = this;
            $btn = $(this).button('loading');
            $.get($(this).data('childs'), function(resp){
                $modal.attr('data-href', $(self).attr('href') );
                $modal.find('.modal-body').html(resp.content);
                $modal.modal({});
            }, 'json');
        });

        $modal.on('hidden.bs.modal', function (e) {
            $modal.removeAttr('data-href');
            $btn.button('reset');
        });

        $('.confirm-delete', $modal).click(function(e){
            e.preventDefault();
            if( confirm('Вы действительно хотите удалить \nэту и все связанные с ней страницы?') ) {
                top.location.href = $modal.data('href');
            }
        });
    });
</script>