<?php
/* @var $this PagesController */
/* @var $model Pages */
/* @var $i18nModel PagesI18n[] */
/* @var $form CActiveForm */
/* @var $codes array */
/* @var $menu array */
/* @var $class string */

$modelErrors = $model->getErrors();
?>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'pages-form',
		'action' => array('save'),
		// Please note: When you enable ajax validation, make sure the corresponding
		// controller action is handling ajax validation correctly.
		// There is a call to performAjaxValidation() commented in generated controller code.
		// See class documentation of CActiveForm for details on this.
		'enableAjaxValidation'=>false,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
	));

	echo $form->hiddenField($model, 'dummy', array('value' => 1));

	if( !$model->getIsNewRecord() ) {
		echo $form->hiddenField($model, 'id');
	}

	?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php echo $form->errorSummary($model, '<div class="alert alert-success" role="alert">', '</div>'); ?>


	<div class="row">
		<div class="col-md-9">


			<div role="tabpanel">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="myTab">
					<?php foreach($this->languages as $lang): ?>
						<li><a href="#<?=$lang->code?>" class="<?= $lang->code ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab"><?=$lang->title?></a></li>
					<?php endforeach; ?>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<?php foreach($this->languages as $lang): ?>
						<div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
							<?php $errors = $i18nModel[$lang->id]->getErrors();?>

							<br/>

							<div class="row">
								<div class="col-md-9"><div class="form-group group-title_<?= $lang->code ?> <?php if(isset($errors['title'])) echo ' has-error'; ?>">
										<?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
										<?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]', 'id' => 'title_'.$lang->code)); ?>
										<?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
										<div class="help-block"></div>
									</div>
								</div>
								<div class="col-md-3">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
								</div>
							</div>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group group-alias_<?= $lang->code ?> <?php if(isset($errors['alias'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'alias', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'alias',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][alias]', 'rows' => 3)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'alias', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'hide_alias_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'hide_alias', array('id' => 'hide_alias_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][hide_alias]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('hide_alias');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group group-short_title_<?= $lang->code ?> <?php if(isset($errors['short_title'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id],'short_title', array('class' => 'control-label')); ?>
                                <?php echo $form->textField($i18nModel[$lang->id],'short_title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][short_title]', 'id' => 'short_title_'.$lang->code)); ?>
                                <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                <div class="help-block"></div>
                            </div>


						</div>
					<?php endforeach; ?>
				</div>

			</div>

			<hr/>

		</div>

		<div class="col-md-3">

			<div class="form-group">
				<?php
				echo $form->labelEx($model, 'parent_id');
				$parent_id  = $model->parent_id;
				if( $model->isNewRecord == 'create' && $_GET['parent_id'] > 0 ) {
					$parent_id = $_GET['parent_id'];
				}
				echo getMenuSelect($class, $menu, $parent_id);
				?>
			</div>

			<div class="form-group group-date <?php echo  $this->error($model, 'sort') ?>">
				<?php echo $form->labelEx($model,'sort', array('class' => 'control-label')); ?>
				<?php echo $form->textField($model,'sort',array('class'=>'form-control', 'id' => 'sort')); ?>
				<div class="help-block"></div>
			</div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'hide_in_menu'));
                echo $form->checkBox($model,'hide_in_menu', array('id' => 'hide_in_menu', 'value' => 1));
                echo $model->getAttributeLabel('hide_in_menu');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'in_new_window'));
                echo $form->checkBox($model,'in_new_window', array('id' => 'in_new_window', 'value' => 1));
                echo $model->getAttributeLabel('in_new_window');
                echo CHtml::closeTag('label');
                ?>
            </div>


            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>

		</div>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
	var fired = 0;
	var inProgress = false;
	var languages = <?php echo CJSON::encode($codes); ?>;

	$(function() {
		$('#pages-form').submit(function(e){
			e.preventDefault();

			fired = 0;
			if( !inProgress ) {
				inProgress = true;
				showLoader();

				// reset errors and error messages
				$('.has-error').removeClass('has-error');
				$('a.error').removeClass('error');
				$('.help-block').each(function () {
					$(this).text('');
				});

				savePage( $(this) );

			}

		});
	});


	function savePage( $form ) {
		$.ajax({
			url: $form.attr('action'),
			data: $form.serialize(),
			dataType: 'json',
			type: $form.attr('method'),
			success: function(resp) {
				if( resp.status == 'ok' ) {
					top.location.href = resp.redirect;
				} else {
					for(var i in resp.errors) {
						console.log(i);
						$('.group-'+i).find('.help-block').text(resp.errors[i]);
						$('.group-'+i).addClass('has-error');
					}
					for(var i in resp.langs) {
						$('a.'+resp.langs[i]).addClass('error');
					}
				}
				hideLoader();
				inProgress = false;
			},
			error: function() {
				hideLoader();
				inProgress = false;
			}
		});
	}



</script>