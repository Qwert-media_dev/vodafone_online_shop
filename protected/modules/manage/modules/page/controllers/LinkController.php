<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 10/7/2015
 * Time: 01:17 PM
 */

class LinkController extends ManageController
{
	protected static $class = 'Pages';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'save'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionCreate() {
		$model = new Pages;
		$class = get_class($model);
		$i18nModel = array();
		foreach($this->languages as $lang) {
			$i18nModel[ $lang->id ] = new PagesI18n;
		}

		$menu =  Pages::model()->getFlatMenu();
		$codes = Languages::model()->getCodes($this->languages);
		$this->render('create', compact('model', 'i18nModel', 'class', 'codes', 'menu'));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$class = get_class($model);
		$i18nModel = array();
		foreach($model->i18n as $item) {
			$i18nModel[ $item->lang_id ] = $item;
		}

		$menu =  Pages::model()->getFlatMenu();
		$codes = Languages::model()->getCodes($this->languages);
		$this->render('update', compact('model', 'i18nModel', 'class', 'codes', 'menu'));
	}


	public function actionSave() {
		$output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

		if(isset($_POST[self::$class]))
		{
			$form = $_POST[self::$class];
			$i18n = $_POST[self::$class.'I18n'];

			$i18nModel = array();

			if( array_key_exists('id', $form) && (int)$form['id'] > 0 ) {
				$model = Pages::model()->findByPk($form['id']);
				foreach($model->i18n as $item) {
					$i18nModel[ $item->lang_id ] = $item;
				}
			} else {
				$model = new Pages();
				foreach($this->languages as $lang) {
					$i18nModel[ $lang->id ] = new PagesI18n;
				}
			}

			$model->type = Pages::TYPE_LINK;
			$model->parent_id = (int)$form['parent_id'];
			$model->sort = (int)$form['sort'];
			$model->hide_in_menu = (int)$form['hide_in_menu'];
			$model->in_new_window = (int)$form['in_new_window'];

			$defaultTitle = '';
			$statuses = array();
			$validate = $model->validate();
			foreach($this->languages as $lang) {
				$currentStatus = (int)$i18n[$lang->id]['status'];
				$statuses[$lang->code] = $currentStatus;
				$i18nModel[$lang->id]->scenario = 'savelink_'.$currentStatus;
				$i18nModel[$lang->id]->lang_id = $lang->id;
				$i18nModel[$lang->id]->status = $currentStatus;
				$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
				$i18nModel[$lang->id]->short_title = $i18n[$lang->id]['short_title'];
				$i18nModel[$lang->id]->alias = $i18n[$lang->id]['alias'];
				$i18nModel[$lang->id]->parent_id = (int)$form['parent_id'];
                $i18nModel[$lang->id]->hide_alias = (int)$i18n[$lang->id]['hide_alias'];

				$validate = $validate && $i18nModel[$lang->id]->validate();

				if( $defaultTitle == '' || ($lang->id == param('defaultLangId') && trim($i18n[$lang->id]['title']) != '') ) {
					$defaultTitle = trim($i18n[$lang->id]['title']);
				}
			}

			if($validate) {
				if( empty($defaultTitle) ) {
					$defaultTitle = '[Без заголовка]';
				}
				$model->title = $defaultTitle;
				$model->statuses = CJSON::encode($statuses);

				$model->save();
				foreach($this->languages as $lang) {
					$i18nModel[$lang->id]->page_id = $model->id;
					$i18nModel[$lang->id]->save();
				}
                $redirect = url_to('manage/page/link/update', array('id' => $model->id));
                if( user()->show_index ) {
                    $redirect = url_to('manage/page/pages/tree');
                }

                $output = array('status' => 'ok', 'redirect' => $redirect);
			} else {
				$errors = array();
				if(sizeof($model->getErrors())) {
					foreach($model->getErrors() as $field => $data) {
						if(sizeof($data))
							$errors[$field] = $data[0];
					}
				}

				$langs = array();
				foreach( $this->languages as $lang ) {
					if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
						foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
							if(sizeof($data)) {
								$errors[$field . '_' . $lang->code] = $data[0];
								$langs[] = $lang->code;
							}
						}
					}
				}

				$langs = array_unique($langs);

				$output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
			}
		}

		echo CJSON::encode($output);
	}

	public function loadModel($id)
	{
		$model = Pages::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}