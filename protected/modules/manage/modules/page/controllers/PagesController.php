<?php

class PagesController extends ManageController
{

    protected static $class = 'Pages';
	public $useFileAPI = true;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(/*'index',*/'save', 'saveimage', /*'getlevel', 'go',*/ 'upload', 'childs'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'tree', 'trash', 'structure', 'restore'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(/*'admin',*/'delete'),
				'users'=>array('@'),
			),
			array('allow',
				'actions'=>array('list', 'more'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


    public function actions()
    {
        return array(
            'saveimage'=>'application.modules.manage.components.SaveImageAction',
        );
    }


	public function actionList() {
		$items = Pages::model()->getLevel(0);
		$this->render('list', compact('items'));
	}


	public function actionMore($parent_id, $level) {
		$content = '';
		$level++;
		$items = Pages::model()->getLevel($parent_id);
		foreach($items as $item) {
			$content .= $this->renderPartial('list-item', compact('item', 'level'), true);
		}
		$resp = [
			'rows' => count($items),
			'content' => $content,
			'item' => '.item'.$parent_id
		];

		echo CJSON::encode($resp);
	}

    public function actionSave() {
        $output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

        if(isset($_POST[self::$class]))
        {
            $form = $_POST[self::$class];
	        $i18n = $_POST[self::$class.'I18n'];

	        $i18nModel = array();

            if( array_key_exists('id', $form) && (int)$form['id'] > 0 ) {
	            $isNew = false;
                $model = Pages::model()->findByPk($form['id']);
                foreach($model->i18n as $item) {
                    $i18nModel[ $item->lang_id ] = $item;
                }
            } else {
	            $isNew = true;
                $model = new Pages();
                foreach($this->languages as $lang) {
                    $i18nModel[ $lang->id ] = new PagesI18n;
                }
            }

	        if( $model->type != Pages::TYPE_MODULE )
	            $model->type = $form['type'];

            $model->parent_id = (int)$form['parent_id'];
	        $model->sort = (int)$form['sort'];
	        $model->hide_in_menu = (int)$form['hide_in_menu'];
	        $model->is_tabbed_menu = (int)$form['is_tabbed_menu'];
	        $model->in_new_window = (int)$form['in_new_window'];
	        if( user()->email == param('adminEmail') ) {
		        $model->controller = trim($form['controller']);
		        $model->extra_class = $form['extra_class'];
		        $model->protected = (int)$form['protected'];

				$model->app_name = $form['app_name'];
				$model->app_store_id = $form['app_store_id'];
				$model->app_package = $form['app_package'];
	        }
			$model->extra_css = $form['extra_css'];
			$model->extra_js = $form['extra_js'];

	        $defaultTitle = '';
	        $statuses = array();
            $validate = $model->validate();
            foreach($this->languages as $lang) {
	            $currentStatus = (int)$i18n[$lang->id]['status'];
	            $statuses[$lang->code] = $currentStatus;
	            $i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->status = $currentStatus;
				$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
                $i18nModel[$lang->id]->alias = $i18n[$lang->id]['alias'];
	            if( $i18nModel[$lang->id]->alias == '' ) {
		            $i18nModel[$lang->id]->alias = URLify::filter($i18n[$lang->id]['title']);
	            }
	            if($model->type == Pages::TYPE_LINK) {
		            if( !(substr($i18nModel[$lang->id]->alias, 0, 5) == 'http:' || substr($i18nModel[$lang->id]->alias, 0, 6) == 'https:' ) ){
			            $i18nModel[$lang->id]->alias = filterUrl($i18nModel[$lang->id]->alias);
		            }
	            } else {
		            $i18nModel[$lang->id]->alias = URLify::filter($i18nModel[$lang->id]->alias);
	            }
                $i18nModel[$lang->id]->body = $i18n[$lang->id]['body'];

	            $i18nModel[$lang->id]->short_title = $i18n[$lang->id]['short_title'];
	            $i18nModel[$lang->id]->og_title = $i18n[$lang->id]['og_title'];
	            $i18nModel[$lang->id]->og_description = $i18n[$lang->id]['og_description'];
	            $i18nModel[$lang->id]->og_image = $i18n[$lang->id]['og_image'];
	            $i18nModel[$lang->id]->image1 = $i18n[$lang->id]['image1'];
	            $i18nModel[$lang->id]->image2 = $i18n[$lang->id]['image2'];
	            $i18nModel[$lang->id]->body2 = $i18n[$lang->id]['body2'];
	            $i18nModel[$lang->id]->parent_id = (int)$form['parent_id'];
                //$i18nModel[$lang->id]->hide_alias = (int)$i18n[$lang->id]['hide_alias'];
	            $i18nModel[$lang->id]->pic1 = $i18n[$lang->id]['pic1'];
	            $i18nModel[$lang->id]->pic2 = $i18n[$lang->id]['pic2'];
	            $i18nModel[$lang->id]->pic3 = $i18n[$lang->id]['pic3'];
	            $i18nModel[$lang->id]->pic4 = $i18n[$lang->id]['pic4'];
	            $i18nModel[$lang->id]->seo_title = $i18n[$lang->id]['seo_title'];
	            $i18nModel[$lang->id]->seo_description = $i18n[$lang->id]['seo_description'];

                $validate = $validate && $i18nModel[$lang->id]->validate();

	            if( $defaultTitle == '' || ($lang->id == param('defaultLangId') && trim($i18n[$lang->id]['title']) != '') ) {
		            $defaultTitle = trim($i18n[$lang->id]['title']);
	            }
            }

            if($validate) {
	            if( empty($defaultTitle) ) {
		            $defaultTitle = '[Без заголовка]';
	            }
	            $model->title = $defaultTitle;
	            $model->statuses = CJSON::encode($statuses);

                $model->save();

	            // обновили root для выделения главной
	            Pages::model()->updateByPk($model->id, array('root' => Pages::model()->getRootId($model)));

                foreach($this->languages as $lang) {
                    $i18nModel[$lang->id]->page_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

	            $method = $isNew ? LogRegistry::METHOD_CREATE : LogRegistry::METHOD_UPDATE;
	            LogRegistry::instance()->log( $method, get_class($model), $model->id, $model->title );

	            (new CachePurger( strtolower(get_class($model)), $model->id ))->process();

	            $params = array('id' => $model->id);
	            if(array_key_exists('lang', $_POST) && $_POST['lang'] != '')
		            $params['lang'] = $_POST['lang'];
	            $redirect = url_to('manage/page/pages/update', $params);
                if( user()->show_index ) {
                    $redirect = url_to('manage/page/pages/tree');
                }

                $output = array('status' => 'ok', 'redirect' => $redirect);
            } else {
                $errors = array();
                if(sizeof($model->getErrors())) {
                    foreach($model->getErrors() as $field => $data) {
                        if(sizeof($data))
                            $errors[$field] = $data[0];
                    }
                }

                $langs = array();
                foreach( $this->languages as $lang ) {
                    if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
                        foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
                            if(sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);

                $output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
            }
        }

        echo CJSON::encode($output);
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Pages;
        $class = get_class($model);
        $i18nModel = array();
        foreach($this->languages as $lang) {
            $i18nModel[ $lang->id ] = new PagesI18n;
        }

		$menu =  Pages::model()->getFlatMenu();
        $codes = Languages::model()->getCodes($this->languages);
		$this->render('create', compact('model', 'i18nModel', 'class', 'codes', 'menu'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = array();
        foreach($model->i18n as $item) {
            $i18nModel[ $item->lang_id ] = $item;
        }

		$menu =  Pages::model()->getFlatMenu();
		$codes = Languages::model()->getCodes($this->languages);
		$this->render('update', compact('model', 'i18nModel', 'class', 'codes', 'menu'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->title );
		(new CachePurger( strtolower(get_class($model)), $model->id ))->process();
		Pages::model()->deleteItem($model->id);
		$tree = Pages::model()->getTree($id);
		$items = array();
		Pages::model()->getFlatTree($tree, $items);
		foreach($items as $item) {
			Pages::model()->deleteItem($item['id']);
		}

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('tree'));
	}

	/**
	 * Lists all models.
	 */
	/*
	public function actionIndex()
	{
		$this->redirect(array('tree'));
		$this->render('index');
	}
	*/

	/**
	 * Manages all models.
	 */
	/*
	public function actionAdmin()
	{
		$this->redirect(array('tree'));
		$model = new Pages('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pages']))
			$model->attributes=$_GET['Pages'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	*/

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Pages::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/*
	public function actionGetlevel() {
		$key = (int)$_GET['key'];
		$items = Pages::model()->getLevel($key);
		echo CJSON::encode($items);
	}
	*/

	/*
	public function actionGo() {
		$cmd = $_GET['cmd'];
		$key = $_GET['key'];
		switch($cmd) {
			case 'edit':
				$model = $this->loadModel($key);
				if($model->type == Pages::TYPE_LINK) {
					$this->redirect(array('link/update', 'id' => $key));
				}
				$this->redirect(array('update', 'id' => $key));
				break;
			case 'create':
				$this->redirect(array('create', 'parent_id' => $key));
				break;
			case 'delete':
				$this->redirect(array('delete', 'id' => $key));
				break;
			default:
				$this->redirect(array('tree'));
		}
	}
	*/

	public function actionUpload() {
		$resp = array('status' => 'fail', 'msg' => 'init');

		if (in_array($_POST['field'], array('pic1', 'pic2', 'pic3', 'pic4', 'og_image'))) {
			$model = new FrontSliderForm($_POST['field']);
			$model->image = CUploadedFile::getInstanceByName('files[0]');
			if ($model->validate()) {
				$filename = URLify::filter($model->image->name, param('urlifyFileLength'), '', true);
				$model->image->saveAs(param('uploadPath') . $filename);
				$resp = array(
						'status' => 'ok',
						'location' => $filename,
				);
			} else {
				$resp['msg'] = $model->getError('image');
				$resp['field'] = $_POST['field'];
			}
		}

		echo CJSON::encode($resp);
	}


	public function actionTree() {
		$this->redirect(['list']);
		$items =  Pages::model()->getFlatMenu();
		$this->render('tree', compact('items'));
	}


	/**
	 * Используется для удаления в корзину
	 *
	 * @param $id
	 * @throws CException
	 */
	public function actionChilds($id) {
		$main = Pages::model()->findByPk($id);
		$items = Pages::model()->getTree($id);
		$flat = array();
		Pages::model()->getFlatTree($items, $flat);
		$content = $this->renderPartial('childs', compact('flat', 'main'), true);
		echo CJSON::encode(['content' => $content]);
	}


	public function actionTrash() {
		$provider = Pages::model()->getTrash();
		$this->render('trash',compact('provider'));
	}

	/**
	 * Используется для восстановления из корзины
	 *
	 * @throws CException
	 */
	public function actionStructure() {
		$tree = Pages::model()->getTree();
		$items = array();
		Pages::model()->getFlatTree($tree, $items);
		$content = $this->renderPartial('structure', ['items' => $items, 'class' => self::$class], true);
		echo CJSON::encode(['content' => $content]);
	}


	public function actionRestore($id) {
		$model = $this->loadModel($id);
		$model->is_deleted = 0;
		$model->parent_id = (int)$_POST['parent_id'];
		$model->save();
		LogRegistry::instance()->log( LogRegistry::METHOD_RESTORE, get_class($model), $model->id, $model->title );
		echo CJSON::encode(['status' => 'ok']);
	}
}
