<?php

class SxgeoRegionsController extends ManageController
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('admin', 'update'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }


    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = SxgeoRegionsI18n::model()->getItems($model->iso);
        foreach($this->languages as $lang) {
            if( !array_key_exists($lang->id, $i18nModel) ) {
                $i18nModel[$lang->id] = new SxgeoRegionsI18n();
            }
        }

        $langs = array();
        if (isset($_POST[$class])) {
            $model->attributes = $_POST[$class];

            $i18n = $_POST[$class.'I18n'];
            $validate = $model->validate();
            foreach($this->languages as $lang) {
                $currentStatus = (int)$i18n[$lang->id]['status'];
                $statuses[$lang->code] = $currentStatus;
                $i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->status = $currentStatus;
                $i18nModel[$lang->id]->name = $i18n[$lang->id]['name'];
                $i18nModel[$lang->id]->popup_name = $i18n[$lang->id]['popup_name'];

                $res = $i18nModel[$lang->id]->validate();
                $validate = $validate && $res;
            }

            if ($validate) {
                //$model->save();

                LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $model->name_ru );

                foreach($this->languages as $lang) {
                    $i18nModel[$lang->id]->region_iso = $model->iso;
                    $i18nModel[$lang->id]->save();
                }
                    
                $redirect = array('update', 'id' => $model->id);
                if( user()->show_index ) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            } else {
                foreach( $this->languages as $lang ) {
                    if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
                        foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
                            if(sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);
            }
        }

        $this->render('update', compact('model', 'i18nModel', 'class', 'langs'));
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new SxgeoRegions('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SxgeoRegions']))
            $model->attributes = $_GET['SxgeoRegions'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SxgeoRegions the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SxgeoRegions::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


}
