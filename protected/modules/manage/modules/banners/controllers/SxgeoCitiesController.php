<?php

class SxgeoCitiesController extends ManageController
{


    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'update'),
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $class = get_class($model);
        $i18nModel = SxgeoCitiesI18n::model()->getItems($model->id);
        foreach($this->languages as $lang) {
            if( !array_key_exists($lang->id, $i18nModel) ) {
                $i18nModel[$lang->id] = new SxgeoCitiesI18n();
            }
        }

        $langs = array();
        if (isset($_POST[$class])) {
            $model->attributes = $_POST[$class];

            $i18n = $_POST[$class.'I18n'];
            //e('$i18n='.print_r($i18n, true));
            $validate = $model->validate();
            foreach($this->languages as $lang) {
                $currentStatus = (int)$i18n[$lang->id]['status'];
                $statuses[$lang->code] = $currentStatus;
                $i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;
                $i18nModel[$lang->id]->lang_id = $lang->id;
                $i18nModel[$lang->id]->status = $currentStatus;
                $i18nModel[$lang->id]->name = $i18n[$lang->id]['name'];
                $i18nModel[$lang->id]->popup_name = $i18n[$lang->id]['popup_name'];

                $res = $i18nModel[$lang->id]->validate();
                $validate = $validate && $res;
            }

            if ($validate) {
                //$model->save();

                LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $model->name_ru );

                foreach($this->languages as $lang) {
                    $i18nModel[$lang->id]->city_id = $model->id;
                    $i18nModel[$lang->id]->save();
                }

                $redirect = array('update', 'id' => $model->id);
                if( user()->show_index ) {
                    $redirect = array('admin');
                }

                $this->redirect($redirect);
            } else {
                foreach( $this->languages as $lang ) {
                    if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
                        foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
                            if(sizeof($data)) {
                                $errors[$field . '_' . $lang->code] = $data[0];
                                $langs[] = $lang->code;
                            }
                        }
                    }
                }

                $langs = array_unique($langs);
            }
        }

        $this->render('update', compact('model', 'i18nModel', 'class', 'langs'));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }


    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new SxgeoCities('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['SxgeoCities']))
            $model->attributes = $_GET['SxgeoCities'];

        $regions = SxgeoRegions::model()->getItems();
        $this->render('admin', compact('model', 'regions'));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SxgeoCities the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = SxgeoCities::model()->findByPk($id);
        $model->scenario = 'update';
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }


}
