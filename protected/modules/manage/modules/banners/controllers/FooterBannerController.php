<?php

class FooterBannerController extends ManageController
{

	public static $class = 'FooterBanner';
	public $useFileAPI = true;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions' => array('create', 'update', 'upload', 'save', 'trash', 'restore'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new FooterBanner;
		$class = get_class($model);

		$i18nModel = array();
		foreach ($this->languages as $lang) {
			$i18nModel[$lang->id] = new FooterBannerI18n();
		}

		$landing = SettingsLanding::model()->find();
		$pages = Pages::model()->getForSliders( [$landing->id1, $landing->id2] );

		$this->render('create', compact('model', 'i18nModel', 'class', 'pages'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$class = get_class($model);

		$i18nModel = array();
		foreach($model->i18n as $item) {
			$i18nModel[$item->lang_id] = $item;
		}

		$landing = SettingsLanding::model()->find();
		$pages = Pages::model()->getForSliders( [$landing->id1, $landing->id2] );

		$this->render('update', compact('model', 'i18nModel', 'class', 'pages'));
	}


	public function actionSave() {
		$output = array('status' => 'fail', 'msg' => 'Нет данных для сохранения', 'errors' => array());

		if(isset($_POST[self::$class]))
		{
			$form = $_POST[self::$class];
			$i18nModel = array();

			if( array_key_exists('id', $form) && (int)$form['id'] > 0 ) {
				$isNew = false;
				$model = FooterBanner::model()->findByPk($form['id']);
				foreach($model->i18n as $item) {
					$i18nModel[ $item->lang_id ] = $item;
				}
			} else {
				$isNew = true;
				$model = new FooterBanner();
				foreach($this->languages as $lang) {
					$i18nModel[ $lang->id ] = new FooterBannerI18n;
				}
			}

			$i18n = $_POST[self::$class.'I18n'];
			$model->position = (int)$form['position'];
			$model->page_id = (int)$form['page_id'];
			$model->title = $form['title'];

			$statuses = array();
			$validate = $model->validate();
			foreach($this->languages as $lang) {
				$currentStatus = (int)$i18n[$lang->id]['status'];
				$statuses[$lang->code] = $currentStatus;
				$i18nModel[$lang->id]->scenario = 'save_'.$currentStatus;
				$i18nModel[$lang->id]->lang_id = $lang->id;
				$i18nModel[$lang->id]->status = $currentStatus;
				$i18nModel[$lang->id]->in_new_window = (int)$i18n[$lang->id]['in_new_window'];
				//$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
				$i18nModel[$lang->id]->alt = $i18n[$lang->id]['alt'];
				$i18nModel[$lang->id]->link = $i18n[$lang->id]['link'];
				$i18nModel[$lang->id]->image = $i18n[$lang->id]['image'];

				$res = $i18nModel[$lang->id]->validate();
				$validate = $validate && $res;
			}

			if($validate) {

				$model->statuses = CJSON::encode($statuses);
				$model->save();

				foreach($this->languages as $lang) {
					$i18nModel[$lang->id]->footer_banner_id = $model->id;
					$i18nModel[$lang->id]->save();
				}

				$method = $isNew ? LogRegistry::METHOD_CREATE : LogRegistry::METHOD_UPDATE;
				LogRegistry::instance()->log( $method, get_class($model), $model->id, $model->title );

				$params = array('id' => $model->id);
				if(array_key_exists('lang', $_POST) && $_POST['lang'] != '')
					$params['lang'] = $_POST['lang'];
				$redirect = url_to('manage/banners/footerBanner/update', $params);
				if( user()->show_index ) {
					$redirect = url_to('manage/banners/footerBanner/admin');
				}


				$output = array('status' => 'ok', 'redirect' => $redirect);
			} else {
				$errors = array();
				if(sizeof($model->getErrors())) {
					foreach($model->getErrors() as $field => $data) {
						if(sizeof($data))
							$errors[$field] = $data[0];
					}
				}

				$langs = array();
				foreach( $this->languages as $lang ) {
					if( sizeof( $i18nModel[ $lang->id ]->getErrors() ) ) {
						foreach( $i18nModel[ $lang->id ]->getErrors() as $field => $data) {
							if(sizeof($data)) {
								$errors[$field . '_' . $lang->code] = $data[0];
								$langs[] = $lang->code;
							}
						}
					}
				}

				$langs = array_unique($langs);

				$output = array('status' => 'fail', 'errors' => $errors, 'langs' => $langs);
			}
		}

		echo CJSON::encode($output);
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->is_deleted = 1;
		$model->save();
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->title );
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	public function actionUpload()
	{
		$resp = array('status' => 'fail', 'msg' => 'init');

		if (in_array($_POST['field'], array('image'))) {
			$model = new FrontSliderForm($_POST['field']);
			$model->image = CUploadedFile::getInstanceByName('files[0]');
			if ($model->validate()) {
				$filename = URLify::filter(FS::hash($model->image->name.time()) . '_' . $model->image->name, param('urlifyFileLength'), '', true);
				$model->image->saveAs(param('uploadPath') . $filename);
				$resp = array(
					'status' => 'ok',
					'location' => $filename,
				);
			} else {
				$resp['msg'] = $model->getError('image');
				$resp['field'] = $_POST['field'];
			}
		}

		echo CJSON::encode($resp);
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new FooterBanner('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FooterBanner']))
			$model->attributes=$_GET['FooterBanner'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return FooterBanner the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=FooterBanner::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function actionTrash()
	{
		$provider = FooterBanner::model()->getDeleted();
		$this->render('trash',compact('provider'));
	}


	public function actionRestore($id) {
		$model = $this->loadModel($id);
		$model->is_deleted = 0;
		$model->save();
		LogRegistry::instance()->log( LogRegistry::METHOD_RESTORE, get_class($model), $model->id, $model->title );
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('trash'));
	}


}
