<?php
/* @var $this FrontSliderController */
/* @var $model FrontSlider */
/* @var $class string */
/* @var $i18nModel FrontSliderI18n[] */
/* @var $pages Pages[] */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */


$this->breadcrumbs = array(
    'Баннеры' => array('admin'),
    'Создать',
);

$this->menu = array(
    array('label' => 'Список баннеров', 'url' => array('admin')),
    array('label' => 'Корзина', 'url' => array('trash')),
);
?>

    <h1>Создание баннера</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'pages', 'geoData', 'geo', 'geoExclude')); ?>