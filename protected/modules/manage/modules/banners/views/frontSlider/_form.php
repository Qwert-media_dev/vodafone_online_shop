<?php
/* @var $this FrontSliderController */
/* @var $model FrontSlider */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel FrontSliderI18n[] */
/* @var $pages Pages[] */
/* @var $geoData array */
/* @var $geo array */
/* @var $geoExclude array */

$modelErrors = $model->getErrors();
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'front-slider-form',
    'action' => array('save'),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
echo $form->hiddenField($model, 'dummy', array('value' => 1));
if( !$model->getIsNewRecord() ) {
    echo $form->hiddenField($model, 'id');
}
?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-9">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach($this->languages as $lang): ?>
                        <li><a href="#<?=$lang->code?>" class="<?= $lang->code ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab"><?=$lang->title?></a></li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors();?>

                            <br/>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group group-title_<?= $lang->code ?> <?php if(isset($errors['title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]', 'id' => 'title_'.$lang->code)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form-group group-first_title_<?= $lang->code ?> <?php if(isset($errors['title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'first_title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'first_title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][first_title]', 'id' => 'first_title_'.$lang->code)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'first_title', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form-group group-second_title_<?= $lang->code ?> <?php if(isset($errors['second_title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'second_title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'second_title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][second_title]', 'id' => 'second_title_'.$lang->code)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form-group group-button_title_<?= $lang->code ?> <?php if(isset($errors['button_title'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'button_title', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'button_title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][button_title]', 'id' => 'button_title_'.$lang->code)); ?>
                                        <?php //echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form-group group-alt_<?= $lang->code ?> <?php if(isset($errors['alt'])) echo ' has-error'; ?>">
                                        <div class="form-group group-alt_<?= $lang->code ?> <?php if(isset($errors['alt'])) echo ' has-error'; ?>">
                                            <?php echo $form->labelEx($i18nModel[$lang->id],'alt', array('class' => 'control-label')); ?>
                                            <?php echo $form->textField($i18nModel[$lang->id],'alt',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][alt]', 'id' => 'alt_'.$lang->code)); ?>
                                            <div class="help-block"></div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="well" style="margin-top: 13px;">
                                        <div class="checkbox">
                                            <?php
                                            echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                            echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                            echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                            echo CHtml::closeTag('label');
                                            ?>
                                        </div>

                                        <div class="checkbox">
                                            <?php
                                            echo CHtml::openTag('label', array('for' => 'in_new_window_'.$lang->code));
                                            echo $form->checkBox($i18nModel[$lang->id],'in_new_window', array('id' => 'in_new_window_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][in_new_window]'));
                                            echo $i18nModel[$lang->id]->getAttributeLabel('in_new_window');
                                            echo CHtml::closeTag('label');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group group-link_<?= $lang->code ?> <?php if(isset($errors['link'])) echo ' has-error'; ?>">
                                <?php echo $form->labelEx($i18nModel[$lang->id],'link', array('class' => 'control-label')); ?>
                                <?php echo $form->textField($i18nModel[$lang->id],'link',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][link]', 'id' => 'link_'.$lang->code)); ?>
                                <div class="help-block"></div>
                            </div>


                            <?php
                            $uploadUrl = url_to('manage/banners/frontSlider/upload');
                            $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic1', 'fieldKey' => 'pic1_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '2600x506 jpg,gif,png', 'url' => $uploadUrl));
                            $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic2', 'fieldKey' => 'pic2_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '1600x506 jpg,gif,png', 'url' => $uploadUrl));
                            $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic3', 'fieldKey' => 'pic3_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '980x360 jpg,gif,png', 'url' => $uploadUrl));
                            $this->widget('FileUploaderWidget', array( 'lang_id' => $lang->id, 'field' => 'pic4', 'fieldKey' => 'pic4_'.$lang->code, 'model' => $i18nModel[$lang->id], 'form' => $form, 'help' => '660x500 jpg,gif,png', 'url' => $uploadUrl));
                            ?>

                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

            <?php $this->widget('GeoSelectorWidget', ['geo' => $geo, 'geoData' => $geoData]); ?>

            <?php $this->widget('GeoExcludeSelectorWidget', ['geo' => $geoExclude, 'geoData' => $geoData]); ?>
            
        </div>
        <div class="col-md-3">

            <div class="form-group page_id <?php echo  $this->error($model, 'page_id') ?>">
                <?php echo $form->labelEx($model,'page_id', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'page_id', CHtml::listData($pages, 'id', 'title'), array('class'=>'form-control', 'id' => 'page_id')); ?>
                <div class="help-block"></div>
            </div>

            <div class="form-group group-date <?php echo  $this->error($model, 'sort') ?>">
                <?php echo $form->labelEx($model,'sort', array('class' => 'control-label')); ?>
                <?php echo $form->textField($model,'sort',array('class'=>'form-control', 'id' => 'sort')); ?>
                <div class="help-block"></div>
            </div>

            <div class="form-group title_color <?php echo  $this->error($model, 'title_color') ?>">
                <?php echo $form->labelEx($model,'title_color', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'title_color', param('front_slider_colors'), array('class'=>'form-control', 'id' => 'title_color')); ?>
                <div class="help-block"></div>
            </div>

            <div class="form-group second_title_color <?php echo  $this->error($model, 'second_title_color') ?>">
                <?php echo $form->labelEx($model,'second_title_color', array('class' => 'control-label')); ?>
                <?php echo $form->dropDownList($model,'second_title_color', param('front_slider_colors'), array('class'=>'form-control', 'id' => 'second_title_color')); ?>
                <div class="help-block"></div>
            </div>

            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>


<?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    var fired = 0;
    var inProgress = false;

    $(function(){

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>

        $('#front-slider-form').submit(function(e){
            e.preventDefault();

            fired = 0;
            if( !inProgress ) {
                inProgress = true;
                showLoader();

                // reset errors and error messages
                $('.has-error').removeClass('has-error');
                $('a.error').removeClass('error');
                $('.help-block').each(function () {
                    $(this).text('');
                });

                savePage( $(this) );

            }

        });
    });

    function savePage($form) {
        $.ajax({
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            type: $form.attr('method'),
            success: function(resp) {
                if( resp.status == 'ok' ) {
                    top.location.href = resp.redirect;
                } else {
                    for(var i in resp.errors) {
                        console.log(i);
                        $('.group-'+i).find('.help-block').text(resp.errors[i]);
                        $('.group-'+i).addClass('has-error');
                    }
                    for(var i in resp.langs) {
                        $('a.'+resp.langs[i]).addClass('error');
                    }
                }
                hideLoader();
                inProgress = false;
            },
            error: function() {
                hideLoader();
                inProgress = false;
            }
        });
    }
</script>