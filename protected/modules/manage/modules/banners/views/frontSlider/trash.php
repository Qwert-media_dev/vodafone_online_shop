<?php
/* @var $this FrontSliderController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Баннеры'=>array('admin'),
    'Корзина',
);

$this->menu=array(
    array('label'=>'Добавить баннер', 'url'=>array('create')),
    array('label'=>'Список баннеров', 'url'=>array('admin')),
);
?>

    <h1>Список удаленных баннеров</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'front-slider-grid',
    'dataProvider'=> $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    'columns'=>array(
        'title',
        array(
            'name' => 'statuses',
            'type' => 'raw',
            'value' => 'displayStatuses($data->statuses)'
        ),
        'sort',
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{restore}',
            'buttons' => array(
                'restore' => array(
                    'label' => 'Восстановить',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("manage/banners/frontSlider/restore", array("id" => $data->id))',
                    'options' => array(
                        'class' => 'btn btn-success btn-xs',
                        'confirm' => 'Вы уверены ?'
                    )
                ),
            )
        ),
    ),
));
