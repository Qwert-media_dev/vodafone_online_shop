<?php
/* @var $this SxgeoCitiesController */
/* @var $model SxgeoCities */
/* @var $regions SxgeoRegions[] */

$this->breadcrumbs = array(
    'Sxgeo Cities' => array('admin'),
    'Manage',
);
?>

<h1>Города</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sxgeo-cities-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active',
    ),
    'enableSorting' => false,
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'region_id',
            'value' => '$data->region->name_ru',
            'filter' => CHtml::activeDropDownList($model, 'region_id', CHtml::listData($regions, 'id', 'name_ru'), array('class' => 'form-control', 'empty' => 'Все'))
        ),
        array(
            'name' => 'name_ru',
            'filter' => CHtml::activeTextField($model, 'name_ru', array('class' => 'form-control'))
        ),
        array(
            'header' => 'Публикация',
            'type' => 'raw',
            'value' => 'displayStatuses(SxgeoCitiesI18n::model()->getStatuses($data->id))'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
            )
        ),
    ),
));

