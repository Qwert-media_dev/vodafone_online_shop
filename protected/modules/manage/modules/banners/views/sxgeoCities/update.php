<?php
/* @var $this SxgeoCitiesController */
/* @var $model SxgeoCities */
/* @var $i18nModel SxgeoCitiesI18n[] */
/* @var $class string */
/* @var $langs array */


$this->breadcrumbs = array(
    'Sxgeo Cities' => array('index'),
    $model->id => array('view', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Города', 'url' => array('admin')),
);
?>

<h1>Редактирование <?php echo $model->region->name_ru, '/', $model->name_ru; ?></h1>

<?php $this->renderPartial('_form', compact('model', 'i18nModel', 'class', 'langs')); ?>