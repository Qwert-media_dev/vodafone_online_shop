<?php
/* @var $this FrontSliderController */
/* @var $model FrontSlider */

$this->breadcrumbs=array(
        'Баннеры в подвале'=>array('admin'),
        'Список',
);

$this->menu=array(
        array('label'=>'Добавить баннер', 'url'=>array('create')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Баннеры в подвале</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
        'id'=>'front-slider-grid',
        'dataProvider'=>$model->search(),
        'itemsCssClass' => 'table table-striped',
        'pagerCssClass' => 'pager-container',
        'pager' => array(
            'class'=>'CLinkPager',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '&laquo;',
            'lastPageLabel' => '&raquo;',
            'nextPageLabel' => false,
            'prevPageLabel' => false,
            'header' => false,
            'selectedPageCssClass' => 'active'
        ),
        'enableSorting' => false,
        'columns'=>array(
            'title',
            array(
                'name' => 'Раздел',
                'type' => 'raw',
                'value' => '$data->page->title'
            ),
            array(
                'name' => 'statuses',
                'type' => 'raw',
                'value' => 'displayStatuses($data->statuses)'
            ),
            'position',
            array(
                'class'=>'CButtonColumn',
                'htmlOptions' => array('width' => 240, 'align' => 'center'),
                'template' => '{update} {delete}',
                'buttons' => array(
                    'update' => array(
                        'imageUrl' => false,
                        'options' => array(
                            'class' => 'btn btn-info btn-xs'
                        )
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'options' => array(
                            'class' => 'btn btn-danger btn-xs'
                        )
                    ),
                )
            ),
        ),
));
