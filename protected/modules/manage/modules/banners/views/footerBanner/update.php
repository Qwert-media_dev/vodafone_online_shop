<?php
/* @var $this FooterBannerController */
/* @var $model FooterBanner */
/* @var $i18nModel FooterBannerI18n[] */
/* @var $class string */
/* @var $pages Pages[] */

$this->breadcrumbs=array(
        'Баннеры в подвале'=>array('admin'),
        $model->title=>array('update','id'=>$model->id),
        'Редактирование',
);

$this->menu=array(
        array('label'=>'Добавить баннер', 'url'=>array('create')),
        array('label'=>'Список баннеров', 'url'=>array('admin')),
        array('label'=>'Корзина', 'url'=>array('trash')),
);
?>

<h1>Редактирование баннера</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'pages')); ?>