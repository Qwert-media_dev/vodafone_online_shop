<?php
/* @var $this SxgeoRegionsController */
/* @var $model SxgeoRegions */

$this->breadcrumbs = array(
    'Sxgeo Regions' => array('admin'),
    'Manage',
);
?>

<h1>Области</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'sxgeo-regions-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class' => 'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active',
    ),
    'enableSorting' => false,
    //'filter'=>$model,
    'columns' => array(
        'iso',
        'name_ru',
        array(
            'header' => 'Публикация',
            'type' => 'raw',
            'value' => 'displayStatuses(SxgeoRegionsI18n::model()->getStatuses($data->iso))'
        ),
        array(
            'class' => 'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
            )
        ),
    ),
)); ?>
