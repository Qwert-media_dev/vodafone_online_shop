<?php
/* @var $this SxgeoRegionsController */
/* @var $model SxgeoRegions */
/* @var $i18nModel SxgeoRegionsI18n[] */
/* @var $class string */
/* @var $langs array */

$this->breadcrumbs = array(
    'Sxgeo Regions' => array('admin'),
    $model->id => array('update', 'id' => $model->id),
    'Update',
);

$this->menu = array(
    array('label' => 'Области', 'url' => array('admin')),
);
?>

<h1>Редактирование <?php echo $model->iso, ' (', $model->name_ru, ')'; ?></h1>

<?php $this->renderPartial('_form', compact('model', 'i18nModel', 'class', 'langs')); ?>