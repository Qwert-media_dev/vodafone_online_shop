<?php
/* @var $this SxgeoRegionsController */
/* @var $model SxgeoRegions */
/* @var $form CActiveForm */
/* @var $i18nModel SxgeoRegionsI18n[] */
/* @var $class string */
/* @var $langs array */
?>

<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'sxgeo-regions-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    echo CHtml::activeHiddenField($model, 'dummy', ['value' => 1]);
    ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <?php //echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="col-md-9">

            <div role="tabpanel">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist" id="myTab">
                    <?php foreach($this->languages as $lang): ?>
                        <li>
                            <a href="#<?=$lang->code?>" class="<?php echo $lang->code; if( in_array($lang->code, $langs) ) echo ' error';  ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
                                <?=$lang->title?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <?php foreach($this->languages as $lang): ?>
                        <div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
                            <?php $errors = $i18nModel[$lang->id]->getErrors();?>

                            <br/>

                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group group-name_<?= $lang->code ?> <?php if(isset($errors['name'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'name', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'name',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][name]', 'id' => 'name_'.$lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>

                                    <div class="form-group group-popup_name_<?= $lang->code ?> <?php if(isset($errors['popup_name'])) echo ' has-error'; ?>">
                                        <?php echo $form->labelEx($i18nModel[$lang->id],'popup_name', array('class' => 'control-label')); ?>
                                        <?php echo $form->textField($i18nModel[$lang->id],'popup_name',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][popup_name]', 'id' => 'popup_name_'.$lang->code)); ?>
                                        <div class="help-block"></div>
                                    </div>

                                </div>
                                <div class="col-md-3">
                                    <div class="checkbox" style="margin-top: 30px;">
                                        <?php
                                        echo CHtml::openTag('label', array('for' => 'status_'.$lang->code));
                                        echo $form->checkBox($i18nModel[$lang->id],'status', array('id' => 'status_'.$lang->code, 'value' => 1, 'name' => $class.'I18n['.$lang->id.'][status]'));
                                        echo $i18nModel[$lang->id]->getAttributeLabel('status');
                                        echo CHtml::closeTag('label');
                                        ?>
                                    </div>
                                </div>
                            </div>


                        </div>
                    <?php endforeach; ?>
                </div>

            </div>

        </div>
        <div class="col-md-3">

            <div class="form-group buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->