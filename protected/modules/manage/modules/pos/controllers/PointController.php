<?php

class PointController extends ManageController
{


	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'getcities', 'status', 'export'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Pos;
		$class = get_class($model);
		$i18nModel = array();
		foreach($this->languages as $lang) {
			$i18nModel[$lang->id] = new PosI18n();
		}

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$i18n = $_POST[$class.'I18n'];

			$model->pos_region_id = $form['pos_region_id'];
			$model->pos_city_id = $form['pos_city_id'];
			$model->status = $form['status'];
			$model->lat = $form['lat'];
			$model->lng = $form['lng'];
			$model->zoom = $form['zoom'];

			$validate = $model->validate();

			$title = '';
			foreach($this->languages as $lang) {
				$i18nModel[$lang->id]->lang_id = $lang->id;
				//$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
				$i18nModel[$lang->id]->address = $i18n[$lang->id]['address'];
				$i18nModel[$lang->id]->local_name = $i18n[$lang->id]['local_name'];
				$i18nModel[$lang->id]->metro = $i18n[$lang->id]['metro'];
				$i18nModel[$lang->id]->how_to_find = $i18n[$lang->id]['how_to_find'];
				$i18nModel[$lang->id]->work_hours = $i18n[$lang->id]['work_hours'];

				if( $lang->id == param('defaultLangId') ) {
					$title = $i18n[$lang->id]['address'];
				}

				$validateMulti = $i18nModel[$lang->id]->validate();
				$validate = $validate && $validateMulti;

				if( !$validateMulti ) {
					$multiErrors[ $lang->code ][] = $i18nModel[$lang->id]->getErrors();
				}
			}

			if($validate) {

				$model->save();

				foreach($this->languages as $lang) {
					$i18nModel[$lang->id]->pos_id = $model->id;
					$i18nModel[$lang->id]->save();
				}

				LogRegistry::instance()->log( LogRegistry::METHOD_CREATE, get_class($model), $model->id, $title );
				user()->setFlash('success', 'Сохранено');

				if(user()->show_index) {
					$redirect = array('admin');
				} else {
					if(array_key_exists('lang', $_POST) && $_POST['lang'] != '')
						$redirect = array('update', 'id' => $model->id, 'lang' => $_POST['lang']);
					else
						$redirect = array('update', 'id' => $model->id);
				}

				$this->redirect($redirect);
			}
		}

		$regions = PosRegion::model()->getItems( param('defaultLangId') );
		$cities = [];

		$this->render('create', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions', 'cities'));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$class = get_class($model);
		$i18nModel = array();
		foreach($model->i18n as $item) {
			$i18nModel[ $item->lang_id ] = $item;
		}

		if(isset($_POST[$class]))
		{
			$form = $_POST[$class];
			$i18n = $_POST[$class.'I18n'];

			$model->pos_region_id = $form['pos_region_id'];
			$model->pos_city_id = $form['pos_city_id'];
			$model->status = $form['status'];
			$model->lat = $form['lat'];
			$model->lng = $form['lng'];
			$model->zoom = $form['zoom'];

			$validate = $model->validate();

			$title = '';
			foreach($this->languages as $lang) {
				$i18nModel[$lang->id]->lang_id = $lang->id;
				//$i18nModel[$lang->id]->title = $i18n[$lang->id]['title'];
				$i18nModel[$lang->id]->address = $i18n[$lang->id]['address'];
				$i18nModel[$lang->id]->local_name = $i18n[$lang->id]['local_name'];
				$i18nModel[$lang->id]->metro = $i18n[$lang->id]['metro'];
				$i18nModel[$lang->id]->how_to_find = $i18n[$lang->id]['how_to_find'];
				$i18nModel[$lang->id]->work_hours = $i18n[$lang->id]['work_hours'];

				if( $lang->id == param('defaultLangId') ) {
					$title = $i18n[$lang->id]['address'];
				}

				$validateMulti = $i18nModel[$lang->id]->validate();
				$validate = $validate && $validateMulti;

				if( !$validateMulti ) {
					$multiErrors[ $lang->code ][] = $i18nModel[$lang->id]->getErrors();
				}
			}

			if($validate) {

				$model->save();

				foreach($this->languages as $lang) {
					$i18nModel[$lang->id]->pos_id = $model->id;
					$i18nModel[$lang->id]->save();
				}

				LogRegistry::instance()->log( LogRegistry::METHOD_UPDATE, get_class($model), $model->id, $title );
				user()->setFlash('success', 'Сохранено');

				if(user()->show_index) {
					$redirect = array('admin');
				} else {
					if(array_key_exists('lang', $_POST) && $_POST['lang'] != '')
						$redirect = array('update', 'id' => $model->id, 'lang' => $_POST['lang']);
					else
						$redirect = array('update', 'id' => $model->id);
				}

				$this->redirect($redirect);
			}
		}

		$regions = PosRegion::model()->getItems( param('defaultLangId') );
		$cities = CHtml::listData( PosCity::model()->getItemsByRegion($model->pos_region_id, param('defaultLangId')), 'id', 'i18n.0.title');

		$this->render('update', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions', 'cities'));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		LogRegistry::instance()->log( LogRegistry::METHOD_DELETE, get_class($model), $model->id, $model->i18n[0]->title );
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}


	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{

		$regions = CHtml::listData( PosRegion::model()->getItems( param('defaultLangId')), 'id', 'i18n.0.title' );
		$cities = CHtml::listData( PosCity::model()->getItemsByRegion( $_GET['Pos']['pos_region_id'], param('defaultLangId') ), 'id', 'i18n.0.title' );
		$allCities = CHtml::listData( PosCity::model()->getItems( param('defaultLangId') ), 'id', 'i18n.0.title' );

		if( $_GET['Pos']['pos_region_id'] > 0 ) {
			if( !in_array( $_GET['Pos']['pos_city_id'], array_keys($cities) ) ) {
				unset($_GET['Pos']['pos_city_id']);
			}
		}

		$model=new Pos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pos']))
			$model->attributes=$_GET['Pos'];


		$this->render('admin', compact('model', 'regions', 'cities', 'allCities'));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Pos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function actionGetcities() {
		$content = '';
		if(array_key_exists('Pos',$_POST)) {
			$items = PosCity::model()->getItemsByRegion( $_POST['Pos']['pos_region_id'], param('defaultLangId') );
			foreach($items as $item) {
				$params = array('value' => $item->id);
				if( $_POST['Pos']['pos_city_id'] == $item->id )
					$params['selected'] = 'selected';
				$content .= CHtml::tag('option', $params,CHtml::encode($item->i18n[0]->title),true);
			}
		}
		echo CJSON::encode(['content' => $content]);
	}

	public function actionStatus($id, $status) {
		$model = $this->loadModel($id);
		Pos::model()->updateByPk($id, array('status' => $status));
		LogRegistry::instance()->log( LogRegistry::METHOD_STATUS, get_class($model), $model->id, $model->i18n[0]->title, $status );

		$this->redirect(array('admin'));
	}


	public function actionExport() {
        $filename = 'points.csv';
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        $f = fopen('php://output', 'w');

        $sql = 'select 
                    ri.title as region_title, 
                    ci.title as city_title, 
                    p.lat, p.lng, 
                    i.title, 
                    i.address, 
                    i.local_name, 
                    i.metro, 
                    i.how_to_find, 
                    i.work_hours
                from pos p
                join pos_region_i18n ri ON ri.pos_region_id = p.pos_region_id
                join pos_city_i18n ci ON ci.pos_city_id = p.pos_city_id
                join pos_i18n i ON i.pos_id = p.id
                where ri.lang_id = 1 AND ci.lang_id = 1 AND i.lang_id = 1
                order by region_title, city_title, i.title';
        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $dataReader = $command->query();
        $header = [
            'region_title',
            'city_title',
            'lat',
            'lng',
            'title',
            'address',
            'local_name',
            'metro',
            'how_to_find',
            'work_hours'
        ];
        fputcsv($f, $header, ',');
        foreach($dataReader as $row) {
            fputcsv($f, $row, ',');
        }
	}
}
