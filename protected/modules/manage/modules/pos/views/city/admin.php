<?php
/* @var $this CityController */
/* @var $model PosCity */
/* @var $regions array */

$this->breadcrumbs=array(
	'Города'=>array('admin'),
	'Управление',
);

$this->menu=array(
	array('label'=>'Добавить город', 'url'=>array('create')),
);
?>

<h1>Список городов</h1>


<?php
Yii::import('zii.widgets.grid.CGridView');
$this->widget('PointsGridView', array(
	'id'=>'pos-city-grid',
    'regions' => $regions,
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	'filter'=>$model,
	'columns'=>array(
		//'id',
        array(
            'name' => 'posRegion.i18n.0.title',
            'value' => '$this->grid->regions[$data->pos_region_id]',
            'filter' => CHtml::activeDropDownList($model, 'pos_region_id', $regions, array('class' => 'form-control', 'empty' => ' '))
        ),
		'i18n.0.title',
        array(
            'name' => 'points',
            'filter' => false
        ),
		array(
			'class'=>'CButtonColumn',
			'htmlOptions' => array('width' => 300, 'align' => 'right'),
			'template' => '{sort} {update} {delete}',
			'buttons' => array(
				'sort' => array(
					'imageUrl' => false,
					'label' => 'Сортировать',
					'url' => 'Yii::app()->createUrl("manage/pos/city/sort", array("id" => $data->id))',
					'visible' => '$data->points > 1',
					'options' => array(
						'class' => 'btn btn-default btn-xs'
					)
				),
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					)
				),
			)
		),
	),
));
