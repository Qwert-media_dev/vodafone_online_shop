<?php
/* @var $this CityController */
/* @var $model PosCity */
/* @var $regions PosRegion[]*/
/* @var $class string */
/* @var $i18nModel PosCityI18n[] */
/* @var $multiErrors array */

$this->breadcrumbs=array(
	'Pos Cities'=>array('admin'),
	$model->id=>array('edit','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить город', 'url'=>array('create')),
	array('label'=>'Список городов', 'url'=>array('admin')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions')); ?>