<?php
/* @var $this CityController */
/* @var $data Pos */
?>

<li class="ui-state-default" id="<?= $data->id ?>">
    <span class="glyphicon glyphicon-sort"></span><?php echo $data->i18n[0]->title, ' (', $data->i18n[0]->address, ')'; ?>
</li>