<?php
/* @var $dataProvider CActiveDataProvider */
/* @var $model PosCity */

$this->breadcrumbs=array(
    'Города'=>array('admin'),
    $model->i18n[0]->title,
    'Сортировка',
);

$this->menu=array(
    array('label'=>'Список городов', 'url'=>array('admin')),
);
?>

<style>
    .sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
    .sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 40px; }
    .sortable li span { margin-left: -1.3em; margin-right: 10px; }
</style>

<h1>Сортировка точек (<?= $model->i18n[0]->title ?>)</h1>

<a class="btn btn-success trigger" href="#save">Сохранить</a>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView'=>'_view',
    'itemsTagName' => 'ul',
    'itemsCssClass' => 'sortable'
));
?>

<a class="btn btn-success trigger" href="#save">Сохранить</a>

<script>
    $(function() {
        $( ".sortable" ).sortable({
            placeholder: "glyphicon"
        });
        $( ".sortable" ).disableSelection();

        $('.trigger').click(function(e){
            e.preventDefault();
            $.post('<?= url_to('manage/pos/city/sort') ?>', {list: $( ".sortable" ).sortable('toArray')}, function(resp){
                top.location.reload(true);
            }, 'json');
        })
    });
</script>