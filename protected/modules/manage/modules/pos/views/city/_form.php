<?php
/* @var $this CityController */
/* @var $model PosCity */
/* @var $form CActiveForm */
/* @var $regions PosRegion[]*/
/* @var $class string */
/* @var $i18nModel PosCityI18n[] */
/* @var $multiErrors array */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pos-city-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-9">
			<div role="tabpanel">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="myTab">
					<?php foreach($this->languages as $lang): ?>
						<li>
							<a href="#<?=$lang->code?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
								<?=$lang->title?>
								<?php if( count($multiErrors[$lang->code]) ): ?>
									<span class="label label-danger"> <span class="glyphicon glyphicon-warning-sign"></span> Ошибка</span>
								<?php endif; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<?php foreach($this->languages as $lang): ?>
						<div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
							<?php $errors = $i18nModel[$lang->id]->getErrors();?>

							<br/>

							<div class="form-group<?php if(isset($errors['title'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
								<?php echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]')); ?>
								<?php echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
							</div>

						</div>
					<?php endforeach; ?>
				</div>

			</div>
		</div>
		<div class="col-md-3">
			<div class="form-group">
				<?php echo $form->labelEx($model,'pos_region_id', array('class' => 'control-label')); ?>
				<?php echo $form->dropDownList($model,'pos_region_id', CHtml::listData($regions, 'id', 'i18n.0.title'), array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'pos_region_id', array('class' => 'help-block')); ?>
			</div>

			<div class="form-group buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
			</div>
		</div>
	</div>


<?php $this->endWidget(); ?>

</div><!-- form -->


