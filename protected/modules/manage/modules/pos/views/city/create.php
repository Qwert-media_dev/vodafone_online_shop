<?php
/* @var $this CityController */
/* @var $model PosCity */
/* @var $regions PosRegion[]*/
/* @var $class string */
/* @var $i18nModel PosCityI18n[] */
/* @var $multiErrors array */

$this->breadcrumbs=array(
	'Города'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список городов', 'url'=>array('admin')),
);
?>

<h1>Добавить город</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions')); ?>