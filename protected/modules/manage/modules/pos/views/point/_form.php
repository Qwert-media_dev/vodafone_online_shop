<?php
/* @var $this PointController */
/* @var $model Pos */
/* @var $form CActiveForm */
/* @var $class string */
/* @var $i18nModel PosI18n[] */
/* @var $multiErrors array */
/* @var $regions PosRegion[] */
/* @var $cities array */
?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru-RU"></script>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'pos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
));
echo CHtml::hiddenField('lang', '', ['id' => 'lang']);
echo $form->hiddenField($model,'lat', array('id' => 'lat'));
echo $form->hiddenField($model,'lng', array('id' => 'lng'));
echo $form->hiddenField($model,'zoom', array('id' => 'zoom'));
?>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php //echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-9">

			<div role="tabpanel">

				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" id="myTab">
					<?php foreach($this->languages as $lang): ?>
						<li data-lang="<?= $lang->code ?>">
							<a href="#<?=$lang->code?>" class="<?= $lang->code ?>" aria-controls="<?=$lang->code?>" role="tab" data-toggle="tab">
								<?=$lang->title?>
								<?php if( count($multiErrors[$lang->code]) ): ?>
									<span class="label label-danger"> <span class="glyphicon glyphicon-warning-sign"></span> Ошибка</span>
								<?php endif; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>

				<!-- Tab panes -->
				<div class="tab-content">
					<?php foreach($this->languages as $lang): ?>
						<div role="tabpanel" class="tab-pane" id="<?=$lang->code?>">
							<?php $errors = $i18nModel[$lang->id]->getErrors();?>

							<br/>

							<!--div class="form-group<?php if(isset($errors['title'])) echo ' has-error'; ?>">
								<?php #echo $form->labelEx($i18nModel[$lang->id],'title', array('class' => 'control-label')); ?>
								<?php #echo $form->textField($i18nModel[$lang->id],'title',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][title]', 'id' => 'title_'.$lang->code)); ?>
								<?php #echo $form->error($i18nModel[$lang->id],'title', array('class' => 'help-block')); ?>
							</div-->

							<div class="form-group<?php if(isset($errors['address'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'address', array('class' => 'control-label')); ?>
								<div class="row">
									<div class="col-md-11">
										<?php echo $form->textField($i18nModel[$lang->id],'address',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][address]', 'id' => 'address_'.$lang->code )); ?>
									</div>
									<?php if($lang->code == param('defaultLang')): ?>
									<div class="col-md-1">
										<a href="#" class="btn btn-default find" data-lang="<?= $lang->code ?>"><span class="glyphicon glyphicon-screenshot"></span></a>
									</div>
									<?php endif; ?>
								</div>

								<?php echo $form->error($i18nModel[$lang->id],'address', array('class' => 'help-block')); ?>
							</div>

							<div class="form-group<?php if(isset($errors['local_name'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'local_name', array('class' => 'control-label')); ?>
								<?php echo $form->textField($i18nModel[$lang->id],'local_name',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][local_name]')); ?>
								<?php echo $form->error($i18nModel[$lang->id],'local_name', array('class' => 'help-block')); ?>
							</div>

							<div class="form-group<?php if(isset($errors['metro'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'metro', array('class' => 'control-label')); ?>
								<?php echo $form->textField($i18nModel[$lang->id],'metro',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][metro]')); ?>
								<?php echo $form->error($i18nModel[$lang->id],'metro', array('class' => 'help-block')); ?>
							</div>

							<div class="form-group<?php if(isset($errors['how_to_find'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'how_to_find', array('class' => 'control-label')); ?>
								<?php echo $form->textArea($i18nModel[$lang->id],'how_to_find',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][how_to_find]')); ?>
								<?php echo $form->error($i18nModel[$lang->id],'how_to_find', array('class' => 'help-block')); ?>
							</div>

							<div class="form-group<?php if(isset($errors['work_hours'])) echo ' has-error'; ?>">
								<?php echo $form->labelEx($i18nModel[$lang->id],'work_hours', array('class' => 'control-label')); ?>
								<?php echo $form->textArea($i18nModel[$lang->id],'work_hours',array('class'=>'form-control', 'name' => $class.'I18n['.$lang->id.'][work_hours]')); ?>
								<?php echo $form->error($i18nModel[$lang->id],'work_hours', array('class' => 'help-block')); ?>
							</div>

							<?php if($lang->code == param('defaultLang')): ?>
							<div class="well">
								<div id="gmap_canvas" style="height:450px;width:100%;"></div>
							</div>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>

			</div>

		</div>
		<div class="col-md-3">

			<div class="row">
				<?php echo $form->labelEx($model,'pos_region_id', array('class' => 'control-label')); ?>
				<?php echo $form->dropDownList($model,'pos_region_id', CHtml::listData($regions, 'id', 'i18n.0.title'), array(
						'class' => 'form-control',
						#'ajax' => array(
						#	'type'=>'POST',
						#	'url'=>CController::createUrl('point/getcities'),
						#	'update'=>'#city_id'
						#),
						'id' => 'region_id'
				)); ?>
				<?php echo $form->error($model,'pos_region_id', array('class' => 'help-block')); ?>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'pos_city_id', array('class' => 'control-label')); ?>
				<?php echo $form->dropDownList($model,'pos_city_id', $cities, array('class' => 'form-control', 'id' => 'city_id')); ?>
				<?php echo $form->error($model,'pos_city_id', array('class' => 'help-block')); ?>
			</div>

			<div class="checkbox">
				<?php
				echo CHtml::openTag('label', array('for' => 'status'));
				echo $form->checkBox($model,'status', array('id' => 'status', 'value' => 1));
				echo $model->getAttributeLabel('status');
				echo CHtml::closeTag('label');
				?>
			</div>

			<div class="form-group buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success')); ?>
			</div>
		</div>
	</div>



<?php $this->endWidget(); ?>

</div><!-- form -->


<script>
	var map, infowindow, marker, marker1;
	var geocoder = new google.maps.Geocoder();
	var zoom = <?php echo empty($model->zoom) ? param('map')['zoom'] : $model->zoom; ?>;
	var lat = '<?php echo empty($model->lat) ? param('map')['lat'] : $model->lat; ?>';
	var lng = '<?php echo empty($model->lng) ? param('map')['lng'] : $model->lng; ?>';
	var title, city, address;

	$(function() {

        $('#myTab a').on('shown.bs.tab', function (e) {
            $('#lang').val($('#myTab li.active a').attr('aria-controls'));
        });
        $('#lang').val($('#myTab li.active a').attr('aria-controls'));

        <?php if($_GET['lang'] != ''): ?>
        $('#myTab a.<?= $_GET['lang'] ?>').click();
        <?php endif; ?>

		var lang = $('#myTab li.active').data('lang');
		updateInfo(lang);
		init_map();

		setTimeout(function(){
			$('#region_id').change();
		}, 0);

		$('.find').click(function(e) {
			e.preventDefault();
			findPosition($(this).data('lang'));
		});


        $('#region_id').on('change', function(){
            $.post('<?= CController::createUrl('point/getcities') ?>', $('#pos-form').serialize(), function(resp){
                $('#city_id').html(resp.content);
            }, 'json' );
        });

	});

	function init_map(){
		var myOptions = {
			zoom: zoom,
			center:new google.maps.LatLng(lat, lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
		marker = new google.maps.Marker({map: map, position: new google.maps.LatLng(lat, lng),draggable:true});
		infowindow = new google.maps.InfoWindow({content: formatContent() });
		google.maps.event.addListener(marker, "click", function(){
			infowindow.open(map,marker);
		});
		infowindow.open(map,marker);

		// drag stuff
		google.maps.event.addListener(marker, 'dragend', function() {
			geocodePosition( marker.getPosition() );
		});

		geocodePosition( marker.getPosition() );
	}

	function geocodePosition(pos) {
		$('#lat').val( pos.lat() );
		$('#lng').val( pos.lng() );
		$('#zoom').val( map.getZoom() );
	}

	function findPosition(lang) {
		updateInfo(lang);
		var full_address = city + ', ' + address;
		geocoder.geocode({'address': full_address}, function (results, status) {
			//console.log(results);
			if (status == google.maps.GeocoderStatus.OK) {
				mapLat = results[0].geometry.location.lat();
				mapLng = results[0].geometry.location.lng();
				$('#lat').val( mapLat );
				$('#lng').val( mapLng );
				$('#zoom').val( map.getZoom() );

				map.setCenter(new google.maps.LatLng(mapLat,mapLng));
				marker.setPosition(new google.maps.LatLng(mapLat,mapLng));

				var mapPopupContent = formatContent(lang);
				infowindow.setContent(mapPopupContent);
			} else {
				alert("Не получилось найти указанный адрес");
			}
		});
	}

	function updateInfo(lang) {
		title = $('#title_'+lang).val();
		address = $('#address_'+lang).val();
		city = $('#city_id option:selected').text();
	}

	function formatContent(lang) {
		updateInfo(lang);
		return '<p><strong>'+title+'</strong><br/>'+address+'<br/>'+city+'</p>';
	}

</script>