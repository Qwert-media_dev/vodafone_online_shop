<?php
/* @var $this PointController */
/* @var $model Pos */
/* @var $class string */
/* @var $i18nModel PosI18n[] */
/* @var $multiErrors array */
/* @var $regions PosRegion[] */
/* @var $cities array */


$this->breadcrumbs=array(
	'Точки продаж'=>array('admin'),
	$model->i18n[0]->title => array('edit','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	array('label'=>'Добавить точку', 'url'=>array('create')),
	array('label'=>'Список точек', 'url'=>array('admin')),
);
?>

<h1>Редактирование</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions', 'cities')); ?>