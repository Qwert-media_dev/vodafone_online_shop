<?php
/* @var $this PointController */
/* @var $model Pos */
/* @var $class string */
/* @var $i18nModel PosI18n[] */
/* @var $multiErrors array */
/* @var $regions PosRegion[] */
/* @var $cities array */

$this->breadcrumbs=array(
	'Точки продаж'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список точек', 'url'=>array('admin')),
);
?>

<h1>Добавить точку</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors', 'regions', 'cities')); ?>