<?php
/* @var $this PointController */
/* @var $model Pos */
/* @var $regions array */
/* @var $cities array */
/* @var $allCities array */

$this->breadcrumbs=array(
	'Точки продаж'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить точку', 'url'=>array('create')),
);

?>

<h1>Список точек</h1>


<?php
Yii::import('zii.widgets.grid.CGridView');
$this->widget('PointsGridView', array(
	'id'=>'pos-grid',
    //'ajaxUpdate' => false,
    'regions' => $regions,
    'cities' => $allCities,
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
    ),
	'filter'=>$model,
	'columns'=>array(
        array(
            'name' => 'pos_region_id',
            'value' => '$this->grid->regions[$data->pos_region_id]',
            'filter' => CHtml::activeDropDownList($model, 'pos_region_id', $regions, array('class' => 'form-control', 'empty' => ' '))
        ),
		array(
            'name' => 'pos_city_id',
            'value' => '$this->grid->cities[$data->pos_city_id]',
            'filter' => CHtml::activeDropDownList($model, 'pos_city_id', $cities, array('class' => 'form-control', 'empty' => ' '))
        ),
		'i18n.0.address',
        array(
            'name' => 'sort',
            'value' => '$data->sort',
            'filter' => false
        ),
		array(
			'name' => 'status',
			'value' => 'param("status")[$data->status]',
			'filter' => CHtml::activeDropDownList($model, 'status', param('status'), array('class' => 'form-control', 'empty' => ' '))
		),
		array(
			'class'=>'CButtonColumn',
				'htmlOptions' => array('width' => 240, 'align' => 'center'),
				'template' => '{on} {off} {update} {delete}',
				'buttons' => array(
					'on' => array(
						'label' => '<span class="glyphicon glyphicon-eye-open"></span>',
						'url' => 'Yii::app()->createUrl("manage/pos/point/status", array("id" => $data->id, "status" => 1))',
						'visible' => '$data->status == Pos::STATUS_INACTIVE',
						'options' => array(
							'class' => 'btn btn-xs',
							'alt' => 'Включить'
						)
					),
					'off' => array(
						'label' => '<span class="glyphicon glyphicon-eye-close"></span>',
						'url' => 'Yii::app()->createUrl("manage/pos/point/status", array("id" => $data->id, "status" => 0))',
						'visible' => '$data->status == Pos::STATUS_ACTIVE',
						'options' => array(
							'class' => 'btn btn-xs '
						),
					),
					'update' => array(
						'imageUrl' => false,
						'options' => array(
							'class' => 'btn btn-info btn-xs'
						)
					),
					'delete' => array(
						'imageUrl' => false,
						'options' => array(
							'class' => 'btn btn-danger btn-xs'
						)
					),
			)
		),
	),
));
?>


<script>
    $(function(){
        $('.filters select, .filters input').addClass('form-control');
    });
</script>
