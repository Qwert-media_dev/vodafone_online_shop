<?php
/* @var $this RegionController */
/* @var $model PosRegion */
/* @var $class string */
/* @var $i18nModel PosRegionI18n[] */
/* @var $multiErrors array */

$this->breadcrumbs=array(
	'Регионы'=>array('admin'),
	'Создание',
);

$this->menu=array(
	array('label'=>'Список регионов', 'url'=>array('admin')),
);
?>

<h1>Добавить регион</h1>

<?php $this->renderPartial('_form', compact('model', 'class', 'i18nModel', 'multiErrors')); ?>