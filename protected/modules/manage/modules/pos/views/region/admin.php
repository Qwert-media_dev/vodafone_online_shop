<?php
/* @var $this RegionController */
/* @var $model PosRegion */

$this->breadcrumbs=array(
	'Регионы'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Добавить регион', 'url'=>array('create')),
);
?>

<h1>Список регионов</h1>

<?php if( user()->hasFlash('success') ): ?>
	<div class="alert alert-success"><?= user()->getFlash('success') ?></div>
<?php endif; ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'pos-region-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		'i18n.0.title',
		array(
			'class'=>'CButtonColumn',
			'htmlOptions' => array('width' => 240, 'align' => 'center'),
			'template' => '{update} {delete}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					)
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					)
				),
			)
		),
	),
));

