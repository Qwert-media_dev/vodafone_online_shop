<?php
/* @var $this NewsController */
/* @var $provider CActiveDataProvider */

$this->breadcrumbs=array(
    'Новости'=>array('admin'),
    'Список новостей',
);

// $this->menu=array(
//     array('label'=>'Добавить телефон', 'url'=>array('create')),
//     array('label'=>'Архив', 'url'=>array('archive')),
//     array('label'=>'Корзина', 'url'=>array('trash')),
// );

?>

    <script>
        $(function(){
            $( "#date" ).datepicker({
                showAnim: "show",
                dateFormat: "yy-mm-dd"
            });
        });
    </script>

<h1>Список телефонов</h1>


<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'news-grid',
    'dataProvider' => $provider,
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
    //'filter' => $model,
    'columns' => array(
        'title',
        'price',
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
            'template' => '{update} {delete}',
            // 'buttons' => array(
                // 'update' => array(
                //     'imageUrl' => false,
                //     'options' => array(
                //         'class' => 'btn btn-info btn-xs'
                //     )
                // ),
                // 'delete' => array(
                //     'imageUrl' => false,
                //     'options' => array(
                //         'class' => 'btn btn-danger btn-xs'
                //     )
                // ),
            // )
        ),
    ),
));