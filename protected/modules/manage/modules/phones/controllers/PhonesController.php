<?php

class PhonesController extends ManageController
{

    const TYPE = 'phones';
    protected static $class = 'News';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('index'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function actions()
    {
        return array(
            'saveimage'=>'application.modules.manage.components.SaveImageAction',
        );
    }

  

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $provider = News::model()->getTable(self::TYPE, 0);
        $this->render('admin',compact('provider'));
    }

    
    public function actionIndex()
    {
        // Yii::app()->cache->flush();
        $provider = new CActiveDataProvider('Phones');
        $this->render('index',compact('provider'));
    }
  
    public function loadModel($id)
    {
        $model=News::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }


}
