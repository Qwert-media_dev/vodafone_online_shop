<?php
/* @var $this AdminGroupsController */
/* @var $model AdminGroups */
/* @var $form CActiveForm */
/* @var $rules array */
/* @var $selected array */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admin-groups-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

	<?php echo $form->errorSummary($model, '<div class="alert alert-danger" role="alert">', '</div>'); ?>

    <div class="form-group group-date <?php echo  $this->error($model, 'title') ?>">
		<?php echo $form->labelEx($model,'title', array('class' => 'control-label')); ?>
		<?php echo $form->textField($model,'title',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'title', array('class' => 'help-block')); ?>
	</div>

    <label for="">Уровни доступа</label>
    <div class="well">
        <?php echo CHtml::checkBoxList(get_class($model).'[rules]', $selected, $rules); ?>
    </div>

    <div class="form-group buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', array('class' => 'btn btn-success btn-save', 'data-loading-text' => 'Загрузка...')); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->