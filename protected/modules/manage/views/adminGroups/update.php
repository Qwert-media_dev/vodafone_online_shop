<?php
/* @var $this AdminGroupsController */
/* @var $model AdminGroups */
/* @var $rules array */
/* @var $selected array */

$this->breadcrumbs=array(
	'Группы доступа'=>array('admin'),
	$model->title=>array('update','id'=>$model->id),
	'Редактировать',
);

$this->menu=array(
	array('label'=>'Добавить группу', 'url'=>array('create')),
	array('label'=>'Список групп', 'url'=>array('admin')),
);
?>

<h1>Редактирование группы "<?php echo $model->title; ?>"</h1>

<?php $this->renderPartial('_form', compact('model', 'rules', 'selected')); ?>