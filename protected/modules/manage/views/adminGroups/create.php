<?php
/* @var $this AdminGroupsController */
/* @var $model AdminGroups */
/* @var $rules array */
/* @var $selected array */

$this->breadcrumbs=array(
	'Группы доступа'=>array('index'),
	'Добавление',
);

$this->menu=array(
	array('label'=>'Список групп', 'url'=>array('admin')),
);
?>

<h1>Создание группы</h1>

<?php $this->renderPartial('_form', compact('model', 'rules', 'selected')); ?>