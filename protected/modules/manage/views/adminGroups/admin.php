<?php
/* @var $this AdminGroupsController */
/* @var $model AdminGroups */

$this->breadcrumbs=array(
	'Группы доступа'=>array('admin'),
	'Список',
);

$this->menu=array(
	array('label'=>'Создать группу', 'url'=>array('create')),
);
?>

<h1>Группы доступа</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admin-groups-grid',
	'dataProvider'=>$model->search(),
    'itemsCssClass' => 'table table-striped',
	'pagerCssClass' => 'pager-container',
	'pager' => array(
		'class'=>'CLinkPager',
		'htmlOptions' => array('class' => 'pagination'),
		'firstPageLabel' => '&laquo;',
		'lastPageLabel' => '&raquo;',
		'nextPageLabel' => false,
		'prevPageLabel' => false,
		'header' => false,
        'selectedPageCssClass' => 'active'
	),
	//'filter'=>$model,
	'columns'=>array(
		'title',
        array(
            'name' => 'protected',
                'type' => 'raw',
                'value' => '$data->getProtectedSign()'
        ),
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 240, 'align' => 'center'),
			'template' => '{update} {delete}',
			'buttons' => array(
				'update' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-info btn-xs'
					),
                    'visible' => '$data->protected==0'
				),
				'delete' => array(
					'imageUrl' => false,
					'options' => array(
						'class' => 'btn btn-danger btn-xs'
					),
                    'visible' => '$data->protected==0'
				),
			)
		),
	),
));
