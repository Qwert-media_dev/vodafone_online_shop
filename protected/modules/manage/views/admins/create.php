<?php
/* @var $this AdminsController */
/* @var $model Admins */
/* @var $groups AdminGroups[] */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	'Добавить',
);

$this->menu=array(
	array('label'=>'Управление пользователями', 'url'=>array('admin')),
);
?>

<h1>Добавить пользователя</h1>

<?php $this->renderPartial('_form', compact('model', 'groups')); ?>