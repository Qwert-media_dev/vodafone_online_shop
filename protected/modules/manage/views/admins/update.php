<?php
/* @var $this AdminsController */
/* @var $model Admins */
/* @var $groups AdminGroups[] */

$this->breadcrumbs=array(
	'Пользователи'=>array('admin'),
	$model->name=>array('update','id'=>$model->id),
	'Редактирование',
);

$this->menu=array(
	//array('label'=>'List Admins', 'url'=>array('index')),
	array('label'=>'Добавить пользователя', 'url'=>array('create')),
	//array('label'=>'View Admins', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление пользователями', 'url'=>array('admin')),
);
?>

<h1>Редактирование пользователя <?php echo $model->name; ?></h1>

<?php $this->renderPartial('_form', compact('model', 'groups')); ?>