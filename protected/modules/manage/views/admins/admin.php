<?php
/* @var $this AdminsController */
/* @var $model Admins */

$this->breadcrumbs=array(
	'Пользователи'=>array('admiin'),
	'Управление',
);

$this->menu=array(
	//array('label'=>'Список админов', 'url'=>array('index')),
	array('label'=>'Добавить пользователя', 'url'=>array('create')),
);

?>

<h1>Управление пользователями</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'admins-grid',
    'itemsCssClass' => 'table table-striped',
    'pagerCssClass' => 'pager-container',
    'pager' => array(
        'class'=>'CLinkPager',
        'htmlOptions' => array('class' => 'pagination'),
        'firstPageLabel' => '&laquo;',
        'lastPageLabel' => '&raquo;',
        'nextPageLabel' => false,
        'prevPageLabel' => false,
        'header' => false,
        'selectedPageCssClass' => 'active'
    ),
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		'name',
		'email',
        'group.title',
        array(
			'name' => 'status',
			'value' => 'param("status")[$data->status]',
			//'filter' => CHtml::activeDropDownList($model, 'status', param('status'), array('class' => 'form-control', 'empty' => ' '))
		),
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array('width' => 200),
            'template' => '{update} {delete}',
            'buttons' => array(
                'update' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-info btn-xs'
                    )
                ),
                'delete' => array(
                    'imageUrl' => false,
                    'options' => array(
                        'class' => 'btn btn-danger btn-xs'
                    )
                ),
            )
		),
	),
)); ?>
