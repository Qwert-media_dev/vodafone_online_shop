<?php
/* @var $this AdminsController */
/* @var $model Admins */
/* @var $form CActiveForm */
/* @var $groups AdminGroups[] */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'admins-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

    <!-- fake fields are a workaround for chrome autofill getting the wrong fields -->
    <input style="display:none" type="text" name="fakeusernameremembered"/>
    <input style="display:none" type="password" name="fakepasswordremembered"/>

	<p class="note">Поля отмеченные <span class="required">*</span> обязательные.</p>

    <div class="row">
        <div class="col-md-4"></div>

        <div class="col-md-4 well">
            <?php echo $form->errorSummary($model, '<div class="alert alert-danger" role="alert">', '</div>'); ?>

            <div class="form-group <?php echo  $this->error($model, 'name') ?>">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>

            <div class="form-group <?php echo  $this->error($model, 'email') ?>">
                <?php echo $form->labelEx($model,'email'); ?>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>

            <div class="form-group <?php echo  $this->error($model, 'password') ?>">
                <?php echo $form->labelEx($model,'password'); ?>
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255, 'value' => '', 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>

            <div class="form-group <?php echo  $this->error($model, 'group_id') ?>">
                <?php echo $form->labelEx($model,'group_id'); ?>
                <?php echo $form->dropDownList($model,'group_id', CHtml::listData($groups, 'id', 'title'),array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'group_id'); ?>
            </div>


            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'show_index'));
                echo $form->checkBox($model,'show_index', array('id' => 'show_index', 'value' => 1));
                echo $model->getAttributeLabel('show_index');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="checkbox">
                <?php
                echo CHtml::openTag('label', array('for' => 'status'));
                echo $form->checkBox($model,'status', array('id' => 'status', 'value' => 1));
                echo $model->getAttributeLabel('status');
                echo CHtml::closeTag('label');
                ?>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?php echo CHtml::link('Отмена', array('admins/admin'), ['class' => 'btn btn-default', 'style' => 'width:100%']); ?>
                </div>
                <div class="col-md-6">
                    <?php echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('class' => 'btn btn-success', 'style' => 'width:100%')); ?>
                </div>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>



<?php $this->endWidget(); ?>

</div><!-- form -->