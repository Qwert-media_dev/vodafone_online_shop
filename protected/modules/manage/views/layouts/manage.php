<?php
/* @var $this ManageController */

Yii::app()->clientScript->registerCoreScript('jquery');

$cs = Yii::app()->getClientScript();
$scriptUrl = $cs->getCoreScriptUrl() . '/jui/js/';
$cssUrl = $cs->getCoreScriptUrl() . '/jui/css/base/';
Yii::app()->clientScript->registerScriptFile($scriptUrl . 'jquery-ui.min.js');
Yii::app()->clientScript->registerCssFile($cssUrl . 'jquery-ui.css');

$controller = Yii::app()->controller->id;
$action = Yii::app()->controller->action->id;
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo Yii::app()->name; ?></title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">

    <!--link rel="stylesheet" type="text/css" href="/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" /-->
    <link rel="stylesheet" href="/css/manage.css?1">

    <!-- Latest compiled and minified JavaScript -->
    <script src="/js/bootstrap.min.js"></script>

    <?php if($this->useFileAPI) { ?>
    <script>
        window.FileAPI = {
            debug: true, // debug mode
            staticPath: '/js/jquery.fileapi/FileAPI/' // path to *.swf
        };
    </script>
    <script type="text/javascript" src="/js/jquery.fileapi/FileAPI/FileAPI.min.js"></script>
    <script type="text/javascript" src="/js/jquery.fileapi/jquery.fileapi.min.js"></script>
    <?php } ?>
    <script src="/js/manage.js?1"></script>
</head>
<body class="loaded">

<div id="ol"></div>

<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/" target="_blank"><?php echo Yii::app()->name; ?></a>
        </div>
        <div class="navbar-collapse collapse">
            <?php $this->widget('ManageMenuWidget'); ?>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="<?php echo url_to('user/logout'); ?>">Выход</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">

    <div class="row">
        <?php
        if( sizeof($this->menu) ) {
            $this->widget('zii.widgets.CMenu', array(
                'items'=> $this->menu,
                'htmlOptions' => array('class' => 'nav nav-tabs', 'role' => 'tablist')
            ));
        }
        ?>
    </div>
    <div class="row">
        <?php echo $content; ?>
    </div>

</div> <!-- /container -->

</body>
</html>