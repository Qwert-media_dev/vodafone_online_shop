<?php
/* @var $this SearchLogController */
/* @var $model SearchLog */

$this->breadcrumbs=array(
	'Search Logs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SearchLog', 'url'=>array('index')),
	array('label'=>'Create SearchLog', 'url'=>array('create')),
	array('label'=>'View SearchLog', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SearchLog', 'url'=>array('admin')),
);
?>

<h1>Update SearchLog <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>