<?php
/* @var $this SearchLogController */
/* @var $model SearchLog */

$this->breadcrumbs=array(
	'Search Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SearchLog', 'url'=>array('index')),
	array('label'=>'Manage SearchLog', 'url'=>array('admin')),
);
?>

<h1>Create SearchLog</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>