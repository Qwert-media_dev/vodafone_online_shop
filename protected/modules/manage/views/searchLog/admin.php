<?php
/* @var $this SearchLogController */
/* @var $model SearchLog */

$this->breadcrumbs = array(
    'Search Logs' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List SearchLog', 'url' => array('index')),
    array('label' => 'Create SearchLog', 'url' => array('create')),
);

?>

<h1>Manage Search Logs</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'search-log-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'query',
        'code',
        array(
            'name' => 'response',
            'type' => 'raw',
            'value' => 'formatXML($data->response)'
        ),
        'exception',
        'created_at',
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
