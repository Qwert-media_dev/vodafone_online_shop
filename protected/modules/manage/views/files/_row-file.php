<?php
/* @var $this FilesController */
/* @var $item Files */
/* @var $folder_id integer */
?>
<tr class="file-<?= $item->id; ?>">
    <td class="box-to-copy" data-url="http://<?= $_SERVER['HTTP_HOST'] ?>/images/upload/<?= $item->local_name ?>" style="word-break: break-all;">
        <a class="copy" href="#" data-clipboard-text="http://<?= $_SERVER['HTTP_HOST'] ?>/images/upload/<?= $item->local_name ?>"> <span class="glyphicon glyphicon-copy"></span> </a>
        <?= $item->original_name ?>
    </td>
    <td>
        <?php if(in_array($item->ext, $this->images)): ?>
            <span class="glyphicon glyphicon-picture preview" title="Preview" data-content="<?= CHtml::encode(CHtml::image('/images/upload/'.Img::toShove('uploadPath', $item->local_name, 200, 200) )) ?>"></span>
        <?php endif; ?>
    </td>
    <td><?= $item->ext ?></td>
    <td><?= FS::fileSize($item->size) ?></td>
    <td><?= $item->created_at == 'NOW()' ? date('Y-m-d H:i:s') : $item->created_at ?></td>
    <td nowrap="nowrap">
        <?php
        if( $folder_id ) {
            echo CHtml::link( 'убрать из папки', array('files/removefromfolder', 'id' => $item->id, 'folder_id' => (int)$_GET['folder_id']), array('class' => 'btn btn-link') );
        }
        ?>
        <a class="btn btn-info btn-xs" href="<?php echo url_to('manage/files/download', array('id' => $item->id)); ?>">скачать</a>
        <a class="btn btn-danger btn-xs delete" href="<?php echo url_to('manage/files/delete', array('id' => $item->id)); ?>">удалить</a>
    </td>
    <td>
        <?php if( !$folder_id ): ?>
        <div class="dropdown">
            <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                отправить в
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu menu-with-folders" aria-labelledby="dropdownMenu1" data-id="<?= $item->id ?>">

            </ul>
        </div>
        <?php endif; ?>
    </td>
</tr>