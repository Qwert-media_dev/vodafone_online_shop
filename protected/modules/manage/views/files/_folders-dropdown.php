<?php
/* @var $items FilesFolders */
?>

<?php foreach($items as $item): ?>
    <li>
        <a href="<?= url_to('manage/files/addtofolder', array('folder_id' => $item->id)) ?>" class="copy-to-folder" >
            <?= $item->title ?>
        </a>
    </li>
<?php endforeach; ?>
