<?php
/* @var $items FilesFolders[] */
/* @var $folder_id integer */

foreach($items as $item) {
    $this->renderPartial('_row-folder', compact('item', 'folder_id'));
}