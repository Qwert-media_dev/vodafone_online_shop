<?php
/* @var $item FilesFolders */
/* @var $folder_id integer */

$params = array();
if($folder_id == $item->id) {
    $params['class'] = 'active';
}
?>
<li data-id="<?= $item->id ?>" data-title="<?= CHtml::encode($item->title) ?>" id="folder_<?= $item->id ?>">
    <span><?= CHtml::link( sprintf('%s (%s)', $item->title, $item->cnt) , array('files/admin', 'folder_id' => $item->id), $params) ?></span>
    <?php if(!$item->protected): ?>
    <span class="actions" style="display:none">
        <a href="#"><span class="glyphicon glyphicon-pencil"></span></a>
        <a href="#"><span class="glyphicon glyphicon-remove"></span></a>
    </span>
    <?php endif; ?>
</li>