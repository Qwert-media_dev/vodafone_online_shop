<?php
/* @var $items Files[] */
/* @var $categories FilesFolders[] */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Files</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <style>
        body {font-family:Sans-Serif}
        a.active {
            font-weight:bold;
        }
        /*#files {margin:20px;}*/
        /*#files a {display:block;border:#D8D8D8 2px solid;margin:3px;padding:5px 15px;box-sizing:border-box;text-decoration:none;}*/
    </style>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-9">
            <h1>Файлы</h1>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Файл</th>
                    <th>Размер</th>
                    <th>Дата создания</th>
                </tr>
                </thead>
                <?php foreach($items as $item): ?>
                    <tr>
                        <td><a href="/images/upload/<?= $item->local_name ?>" class="file"><?= $item->original_name ?></a></td>
                        <td><?= FS::fileSize($item->size) ?></td>
                        <td><?= $item->created_at ?></td>
                    </tr>
                <?php endforeach; ?>

            </table>
        </div>
        <div class="col-md-3">
            <h1>Папки</h1>
            <ul class="list-unstyled">
                <li>
                    <?php
                    $param = (int)$_GET['folder_id'] == 0 ? ['class' => 'active'] : [];
                    echo CHtml::link('Все', ['export'], $param);
                    ?>
                </li>
                <?php foreach($categories as $category): ?>
                <li>
                    <?php
                    $param = $_GET['folder_id'] == $category->id ? ['class' => 'active'] : [];
                    echo CHtml::link($category->title.' '.CHtml::tag('span', ['class' => 'badge'], $category->cnt ), ['export', 'folder_id' => $category->id], $param);
                    ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<div id="files">

</div>

<script src="/js/jquery-1.10.1.min.js" type="text/javascript"></script>
<script>
    jQuery(document).ready(function ($) {

        //Optional: specify custom height
        window.frameElement.style.height = '500px';

        $("a.file").click(function () {
            selectAsset($(this).attr('href'));
            return false;
        });

    });

    /* 
     USE THIS FUNCTION TO SELECT CUSTOM ASSET WITH CUSTOM VALUE TO RETURN
     An asset can be a file, an image or a page in your own CMS
     */
    function selectAsset(assetValue) {
        //Get selected URL
        if (window.frameElement.id == 'pageBrowse') {
            var field = $(window.frameElement).data('field');
            var lang = $(window.frameElement).data('lang');
            parent.top.$('#'+field+'_'+lang).val(assetValue);
            parent.top.$('#file-modal').modal('hide');
        } else {
            var inp = parent.top.$('#active-input').val();
            parent.top.$('#' + inp).val(assetValue);
        }

        //Close dialog
        if (window.frameElement.id == 'ifrFileBrowse') parent.top.$("#md-fileselect").data('simplemodal').hide();
        if (window.frameElement.id == 'ifrImageBrowse') parent.top.$("#md-imageselect").data('simplemodal').hide();

    }
</script>
</body>
</html>