<?php
/* @var $this FilesController */
/* @var $items Files[] */
/* @var $folders FilesFolders[] */
/* @var $extentions Files[] */
/* @var $folder_id integer */

$this->breadcrumbs=array(
	'Файлы'=>array('admin'),
	'Управление файлами',
);

?>

<script type="text/javascript" src="/js/dropzone.js"></script>


<h1>Управление файлами</h1>

<?php if( param('adminEmail') == user()->email ): ?>
<blockquote>
    upload_max_filesize=<?= ini_get('upload_max_filesize') ?><br>
    max_file_uploads=<?= ini_get('max_file_uploads') ?><br>
    max_post_size=<?= ini_get('max_post_size') ?><br>
    memory_limit=<?= ini_get('memory_limit') ?>
</blockquote>
<?php endif; ?>

<p>
    <span class="label label-success">Лимит на 1 файл: 12Mb</span>
</p>

<div id="clipboard-container"><textarea id="clipboard"></textarea></div>
<!-- <input type="text" class="paste-input" />
<div class="text info-box"></div> -->

<div class="alerts">
    <?php if( user()->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo user()->getFlash('success'); ?>
        </div>
    <?php endif; ?>

</div>

<!-- drop zone  -->
<div id="dropzone">
    <div class="drop-field">
        <p>Перетяните сюда файл или нажмите<br> <span class="button"><span>Загрузить</span></span></p>
    </div>
    <div class="fallback">
        <input name="file" type="file" multiple />
    </div>
</div>
<!-- drop zone / -->

<div class="progress" style="display: none;">
    <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
        <span class="sr-only">0% Complete (success)</span>
    </div>
</div>

<div class="row">
    <div class="col-md-10">
        <table class="table table-striped" id="table">
            <thead>
            <tr>
                <th>Файл</th>
                <th></th>
                <th>Расширение</th>
                <th>Размер</th>
                <th>Дата</th>
                <th nowrap="nowrap">Действия</th>
                <th></th>
            </tr>
            <?php
            $params = array();
            if( array_key_exists('folder_id', $_GET) ) {
                $params['folder_id'] = $_GET['folder_id'];
            }
            $formAction = url_to('manage/files/admin', $params);
            echo CHtml::beginForm($formAction, 'get', array('id' => 'filter-form'));
            ?>
            <tr>
                <th><?= CHtml::textField('filter[original_name]', $_GET['filter']['original_name'], array('class' => 'form-control live-fields')) ?></th>
                <th></th>
                <th><?= CHtml::dropDownList('filter[ext]', $_GET['filter']['ext'], CHtml::listData($extentions, 'ext', 'ext'), array('empty' => 'Все', 'class' => 'form-control live-fields')) ?></th>
                <th></th>
                <th></th>
                <th nowrap="nowrap"></th>
                <th></th>
            </tr>
            <?php echo CHtml::endForm(); ?>
            </thead>
            <tbody id="table-body">
            <?php
            foreach($items as $item) {
                $this->renderPartial('_row-file', compact('item', 'folder_id'));
            }
            ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-2">
        <div class="row">
            <div class="col-md-4">
                <label for="">Папки</label>
            </div>
            <div class="col-md-8">
                <a class="create" href="#">Создать папку</a>
            </div>
        </div>


        <?php echo CHtml::beginForm(array('files/createfolder'), 'post', array('id' => 'add-folder')); ?>
        <div class="row" id="create-folder" style="display:none">
            <div class="col-md-10" style="margin-right: -10px;"><?php echo CHtml::textField('title', '', array('class' => 'form-control title')); ?></div>
            <div class="col-md-2" style="padding-left: 0;"> <button class="btn btn-success">OK</button> </div>
        </div>
        <?php echo CHtml::endForm(); ?>

        <div class="reset-folders"><?php
            $params = array();
            if( $folder_id == 0 ) $params['class'] = 'active';
            echo CHtml::link( 'Все' , array('files/admin'), $params);
            ?></div>
        <ul class="folders sortable droppable"></ul>
        <div class="row">
            <div class="col-md-6">
                <a class="btn btn-success btn-xs btn-sorting" href="#" id="apply-sorting" style="display:none">Сохранить</a>
            </div>
            <div class="col-md-6">
                <a class="btn btn-danger btn-xs btn-sorting" href="#" id="cancel-sorting" style="display:none;">Отменить</a>
            </div>
        </div>


    </div>
</div>




<script>
    var folder_id = <?= (int)$folder_id ?>;
    $(function(){

        updateMenuDD();
        updateFoldersMenu();

        $("#dropzone").dropzone({
            url: "<?= url_to('manage/files/bulk', array('folder_id' => $folder_id)) ?>",
            addRemoveLinks: "dictRemoveFile",
            clickable: 'div.drop-field p span.button',
            paramName: 'file',
            init: function() {

                this.on('error', function(p1, errorMessage, xhr){
                    addError('Ошибка: ' + xhr.statusText);
                });

                this.on('success', function(data, resp){

                    if(typeof resp == 'string') resp = $.parseJSON(resp);

                    if( resp.status== 'ok' ) {
                        $('#table-body').prepend(resp.item.row);
                        initClipboard( $('.file-'+resp.item.id+' .copy').get(0) );
                        updateFoldersMenu();
                    } else  {
                        console.log(resp);
                        if( resp.reason == 'no-data' ) {
                            addError('Ошибка: Не получилось загрузить файл');
                        } else {
                            addError('Ошибка: '+resp.file+', error='+resp.reason);
                        }
                    }

                });

                this.on('totaluploadprogress', function(data, resp){
                    //console.log('totaluploadprogress', data);
                    if(data < 100) {
                        $('#dropzone').hide();
                        $('.progress').show().find('.progress-bar').css('width', data+'%');
                    } else {
                        $('#dropzone').show();
                        $('.progress').hide();
                    }
                });

            }
        });

        $(document).on('click', 'a.delete', function(e){
            return confirm('Удалить файл ?');
        });

        $('.create').click(function(e){
            e.preventDefault();
            $('#create-folder').toggle();
        });

        $('#add-folder').submit(function(e){
            e.preventDefault();
            var $form = $(this);
            $.ajax({
                url: $form.attr('action'),
                data: $form.serialize(),
                dataType: 'json',
                type: $form.attr('method'),
                success: function(resp) {
                    if( resp.status == 'ok' ) {
                        if( resp.old ) {
                            $('.folders').find('li[data-id="'+resp.id+'"]').replaceWith(resp.row);
                        } else {
                            $('.folders').append(resp.row);
                        }
                        updateMenuDD();
                    }
                    $('#create-folder').toggle();
                    $('#add-folder').find('.id').remove();
                    $('#add-folder').find('.title').val('');
                },
                error: function() {
                    $('#create-folder').toggle();
                }
            });
        });

        
        $('.live-fields').change(function(e){
            $('#filter-form').submit();
        });


        $(document).on({
            mouseenter: function(event){
                $(this).find('.actions').show();
            },
            mouseleave: function(event){
                $(this).find('.actions').hide();
            }
        }, '.folders li');


        $(document).on('click', 'a .glyphicon-pencil', function(e){
            e.preventDefault();
            var $form = $('#add-folder');
            var $row = $(this).parent().parent().parent();
            var id = $row.data('id');
            var title = $row.data('title');
            if( !$('#add-folder .id').length ) {
                $form.append( '<input type="hidden" name="id" class="id" value="'+id+'" >' );
            }
            $form.find('.title').val(title);
            $('#create-folder').show();
        });


        $(document).on('click', '.glyphicon-remove', function(e){
            e.preventDefault();
            var $row = $(this).parent().parent().parent();
            var id = $row.data('id');
            if( confirm('Удалить папку ?') ) {
                $.post('<?= url_to('manage/files/deletefolder' ) ?>', {"id": id}, function(resp){
                    console.log(resp);
                    if(resp.status == 'ok') {
                        $row.remove();
                        updateMenuDD();
                    }
                }, 'json');
            }
        });

        $('#apply-sorting').click(function(e){
            e.preventDefault();
            $.post('<?= url_to('manage/files/sortfolders') ?>', {"items":$( ".sortable" ).sortable('toArray')}, function (resp) {
                if(resp.status != 'ok') {
                    $( ".sortable" ).sortable('cancel');
                }
                $('.btn-sorting').hide();
            }, 'json');
        });

        $('#cancel-sorting').click(function(e){
            e.preventDefault();
            $( ".sortable" ).sortable('cancel');
            $('.btn-sorting').hide();
        });


        $( ".sortable" ).sortable({
            placeholder: "ui-state-highlight",
            stop: function( event, ui ) {
                $('.btn-sorting').show();
            }
        });
        $( ".sortable" ).disableSelection();


        $(document).on('click', '.copy-to-folder', function(e){
            e.preventDefault();
            $.post( $(this).attr('href'), {file_id: $(this).parent().parent().data('id')}, function(resp){
                if(resp.msg != '') {
                    var content = '<div class="alert alert-'+resp.class+' alert-dismissible" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        resp.msg +
                        '</div>';
                    $('.alerts').prepend(content);
                    updateFoldersMenu();
                }
            }, 'json' );
        });

        $('.copy').click(function(e){ e.preventDefault(); });
        $('.copy').each(function(){
            initClipboard( $(this).get(0) );
        });

        initPreview();
    }); // onload


    function initClipboard( element ) {
        var client = new ZeroClipboard( element );

        client.on( "ready", function( readyEvent ) {

            client.on( "aftercopy", function( event ) {
                // `this` === `client`
                // `event.target` === the element that was clicked
                var $row = $(event.target).parent().parent();
                $row.effect( "highlight", {}, 500, function(){ $row.show(); } );
                //console.log("Copied text to clipboard: " + event.data["text/plain"] );
            } );
        } );
    }


    /**
     * Обновляет выпадающее меню для копирования файла в папку
     */
    function updateMenuDD() {
        $.get('<?= url_to('manage/files/getfolders') ?>', function (resp) {
            $('.menu-with-folders').html(resp);
        });
    }

    /**
     * Обновляет меню папок
     */
    function updateFoldersMenu() {
        $.get('<?= url_to('manage/files/getmenu') ?>', {folder_id: folder_id}, function(resp){
            $('.folders').html(resp);
        });
    }

    function initPreview() {
        $('.preview').popover({trigger: 'hover', html: true});
    }


    function addError(msg) {
        $('.alerts').append('<div class="alert alert-danger alert-dismissible" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
            msg + '</div>');
    }


</script>

<script type="text/javascript" src="/js/trello.js"></script>
<script type="text/javascript" src="/js/ZeroClipboard.min.js"></script>