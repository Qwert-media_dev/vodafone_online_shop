<?php

class Cache {

    public static function set($key, $data, $dependency=null) {
        Yii::app()->cache->set($key, $data, Yii::app()->params['cache_ttl'], $dependency);
    }

    public static function get($key) {
        return Yii::app()->cache->get($key);
    }

    public static function delete($key) {
        Yii::app()->cache->delete($key);
    }

}