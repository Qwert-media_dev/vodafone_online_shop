<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 19.05.2015
 * Time: 15:44
 */

Yii::import('application.vendor.*');
require 'autoload.php';
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'gd'));

class Img {

    public static function crop($folder, $image, $width, $height) {
        if( $image == '' || !file_exists(Yii::app()->params[$folder] . $image) ) return 'no-image.jpg';
        $targetName = sprintf('crop-%d-%d-%s', $width, $height, $image);

        if( !file_exists( Yii::app()->params[$folder] . $targetName ) ) {
            $img = Image::make( Yii::app()->params[$folder] . $image );
            $img->fit($width, $height);
            $img->crop($width, $height);
            $img->save( Yii::app()->params[ $folder ] . $targetName );
        }

        return $targetName;
    }

	public static function  toShove($folder, $image, $width, $height) {
		if( $image == '' || !file_exists(Yii::app()->params[$folder] . $image) ) return 'no-image.jpg';
		$info = getimagesize(Yii::app()->params[$folder] . $image);
		$sizes = self::toBound(['width' => $info[0], 'height' => $info[1]], ['width' => $width, 'height' => $height]);
		$targetName = sprintf('cropp-%d-%d-%s', $sizes['width'], $sizes['height'], $image);
		if( !file_exists( Yii::app()->params[$folder] . $targetName ) ) {
			$back = Image::canvas($width, $height, '#FFFFFF');

			$img = Image::make( Yii::app()->params[$folder] . $image );
			$img->resize($sizes['width'], $sizes['height']);
			//$img->save( Yii::app()->params[ $folder ] . $targetName );

			$back->insert($img, 'center');
			$back->save( Yii::app()->params[ $folder ] . $targetName );
		}
		return $targetName;
	}

    /**
     * Впихиваем невпихуеваемое
     * Имея размеры исходной картинки и размеры бокса в который надо ее втиснуть, возвращает размеры
     * скукоженной картинки прямо под бокс
     *
     * @param $image array - array('width' => ??, 'height' => ??) размеры исходного изображения
     * @param $box array - array('width' => ??, 'height' => ??) размеры доступной коробки
     * @return array - array('width' => ??, 'height' => ??) конечные размеры изображения для помещения в коробку
     *
     */
    public static function toBound($image, $box) {
        $widthScale = $heightScale = 0;

        if($image['width'] != 0)
            $widthScale = $box['width'] / $image['width'];

        if($image['height'] != 0)
            $heightScale = $box['height'] / $image['height'];

        $scale = min($widthScale, $heightScale);


        return array('width' => (int)($image['width']*$scale), 'height' => (int)($image['height']*$scale) );
    }
}