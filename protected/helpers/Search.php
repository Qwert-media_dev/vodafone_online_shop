<?php
Yii::import('application.vendor.*');
require('autoload.php');


class Search
{

    const LIMIT = 5;
    const PAGE = 'page';

    protected $devKey;
    protected $engineID;
    protected $pagination;
    protected $items = [];
    protected $total=0;
    protected $query;
    protected $currentPage=0;

    public function __construct($query)
    {
        $this->devKey = param('cse')['devKey'];
        $this->engineID = param('cse')['engineID'];
        $this->currentPage = (int)$_GET[self::PAGE]-1;
        if($this->currentPage < 0) $this->currentPage = 0;
        $this->query = trim($query);
        if ($this->query != '') {
            $response = $this->fetchData($this->query);
            $this->parseData($response);
        }
    }

    /*
    public function getResults()
    {
        return new CArrayDataProvider($this->items, array(
            'id' => 'search',
            'pagination' => [
                'pageSize' => 5
            ]

        ));
    }
    */

    public function getItems() {
        return $this->items;
    }

    public function getTotal() {
        return $this->total;
    }

    public function getPagination() {
        return $this->pagination;
    }

    protected function getFirstIndex() {
        return ($this->currentPage*self::LIMIT)+1;
    }

    protected function parseData($json)
    {
        $data = CJSON::decode($json);
        if (is_array($data) && array_key_exists('items', $data)) {
            $firstIndex = $this->getFirstIndex();
            foreach ($data['items'] as $idx => $item) {
                $this->items[] = [
                    'num' => $firstIndex + $idx,
                    'title' => $item['htmlTitle'],
                    'href' => $item['link'],
                    'summary' => $item['htmlSnippet']
                ];
            }

            $this->total = $data['searchInformation']['totalResults'];
            $this->pagination = new CPagination( $this->total );
            $this->pagination->pageSize = self::LIMIT;
            return true;
        }
        return false;
    }


    protected function fetchData($query)
    {
        //$url = sprintf("https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&alt=atom&num=10&q=%s", $this->devKey, $this->engineID, $query);
        //$url = sprintf("https://www.google.com/cse?cx=%s&client=google-csbe&output=xml_no_dtd&q=%s", $this->engineID, $query);
        if($this->currentPage) {
            $start = $this->getFirstIndex();
            $url = sprintf("https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&q=%s&num=%d&start=%d", $this->devKey, $this->engineID, $query, self::LIMIT, $start);
        } else {
            $url = sprintf("https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&q=%s&num=%d", $this->devKey, $this->engineID, $query, self::LIMIT);
        }

        $client = new GuzzleHttp\Client();
        try {
            $res = $client->get($url);
            $code = $res->getStatusCode();
            $content = $res->getBody();
            $exception = '';
        } catch (Exception $e) {
            $code = $e->getCode();
            $content = '';
            $exception = $e->getMessage();
            e('[Search] error=' . $e->getCode() . ', ' . $exception);
        }

        #$model = new SearchLog;
        #$model->query = $query;
        #$model->code = $code;
        #$model->response = $content;
        #$model->exception = $exception;
        #$model->save(false);

        return $content;
    }
}