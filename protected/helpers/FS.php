<?php

class FS {

    public static function hash($val) {
        return sprintf('%x',crc32($val));
    }

    /**
     * Deletes directory
     *
     * @param string $path
     */
    public static function rmDir($path)
    {
        if (is_dir($path)) {
            $objects = scandir($path);

            foreach ($objects as $object) {
                if ($object != '.' && $object != '..') {
                    $objectPath = "{$path}/{$object}";

                    if (filetype($objectPath) == 'dir') {
                        self::rmDir($objectPath);
                    } else {
                        unlink($objectPath);
                    }
                }
            }

            reset($objects);
            rmdir($path);
        }
    }

    public static function getSegments( $url = '' ) {
        if(empty($url)) {
            $url = $_SERVER['REQUEST_URI'];
        }

        // remove params
        if(strpos($url, '?')) {
            $url = substr($url, 0, strpos($url, '?'));
        }

        $url = trim($url, '/');
        return explode('/', $url);
    }

    public static function fileSize($value) {
        $sizeFormat = array('base'=>1024,'decimals'=>2);
        $units = array('B','KB','MB','GB','TB');
        $base = $sizeFormat['base'];
        for($i=0; $base<=$value; $i++) $value=$value/$base;
        return round($value, $sizeFormat['decimals']) . ' ' . $units[$i];
    }

}