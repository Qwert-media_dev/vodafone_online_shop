<?php

class Mail
{

    public static function send($email, $subject, $template, $info)
    {
        $mail = new PHPMailer(true);
        $mail->CharSet = 'utf-8';
        try {

            if( count( Yii::app()->params['smtp'] ) ) {
                $mail->IsSMTP();
                $mail->Host = Yii::app()->params['smtp']['host'];
                $mail->SMTPAuth = Yii::app()->params['smtp']['auth'];
                $mail->Username = Yii::app()->params['smtp']['username'];
                $mail->Password = Yii::app()->params['smtp']['password'];
                $mail->Port = Yii::app()->params['smtp']['port'];
                #$mail->SMTPSecure = Yii::app()->params['smtp']['secure'];
                #$mail->Hostname = Yii::app()->params['smtp']['id-hostname'];
            }
            
            $mail->AddAddress( $email, $info['name'] );
            $mail->SetFrom( param('from')['email'], param('from')['name'] );
            $mail->Subject = $subject;
            $mail->MsgHTML( app()->controller->renderPartial($template, $info, true) );
            $mail->IsHTML = true;
            $mail->Send();
        } catch (phpmailerException $e) {
            Yii::log('[send::phpmailerException] ' . $e->errorMessage(), 'warning');
        } catch (Exception $e) {
            Yii::log('[send::Exception] ' . $e->getMessage(), 'warning');
        }

    }

    
    public static function sendFeedback($email, $info)
    {
        self::send($email, $info['subject'], 'application.views.site.feedback', $info);
    }

}
