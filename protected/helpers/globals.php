<?php

/*
if(YII_DEBUG)
	include dirname(__FILE__).'/../extensions/hackerconsole/main.php';

if(!function_exists('info2')) {
	function info2($var) {
		// do nothing
	}
}
*/

function app() {
    return Yii::app();
}

function user() {
    return Yii::app()->user;
}


function param($name) {
    return Yii::app()->params[$name];
}

function url_to($route, $params=array()) {
    return Yii::app()->createUrl($route, $params);
}

function dump($obj, $depth=5, $color=true) {
    CVarDumper::dump($obj, $depth, $color);
}


function e($str, $level='warning') {
    Yii::log($str, $level);
}

function s() {
    return Yii::app()->session;
}

/**
 * Рендер select элемента для parent_id
 *
 * @param string $class
 * @param array $items
 * @param int $default
 * @return string
 */
function getMenuSelect($class, $items, $default=0) {
    $output  = '<select name="'.$class.'[parent_id]" class="form-control" id="parent_id">';
    $output .= '<option value="0">Корень</option>';
    foreach($items as $item) {
        $current = $item['id'] == $default ? ' selected="selected"' : '';
        $output .= sprintf('<option value="%s"%s>%s</option>', $item['id'], $current, space('--', $item['level']) . $item['title']);
    }
    $output .= '</select>';

    return $output;
}


function space($space, $num) {
    $res = '';
    for($i = 0; $i<$num; $i++) {
        $res .= $space;
    }
    return  $res;
}


function displayStatuses($statuses) {
    $str = '';
    $items = CJSON::decode($statuses);
    if( !count($items) ) return $str;

    foreach($items as $code => $status) {
        $str .= $status ? '<span class="label label-success">'.$code.'</span> ' : '<span class="label label-danger">'.$code.'</span> ';
    }

    return $str;
}


function getSegments( $url = '' ) {
    if(empty($url)) {
        $url = $_SERVER['REQUEST_URI'];
    }

    // remove params
    if(strpos($url, '?')) {
        $url = substr($url, 0, strpos($url, '?'));
    }

    $url = ltrim($url, '/');
    $url = rtrim($url, '/');
    return explode('/', $url);
}

/**
 * Дополнительная фильтрация для внутренних ссылок
 *
 * @param string $url
 * @return string
 */
function filterUrl($url) {
	$url = trim($url);
	$url = strtolower($url);
	return preg_replace('/[^a-z0-9\/\-]+/', '-', $url);

}

function flattenString($str) {
	$str = strip_tags($str);
	$str = str_replace( array("\n", "\r", "\t"), ' ', $str );
	$str = preg_replace('/ {2,}/', '', $str);
	$str = trim($str);
	return $str;
}


function cleanupPathInfo($str) {
	$pattern = '/[a-z0-9\/\-\_]/m';
	foreach(range(0, strlen($str)-1) as $idx) {
		if( !preg_match($pattern, $str[$idx]) ) {
			$str = substr($str, 0, $idx);
			$str = str_replace('//', '/', $str);
			return $str;
		}
	}
	$str = str_replace('//', '/', $str);
	return $str;
}

function formatXML($string)
{
    $string = trim($string);
    if ($string == '') return '[empty]';
    $dom = new DOMDocument;
    $dom->preserveWhiteSpace = false;
    $dom->loadXML($string);
    $dom->formatOutput = true;
    return '<textarea cols="100" rows="5" disabled="true">' . CHtml::encode($dom->saveXml()) . '</textarea>';
}

function getGeo() {
    $geoMark = (string)app()->request->cookies['section'];
    $ip = getRealIP();
    if( $geoMark === null || $geoMark == '' ) {
        $geo = GeoHelper::detect($ip);
        $detect = 1;
    } else {
        $geo = explode('_', $geoMark, 2);
        $detect = 0;
    }
    GeoLogger::instance()->addParam('detect', $detect);
    GeoLogger::instance()->addParam('geo', implode('_', $geo));
    GeoLogger::instance()->addParam('ip', $ip);
    
    return $geo;
}

function getRealIP() {
    return isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
}