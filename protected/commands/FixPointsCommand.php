<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/10/2015
 * Time: 05:51 PM
 */
class FixPointsCommand extends CConsoleCommand
{

	public function run($args) {
		$this->fixRegion();
		$this->fixCity();
	}

	protected function fixRegion() {
		$criteria = new CDbCriteria();
		$criteria->select = 't.id, count(t2.pos_region_id) as num';
		$criteria->join = 'JOIN pos_region_i18n t2 ON t2.pos_region_id=t.id';
		$criteria->group = 't2.pos_region_id';
		$criteria->having = 'num < 3';
		$items = PosRegion::model()->findAll($criteria);
		foreach($items as $item) {
			$i = new PosRegionI18n();
			$i->lang_id = 2;
			$i->pos_region_id = $item->id;
			$i->save(false);
		}
		echo "\nRegion done";
	}

	protected function fixCity() {
		$criteria = new CDbCriteria();
		$criteria->select = 't.id, count(t2.pos_city_id) as num';
		$criteria->join = 'JOIN pos_city_i18n t2 ON t2.pos_city_id=t.id';
		$criteria->group = 't2.pos_city_id';
		$criteria->having = 'num < 3';
		$items = PosCity::model()->findAll($criteria);
		foreach($items as $item) {
			$i = new PosCityI18n();
			$i->lang_id = 2;
			$i->pos_city_id = $item->id;
			$i->save(false);
		}
		echo "\nCity done";
	}
}