<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/12/16
 * Time: 19:26
 */
class ImportPointsCommand extends CConsoleCommand
{ 

    public function run($args)
    {
        $importer = new NetworkImporter();
        $importer->run();
    }
}