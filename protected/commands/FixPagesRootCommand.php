<?php

/**
 *  После ввода в таблицу Pages колонки root, этот скрипт должен
 *  обновить для всех страниц значение этого поля
 */
class FixPagesRootCommand extends CConsoleCommand
{

	public function run($args) {
		$items = Pages::model()->findAll();
		foreach($items as $item) {
			Pages::model()->updateByPk($item->id, array('root' => Pages::model()->getRootId($item)));
			echo "\nSaved \$page_id=".$item->id;
		}
		echo "\nDone\n";
	}
}