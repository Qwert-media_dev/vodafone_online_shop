<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 6/29/16
 * Time: 15:37
 */

class ImportGeoCommand extends CConsoleCommand
{

    use GeoTrait;

    protected $downloadUrl = 'https://sypexgeo.net/files/SxGeo_Info.zip';
    protected $folder;

    public function run($args)
    {
        $this->folder = Yii::getPathOfAlias('application').'/data/';
        $filename = $this->download();
        if( $filename !== false ) {
            $folder = $this->unpack($filename);
            if( $folder !== false ) {
                $this->importData($folder);
            }

            $this->cleanup($filename);
        }
    }


    protected function importData($folder) {
        if( file_exists($folder.'/city.tsv') ) {
            SxgeoCities::model()->deleteAll();
            $lines = file($folder.'/city.tsv');
            foreach ($lines as $line) {
                $line = trim($line);
                $row = explode("\t", $line);
                $model = new SxgeoCities();
                $model->id = $row[0];
                $model->region_id = $row[1];
                $model->name_ru = $row[2];
                $model->name_en = $row[3];
                $model->lat = $row[4];
                $model->lon = $row[5];
                //$model->okato = $row[6];
                $model->save(false);
            }
        }

        if( file_exists($folder.'/region.tsv') ) {
            SxgeoRegions::model()->deleteAll();
            $lines = file($folder.'/region.tsv');
            foreach($lines as $line) {
                $line = trim($line);
                $row = explode("\t", $line);
                $model = new SxgeoRegions();
                $model->id = $row[0];
                $model->iso = $row[1];
                $model->country = $row[2];
                $model->name_ru = $row[3];
                $model->name_en = $row[4];
                $model->timezone = $row[5];
                //$model->okato = $row[6];
                $model->save(false);
            }
        }

        if( file_exists($folder.'/country.tsv') ) {
            SxgeoCountry::model()->deleteAll();
            $lines = file($folder.'/country.tsv');
            foreach($lines as $line) {
                $line = trim($line);
                $row = explode("\t", $line);
                $model = new SxgeoCountry();
                $model->id = $row[0];
                $model->iso = $row[1];
                $model->continent = $row[2];
                $model->name_ru = $row[3];
                $model->name_en = $row[4];
                $model->lat = $row[5];
                $model->lon = $row[6];
                $model->timezone = isset($row[7]) ? $row[7] : '';
                $model->save(false);
            }
        }


    }
}