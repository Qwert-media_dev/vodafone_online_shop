<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/6/16
 * Time: 19:05
 */
class ImportGetCityCommand extends CConsoleCommand
{
    use GeoTrait;

    protected $downloadUrl = 'https://sypexgeo.net/files/SxGeoCity_utf8.zip';
    protected $dbFilename = 'SxGeoCity.dat';
    protected $folder;

    public function run($args)
    {
        $this->folder = Yii::getPathOfAlias('application').'/data/';
        $filename = $this->download('db');
        if( $filename !== false ) {
            // rename prev
            if( file_exists($this->folder.$this->dbFilename) ) {
                rename($this->folder.$this->dbFilename, $this->folder.$this->dbFilename.'.bak');
            }
            
            $folder = $this->unpack($filename);
            if( $folder !== false ) {
                rename($folder.'/SxGeoCity.dat', $this->folder.$this->dbFilename);
                if( file_exists($this->folder.$this->dbFilename.'.bak') ) {
                    unlink($this->folder.$this->dbFilename.'.bak');
                }
            }

            $this->cleanup($filename);
            if( file_exists($this->folder.$this->dbFilename.'.bak') ) {
                rename($this->folder.$this->dbFilename.'.bak', $this->folder.$this->dbFilename);
            }
        }
    }
}