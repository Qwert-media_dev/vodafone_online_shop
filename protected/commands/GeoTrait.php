<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/6/16
 * Time: 19:22
 */

Yii::import('application.vendor.*');
require 'autoload.php';

trait GeoTrait {

    protected function download($prefix='geo') {
        try {
            $filename = sprintf('%s-%s.zip', $prefix, date('Y-m-d-H-i-s'));
            $client = new GuzzleHttp\Client();
            $client->get($this->downloadUrl, [
                'save_to' => $this->folder . $filename
            ]);
            return $filename;
        } catch (Exception $e) {
            return false;
        }
    }


    protected function unpack($filename) {
        $zip = new ZipArchive;
        $res = $zip->open($this->folder.$filename);
        if ($res === true) {
            $path = $this->folder . substr($filename, 0, -4);
            $zip->extractTo($path);
            $zip->close();
            return $path;
        } else {
            return false;
        }
    }

    protected function cleanup($filename) {
        $extractFolder = $this->folder . substr($filename, 0, -4).'/';
        if( file_exists($extractFolder) && is_dir($extractFolder) ) {
            FS::rmDir($extractFolder);
        }

        if( $filename != '' && file_exists($this->folder.$filename) ) {
            unlink($this->folder.$filename);
        }
    }
}