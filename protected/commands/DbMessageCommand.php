<?php
/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/21/2015
 * Time: 05:03 PM
 */

Yii::import('system.cli.commands.MessageCommand', true);

class DbMessageCommand extends MessageCommand
{
	protected function generateMessageFile($messages,$fileName,$overwrite,$removeOld,$sort, $fileHeader)
	{
		// win fix
		$fileName = str_replace('\\', '/', $fileName);
		if (preg_match('@/(..)/([^\\/:"*?<>|]+?)\.php$@i', $fileName, $matches))
		{
			$language = $matches[1];
			$category = $matches[2];

			foreach ($messages as $message)
			{
				// $message contains the string, $category has the category and $language is the current language
				// Add to your DB here
				echo "{$category}\t{$language}\t{$message}\n";

				if( SourceMessage::model()->countByAttributes(['category' => $category, 'message' => $message]) == 0 ) {
					$sm = new SourceMessage();
					$sm->category = $category;
					$sm->message = $message;
					if($sm->save()) {

					}
				}
			}
		}
	}
}