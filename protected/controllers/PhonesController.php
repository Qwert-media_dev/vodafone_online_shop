<?php

class PhonesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column9';

	/**
	 * @return array action filters
	 */
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            
		$dataProvider=new CActiveDataProvider('Phones');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Phones the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Phones::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Phones $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='phones-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	//
	public function actionForm()
	{
		$model= new Orders();
                if(isset($_POST['Orders'])){
                    $model->attributes=$_POST['Orders'];
                    $model->status=0;
                    $model->save();
                    $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                     $subject='=?UTF-8?B?'.base64_encode('helloч ').'?=';
                     $headers="From: $name <{$model->email}>\r\n".
                             "Reply-To: {$model->email}\r\n".
                             "MIME-Version: 1.0\r\n".
                             "Content-Type: text/plain; charset=UTF-8";
                     mail(Yii::app()->params['adminEmail'],'hello',$headers);
                    Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
                }
		$this->render('_buyForm',array(
			'model'=>$model,
		));
	}
	//export orders
    public function actionExport() {
            $model=  Orders::model()->findAll();
            // //
            // // get a reference to the path of PHPExcel classes 
            // // Turn off our amazing library autoload 
            spl_autoload_unregister(array('YiiBase','autoload'));             
            Yii::import('application.extensions.phpexcel.Classes.PHPExcel', true);
            $objPHPExcel = new PHPExcel();
            spl_autoload_register(array('YiiBase','autoload'));
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("PDF Test Document")
                ->setSubject("PDF Test Document")
                ->setDescription("Test document for PDF, generated using PHP classes.")
                ->setKeywords("pdf php")
                ->setCategory("Test result file");
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'id')
                ->setCellValue('B1', 'Имя')
                ->setCellValue('C1', 'Email')
                ->setCellValue('D1', 'Город')
                ->setCellValue('E1', 'Телефон')
                ->setCellValue('F1', 'Магазин')
                ->setCellValue('G1', 'Статус')
                ->setCellValue('H1', 'Коментарий');
            foreach ($model as $k=>$one){
                
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.($k+2), $one->id)
                ->setCellValue('B'.($k+2), $one->name)
                ->setCellValue('C'.($k+2), $one->email)
                ->setCellValue('D'.($k+2), $one->city)
                ->setCellValue('E'.($k+2), $one->phone)
                ->setCellValue('F'.($k+2), $one->shop)
                ->setCellValue('G'.($k+2), $one->status)
                ->setCellValue('H'.($k+2), $one->comment);
            }
            
            // Rename sheet
            $objPHPExcel->getActiveSheet()->setTitle('Simple');
            // Set active sheet index to the first sheet, 
            // so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);
            // Redirect output to a client’s web browser (Excel2007)
            header('Content-Type: application/xls');
            header('Content-Disposition: attachment;filename="01simple.xls"');
            header('Cache-Control: max-age=0');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            Yii::app()->end();
 
       
            
        }
    //import city/shops
    public function actionImport()
	{
            $model=new ImportForm();
            
            if(isset($_POST['ImportForm'])){
                spl_autoload_unregister(array('YiiBase','autoload'));             
                Yii::import('application.extensions.phpexcel.Classes.PHPExcel.IOFactory', true);             
                spl_autoload_register(array('YiiBase','autoload'));
                $model->file=CUploadedFile::getInstance($model,'file');
                $path=Yii::getPathOfAlias('webroot').'/images/'.$model->file->getName();
                $model->file->saveAs($path);
                // Открываем файл
                $xls = PHPExcel_IOFactory::load($path);
                // Устанавливаем индекс активного листа
                $xls->setActiveSheetIndex(0);
                // Получаем активный лист
                $sheet = $xls->getActiveSheet();
                Shops::model()->deleteAll();
                for ($i = 1; $i <= $sheet->getHighestRow(); $i++) {  
                    $nColumn = PHPExcel_Cell::columnIndexFromString(
                    $sheet->getHighestColumn());
                    
                    $shop = new Shops();
                    $shop->city=$sheet->getCellByColumnAndRow(0, $i)->getValue();
                    $shop->shop=$sheet->getCellByColumnAndRow(1, $i)->getValue();
                    $shop->save();
                }
                unlink($path);
            }
		$this->render('import',array(
			'model'=>$model,
		));
	}
}
