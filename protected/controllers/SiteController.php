<?php


class SiteController extends Controller
{

	const CONTROLLER = 'pay';

	public $layout = 'inner';

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'NumericCaptchaAction',
				'backColor'=>0xCCCCCC,
				'foreColor' => 0xE60000,
				'width' => 80,
				'height' => 45,
				'minLength' => 4,
				'maxLength' => 4,
				//'showRefreshButton' => false
			),
		);
	}

	public function actionIndex()
	{
		$this->layout = 'main';
		$landingID = is_null(app()->session->get('landingID')) ? $this->landing->id1 : app()->session->get('landingID');
		$this->widgetContent = $this->widget('FrontSliderWidget', ['page_id' => $landingID], true);
		$this->footerBanner = $this->widget('FrontFooterBannerWidget', ['page_id' => $landingID], true);
		$item = Pages::model()->getById( $landingID, $this->language->id );

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$params = $lang->code == param('defaultLang') ? [] : ['lang' => $lang->code];
			$alts[$lang->code] = app()->createAbsoluteUrl('site/index', $params);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$this->render('index', compact('item'));
	}


	public function actionError()
	{
		$this->layout = 'general';
		$error = Yii::app()->errorHandler->error;
		$this->render('error', compact('error'));
	}

	public function actionPage($lang, $url)
	{
		$segments = explode('/', $url);
		$item = $this->fetchNode($segments);

		if( $item->type == Pages::TYPE_NOT_A_LINK ) {
			// searching next child
			$item = Pages::model()->findNextChild($item->id, $this->language->id);
			if( is_null($item) ) {
				throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
			}
			if( $item->type == Pages::TYPE_LINK ) {
				$redirectUrl = $item->i18n[0]->alias;
			} else {
				$redirectUrl = sprintf('/%s/%s/%s', $this->language->code, $url, URLify::filter($item->i18n[0]->alias));
			}
			$this->redirect($redirectUrl, true, param('seo_redirect_code'));
		}

		$this->pageTitle = $item->i18n[0]->title;
        if( $item->i18n[0]->pic1 != '' || $item->i18n[0]->pic2 != '' || $item->i18n[0]->pic3 != '' || $item->i18n[0]->pic4 != '' ) {
	        $this->widgetContent = $this->widget('PromoImageWidget', array(
			        'pic1' => $item->i18n[0]->pic1,
			        'pic2' => $item->i18n[0]->pic2,
			        'pic3' => $item->i18n[0]->pic3,
			        'pic4' => $item->i18n[0]->pic4,
	        ), true);
            $this->layout = 'inner-with-image';
        }

		if( in_array($item->id, [$this->landing->id1, $this->landing->id2]) ) {
			$this->layout = 'main';
			$this->widgetContent = $this->widget('FrontSliderWidget', ['page_id' => $item->id], true);
			$this->footerBanner = $this->widget('FrontFooterBannerWidget', ['page_id' => $item->id], true);
		}

		if( in_array($item->root, [$this->landing->id1, $this->landing->id2]) ) {
			app()->session->add('landingID', $item->root);
		}

		if( $item->extra_class != '' ) {
			$this->extraContainerClass = $item->extra_class;
		}

		$this->og_title         = trim($item->i18n[0]->og_title);
		$this->og_description   = trim($item->i18n[0]->og_description);
		$this->og_image         = trim($item->i18n[0]->og_image);
		$this->seo_title        = trim($item->i18n[0]->seo_title);
		$this->seo_description  = trim($item->i18n[0]->seo_description);

		if( $item->app_name != '' && $item->app_store_id != '' && $item->app_package != '' ) {
			$this->isAppLink = true;
            $this->appLinkData = [
                'name' => $item->app_name,
                'store_id' => $item->app_store_id,
                'package' => $item->app_package
            ];
		}

		$this->extra_css = $item->extra_css;
        $this->extra_js = $item->extra_js;
		
		// Seo start
		$alts = [];
		$i18n = PagesI18n::model()->getPageItem($item->id);
		foreach($this->languages as $lang) {
			$path = Pages::model()->expandPath($item->id, $lang->id);
			$path = array_reverse($path);
			if( $i18n[$lang->id]->status == Pages::STATUS_ACTIVE )
				$alts[$lang->code] = sprintf('%s/%s', app()->createAbsoluteUrl('site/index', ['lang' => $lang->code]), implode('/', $path));
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$this->render('page', compact('item'));
	}

	/**
	 * Поиск конечной ноды в древе
	 * Поиск слева на право
	 *
	 * @param $segments
	 * @return null|Pages
	 * @throws CHttpException
	 */
	protected function fetchNode($segments) {
		if(!sizeof($segments)) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}

		//$url = implode('/', $segments);

		$path = array();
		$node = null;
		$parent_id = 0;
		$segments = array_reverse($segments);
		while( sizeof($segments) ) {
			$current = array_pop($segments);
			$path[] = $current;
			$node = Pages::model()->getByAlias($this->language->id, $parent_id, $current);
			if($node===null || $node->i18n[0]->status == Pages::STATUS_INACTIVE || $node->is_deleted) {
				throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
			}

			/*
			if( $node->type == Pages::TYPE_MODULE ) {
				$service = new RouteService($url, $node->controller, $current);
				$forward = $service->getForward();

				$this->breadcrumbs[] = array('title' => $node->i18n[0]->title, 'url' => '/'.$this->language->code.'/'.implode('/', $path) );
				Yii::app()->params['transfer-bc'] = $this->breadcrumbs;
				Yii::app()->params['transfer-title'] = $node->i18n[0]->title;
				Yii::app()->params['transfer-path'] = $path;
				$this->forward($forward);
			}
			*/

			$title = $node->i18n[0]->short_title == '' ? $node->i18n[0]->title : $node->i18n[0]->short_title;
			$this->breadcrumbs[] = array('title' => $title, 'url' => '/'.$this->language->code.'/'.implode('/', $path) );
			$parent_id = $node->id;
		}

		return $node;
	}


	public function actionPayment() {
		$this->extraContainerClass = ' pay';

		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
        if( !is_null($item) ) {
            $this->pageTitle = $item->i18n[0]->title;
            $this->populateBreadcrumbs($item);
	        $this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
        }

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('site/payment', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

        $values = PaymentPresets::model()->getItems();
		$this->render('payment', compact('item', 'values'));
	}

	public function actionProcess() {
		$this->layout = ' ';
		$badUrl = url_to('site/payment', array('lang' => $this->language->code));

		//dump($_POST);exit;
		if(Yii::app()->request->isPostRequest) {
			$target = app()->request->getPost('target');
			$method = app()->request->getPost('method');
			$num = trim(app()->request->getPost('num'));
			$account = trim(app()->request->getPost('account'));
			$code = app()->request->getPost('code');
			$sum = trim(app()->request->getPost('sum'));


			if( !in_array($target, ['phone', 'account']) ) {
				$this->redirect($badUrl);
			}

			if( !in_array($method, ['portmone', 'pb', 'easypay']) ) {
				$this->redirect($badUrl);
			}

			if( !in_array($code, array_keys(param('codes'))) ) {
				$this->redirect($badUrl);
			}

			if( $sum<1 ) {
				$this->redirect($badUrl);
			}

			if( $target == 'phone' && !preg_match('/^\d{7}$/', $num) ) {
				$this->redirect($badUrl);
			}

			if( $target == 'account' && !preg_match('/^\d{12}$/', $account) ) {
				$this->redirect($badUrl);
			}


			if( $method == 'pb' ) {
				$params = array(
					'lang' => $this->language->code,
					'amount' => $sum
				);
				if( $target == 'phone' ) {
					$params['phone'] = sprintf('38%s%s', $code, $num);
				}
				if( $target == 'account' ) {
					$params['account'] = $account;
				}

				$urlLang = $this->language->code;
				if($urlLang == 'uk') {
					$urlLang = 'ua';
				}

				$this->redirect( sprintf('https://secure.privatbank.ua/%s/company/vodafone?%s', $urlLang, http_build_query($params)));
			}


			if( $method == 'easypay' ) {
				$params = array(
						'lang' => $this->language->code,
						'amount' => $sum
				);
				if( $target == 'phone' ) {
					$params['phone'] = sprintf('38%s%s', $code, $num);
				}
				if( $target == 'account' ) {
					$params['account'] = $account;
				}
				$this->redirect( 'https://easypay.ua/vodafone?'.http_build_query($params));
			}


			if( $method == 'portmone' ) {
				$this->render('process', compact('target', 'sum', 'account', 'code', 'num'));
				Yii::app()->end();
			}
		}

		$this->redirect($badUrl);
	}

	public function actionSitemap() {
		$this->layout = 'general';
		$this->pageTitle = Yii::t('app', 'Мапа сайту');

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('site/sitemap', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$tree = array_chunk($this->mainMenu, 3);
		$this->render('sitemap', compact('tree'));
	}


    public function actionOldie() {
	    // Seo start
	    $alts = [];
	    foreach($this->languages as $lang) {
		    $alts[$lang->code] = app()->createAbsoluteUrl('site/oldie', ['lang' => $lang->code]);
	    }
	    Alts::instance()->addItems($alts);
	    // Seo end

	    $this->renderPartial('oldie');
    }


	public function actionSetgeo() {
        if( array_key_exists('id', $_POST) ) {
            $params = explode('_', $_POST['id'], 2);
            $realData = false;
            if( $params[1] == 0 ) {
                if( SxgeoRegions::model()->findByAttributes(['id' => (int)$params[0]]) !== null ) {
                    $realData = true;
                }
            } elseif( SxgeoCities::model()->findByAttributes(['id' => (int)$params[1], 'region_id' => (int)$params[0]]) !== null ) {
                $realData = true;
            }
            if( $realData ) {
                app()->request->cookies['section'] = new CHttpCookie('section', sprintf('%d_%d', $params[0], $params[1] ));
                echo 'ok';
                app()->end();
            }
        }
        echo 'fail';
    }


	public function actionFeedback() {
		header('Content-Type: application/json');
		$resp = ['error' => Yii::t('feedback', 'Дефолтное сообщение об ошибке'), 'errors' => []];
		
		$p = $_POST;
		#e('[post] '.print_r($_POST, true));
		if( in_array($p['type'], ['gratitude', 'idea', 'appeal', '']) ) {
			$feedback = new FeedbackHandler();
			if($feedback->validate()) {
				$feedback_id = $feedback->save();
				$sender = new FeedbackSender($feedback_id);
                $sender->send();
				$resp = ['success' => Yii::t('feedback', 'Дякуємо за ваш відгук!')];
			} else {
				$resp['errors'] = $feedback->getErrors();
			}

		}

		echo CJSON::encode($resp);
	}

}

