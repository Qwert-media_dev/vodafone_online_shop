<?php

/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 7/26/16
 * Time: 19:03
 */
class RatesController extends Controller
{

    public $layout = 'inner';
    public $useVariantSelector = false;
    public $extraContainerClass = 'transparent new_tarif';

    public function actionIndex($type) {

        $item = Pages::model()->getModulePageByController('rate_'.$type, $this->language->id);
        if(!is_null($item)) {
            //$this->pageTitle = $item->i18n[0]->title;
            $this->populateBreadcrumbs($item);
            $this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
        }

        $settings = RatesSettings::model()->getByType( $type, $this->language->id );
        if( $settings !== null ) {
            if ($settings->i18n[0]->pic1 != '' || $settings->i18n[0]->pic2 != '' || $settings->i18n[0]->pic3 != '' || $settings->i18n[0]->pic4 != '') {
                $this->widgetContent = $this->widget('PromoImageWidget', array(
                    'pic1' => $settings->i18n[0]->pic1,
                    'pic2' => $settings->i18n[0]->pic2,
                    'pic3' => $settings->i18n[0]->pic3,
                    'pic4' => $settings->i18n[0]->pic4,
                ), true);
                $this->layout = 'inner-with-image';
            }

            $this->pageTitle = $settings->i18n[0]->title;
            $this->og_title         = trim($settings->i18n[0]->og_title);
            $this->og_description   = trim($settings->i18n[0]->og_description);
            $this->og_image         = trim($settings->i18n[0]->og_image);
        }

        $items = Rates::model()->getActiveItems( $type, $this->language->id, getGeo() );
        $this->render('index', compact('items', 'type'));
    }
    

    public function actionShow($type, $alias) {

        $item = Pages::model()->getModulePageByController('rate_'.$type, $this->language->id);
        if(!is_null($item)) {
            //$this->pageTitle = $item->i18n[0]->title;
            $this->populateBreadcrumbs($item);
            $this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
            array_pop($this->breadcrumbs);
            $this->breadcrumbs[] = array('title' => $this->getTypeLabel($type), 'url' => url_to('rates/index', ['lang' => $this->language->code, 'type' => $type]) );
            //$this->breadcrumbs[] = array('title' => $this->getTypeLabel($type), 'url' => url_to('rates/index', ['lang' => $this->language->code, 'type' => $type]) );
        }

        $item = Rates::model()->getActiveByAlias( $type, $alias, $this->language->id );
        if( $item === null || $item->is_archive )
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));


        $inGeo = false;
        $rates = Rates::model()->getActiveItems( $type, $this->language->id, getGeo() );
        foreach($rates as $rate) {
            if($rate->id == $item->id) {
                $inGeo = true;
            }
        }
        if( !$inGeo ) {
            $this->redirect(['rates/index', 'lang' => $this->language->code, 'type' => $type]);
        }


        $this->layout = 'rate-inner';
        if ($item->i18n[0]->pic1 != '' || $item->i18n[0]->pic2 != '' || $item->i18n[0]->pic3 != '' || $item->i18n[0]->pic4 != '') {
            $this->widgetContent = $this->widget('PromoImageWidget', array(
                'pic1' => $item->i18n[0]->pic1,
                'pic2' => $item->i18n[0]->pic2,
                'pic3' => $item->i18n[0]->pic3,
                'pic4' => $item->i18n[0]->pic4,
            ), true);
            $this->layout = 'rates-with-image';
            $this->widgetContent2 = $this->widget('RatesTabWidget', ['current' => $item], true);
        } else {
            $this->widgetContent = $this->widget('RatesTabWidget', ['current' => $item], true);
        }

        $this->pageTitle = $item->i18n[0]->caption;
        $this->og_title         = trim($item->i18n[0]->og_title);
        $this->og_description   = trim($item->i18n[0]->og_description);
        $this->og_image         = trim($item->i18n[0]->og_image);
        
        $this->render('show', compact('item'));
    }


    public function actionArchiveshow($type, $alias) {

        $item = Pages::model()->getModulePageByController('rate_'.$type, $this->language->id);
        if(!is_null($item)) {
            //$this->pageTitle = $item->i18n[0]->title;
            $this->populateBreadcrumbs($item);
            $this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
            array_pop($this->breadcrumbs);
            $this->breadcrumbs[] = array('title' => $this->getTypeLabel($type), 'url' => url_to('rates/index', ['lang' => $this->language->code, 'type' => $type]) );
            //$this->breadcrumbs[] = array('title' => Yii::t('app', 'Актуальнi тарифи') , 'url' => url_to('rates/archive', ['lang' => $this->language->code, 'type' => $type]) );
            $this->breadcrumbs[] = array('title' => Yii::t('app', 'Актуальнi тарифи') , 'url' => url_to('rates/archive', ['lang' => $this->language->code, 'type' => $type]) );
        }

        $item = Rates::model()->getActiveByAlias( $type, $alias, $this->language->id );
        if( $item === null || !$item->is_archive  )
            throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));

        if ($item->i18n[0]->pic1 != '' || $item->i18n[0]->pic2 != '' || $item->i18n[0]->pic3 != '' || $item->i18n[0]->pic4 != '') {
            $this->widgetContent = $this->widget('PromoImageWidget', array(
                'pic1' => $item->i18n[0]->pic1,
                'pic2' => $item->i18n[0]->pic2,
                'pic3' => $item->i18n[0]->pic3,
                'pic4' => $item->i18n[0]->pic4,
            ), true);
            $this->layout = 'rates-with-image';
            $this->widgetContent2 = $this->widget('RatesTabWidget', ['current' => $item], true);
        } else {
            $this->widgetContent = $this->widget('RatesTabWidget', ['current' => $item], true);
        }
        
        $this->pageTitle = $item->i18n[0]->caption;
        $this->og_title         = trim($item->i18n[0]->og_title);
        $this->og_description   = trim($item->i18n[0]->og_description);
        $this->og_image         = trim($item->i18n[0]->og_image);

        $this->useVariantSelector = true;

        $this->render('show', compact('item'));
    }

    
    public function actionArchive($type) {

        $item = Pages::model()->getModulePageByController('rate_'.$type, $this->language->id);
        if(!is_null($item)) {
            //$this->pageTitle = $item->i18n[0]->title;
            $this->populateBreadcrumbs($item);
            $this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
            $this->breadcrumbs[] = array('title' => Yii::t('app', 'Актуальнi тарифи') , 'url' => url_to('rates/archive', ['lang' => $this->language->code, 'type' => $type]) );
            $this->breadcrumbs[] = array('title' => Yii::t('app', 'Актуальнi тарифи') , 'url' => url_to('rates/archive', ['lang' => $this->language->code, 'type' => $type]) );
        }

        $this->extraContainerClass = 'transparent new_tarif';
        $geo = getGeo();
        $items = Rates::model()->getArchiveItems( $type, $this->language->id, Rates::STATUS_ACTIVE, $geo );
        $inactive = Rates::model()->getArchiveItems( $type, $this->language->id, Rates::STATUS_INACTIVE, $geo );
        $this->render('archive', compact('items', 'type', 'inactive'));
    }
    

    public function getTypeLabel($type) {
        $types = [
            'private' => Yii::t('app', 'Передплата'),
            'contract' => Yii::t('app', 'Контракт'),
            'business' => Yii::t('app', 'Бизнес')
        ];
        return $types[$type];
    }
}