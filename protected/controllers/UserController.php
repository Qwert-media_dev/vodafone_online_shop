<?php
/**
 * Created by PhpStorm.
 * User: sasha
 * Date: 05.02.2015
 * Time: 21:15
 */

class UserController extends ManageController {

    public function actionLogin() {
        $this->layout = 'login';

        if( !user()->isGuest )
            $this->redirect(array('site/index'));

        $model = new Admins('login');

        if (isset($_POST['form'])) {
            $form = $_POST['form'];
            $model->email = $form['email'];
            $model->password = $form['password'];
            if ($model->validate()) {
                $this->redirect(array('manage/default/index'));
            }
        }

        $this->render('login', array('form' => $model));
    }


    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(array('manage/default/index'));
    }

}