<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/16/2015
 * Time: 04:58 PM
 */
class VacancyController extends Controller
{
	const CONTROLLER = 'news_vacancy';

	public function actionIndex() {
		$this->layout = 'news';

		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->pageTitle = $item->i18n[0]->title;
			$this->populateBreadcrumbs($item);
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}

		$this->widgetContent = $this->widget('NewsFilterWidget', ['is_archive' => 0, 'type' => News::TYPE_VACANCY], true);

		$params = News::model()->getActiveWithFilter( News::TYPE_VACANCY, $this->language->id );

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('vacancy/index', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$this->render('index', $params);
	}

	public function actionArchive() {
		$this->layout = 'news';

		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->pageTitle = $item->i18n[0]->title;
			$this->populateBreadcrumbs($item);
            $this->breadcrumbs[] = array('title' => Yii::t('app', 'Архів'), 'url' => url_to('vacancy/archive', array('lang' => $this->language->code)));
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}

		$this->widgetContent = $this->widget('NewsFilterWidget', ['is_archive' => 1, 'type' => News::TYPE_VACANCY], true);

		$params = News::model()->getActiveWithFilter( News::TYPE_VACANCY, $this->language->id, 1 );

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('vacancy/archive', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$this->render('archive', $params);
	}

	public function actionShow($id) {
		$this->layout = 'inner';

		// крохи
		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->populateBreadcrumbs($item);
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}

		$item = News::model()->getById($id, News::TYPE_VACANCY, $this->language->id);
		if( is_null($item) ) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}
		$this->breadcrumbs[] = array('title' => $item->i18n[0]->title, 'url' => url_to('vacancy/index', array('lang' => $this->language->code)));

		$this->pageTitle = $item->i18n[0]->title;
		$this->seo_description = $item->i18n[0]->short;

		// Seo start
		$alts = [];
		$i18n = NewsI18n::model()->getNewsItem($id);
		foreach($this->languages as $lang) {
			if( $i18n[$lang->id]->status == News::STATUS_ACTIVE )
				$alts[$lang->code] = app()->createAbsoluteUrl('vacancy/show', [
					'lang' => $lang->code,
					'id' => $id,
					'alias' => URLify::filter($i18n[$lang->id]->title)
				]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$og_title = trim($item->i18n[0]->og_title);
		$og_description = trim($item->i18n[0]->og_description);
		$this->og_title         = $og_title == '' ? $item->i18n[0]->title : $og_title;
		$this->og_description   = $og_description == '' ? $item->i18n[0]->short : $og_description;
		$this->og_image         = trim($item->i18n[0]->og_image);

		$this->render('show', compact('item'));
	}

	public function actionArchiveshow($id) {
		$this->layout = 'inner';

		// крохи
		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->populateBreadcrumbs($item);
			//$this->breadcrumbs[] = array('title' => $item->i18n[0]->title, 'url' => url_to('news/index', array('lang' => $this->language->code)) );
			//$this->breadcrumbs[] = array('title' => Yii::t('app', 'Архів'), 'url' => url_to('vacancy/archive', array('lang' => $this->language->code))); // FIXME: проверить почему надо дублировать
			$this->breadcrumbs[] = array('title' => Yii::t('app', 'Архів'), 'url' => url_to('vacancy/archive', array('lang' => $this->language->code)));
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}


		$item = News::model()->getById($id, News::TYPE_VACANCY, $this->language->id, 1);
		if( is_null($item) ) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}
        $this->breadcrumbs[] = array('title' => $item->i18n[0]->title, 'url' => url_to('vacancy/archiveshow', array('lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title) )));
		$this->pageTitle = $item->i18n[0]->title;

		// Seo start
		$alts = [];
		$i18n = NewsI18n::model()->getNewsItem($id);
		foreach($this->languages as $lang) {
			if( $i18n[$lang->id]->status == News::STATUS_ACTIVE )
				$alts[$lang->code] = app()->createAbsoluteUrl('vacancy/archiveshow', [
					'lang' => $lang->code,
					'id' => $id,
					'alias' => URLify::filter($i18n[$lang->id]->title)
				]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$og_title = trim($item->i18n[0]->og_title);
		$og_description = trim($item->i18n[0]->og_description);
		$this->og_title         = $og_title == '' ? $item->i18n[0]->title : $og_title;
		$this->og_description   = $og_description == '' ? $item->i18n[0]->short : $og_description;
		$this->og_image         = trim($item->i18n[0]->og_image);

		$this->render('archive-show', compact('item'));
	}

}