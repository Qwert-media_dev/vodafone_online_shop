<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/5/2015
 * Time: 04:27 PM
 */
class NetworkController extends Controller
{

	const CONTROLLER = 'network';

	public $useGoogleMap = true;
	public $layout = 'inner';
	public $extraContainerClass = ' transparent network';


	public function actionIndex() {
		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->pageTitle = $item->i18n[0]->title;
			$this->populateBreadcrumbs($item);
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}

		$folder = Yii::getPathOfAlias('application.data').'/';
		$files = CJSON::decode( file_get_contents($folder.'package.json') );
		$points = CJSON::decode( file_get_contents($folder.$files['points'][$this->language->code]) );
		$temp = CJSON::decode( file_get_contents($folder.$files['states'][$this->language->code]) );
		$states = [];
		foreach($temp as $t) {
			$states[$t['id']] = $t;
		}

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('network/index', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end


		$this->render('index', compact('item', 'points', 'states'));
	}


	public function actionLoad($key) {
		$folder = Yii::getPathOfAlias('application.data').'/';
		$files = CJSON::decode( file_get_contents($folder.'package.json') );
		if( !array_key_exists($key, $files) ) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}

		if( !array_key_exists($this->language->code, $files[$key]) ) {
			throw new CHttpException(404, Yii::t('app', 'Запрашиваемая страница не найдена'));
		}

		header('Content-Type: application/json');
		$content = file_get_contents($folder . $files[$key][$this->language->code]);
		if( $key == 'states' ) {
			$items = CJSON::decode($content);
			$temp = [];
			foreach($items as $item) {
				$temp[$item['id']] = $item;
			}
			ksort($temp);
			$items = [];
			foreach($temp as $item) {
				$items[] = $item;
			}
			$content = CJSON::encode($items);
		}
		echo $content;
	}

	public function actionSettings() {
		$resp = ['error' => Yii::t('app', 'Не верные данные')];

		$post = $_POST;
		if( array_key_exists('code', $post) && array_key_exists('num', $post) && array_key_exists('captcha', $post) ) {
			$model = new SettingsForm();
			$model->attributes = $post;
			if( $model->validate() ) {
				$service = new SettingsService( $post['code'].$post['num'] );
				$status = $service->canRecieve();

				if( $status == SettingsService::STATUS_SUPPORTED ) {
					$service->send();
					$resp = ['success' => Yii::t('app', 'Настройки успешно отправлены на ваш телефон')];
				} elseif( $status == SettingsService::STATUS_UNSUPPORTED ) {
                    $resp = ['error' => Yii::t('app', 'Ваш телефон не принимает наши настройки.')];
                } elseif( $status == SettingsService::STATUS_NOT_DETECTED ) {
                    $resp = ['error' => Yii::t('app', 'Терминал не определен')];
                } else {
                    $resp = ['error' => Yii::t('app', 'Блокировка абонента / Неисправность платформы')];
                }
			} 
		}

		echo CJSON::encode($resp);
	}


}