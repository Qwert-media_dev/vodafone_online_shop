<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/24/2015
 * Time: 08:14 PM
 */
class SearchController extends Controller
{

    public $layout = 'general';
    public $noIndex = true;

    public function actionIndex()
    {
        $this->pageTitle = Yii::t('app', 'Результати пошуку');
        $search = new Search($_GET['q']);
        #$provider = $search->getResults();
        #$total = $search->getTotal();

        // Seo start
        $alts = [];
        foreach ($this->languages as $lang) {
            $alts[$lang->code] = app()->createAbsoluteUrl('search/index', ['lang' => $lang->code]);
        }
        Alts::instance()->addItems($alts);
        // Seo end

        $this->render('index', compact('provider', 'total', 'search'));
    }

}