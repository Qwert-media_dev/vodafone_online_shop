<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 12/2/2015
 * Time: 04:06 PM
 */
class SitemapController extends Controller
{
	public $layout = 'sitemap';

	public function beforeAction($action) {
		header("Content-type: text/xml");
		parent::beforeAction($action);
		return true;
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionSitemap() {
		$urls = array();

		// Index
		$url = app()->createAbsoluteUrl('site/index', array('lang' => $this->language->code ));
		$lastmod = date('c') ;
		$changefreq = 'weekly';
		$priority = '0.8';
		$urls[$url] = array(
			'lastmod' => $lastmod,
			'changefreq' => $changefreq,
			'priority' => $priority
		);

		// News
		$items = News::model()->getActiveItems(News::TYPE_NEWS, $this->language->id, 0);
		foreach($items as $item) {
			$url = app()->createAbsoluteUrl('news/show', array('lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title) ));
			$lastmod = date('c', strtotime($item->news_dt));
			$changefreq = 'monthly';
			$priority = '0.8';
			$urls[$url] = array(
				'lastmod' => $lastmod,
				'changefreq' => $changefreq,
				'priority' => $priority
			);
		}

		// News archive
		$items = News::model()->getActiveItems(News::TYPE_NEWS, $this->language->id, 1);
		foreach($items as $item) {
			$url = app()->createAbsoluteUrl('news/archiveshow', array('lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title) ));
			$lastmod = date('c', strtotime($item->news_dt));
			$changefreq = 'monthly';
			$priority = '0.7';
			$urls[$url] = array(
				'lastmod' => $lastmod,
				'changefreq' => $changefreq,
				'priority' => $priority
			);
		}


		// Press
		$items = News::model()->getActiveItems(News::TYPE_PRESS, $this->language->id, 0);
		foreach($items as $item) {
			$url = app()->createAbsoluteUrl('press/show', array('lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title) ));
			$lastmod = date('c', strtotime($item->news_dt));
			$changefreq = 'monthly';
			$priority = '0.8';
			$urls[$url] = array(
				'lastmod' => $lastmod,
				'changefreq' => $changefreq,
				'priority' => $priority
			);
		}

		// Press archive
		$items = News::model()->getActiveItems(News::TYPE_PRESS, $this->language->id, 1);
		foreach($items as $item) {
			$url = app()->createAbsoluteUrl('press/archiveshow', array('lang' => $this->language->code, 'id' => $item->id, 'alias' => URLify::filter($item->i18n[0]->title) ));
			$lastmod = date('c', strtotime($item->news_dt));
			$changefreq = 'monthly';
			$priority = '0.7';
			$urls[$url] = array(
				'lastmod' => $lastmod,
				'changefreq' => $changefreq,
				'priority' => $priority
			);
		}

		// Pos
		$item = Pages::model()->findByAttributes(['controller' => 'pos']);
		$url = app()->createAbsoluteUrl('pos/index', array('lang' => $this->language->code ));
		$lastmod = is_null($item) ? date('c') : date('c', strtotime($item->created_at));
		$changefreq = 'weekly';
		$priority = '0.7';
		$urls[$url] = array(
			'lastmod' => $lastmod,
			'changefreq' => $changefreq,
			'priority' => $priority
		);

		// Sitemap
		$url = app()->createAbsoluteUrl('site/sitemap', array('lang' => $this->language->code ));
		$lastmod = date('c') ;
		$changefreq = 'weekly';
		$priority = '0.7';
		$urls[$url] = array(
			'lastmod' => $lastmod,
			'changefreq' => $changefreq,
			'priority' => $priority
		);

		// Menu
		$items = Pages::model()->getActiveTree(0, 0, $this->language->id);
		$this->processMenu($items, $this->language->code, $urls);

		$this->render('sitemap', compact('urls'));
	}

	protected function processMenu($items, $code, &$urls) {
		$protocol = app()->request->isSecureConnection ? 'https' : 'http';
		foreach( $items as $item ) {
			if ($item['type'] != Pages::TYPE_LINK) {
				$path = Pages::model()->expandPath($item['id'], $this->language->id);
				$path = array_reverse($path);
				$url = implode('/', $path);
			}

			if( $item['type'] == Pages::TYPE_LINK ) {
				$url = sprintf('%s://%s%s', $protocol, $_SERVER['HTTP_HOST'], $item['alias']);
			} else {
				$url = sprintf('%s://%s/%s/%s', $protocol, $_SERVER['HTTP_HOST'], $code, $url);
			}

			switch( $item['level'] ) {
				case 0:
					$priority = 1;
					$changefreq = 'daily';
					break;
				case 1:
				case 2:
					$priority = 0.8;
					$changefreq = 'hourly';
					break;
				default:
					$priority = 0.7;
					$changefreq = 'monthly';
			}

			$lastmod = date('c', strtotime($item['created_at']) );
			$urls[$url] = array(
				'lastmod' => $lastmod,
				'changefreq' => $changefreq,
				'priority' => $priority
			);

			if(count($item['children'])) {
				$this->processMenu($item['children'], $code, $urls);
			}
		}
	}

}