<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/4/2015
 * Time: 06:14 PM
 */
class PosController extends Controller
{

	const CONTROLLER = 'pos';
	public $useGoogleMap = true;
	public $layout = 'pos';

	public function actionIndex() {
		$item = Pages::model()->getModulePageByController(self::CONTROLLER, $this->language->id);
		if(!is_null($item)) {
			$this->pageTitle = $item->i18n[0]->title;
			$this->populateBreadcrumbs($item);
			$this->setVirtualPathForRegistry($item->id, $this->language->id, $this->language->code);
		}

		$params = Pos::model()->getActiveItems( $this->language->id );
		$ids = array();
		foreach($params['items'] as $item) {
			$ids[] = $item->pos_city_id;
		}

		// Seo start
		$alts = [];
		foreach($this->languages as $lang) {
			$alts[$lang->code] = app()->createAbsoluteUrl('pos/index', ['lang' => $lang->code]);
		}
		Alts::instance()->addItems($alts);
		// Seo end

		$params['cityTitles'] = PosCity::model()->getByIds($ids, $this->language->id);
		$this->render('index', $params);
	}
}