<?php

/**
 * This is the model class for table "rates_settings_i18n".
 *
 * The followings are the available columns in table 'rates_settings_i18n':
 * @property integer $id
 * @property integer $rates_settings_id
 * @property integer $lang_id
 * @property string $pic1
 * @property string $pic2
 * @property string $pic3
 * @property string $pic4
 * @property string $og_title
 * @property string $og_description
 * @property string $og_image
 * @property string $title
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Languages $lang
 * @property RatesSettings $ratesSettings
 */
class RatesSettingsI18n extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'rates_settings_i18n';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_id', 'required'),
            array('lang_id', 'numerical', 'integerOnly' => true),
            //array('pic1, pic2, pic3, pic4, og_title, og_description, og_image, title, description', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, rates_settings_id, lang_id, pic1, pic2, pic3, pic4, og_title, og_description, og_image, title, description', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
            'ratesSettings' => array(self::BELONGS_TO, 'RatesSettings', 'rates_settings_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'rates_settings_id' => 'Rates Settings',
            'lang_id' => 'Lang',
            'pic1' => 'Pic1',
            'pic2' => 'Pic2',
            'pic3' => 'Pic3',
            'pic4' => 'Pic4',
            'og_title' => 'Og Title',
            'og_description' => 'Og Description',
            'og_image' => 'Og Image',
            'title' => 'Title',
            'description' => 'Description',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('rates_settings_id', $this->rates_settings_id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('pic1', $this->pic1, true);
        $criteria->compare('pic2', $this->pic2, true);
        $criteria->compare('pic3', $this->pic3, true);
        $criteria->compare('pic4', $this->pic4, true);
        $criteria->compare('og_title', $this->og_title, true);
        $criteria->compare('og_description', $this->og_description, true);
        $criteria->compare('og_image', $this->og_image, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RatesSettingsI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getItems($rates_settings_id)
    {
        $data = [];
        $items = self::model()->findAllByAttributes(['rates_settings_id' => $rates_settings_id]);
        foreach ($items as $item) {
            $data[$item->lang_id] = $item;
        }
        return $data;
    }
}
