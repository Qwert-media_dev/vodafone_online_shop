<?php

/**
 * This is the model class for table "rates_settings".
 *
 * The followings are the available columns in table 'rates_settings':
 * @property integer $id
 * @property string $type
 *
 * The followings are the available model relations:
 * @property RatesSettingsI18n[] $i18n
 */
class RatesSettings extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'rates_settings';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type', 'length', 'max' => 8),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'i18n' => array(self::HAS_MANY, 'RatesSettingsI18n', 'rates_settings_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RatesSettings the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getByType($type, $lang_id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type';
        $criteria->params = [':type' => $type];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rates_settings_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id)
        ));
        return self::model()->find($criteria);
    }
}
