<?php

/**
 * This is the model class for table "pos".
 *
 * The followings are the available columns in table 'pos':
 * @property integer $id
 * @property integer $pos_city_id
 * @property integer $sort
 * @property integer $status
 * @property double $lat
 * @property double $lng
 * @property integer $zoom
 * @property integer $pos_region_id
 * @property integer $type
 *
 * The followings are the available model relations:
 * @property PosCity $posCity
 * @property PosI18n[] $i18n
 */
class Pos extends CActiveRecord
{

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pos_city_id, pos_region_id, lat, lng, zoom', 'required'),
			array('pos_city_id, zoom, pos_region_id', 'numerical', 'integerOnly'=>true),
			array('lat, lng', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pos_city_id, sort, status, lat, lng, zoom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'posCity' => array(self::BELONGS_TO, 'PosCity', 'pos_city_id'),
			'posRegion' => array(self::BELONGS_TO, 'PosRegion', 'pos_region_id'),
			'i18n' => array(self::HAS_MANY, 'PosI18n', 'pos_id', 'together' => true),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pos_region_id' => 'Регион',
			'pos_city_id' => 'Город',
			'sort' => 'Очередность',
			'status' => 'Публикация',
			'lat' => 'Lat',
			'lng' => 'Lng',
			'zoom' => 'Zoom',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pos_region_id',$this->pos_region_id);
		$criteria->compare('pos_city_id',$this->pos_city_id);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('status',$this->status);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lng',$this->lng);
		$criteria->compare('zoom',$this->zoom);

		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => param('defaultLangId'))
		));

		$criteria->order = 'i18n.address';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => ['pageSize' => 25]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getItemsByCity($pos_city_id, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.pos_city_id=:pos_city_id';
		$criteria->params = array(':pos_city_id' => $pos_city_id);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));

		$criteria->order = 't.pos_city_id, t.sort, i18n.address';

		return self::model()->findAll($criteria);
	}

	public function getActiveItems($lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.status=:status';
		$criteria->params = array(':status' => self::STATUS_ACTIVE);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));


		$sortPoints = false;
		if( array_key_exists('filter', $_GET) ) {
			$filter = $_GET['filter'];
			if( array_key_exists('region', $filter) && (int)$filter['region'] > 0 ) {
				$criteria->mergeWith(array(
					'condition' => 't.pos_region_id=:pos_region_id',
					'params' => array(':pos_region_id' => (int)$filter['region'])
				));
			}

			if( array_key_exists('city', $filter) && (int)$filter['city'] > 0 && (int)$filter['region'] > 0 ) {
				$criteria->mergeWith(array(
						'condition' => 't.pos_city_id=:pos_city_id',
						'params' => array(':pos_city_id' => (int)$filter['city'])
				));
				$sortPoints = true;
				$criteria->order = 't.sort';
			}
		}

		if(!$sortPoints) {
			$criteria->join = 'JOIN pos_city_i18n c ON c.pos_city_id=t.pos_city_id AND c.lang_id=:lang_id';
			//$criteria->order = 't.pos_city_id, t.sort, i18n.address';
			$criteria->order = 'c.title';
		}

		$count = self::model()->count($criteria);

		$pages = new CPagination($count);
		$pages->pageSize = param('perPos');
		$pages->applyLimit($criteria);

		return array('count' => $count, 'items' => self::model()->findAll($criteria), 'pages' => $pages);
	}

}
