<?php

/**
 * This is the model class for table "sxgeo_regions".
 *
 * The followings are the available columns in table 'sxgeo_regions':
 * @property integer $id
 * @property string $iso
 * @property string $country
 * @property string $name_ru
 * @property string $name_en
 * @property string $timezone
 * @property string $okato
 * @property integer $status
 */
class SxgeoRegions extends CActiveRecord
{
    
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $dummy;
    public $external_name;
    public $region_name;
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sxgeo_regions';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            #array('name_uk', 'required', 'on' => 'update'),
            #array('status', 'safe', 'on' => 'update'),
            
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, iso, country, name_ru, name_en, timezone, okato', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'iso' => 'Iso',
            'country' => 'Country',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'timezone' => 'Timezone',
            'okato' => 'Okato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('iso', $this->iso, true);
        $criteria->compare('country', 'UA', true);
        $criteria->compare('name_ru', $this->name_ru, true);
        $criteria->compare('name_en', $this->name_en, true);
        $criteria->compare('timezone', $this->timezone, true);
        $criteria->compare('okato', $this->okato, true);

        $criteria->order = 'name_ru';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 40)
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SxgeoRegions the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getIds() {
        $list = [];
        $items = self::model()->findAllByAttributes(['country' => 'UA']);
        foreach($items as $item) {
            $list[] = $item->id;
        }
        return $list;
    }
    
    public function getItems() {
        $criteria = new CDbCriteria();
        $criteria->condition = 'country=:country';
        $criteria->params = [':country' => 'UA'];
        $criteria->order = 'name_ru';
        return self::model()->findAll($criteria);
    }
    
    
    public function getName($id, $lang_id) {
        $criteria = new CDbCriteria();
        $criteria->select = 'i18n.popup_name as region_name';
        $criteria->condition = 't.id=:id AND i18n.lang_id=:lang_id';
        $criteria->params = [':id' => $id, ':lang_id' => $lang_id];
        $criteria->join = 'JOIN sxgeo_regions_i18n i18n ON i18n.region_iso=t.iso';
        $item = self::model()->find($criteria);
        if( is_null($item) )
            GeoLogger::instance()->pull();

        return is_null($item) ? Yii::t('geo', 'Unknown place') : $item->region_name;
    }
}
