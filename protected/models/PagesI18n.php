<?php

/**
 * This is the model class for table "pages_i18n".
 *
 * The followings are the available columns in table 'pages_i18n':
 * @property integer $id
 * @property integer $lang_id
 * @property integer $page_id
 * @property string $title
 * @property string $alias
 * @property string $body
 * @property integer $status
 * @property string $short_title
 * @property string $og_title
 * @property string $og_description
 * @property string $og_image
 * @property string $image1
 * @property string $image2
 * @property string $body2
 * @property string $redactor
 * @property integer $parent_id
 * @property integer $hide_alias
 * @property string $pic1
 * @property string $pic2
 * @property string $pic3
 * @property string $pic4
 * @property string $seo_title
 * @property string $seo_description
 *
 * The followings are the available model relations:
 * @property Pages $page
 * @property Languages $lang
 */
class PagesI18n extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages_i18n';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lang_id, title', 'required', 'on' => 'save_1'),
			array('alias', 'checkAliasDuplicates', 'on' => 'save_1'),
			array('lang_id', 'required', 'on' => 'save_0'),

			array('lang_id, title', 'required', 'on' => 'savelink_1'),
			array('alias', 'checkAliasDuplicates', 'on' => 'savelink_1'),
			array('lang_id', 'required', 'on' => 'savelink_0'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lang_id, page_id, title, alias, body', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'page' => array(self::BELONGS_TO, 'Pages', 'page_id'),
			'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lang_id' => 'Lang',
			'page_id' => 'Page',
			'title' => 'Название',
			'alias' => 'Адрес страницы',
			'body' => 'Описание',
			'body2' => 'Описание после модульного блока',
			'status' => 'Публикация',
			'og_title' => 'Заголовок',
			'og_description' => 'Описание',
			'og_image' => 'Бейджик',
			'image1' => ' Промо картинка адаптация 1',
			'image2' => ' Промо картинка адаптация 2',
			'short_title' => 'Короткое название',
            'hide_alias' => 'Скрыть ссылку в меню',
			'pic1' => 'Картинка 1',
			'pic2' => 'Картинка 2',
			'pic3' => 'Картинка 3',
			'pic4' => 'Картинка 4',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('lang_id', $this->lang_id);
		$criteria->compare('page_id', $this->page_id);
		$criteria->compare('title', $this->title, true);
		$criteria->compare('alias', $this->alias, true);
		$criteria->compare('body', $this->body, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PagesI18n the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/*
	public function beforeSave()
	{

		if ($this->alias == '') {
			$this->alias = URLify::filter($this->title);
		}

		return parent::beforeSave();
	}
	*/

	public function checkAliasDuplicates($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$criteria = new CDbCriteria();
			$criteria->condition = 'alias=:alias AND parent_id=:parent_id AND page_id!=:page_id AND lang_id=:lang_id';
			$criteria->params = array(':alias' => $this->alias, ':parent_id' => $this->parent_id, ':page_id' => (int)$this->page_id, ':lang_id' => $this->lang_id);

			if( self::model()->count($criteria) ) {
				$this->addError('alias', 'На этом уровне уже есть такая ссылка!');
			}
		}
	}

	/**
	 * Для SEO
	 *
	 * @param $page_id
	 * @return self[]
	 */
	public function getPageItem($page_id) {
		$data = [];
		$items = self::model()->findAllByAttributes(['page_id' => $page_id]);
		foreach($items as $item) {
			$data[ $item->lang_id ] = $item;
		}
		return $data;
	}
}
