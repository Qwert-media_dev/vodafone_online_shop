<?php

/**
 * This is the model class for table "geo_exclude".
 *
 * The followings are the available columns in table 'geo_exclude':
 * @property integer $id
 * @property string $table
 * @property integer $table_pk
 * @property integer $region_id
 * @property integer $city_id
 */
class GeoExclude extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'geo_exclude';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('table, table_pk, region_id, city_id', 'required'),
            array('table_pk, region_id, city_id', 'numerical', 'integerOnly' => true),
            array('table', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, table, table_pk, region_id, city_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'table' => 'Table',
            'table_pk' => 'Table Pk',
            'region_id' => 'Region',
            'city_id' => 'City',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('table', $this->table, true);
        $criteria->compare('table_pk', $this->table_pk);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('city_id', $this->city_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return GeoExclude the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getItems($table, $table_pk) {
        $data = [];
        $items = self::model()->findAllByAttributes(['table' => $table, 'table_pk' => $table_pk]);
        foreach ($items as $item) {
            $data[] = $item->region_id . '_' . $item->city_id;
        }
        return $data;
    }


    public function populate($table, $table_pk, $items) {
        self::model()->deleteAllByAttributes(['table' => $table, 'table_pk' => $table_pk]);
        if (!count($items)) return;

        if (in_array('0_0', $items)) {
            $model = new self;
            $model->table = $table;
            $model->table_pk = $table_pk;
            $model->region_id = 0;
            $model->city_id = 0;
            $model->save();
        } else {
            foreach ($items as $item) {
                $params = explode('_', $item);
                $model = new self;
                $model->table = $table;
                $model->table_pk = $table_pk;
                $model->region_id = (int)$params[0];
                $model->city_id = (int)$params[1];
                $model->save();
            }
        }
    }
    
    public function getExcludeItems($table, $region_id, $city_id) {
        $list = [];
        $items = self::model()->findAllByAttributes(['table' => $table, 'region_id' => $region_id, 'city_id' => $city_id]);
        foreach ($items as $item) {
            $list[] = $item->table_pk;
        }
        return $list;
    }
}
