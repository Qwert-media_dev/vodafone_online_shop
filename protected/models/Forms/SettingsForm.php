<?php

/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 2/12/2016
 * Time: 08:49 PM
 */
class SettingsForm extends  CFormModel
{

	public $code;
	public $num;
	public $captcha;

	public function rules()
	{
		return array(
			array('code, num, captcha', 'required'),
			array('captcha', 'captcha', 'allowEmpty' => !extension_loaded('gd'), 'captchaAction' => 'site/captcha'),
			array('code, num, captcha', 'safe'),
		);
	}


}