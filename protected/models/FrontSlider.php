<?php

/**
 * This is the model class for table "front_slider".
 *
 * The followings are the available columns in table 'front_slider':
 * @property integer $id
 * @property integer $sort
 * @property string $title
 * @property string $statuses
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_deleted
 * @property integer $page_id
 * @property string $title_color
 * @property string $second_title_color
 *
 * The followings are the available model relations:
 * @property FrontSliderI18n[] $i18n
 */
class FrontSlider extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'front_slider';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //array('sort, title, statuses, created_at, updated_at', 'required'),
            //array('sort', 'numerical', 'integerOnly'=>true),
            //array('title, statuses', 'length', 'max'=>255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, sort, title, statuses, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'i18n' => array(self::HAS_MANY, 'FrontSliderI18n', 'front_slider_id', 'together' => true),
            'page' => array(self::BELONGS_TO, 'Pages', 'page_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'sort' => 'Очередность',
            'title' => 'Название',
            'statuses' => 'Публикации',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'page_id' => 'Раздел'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->condition = 't.is_deleted=0';
        $criteria->compare('id', $this->id);
        $criteria->compare('sort', $this->sort);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('statuses', $this->statuses, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);

        $criteria->order = 't.sort';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 25]
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FrontSlider the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at'
            )
        );
    }

    public function getItems($page_id, $lang_id, $geo)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.is_deleted=:is_deleted AND t.page_id=:page_id';
        $criteria->params = array(':is_deleted' => 0, ':page_id' => $page_id);
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.front_slider_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));

		if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join front_slider_geo g ON g.front_slider_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join front_slider_geo g ON g.front_slider_id=t.id',
            ]);
        }
        
        $criteria->group = 't.id';
        $criteria->order = 't.sort';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->findAll($criteria);
    }

    public function getDeleted()
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.is_deleted=1';
        $criteria->order = 't.sort';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
