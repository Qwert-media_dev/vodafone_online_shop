<?php

/**
 * This is the model class for table "front_slider_geo".
 *
 * The followings are the available columns in table 'front_slider_geo':
 * @property integer $id
 * @property integer $front_slider_id
 * @property integer $region_id
 * @property integer $city_id
 *
 * The followings are the available model relations:
 * @property FrontSlider $frontSlider
 */
class FrontSliderGeo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'front_slider_geo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('front_slider_id, region_id, city_id', 'required'),
            array('front_slider_id, region_id, city_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, front_slider_id, region_id, city_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'frontSlider' => array(self::BELONGS_TO, 'FrontSlider', 'front_slider_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'front_slider_id' => 'Front Slider',
            'region_id' => 'Region',
            'city_id' => 'City',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('front_slider_id', $this->front_slider_id);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('city_id', $this->city_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FrontSliderGeo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function populate($front_slider_id, $items)
    {
        self::model()->deleteAllByAttributes(['front_slider_id' => $front_slider_id]);
        if(!count($items)) return;

        if( in_array('0_0', $items) ) {
            $model = new self;
            $model->front_slider_id = $front_slider_id;
            $model->region_id = 0;
            $model->city_id = 0;
            $model->save();
        } else {
            foreach ($items as $item) {
                $params = explode('_', $item);
                $model = new self;
                $model->front_slider_id = $front_slider_id;
                $model->region_id = (int)$params[0];
                $model->city_id = (int)$params[1];
                $model->save();
            }
        }
    }

    public function getItems($front_slider_id) {
        $data = [];
        $items = self::model()->findAllByAttributes(['front_slider_id' => $front_slider_id]);
        foreach ($items as $item) {
            $data[] = $item->region_id.'_'.$item->city_id;
        }

        return $data;
    }
}
