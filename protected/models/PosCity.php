<?php

/**
 * This is the model class for table "pos_city".
 *
 * The followings are the available columns in table 'pos_city':
 * @property integer $id
 * @property integer $pos_region_id
 *
 * The followings are the available model relations:
 * @property Pos[] $poses
 * @property PosRegion $posRegion
 * @property PosCityI18n[] $i18n
 */
class PosCity extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pos_city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pos_region_id', 'required'),
			array('pos_region_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pos_region_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'poses' => array(self::HAS_MANY, 'Pos', 'pos_city_id'),
			'points' => array(self::STAT, 'Pos', 'pos_city_id'),
			'posRegion' => array(self::BELONGS_TO, 'PosRegion', 'pos_region_id'),
			'i18n' => array(self::HAS_MANY, 'PosCityI18n', 'pos_city_id', 'together' => true),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pos_region_id' => 'Регион',
			'points' => 'Кол-во точек'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pos_region_id',$this->pos_region_id);

		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_city_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => param('defaultLangId'))
		));

		$criteria->order = 'i18n.title';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => ['pageSize' => 25]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PosCity the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function getById($id, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.id=:id';
		$criteria->params = array(':id' => $id);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_city_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));

		return self::model()->find($criteria);
	}

	public function getItemsByRegion($pos_region_id, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.pos_region_id=:pos_region_id';
		$criteria->params = array(':pos_region_id' => $pos_region_id);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_city_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		$criteria->order = 'i18n.title';

		return self::model()->findAll($criteria);
	}


	public function getByIds($ids, $lang_id) {
		if( !count($ids) ) return array();

		$list = array();
		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id', $ids);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_city_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));

		$items = self::model()->findAll($criteria);
		foreach ($items as $item) {
			$list[$item->id] = $item->i18n[0]->title;
		}

		return $list;
	}


	public function getItems($lang_id) {
		$criteria = new CDbCriteria();
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.pos_city_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		$criteria->order = 'i18n.title';

		return self::model()->findAll($criteria);
	}
}
