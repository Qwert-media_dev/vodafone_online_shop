<?php

/**
 * This is the model class for table "rates_i18n".
 *
 * The followings are the available columns in table 'rates_i18n':
 * @property integer $id
 * @property integer $rate_id
 * @property integer $lang_id
 * @property integer $status
 * @property string $title
 * @property string $caption
 * @property string $legal
 * @property string $content
 * @property string $og_title
 * @property string $og_description
 * @property string $og_image
 * @property string $second_title
 * @property string $extra_price
 * @property string $pic1
 * @property string $pic2
 * @property string $pic3
 * @property string $pic4
 * @property string $alias
 * @property string $tab_title
 * @property string $content_after
 * 
 */
class RatesI18n extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'rates_i18n';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_id, title, caption, content, alias, tab_title', 'required', 'on' => 'save_1'),
            array('lang_id', 'required', 'on' => 'save_0'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, rate_id, lang_id, status, title, caption, legal, content', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'rate_id' => 'Rate',
            'lang_id' => 'Lang',
            'status' => 'Публикация',
            'title' => 'Название тарифа',
            'caption' => 'Заголовок на внутренней',
            'legal' => 'Легалы',
            'content' => 'Контент',
            'content_after' => 'Контент (после модуля)',
            'og_title' => 'OpenGraph Заголовок',
            'og_description' => 'OpenGraph Описание',
            'og_image' => 'OpenGraph Бейджик',
            'second_title' => 'Доп название',
            'extra_price' => 'Подпись к цене',
            'alias' => 'Адрес страницы',
            'tab_title' => 'Заголовок в табе'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('rate_id', $this->rate_id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('status', $this->status);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('caption', $this->caption, true);
        $criteria->compare('legal', $this->legal, true);
        $criteria->compare('content', $this->content, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return RatesI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function findByTitle($title)
    {
        $ids = array();
        $segments = explode(' ', $title);
        foreach ($segments as $idx => $segment) {
            $segment = trim($segment);
            $segments[$idx] = $segment;
            if ($segment == '') {
                unset($segments[$idx]);
            }
        }

        if (!count($segments)) return $ids;

        $criteria = new CDbCriteria();

        foreach ($segments as $segment) {
            $criteria->addSearchCondition('title', $segment);
        }

        $criteria->group = 'rate_id';

        $items = self::model()->findAll($criteria);
        foreach ($items as $item) {
            $ids[] = $item->rate_id;
        }
        return $ids;
    }
}
