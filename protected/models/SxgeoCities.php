<?php

/**
 * This is the model class for table "sxgeo_cities".
 *
 * The followings are the available columns in table 'sxgeo_cities':
 * @property integer $id
 * @property integer $region_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $lat
 * @property string $lon
 * @property string $okato
 * @property integer $status
 */
class SxgeoCities extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    public $dummy;
    public $name;
    public $city_name;
    public $city_id;
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sxgeo_cities';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            #array('name_uk', 'required', 'on' => 'update'),
            #array('status', 'safe', 'on' => 'update'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, region_id, name_ru, name_en, lat, lon, okato, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'region' => array(self::BELONGS_TO, 'SxgeoRegions', 'region_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'region_id' => 'Region',
            'name_ru' => 'Name Ru',
            'name_en' => 'Name En',
            'lat' => 'Lat',
            'lon' => 'Lon',
            'okato' => 'Okato',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.region_id', $this->region_id);
        $criteria->addInCondition('t.region_id', SxgeoRegions::model()->getIds());
        $criteria->compare('t.name_ru', $this->name_ru, true);
        $criteria->compare('t.name_en', $this->name_en, true);
        $criteria->compare('t.lat', $this->lat, true);
        $criteria->compare('t.lon', $this->lon, true);
        $criteria->compare('t.status', $this->status, true);

        $criteria->order = 't.name_ru';
        $criteria->with = array('region');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 40)
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SxgeoCities the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getName($region_id, $id, $lang_id) {
        $criteria = new CDbCriteria();
        $criteria->select = 'i18n.popup_name as city_name';
        $criteria->condition = 't.id=:id AND i18n.lang_id=:lang_id AND t.region_id=:region_id';
        $criteria->params = [':id' => $id, ':lang_id' => $lang_id, ':region_id' => $region_id];
        $criteria->join = 'JOIN sxgeo_cities_i18n i18n ON i18n.city_id=t.id';
        $item = self::model()->find($criteria);
        if( is_null($item) ) 
            GeoLogger::instance()->pull();
        return is_null($item) ? Yii::t('geo', 'Unknown place') : $item->city_name;
    }
}
