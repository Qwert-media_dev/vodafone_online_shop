<?php

/**
 * This is the model class for table "redirects".
 *
 * The followings are the available columns in table 'redirects':
 * @property integer $id
 * @property string $url_from
 * @property string $url_to
 * @propert string $alarm
 */
class Redirects extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'redirects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('url_from, url_to', 'required'),
			array('url_from, url_to', 'length', 'max'=>255),
			array('url_from', 'unique'),
			array('url_from, url_to', 'filter', 'filter' => 'trim'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, url_from, url_to', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'url_from' => 'Короткая ссылка',
			'url_to' => 'Реальная ссылка',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('url_from',$this->url_from,true);
		$criteria->compare('url_to',$this->url_to,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Redirects the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getTable() {
		$table = array();

		$items = self::model()->findAll();
		foreach($items as $item) {
			$table[$item->url_from] = $item->url_to;
		}

		return $table;
	}
}
