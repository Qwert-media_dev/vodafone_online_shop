<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $type
 * @property string $news_date
 * @property string $created_at
 * @property string $updated_at
 * @property integer $is_archive
 * @property string $news_time
 * @property string $news_dt
 * @property string $title
 * @property string $statuses
 * @property string $is_deleted
 * @property integer $page_id
 *
 * The followings are the available model relations:
 * @property NewsI18n[] $i18n
 */
class News extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

	const TYPE_NEWS = 'news';
	const TYPE_PRESS = 'press';
	const TYPE_VACANCY = 'vacancy';
	const TYPE_DIALOG = 'dialog';


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'news';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('news_date, news_time', 'required'),
            //array('type', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, news_date, created_at, updated_at', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'i18n' => array(self::HAS_MANY, 'NewsI18n', 'news_id', 'together' => true),
            'page' => array(self::BELONGS_TO, 'Pages', 'page_id')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'news_dt' => 'Дата статьи',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'news_date' => 'День',
            'news_time' => 'Время',
            'is_archive' => 'Архивная',
            'title' => 'Заголовок',
            'statuses' => 'Публикации',
			'page_id' => 'Раздел'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->condition = 't.is_delete=0';
        $criteria->compare('id', $this->id);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('news_date', $this->news_date, true);


        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => param('defaultLangId'))
        ));

        $criteria->order = 't.news_dt desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 25]
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return News the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at'
            )
        );
    }

    public function beforeSave()
    {
        $this->news_dt = sprintf('%s %s', $this->news_date, $this->news_time);
        return parent::beforeSave();
    }


    /**
     * Выборка для новостей и пресс-релизов с фильтрацией
     *
     * @param string $type Тип новость или пресс-релиз
     * @param int $is_archive Архивная статья
     * @param int $is_deleted Удаленная статья
     * @return CActiveDataProvider
     */
    public function getTable($type, $is_archive=null, $is_deleted=0)
    {
        $criteria = new CDbCriteria;

        $criteria->condition = 't.type=:type AND t.is_deleted=:is_deleted';
        $criteria->params = array(':type' => $type, ':is_deleted' => $is_deleted);

        if( !is_null($is_archive) ) {
	        $criteria->mergeWith(array(
		        'condition' => 't.is_archive=:is_archive',
		        'params' => array(':is_archive' => $is_archive)
	        ));
        }

        // apply filter
        if (array_key_exists('filter', $_GET)) {
            $filter = $_GET['filter'];

            if (array_key_exists('date', $filter) && $filter['date'] != '') {
                $criteria->mergeWith(array(
                    'condition' => 't.news_date=:news_date',
                    'params' => array(':news_date' => $filter['date'])
                ));
            }

            if (array_key_exists('title', $filter) && $filter['title'] != '') {
                $ids = NewsI18n::model()->findByTitle($filter['title']);
                if (count($ids))
                    $criteria->addInCondition('t.id', $ids);
            }
        }

        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => param('defaultLangId'))
        ));

        $criteria->order = 't.news_dt desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /*
    public function displayStatuses()
    {
        $str = '';
        $items = CJSON::decode($this->statuses);
        if (!count($items)) return $str;

        foreach ($items as $code => $status) {
            $str .= $status ? '<span class="label label-success">' . $code . '</span> ' : '<span class="label label-danger">' . $code . '</span> ';
        }

        return $str;
    }
    */

    public function futureIcon()
    {
        return strtotime($this->news_dt) > time() ? '<span class="glyphicon glyphicon-time"></span> ' : '';
    }


	/**
	 * Выборка для главной страницы новостей
	 *
	 * @param string $type
	 * @param int $lang_id
	 * @return array
	 */
    public function getActiveWithFilter($type, $lang_id, $is_archive=0) {
		$criteria = new CDbCriteria();
	    $criteria->condition = 't.type=:type AND t.news_dt<=:news_dt AND t.is_deleted=:is_deleted AND t.is_archive=:is_archive';
	    $criteria->params = array(':type' => $type, ':news_dt' => date('Y-m-d H:i:s'), ':is_deleted' => 0, ':is_archive' => $is_archive);
	    $criteria->with = array('i18n' => array(
		    'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
		    'joinType' => 'INNER JOIN',
		    'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
	    ));

	    $get = is_null($_GET) ? array() : $_GET;
	    if( array_key_exists('filter', $get) ) {
		    $year = (int)$_GET['filter']['year'];
		    $month = str_pad( (int)$_GET['filter']['month'], 2, '0', STR_PAD_LEFT);

		    if( checkdate($month, 1, $year) ) {
			    $start = sprintf('%d-%d-01', $year, $month);
			    $end = sprintf('%d-%d-%d', $year, $month, date('t', strtotime($start)) );
			    $criteria->mergeWith(array(
				    'condition' => 't.news_date>=:start AND news_date<=:end',
				    'params' => array(':start' => $start, ':end' => $end)
			    ));
		    }
	    }

	    $criteria->order = 't.news_dt desc';

	    $count = self::model()->count($criteria);

	    $pages = new CPagination($count);
	    $pages->pageSize = param('perNews');
	    $pages->applyLimit($criteria);

	    return array('count' => $count, 'items' => self::model()->findAll($criteria), 'pages' => $pages);
    }


    /**
     * Выборка для индексной страницы диалога
     *
     * @param int $page_id
     * @param int $lang_id
     * @return array
     */
    public function getActiveDialogWithFilter($page_id, $lang_id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND t.news_dt<=:news_dt AND t.is_deleted=:is_deleted AND t.page_id=:page_id';
        $criteria->params = array(':type' => self::TYPE_DIALOG, ':news_dt' => date('Y-m-d H:i:s'), ':is_deleted' => 0, ':page_id' => $page_id);
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));

        $get = is_null($_GET) ? array() : $_GET;
        if( array_key_exists('filter', $get) ) {
            $year = (int)$_GET['filter']['year'];
            $month = str_pad( (int)$_GET['filter']['month'], 2, '0', STR_PAD_LEFT);

            if( checkdate($month, 1, $year) ) {
                $start = sprintf('%d-%d-01', $year, $month);
                $end = sprintf('%d-%d-%d', $year, $month, date('t', strtotime($start)) );
                $criteria->mergeWith(array(
                    'condition' => 't.news_date>=:start AND news_date<=:end',
                    'params' => array(':start' => $start, ':end' => $end)
                ));
            }
        }

        $criteria->order = 't.news_dt desc';

        $count = self::model()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = param('perNews');
        $pages->applyLimit($criteria);

        return array('count' => $count, 'items' => self::model()->findAll($criteria), 'pages' => $pages);
    }



    public function getById($id, $type, $lang_id, $is_archive=0) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.id=:id AND t.type=:type AND t.news_dt<=:news_dt AND t.is_deleted=:is_deleted AND t.is_archive=:is_archive';
		$criteria->params = array(':type' => $type, ':news_dt' => date('Y-m-d H:i:s'), ':is_deleted' => 0, ':is_archive' => $is_archive, ':id' => $id);
		$criteria->with = array('i18n' => array(
			'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
			'joinType' => 'INNER JOIN',
			'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));
		return self::model()->find($criteria);
	}


	public function getFormatedNewsDT() {
		$ts = strtotime($this->news_date);
		$months = Yii::app()->locale->getMonthNames('wide', false);
		return sprintf('%s %s %s', date('d', $ts), $months[ date('n', $ts) ], date('Y', $ts));
	}


	public function getActiveDates($type, $lang_id, $is_archive, $page_id=null) {
		$output = array();
		$criteria = new CDbCriteria();
		$criteria->condition = 't.type=:type AND t.news_dt<=:news_dt AND t.is_deleted=:is_deleted AND t.is_archive=:is_archive';
		$criteria->params = array(':type' => $type, ':news_dt' => date('Y-m-d H:i:s'), ':is_deleted' => 0, ':is_archive' => $is_archive);
		$criteria->with = array('i18n' => array(
			'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
			'joinType' => 'INNER JOIN',
			'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));
        if( !is_null($page_id) ) {
            $criteria->mergeWith([
               'condition' => 't.page_id=:page_id',
                'params' => [':page_id' => $page_id]
            ]);
        }
		$criteria->order = 'news_date DESC';
		$criteria->group = 'news_date';
		$items = self::model()->findAll($criteria);
		foreach($items as $item) {
			$split = explode('-', $item->news_date);
			$output[ $split[0] ][ (int)$split[1] ] = 1;
		}

		return $output;
	}

	public function getActiveItems($type, $lang_id, $is_archive) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.type=:type AND t.news_dt<=:news_dt AND t.is_deleted=:is_deleted AND t.is_archive=:is_archive';
		$criteria->params = array(':type' => $type, ':news_dt' => date('Y-m-d H:i:s'), ':is_deleted' => 0, ':is_archive' => $is_archive);
		$criteria->with = array('i18n' => array(
			'condition' => 'i18n.news_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
			'joinType' => 'INNER JOIN',
			'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));

		$criteria->order = 't.news_dt desc';
		return self::model()->findAll($criteria);
	}
}

