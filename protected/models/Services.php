<?php

/**
 * This is the model class for table "services".
 *
 * The followings are the available columns in table 'services':
 * @property integer $id
 * @property string $statuses
 * @property integer $is_deleted
 *
 * The followings are the available model relations:
 * @property ServicesGeo[] $geo
 * @property ServicesI18n[] $i18n
 */
class Services extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'services';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            #array('statuses', 'required'),
            #array('statuses', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, statuses', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'geo' => array(self::HAS_MANY, 'ServicesGeo', 'service_id'),
            'i18n' => array(self::HAS_MANY, 'ServicesI18n', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'statuses' => 'Statuses',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('statuses', $this->statuses, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Services the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getTable($is_deleted = 0)
    {
        $criteria = new CDbCriteria;
        $criteria->condition = 't.is_deleted=:is_deleted';
        $criteria->params = array(':is_deleted' => $is_deleted);

        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.service_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => param('defaultLangId'))
        ));
        
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public function getItem($geo, $lang_id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.is_deleted=:is_deleted';
        $criteria->params = array(':is_deleted' => 0);
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.service_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));

        if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join services_geo g ON g.service_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join services_geo g ON g.service_id=t.id',
            ]);
        }
        $criteria->group = 't.id';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->find($criteria);
    }
}
