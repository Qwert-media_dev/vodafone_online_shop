<?php

/**
 * This is the model class for table "front_slider_i18n".
 *
 * The followings are the available columns in table 'front_slider_i18n':
 * @property integer $id
 * @property integer $lang_id
 * @property integer $front_slider_id
 * @property integer $status
 * @property integer $in_new_window
 * @property string $title
 * @property string $alt
 * @property string $link
 * @property string $pic1
 * @property string $pic2
 * @property string $pic3
 * @property string $pic4
 * @property string $second_title
 * @property string $button_title
 * @property string $first_color
 *
 * The followings are the available model relations:
 * @property FrontSlider $frontSlider
 * @property Languages $lang
 */
class FrontSliderI18n extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'front_slider_i18n';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_id, title, pic1, pic2, pic3, pic4', 'required', 'on' => 'save_1'),
            array('lang_id', 'required', 'on' => 'save_0'),

            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lang_id, front_slider_id, status, in_new_window, title, alt, link, pic1, pic2, pic3, pic4', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'frontSlider' => array(self::BELONGS_TO, 'FrontSlider', 'front_slider_id'),
            'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lang_id' => 'Lang',
            'front_slider_id' => 'Front Slider',
            'status' => 'Публикация',
            'in_new_window' => 'Открыть в новом окне',
            'title' => 'Название',
            'alt' => 'Alt коментрарий',
            'link' => 'Ссылка',
            'pic1' => 'Картинка 1',
            'pic2' => 'Картинка 2',
            'pic3' => 'Картинка 3',
            'pic4' => 'Картинка 4',
            'second_title' => 'Второй заголовок',
            'first_title' => 'Первый заголовок'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('front_slider_id', $this->front_slider_id);
        $criteria->compare('status', $this->status);
        $criteria->compare('in_new_window', $this->in_new_window);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('alt', $this->alt, true);
        $criteria->compare('link', $this->link, true);
        $criteria->compare('pic1', $this->pic1, true);
        $criteria->compare('pic2', $this->pic2, true);
        $criteria->compare('pic3', $this->pic3, true);
        $criteria->compare('pic4', $this->pic4, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FrontSliderI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
