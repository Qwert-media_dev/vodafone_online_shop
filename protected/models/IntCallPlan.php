<?php

/**
 * This is the model class for table "int_call_plan".
 *
 * The followings are the available columns in table 'int_call_plan':
 * @property integer $id
 * @property integer $country_id
 * @property string $type
 * @property string $price
 * @property string $price_uk
 * @property string $price_en
 * @property string $price_ru
 *
 * The followings are the available model relations:
 * @property Country $country
 */
class IntCallPlan extends CActiveRecord
{

	const TYPE_PRIVATE = 'private';
	const TYPE_BUSINESS = 'business';
	const TYPE_CONTRACT = 'contract';

	public $title;
	public $code;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'int_call_plan';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('price_uk, price_en, price_ru', 'required'),
			//array('price', 'numerical'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, country_id, type, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'country' => array(self::BELONGS_TO, 'Country', 'country_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_id' => 'Страна',
			'type' => 'Тип абонента',
			'price' => 'Тариф',
			'price_uk' => 'Тариф (Укр)',
			'price_en' => 'Тариф (Англ)',
			'price_ru' => 'Тариф (Рус)',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('price',$this->price);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => ['pageSize' => 25]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return IntCallPlan the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getItems($type, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->select = 'ci.title, c.code, t.*';
		$criteria->condition = 't.type=:type AND ci.lang_id=:lang_id';
		$criteria->params = array(':type' => $type, ':lang_id' => $lang_id);
		$criteria->join = 'LEFT JOIN country_i18n ci ON ci.country_id=t.country_id LEFT JOIN country c ON c.id=t.country_id';
		$criteria->order = 'ci.title';

		return self::model()->findAll($criteria);
	}
	
	public function getCountryID() {
        $list = [];
		$criteria = new CDbCriteria();
        $criteria->select = 'country_id';
        $criteria->group = 'country_id';
        $items = self::model()->findAll($criteria);
        foreach($items as $item) {
            $list[] = $item->country_id;
        }
        return $list;
	}
    
}
