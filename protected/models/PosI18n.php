<?php

/**
 * This is the model class for table "pos_i18n".
 *
 * The followings are the available columns in table 'pos_i18n':
 * @property integer $id
 * @property integer $lang_id
 * @property integer $pos_id
 * @property string $title
 * @property string $address
 * @property string $local_name
 * @property string $metro
 * @property string $how_to_find
 * @property string $work_hours
 *
 * The followings are the available model relations:
 * @property Pos $pos
 * @property Languages $lang
 */
class PosI18n extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pos_i18n';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lang_id, address', 'required'),
			array('lang_id', 'numerical', 'integerOnly'=>true),
			array('title, address, local_name, metro', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lang_id, pos_id, title, address, local_name, metro, how_to_find, work_hours', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pos' => array(self::BELONGS_TO, 'Pos', 'pos_id'),
			'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lang_id' => 'Lang',
			'pos_id' => 'Pos',
			'title' => 'Название точки',
			'address' => 'Адрес',
			'local_name' => 'Народное название',
			'metro' => 'Метро',
			'how_to_find' => 'Как найти',
			'work_hours' => 'Часы работы',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('pos_id',$this->pos_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('local_name',$this->local_name,true);
		$criteria->compare('metro',$this->metro,true);
		$criteria->compare('how_to_find',$this->how_to_find,true);
		$criteria->compare('work_hours',$this->work_hours,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PosI18n the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function formattedWorkHours() {
		$rows = array();
		$field = trim($this->work_hours);
		$lines = explode("\n", $field);
		foreach($lines as $line) {
			$line = trim($line);
			if($line != '') {
				$rows[] = $line;
			}
		}
		return $rows;
	}
}
