<?php

/**
 * This is the model class for table "admins".
 *
 * The followings are the available columns in table 'admins':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $show_index
 * @property integer $status
 * @property integer $group_id
 *
 */
class Admins extends CActiveRecord
{

	const SALT = 892730498093;
	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admins';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

            array('email, name, password', 'required', 'on' => 'create'),
            array('email', 'email', 'on' => 'create'),
			array('email', 'checkEmail', 'on' => 'create'),

            array('email, name', 'required', 'on' => 'save'),
            array('email', 'email', 'on' => 'save'),
			array('email', 'checkEmail', 'on' => 'save'),

			array('email, password', 'required', 'on' => 'login'),
			array('email', 'email', 'on' => 'login'),
			array('email', 'authenticate', 'on' => 'login'),

            array('email, name, password, show_index', 'safe', 'on'=>array('create', 'save')),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'group' => array(self::BELONGS_TO, 'AdminGroups', 'group_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Имя',
			'email' => 'Email',
			'password' => 'Пароль',
			'group_id' => 'Группа'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('password', $this->password, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Admins the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function checkEmail($attribute, $params) {
		$criteria = new CDbCriteria();
		$criteria->condition = 'email=:email AND id!=:id';
		$criteria->params = array(':email' => $this->email, ':id' => (int)$this->id);
		$item = self::model()->find($criteria);
		if( !is_null($item) ) {
			$this->addError('email', Yii::t('app', 'Такой E-mail уже используется'));
		}
	}

	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$identity = new UserIdentity($this->email, $this->password);
			$identity->authenticate();

			switch ($identity->errorCode) {
				case UserIdentity::ERROR_NONE:
					Yii::app()->user->login($identity, Yii::app()->params['login_life_time']);
					break;
				case UserIdentity::ERROR_USERNAME_INVALID:
					$this->addError('email', 'Логин или пароль не верный');
					break;
				case UserIdentity::ERROR_PASSWORD_INVALID:
					$this->addError('password', 'Не верный пароль');
					break;
			}
		}
	}

	public function authorization_by_form($email, $password)
	{

		$user = self::model()->findByAttributes(array('email' => $email));

		if ($user && CPasswordHelper::verifyPassword($password, $user->password) && $user->status == self::STATUS_ACTIVE ) {

			$data = array();
			$data['model'] = $user;
			$data['user_id'] = $user->id;
			$data['username'] = $user->email;
			$data['email'] = $user->email;
			$data['part1'] = $this->_genPart($user->id, $user->email, 1);
			$data['part2'] = $this->_genPart($user->id, $user->email, 2);
			return $data;
		}

		return false;
	}

	public function authorization_by_cookie($login, $part1, $part2)
	{
		$check = self::model()->getByLogin($login);
		if ($check) {
			if ($part1 == $this->_genPart($check->id, $check->login, 1) && $part2 == $this->_genPart($check->id, $check->login, 2)) {
				return $check;
			}
		}

		return false;
	}

	private function _genPart($user_id, $login, $part)
	{
		if ($part == 1)
			return md5($login . '/' . $user_id . '/' . self::SALT);
		if ($part == 2)
			return md5($user_id . '/' . self::SALT . '/' . $user_id);

		return false;
	}


    public function beforeSave() {
        if ($this->isNewRecord) {
            $this->password = CPasswordHelper::hashPassword($this->password);
        }

        return parent::beforeSave();
    }

}