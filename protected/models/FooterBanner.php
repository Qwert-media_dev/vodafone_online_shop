<?php

/**
 * This is the model class for table "footer_banner".
 *
 * The followings are the available columns in table 'footer_banner':
 * @property integer $id
 * @property string $title
 * @property integer $position
 * @property integer $page_id
 * @property integer $is_deleted
 * @property string $created_at
 * @property string $updated_at
 * @property string $statuses
 *
 * The followings are the available model relations:
 * @property FooterBannerI18n[] $i18n
 */
class FooterBanner extends CActiveRecord
{

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'footer_banner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, position, page_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'i18n' => array(self::HAS_MANY, 'FooterBannerI18n', 'footer_banner_id', 'together' => true),
			'page' => array(self::BELONGS_TO, 'Pages', 'page_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'position' => 'Очередность',
			'page_id' => 'Раздел',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition = 't.is_deleted=0';
		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('position',$this->position);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FooterBanner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function behaviors()
	{
		return array(
			'CTimestampBehavior' => array(
				'class' => 'zii.behaviors.CTimestampBehavior',
				'createAttribute' => 'created_at',
				'updateAttribute' => 'updated_at'
			)
		);
	}


	public function getDeleted() {
		$criteria = new CDbCriteria;
		$criteria->condition = 't.is_deleted=1';
		$criteria->order = 't.position';

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}


	public function getSet($page_id, $lang_id) {
		$set = [];
		$criteria = new CDbCriteria();
		$criteria->condition = 't.is_deleted=:is_deleted AND t.page_id=:page_id';
		$criteria->params = array(':is_deleted' => 0, ':page_id' => $page_id);
		$criteria->with = array('i18n' => array(
			'condition' => 'i18n.footer_banner_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
			'joinType'=>'INNER JOIN',
			'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));

		$criteria->order = 't.position';

		$prepared = [];
		$items = self::model()->findAll($criteria);
		foreach($items as $item) {
			$prepared[ $item->position ][] = $item;
		}

		foreach($prepared as $position => $items) {
			if( count($items) == 1 ) {
				$set[$position] = $items[0];
			} elseif( count($items) > 1 ) {
				$rand = rand(0, count($items)-1 );
				$set[$position] = $items[$rand];
			}
		}

		return $set;
	}
}
