<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property integer $id
 * @property integer $sort
 * @property string $code
 * @property integer $network
 * @property integer $is_europe
 * @property integer $network_id
 * @property integer $network_id_contract
 * @property integer $network_id_prepaid
 * @property integer $is_popular
 * @property string $private_visibility
 * @property string $contract_visibility
 * @property string $business_visibility
 *
 * The followings are the available model relations:
 * @property CountryI18n[] $i18n
 * @property Network $net
 * @property Network $net_contract
 * @property Network $net_prepaid
 */
class Country extends CActiveRecord
{

	public $title;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code', 'required'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sort, code, network, is_europe, title', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'i18n' => array(self::HAS_MANY, 'CountryI18n', 'country_id', 'together' => true),
            'net' => array(self::BELONGS_TO, 'Network', 'network_id'),
            'net_contract' => array(self::BELONGS_TO, 'Network', 'network_id_contract'),
            'net_prepaid' => array(self::BELONGS_TO, 'Network', 'network_id_prepaid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sort' => 'Очередность',
			'code' => 'Код страны',
			'network' => 'Страна Vodafone',
			'is_europe' => 'Это ли Европа ?',
			'network_id' => 'Сеть (Бизнес)',
			'network_id_contract' => 'Сеть (Контракт)',
			'network_id_prepaid' => 'Сеть (Препейд)',
			'is_popular' => 'Популярная',
			'private_visibility' => 'Препейд',
			'contract_visibility' => 'Контракт',
			'business_visibility' => 'Бизнес'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sort',$this->sort);
		if( $this->code != '' )
			$criteria->addSearchCondition('code', '%'.$this->code.'%', false);
		if( $this->title != '' )
			$criteria->addSearchCondition('i18n.title', '%'.$this->title.'%', false);
		$criteria->compare('network',$this->network);
		$criteria->compare('is_europe',$this->is_europe);

		$criteria->order = 'i18n.title';
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.country_id=t.id AND i18n.lang_id=:lang_id',
				'joinType' => 'INNER JOIN',
				'params' => array(':lang_id' => param('defaultLangId'))
		));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => ['pageSize' => 25]
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getNotFromNetwork($lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.network=:network';
		$criteria->params = array(':network' => 0);
		$criteria->order = 'i18n.title';
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.country_id=t.id AND i18n.lang_id=:lang_id',
				'joinType' => 'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		return self::model()->findAll($criteria);
	}

	public function getFromNetwork($lang_id, $field='') {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.network=:network';
		$criteria->params = [':network' => 1];
		if( $field != '' ) {
			$criteria->mergeWith([
				'condition' => 't.'.$field.'=:visibility',
				'params' => [':visibility' => 1]
			]);
		}
		$criteria->order = 'i18n.title';
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.country_id=t.id AND i18n.lang_id=:lang_id',
				'joinType' => 'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		return self::model()->findAll($criteria);
	}

	public function getFromNetworkAndEurope($lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.network=:network AND t.is_europe=:is_europe';
		$criteria->params = array(':network' => 1, ':is_europe' => 1);
		$criteria->order = 'i18n.title';
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.country_id=t.id AND i18n.lang_id=:lang_id',
				'joinType' => 'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		return self::model()->findAll($criteria);
	}


	public function getItems($lang_id, $only=[]) {
        $criteria = new CDbCriteria();
        $criteria->order = 'i18n.title';
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.country_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id)
        ));
		if(count($only)) {
			$criteria->addInCondition('t.id', $only);
		}
        return self::model()->findAll($criteria);
	}

}
