<?php

/**
 * This is the model class for table "rates".
 *
 * The followings are the available columns in table 'rates':
 * @property integer $id
 * @property string $title
 * @property string $statuses
 * @property integer $is_archive
 * @property integer $is_active
 * @property integer $is_top
 * @property string $type
 * @property string $preview
 * @property integer $is_deleted
 * @property string $rate_date
 * @property string $price
 * @property integer $sort
 * @property string $widget
 * 
 * The followings are the available model relations:
 * @property RatesI18n[] $i18n
 * @property RatesAdvantages[] $advantages
 * @property RatesGeo[] $geo
 * @property GeoExclude[] $geoExclude
 */
class Rates extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'rates';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type, preview', 'required'),
            #array('title, statuses, is_archive, is_active, is_top, preview', 'required'),
            #array('is_archive, is_active, is_top', 'numerical', 'integerOnly' => true),
            #array('title, statuses, preview', 'length', 'max' => 255),
            #array('type', 'length', 'max' => 8),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, statuses, is_archive, is_active, is_top, type, preview', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'i18n' => array(self::HAS_MANY, 'RatesI18n', 'rate_id', 'together' => true),
            'advantages' => array(self::HAS_MANY, 'RatesAdvantages', 'rate_id', 'together' => true),
            'geo' => array(self::HAS_MANY, 'RatesGeo', 'rate_id', 'together' => true),
            'geoExclude' => array(self::HAS_MANY, 'GeoExclude', 'table_pk', 'together' => true, 'condition' => 'geoExclude.table=:table', 'params' => [':table' => $this->tableName()]),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Название тарифы',
            'statuses' => 'Публикация',
            'is_archive' => 'Архивная',
            'is_active' => 'Действующий',
            'is_top' => 'Топ-тариф',
            'type' => 'Тип',
            'preview' => 'Картинка превью',
            'rate_date' => 'Дата старта тарифа',
            'price' => 'Стоимость (цифрами)'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('statuses', $this->statuses, true);
        $criteria->compare('is_archive', $this->is_archive);
        $criteria->compare('is_active', $this->is_active);
        $criteria->compare('is_top', $this->is_top);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('preview', $this->preview, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Rates the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function getTable($is_archive = null, $is_deleted = 0)
    {

        $criteria = new CDbCriteria;
        $criteria->condition = 't.is_deleted=:is_deleted';
        $criteria->params = array(':is_deleted' => $is_deleted);

        if (!is_null($is_archive)) {
            $criteria->mergeWith(array(
                'condition' => 't.is_archive=:is_archive',
                'params' => array(':is_archive' => $is_archive)
            ));
        }

        // apply filter
        if (array_key_exists('filter', $_GET)) {
            $filter = $_GET['filter'];

            if (array_key_exists('title', $filter) && $filter['title'] != '') {
                $ids = RatesI18n::model()->findByTitle($filter['title']);
                if (count($ids))
                    $criteria->addInCondition('t.id', $ids);
            }
        }

        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => param('defaultLangId'))
        ));

        $criteria->order = 't.sort';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public function getTypes() {
        return [
            'private' => 'Частным клиентам',
            'contact' => 'Контрактным клиентам',
            'business' => 'Для бизнеса'
        ];
    }


    public function getActiveItems($type, $lang_id, $geo) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND t.is_archive=:is_archive AND t.is_deleted=:is_deleted';
        $criteria->params = [':type' => $type, ':is_archive' => 0, ':is_deleted' => 0];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));
        $criteria->order = 't.sort';

        if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        }
        $criteria->group = 't.id';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->findAll($criteria);
    }


    public function getArchiveItems($type, $lang_id, $is_active, $geo) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND t.is_archive=:is_archive AND t.is_active=:is_active AND t.is_deleted=:is_deleted';
        $criteria->params = [':type' => $type, ':is_archive' => 1, ':is_active' => $is_active, ':is_deleted' => 0];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));

        if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        }

        $criteria->order = 't.sort';
        $criteria->group = 't.id';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->findAll($criteria);
    }


    public function getActiveByAlias($type, $alias, $lang_id) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND i18n.alias=:alias AND t.is_deleted=:is_deleted';
        $criteria->params = [':type' => $type, ':alias' => $alias, ':is_deleted' => 0];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));
        return self::model()->find($criteria);
    }


    public function hasArchive($type, $lang_id, $geo) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND t.is_archive=:is_archive AND t.is_deleted=:is_deleted';
        $criteria->params = [':type' => $type, ':is_archive' => 1, ':is_deleted' => 0];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));
        if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        }
        $criteria->group = 't.id';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->count($criteria) > 0;
    }

    
    public function hasRates($type, $lang_id, $geo) {
        $criteria = new CDbCriteria();
        $criteria->condition = 't.type=:type AND t.is_archive=:is_archive AND t.is_deleted=:is_deleted';
        $criteria->params = [':type' => $type, ':is_archive' => 0, ':is_deleted' => 0];
        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.rate_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
            'joinType' => 'INNER JOIN',
            'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
        ));
        if( $geo == [0,0] ) {
            $criteria->mergeWith([
                'condition' => 'g.region_id=:region_id AND g.city_id=:city_id',
                'params' => [':region_id' => 0, ':city_id' => 0],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        } else {
            $criteria->mergeWith([
                'condition' => '( (g.region_id=:region_id2 AND g.city_id=:city_id2) || (g.region_id=:region_id3 AND g.city_id=:city_id3) )',
                'params' => [
                    ':region_id2' => $geo[0], ':city_id2' => $geo[1],
                    ':region_id3' => $geo[0], ':city_id3' => 0
                ],
                'join' => 'join rates_geo g ON g.rate_id=t.id',
            ]);
        }
        $criteria->group = 't.id';
        $exclude = GeoExclude::model()->getExcludeItems( $this->tableName(), $geo[0], $geo[1] );
        if( count($exclude) ) {
            $criteria->addNotInCondition('t.id', $exclude);
        }
        return self::model()->count($criteria) > 0;
    }
}
