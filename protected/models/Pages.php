<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $type
 * @property integer $parent_id
 * @property string $title
 * @property integer $sort
 * @property integer $hide_in_menu
 * @property integer $in_new_window
 * @property string $statuses
 * @property integer $is_tabbed_menu
 * @property integer $is_deleted
 * @property string $controller
 * @property integer $root
 * @property string $extra_class
 * @property integer $protected
 * @property string $app_name
 * @property string $app_store_id
 * @property string $app_package
 * @property string $extra_css
 * @property string $extra_js
 *
 * The followings are the available model relations:
 * @property PagesI18n[] $i18n
 */
class Pages extends CActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

	const TYPE_PAGE = 'page';
	const TYPE_LINK = 'link';
	const TYPE_MODULE = 'module';
	const TYPE_NOT_A_LINK = 'not-a-link';

	public $dummy;

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'i18n' => array(self::HAS_MANY,'PagesI18n', 'page_id', 'together' => true)
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'status' => 'Статус',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'sort' => 'Очередность в древе',
			'hide_in_menu' => 'Скрыть в меню',
			'parent_id' => 'Родительская страница',
			'in_new_window' => 'Открыть в новом окне',
			'is_tabbed_menu' => 'Меню в виде вкладок',
			'app_store_id' => 'iOS App Store ID',
			'app_package' => 'Android  Package Name'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at'
            )
        );
    }


    public function getByAlias($lang_id, $parent_id, $alias) {
        $criteria = new CDbCriteria();
	    $criteria->condition = 't.parent_id=:parent_id';
	    $criteria->params = array(':parent_id' => $parent_id);

        $criteria->with = array('i18n' => array(
            'condition' => 'i18n.alias=:alias AND i18n.page_id=t.id AND i18n.lang_id=:lang_id',
            'joinType'=>'INNER JOIN',
            'params' => array(':alias' => $alias, ':lang_id' => $lang_id)
        ));

        return self::model()->find($criteria);
    }

	/**
	 * Использовалось для построения древа.
	 * JS, с подгрузкой по одному уровню за раз
	 * DEPRECATED
	 *
	 * @param integer $parent_id
	 * @return array
	 */
	public function getLevel($parent_id) {
		//$data = array();
		$criteria = new CDbCriteria();
		$criteria->condition = 't.parent_id=:parent_id AND t.is_deleted=:is_deleted';
		$criteria->params = array(':parent_id' => $parent_id, ':is_deleted' => 0);
		$criteria->order = 't.sort';
		return self::model()->findAll($criteria);
		/*
		foreach($items as $item) {
			$data[] = array('title' => $item->title, 'key' => $item->id, 'lazy' => true);
		}

		return $data;
		*/
	}


	/**
	 * Собирает дерево для бэкэнда
	 *
	 * @param int $parent_id
	 * @param int $level
	 * @return array
	 */
	public function getTree($parent_id=0, $level=0) {
		$data = array();

		$criteria = new CDbCriteria();
		$criteria->condition = 't.parent_id=:parent_id and t.is_deleted=:is_deleted';
		$criteria->params = array(':parent_id' => $parent_id, ':is_deleted' => 0);
		$criteria->order = 't.sort';
		$criteria->with = array('i18n');

		$items = self::model()->findAll($criteria);
		foreach($items as $item) {
			// исключаем из меню ссылки
			//if( substr($item->alias, 0, 7) == 'http://' || substr($item->alias, 0, 8) == 'https://' ) continue;

			$row = array();
			$row['id'] = $item->id;
			$row['parent_id'] = $item->parent_id;
			$row['level'] = $level;
			$row['title'] = $item->title;
			$row['type'] = $item->type;
			$row['statuses'] = $item->statuses;
			$row['in_new_window'] = $item->in_new_window;
			$row['is_tabbed_menu'] = $item->is_tabbed_menu;
			$row['protected'] = $item->protected;
			$row['root']            = $item->root;
			$row['children'] = self::model()->getTree( $item->id, $level+1 );
			$data[] = $row;
		}

		return $data;
	}

	public function getFlatTree($tree, &$result) {
		foreach($tree as $item) {
			$row = $item;
			unset( $row['children'] );
			$result[] = $row;
			if(sizeof($item['children'])) {
				self::model()->getFlatTree($item['children'], $result);
			}
		}
	}

	/**
	 * Генерирует данные для построение select'а
	 *
	 * @return array
	 */
	public function getFlatMenu() {
		$flat = array();
		self::model()->getFlatTree( self::model()->getTree(), $flat);
		return $flat;
	}


	/**
	 * Собирает дерево для фронтэнда
	 *
	 * @param int $parent_id
	 * @param int $level
	 * @param $lang_id
	 * @return array
	 */
	public function getActiveTree($parent_id=0, $level=0, $lang_id) {
		$data = array();

		$criteria = new CDbCriteria();
		$criteria->condition = 't.parent_id=:parent_id AND t.hide_in_menu=:hide_in_menu AND t.is_deleted=:is_deleted';
		$criteria->params = array(':parent_id' => $parent_id, ':hide_in_menu' => 0, ':is_deleted' => 0);
		$criteria->order = 't.sort';
		$criteria->with = array('i18n' => array(
			'condition' => 'i18n.page_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
			'joinType'=>'INNER JOIN',
			'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));

		$items = self::model()->findAll($criteria);
		foreach($items as $item) {
			$row = array();
			$row['id'] 			    = $item->id;
			$row['parent_id'] 	    = $item->parent_id;
			$row['alias'] 		    = $item->i18n[0]->alias;
			$row['level'] 		    = $level;
			$row['title'] 		    = $item->i18n[0]->short_title == '' ? $item->i18n[0]->title : $item->i18n[0]->short_title;
			$row['type']            = $item->type;
			$row['in_new_window']  = $item->in_new_window;
            $row['hide_alias']      = $item->i18n[0]->hide_alias;
			$row['root']            = $item->root;
			$row['is_tabbed_menu']  = $item->is_tabbed_menu;
			$row['created_at']      = $item->created_at;
			$row['children'] 	    = self::model()->getActiveTree( $item->id, $level+1, $lang_id );
			$data[] = $row;
		}

		return $data;
	}


	public function deleteItem($id) {
		$statuses = [];
		$item = self::model()->with('i18n')->findByPk($id);
		$item->is_deleted = 1;
		$item->parent_id = 0;
		//$item->save(false);
		foreach($item->i18n as $i18n) {
			$i18n->status = self::STATUS_INACTIVE;
			$i18n->save(false);
			$statuses[$i18n->lang->code] = self::STATUS_INACTIVE;
		}
		$item->statuses = CJSON::encode($statuses);
		$item->save(false);
	}

	public function getTrash() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.is_deleted=:is_deleted';
		$criteria->params = array(':is_deleted' => 1);
		$criteria->order = 't.updated_at DESC';

		return new CActiveDataProvider($this, array(
				'criteria' => $criteria,
		));
	}

	/**
	 * Ищем следующую страницу по иерархии
	 *
	 * @param int $parent_id
	 * @param int $lang_id
	 * @return array|mixed|null
	 */
	public function findNextChild($parent_id, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.parent_id=:parent_id AND t.is_deleted=:is_deleted';
		$criteria->params = array(':parent_id' => $parent_id, ':is_deleted' => 0);
		$criteria->order = 't.sort';
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.page_id=t.id AND i18n.lang_id=:lang_id AND i18n.status=:status',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id, ':status' => self::STATUS_ACTIVE)
		));

		return self::model()->find($criteria);
	}

	/**
	 * Для крошек, поиск модульной страницы для построения древа
	 * Пока что поиск идет только по контроллеру, не всегда модульная страница будет модульной,
	 * она так же может быть ссылочной
	 *
	 * @param string $controller
	 * @param integer $lang_id
	 * @return static
	 */
	public function getModulePageByController($controller, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.controller=:controller';
		$criteria->params = array(':controller' => $controller);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.page_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		return self::model()->find($criteria);
	}


	public function getById($id, $lang_id) {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.id=:id';
		$criteria->params = array(':id' => $id);
		$criteria->with = array('i18n' => array(
				'condition' => 'i18n.page_id=t.id AND i18n.lang_id=:lang_id',
				'joinType'=>'INNER JOIN',
				'params' => array(':lang_id' => $lang_id)
		));
		return self::model()->find($criteria);
	}


	public function getRootId(Pages $model) {
		if($model->parent_id == 0) return $model->id;

		$root = null;

		$item = self::model()->findByPk($model->parent_id);
		if($item !== null) {
			$id = $item->parent_id;
			$root = $item->id;
			do {
				$item = self::model()->findByPk($id);
				if($item !== null) {
					$id = (int)$item->parent_id;
					$root = $item->id;
				} else {
					$id = 0;
				}
			}while($id != 0);
		}

		if(is_null($root))
			$root = $model->id;

		return $root;
	}

	/**
	 * Выборка первого уровня меню для настроек
	 *
	 * @return array|mixed|null
	 */
	public function getFirstLevel() {
		$criteria = new CDbCriteria();
		$criteria->condition = 't.parent_id=:parent_id AND t.hide_in_menu=:hide_in_menu AND t.is_deleted=:is_deleted';
		$criteria->params = array(':parent_id' => 0, ':hide_in_menu' => 0, ':is_deleted' => 0);
		$criteria->order = 't.sort';
		return self::model()->findAll($criteria);
	}

	/**
	 * Выбираются страницы для FrontSlider
	 *
	 * @param array $list
	 * @return static[]
	 */
	public function getForSliders( $list ) {
		$criteria = new CDbCriteria();
		$criteria->addInCondition('t.id', $list);
		$criteria->order = 't.sort';
		return self::model()->findAll($criteria);
	}

	/**
	 * Используется для построения sitemap.xml
	 *
	 * @param $id
	 * @param null $lang_id
	 * @return array
	 */
	public function expandPath( $id, $lang_id, $skip_first=false) {
		$path = array();

		$item = self::model()->getById($id, $lang_id);
		if($item !== null) {
			$id = $item->parent_id;
			if(!$skip_first)
				$path[] = $item->i18n[0]->alias;
			do {
				$item = self::model()->getById($id, $lang_id);
				if($item !== null) {
					$path[] = $item->i18n[0]->alias;
				}
				$id = (int)$item->parent_id;
			}while($id != 0);
		}

		return $path;
	}

}
