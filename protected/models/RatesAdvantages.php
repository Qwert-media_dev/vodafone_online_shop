<?php

/**
 * This is the model class for table "rates_advantages".
 *
 * The followings are the available columns in table 'rates_advantages':
 * @property integer $id
 * @property integer $rate_id
 * @property integer $lang_id
 * @property integer $sort
 * @property string $type
 * @property string $title
 * @property string $icon
 */
class RatesAdvantages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rates_advantages';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('title, icon', 'required', 'on' => 'rich'),
            array('title', 'required', 'on' => 'text'),

			#array('rate_id, lang_id, title, icon', 'required'),
			#array('rate_id, lang_id', 'numerical', 'integerOnly'=>true),
			#array('type', 'length', 'max'=>4),
			#array('title, icon', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, rate_id, lang_id, sort, type, title, icon', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rate_id' => 'Rate',
			'lang_id' => 'Lang',
			'sort' => 'Sort',
			'type' => 'Type',
			'title' => 'Title',
			'icon' => 'Icon',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rate_id',$this->rate_id);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('icon',$this->icon,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RatesAdvantages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function populate($rate_id, $items, $type='text') {
		self::model()->deleteAll('rate_id=:rate_id AND type=:type', [':rate_id' => $rate_id, ':type' => $type]);
		if( !count($items) ) return ;

        $sort = 10;
		foreach($items as $item) {
			foreach($item as $lang_id => $data) {
				$model = new self($type);
				$model->lang_id = $lang_id;
				$model->rate_id = $rate_id;
                $model->sort = $sort;
                $model->type = $type;
				$model->title = $data['title'];
				$model->icon = $data['icon'];
				$model->save();
                $sort+=10;
			}
		}
	}


    public function getByRate($rate_id, $type='text') {
        $videos = [];
        $items = self::model()->findAll([
            'condition' => 'rate_id=:rate_id AND type=:type',
            'params' => [':rate_id' => $rate_id, ':type' => $type],
            'order' => 'sort'
        ]);
        foreach($items as $item) {
            $videos['id'.FS::hash(serialize($item))][$item->lang_id] = [
                'title' => $item->title,
                'icon' => $item->icon
            ];
        }

        return $videos;
    }


	public function getItems($rate_id, $type, $lang_id) {
		return self::model()->findAll([
			'condition' => 'rate_id=:rate_id AND type=:type AND lang_id=:lang_id',
			'params' => [':rate_id' => $rate_id, ':type' => $type, ':lang_id' => $lang_id],
			'order' => 'sort'
		]);
	}
}
