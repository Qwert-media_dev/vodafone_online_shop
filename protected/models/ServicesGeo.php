<?php

/**
 * This is the model class for table "services_geo".
 *
 * The followings are the available columns in table 'services_geo':
 * @property integer $id
 * @property integer $service_id
 * @property integer $region_id
 * @property integer $city_id
 *
 * The followings are the available model relations:
 * @property Services $service
 */
class ServicesGeo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'services_geo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('service_id, region_id, city_id', 'required'),
            array('service_id, region_id, city_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, service_id, region_id, city_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'service' => array(self::BELONGS_TO, 'Services', 'service_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'service_id' => 'Service',
            'region_id' => 'Region',
            'city_id' => 'City',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('service_id', $this->service_id);
        $criteria->compare('region_id', $this->region_id);
        $criteria->compare('city_id', $this->city_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ServicesGeo the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function populate($service_id, $items)
    {
        self::model()->deleteAllByAttributes(['service_id' => $service_id]);
        if (!count($items)) return;

        if (in_array('0_0', $items)) {
            $model = new self;
            $model->service_id = $service_id;
            $model->region_id = 0;
            $model->city_id = 0;
            $model->save();
        } else {
            foreach ($items as $item) {
                $params = explode('_', $item);
                $model = new self;
                $model->service_id = $service_id;
                $model->region_id = (int)$params[0];
                $model->city_id = (int)$params[1];
                $model->save();
            }
        }
    }

    public function getItems($service_id)
    {
        $data = [];
        $items = self::model()->findAllByAttributes(['service_id' => $service_id]);
        foreach ($items as $item) {
            $data[] = $item->region_id . '_' . $item->city_id;
        }

        return $data;
    }
}
