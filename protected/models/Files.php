<?php

/**
 * This is the model class for table "files".
 *
 * The followings are the available columns in table 'files':
 * @property integer $id
 * @property string $original_name
 * @property string $local_name
 * @property string $ext
 * @property integer $size
 * @property string $created_at
 * @property string $updated_at
 * @property string $user_id
 *
 * The followings are the available model relations:
 * @property FilesToFolders[] $filesToFolders
 */
class Files extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'files';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('original_name, local_name, ext, size', 'required'),
			array('size', 'numerical', 'integerOnly'=>true),
			array('original_name, local_name, ext', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, original_name, local_name, ext, size, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'filesToFolders' => array(self::HAS_MANY, 'FilesToFolders', 'file_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'original_name' => 'Original Name',
			'local_name' => 'Local Name',
			'ext' => 'Ext',
			'size' => 'Size',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('local_name',$this->local_name,true);
		$criteria->compare('ext',$this->ext,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Files the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors(){
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_at',
                'updateAttribute' => 'updated_at'
            )
        );
    }


    public function getLast() {
        $criteria = new CDbCriteria();
        $criteria->order = 't.created_at DESC';

        if( array_key_exists('filter', $_GET) ) {
            $filter = $_GET['filter'];

            if( array_key_exists('original_name', $filter) && $filter['original_name'] != '' ) {
	            $criteria->addSearchCondition('original_name', '%'.$filter['original_name'].'%', false);
            }

            if( array_key_exists('ext', $filter) && $filter['ext'] != '' ) {
                $criteria->mergeWith(array(
                    'condition' => 't.ext=:ext',
                    'params' => array(':ext' => $filter['ext'])
                ));
            }
        }

        if( array_key_exists('folder_id', $_GET) ) {
            $criteria->mergeWith(array(
                'join' => 'join files_to_folders ftf ON ftf.folder_id='.(int)$_GET['folder_id'],
                'condition' => 'ftf.file_id=t.id'
            ));
        }

        return self::model()->findAll($criteria);
    }

    public function getExts() {
        $criteria = new CDbCriteria();
        $criteria->group = 'ext';
        $criteria->order = 'ext';
        return self::model()->findAll($criteria);
    }

    public function scopes()
    {
        return array(
            'sorted'=>array(
                'order'=>'created_at DESC',
            ),
        );
    }
}
