<?php

/**
 * This is the model class for table "sxgeo_regions_i18n".
 *
 * The followings are the available columns in table 'sxgeo_regions_i18n':
 * @property integer $id
 * @property integer $lang_id
 * @property string $region_iso
 * @property string $name
 * @property string $popup_name
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Languages $lang
 */
class SxgeoRegionsI18n extends CActiveRecord
{

    public $region_name;
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sxgeo_regions_i18n';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('lang_id, name, popup_name', 'required', 'on' => 'save_1'),
            array('lang_id', 'required', 'on' => 'save_0'),
            
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, lang_id, region_iso, name, popup_name, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'lang_id' => 'Lang',
            'region_iso' => 'Region Iso',
            'name' => 'Name',
            'popup_name' => 'Popup Name',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lang_id', $this->lang_id);
        $criteria->compare('region_iso', $this->region_iso, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('popup_name', $this->popup_name, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SxgeoRegionsI18n the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    
	public function getItems($region_iso) {
        $data = [];
        $items = self::model()->findAllByAttributes(['region_iso' => $region_iso]);
        foreach($items as $item) {
            $data[$item->lang_id] = $item;
        }
        return $data;
    }

    public function getStatuses($iso) {
        $data = [];
        $items = self::model()->findAllByAttributes(['region_iso' => $iso]);
        foreach($items as $item) {
            $data[$item->lang->code] = $item->status;
        }
        return CJSON::encode($data);
    }
}
