<?php

/**
 * This is the model class for table "news_i18n".
 *
 * The followings are the available columns in table 'news_i18n':
 * @property integer $id
 * @property integer $lang_id
 * @property integer $news_id
 * @property integer $status
 * @property string $title
 * @property string $short
 * @property string $body
 * @property string $og_title
 * @property string $og_description
 * @property string $og_image
 *
 * The followings are the available model relations:
 * @property News $news
 * @property Languages $lang
 */
class NewsI18n extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news_i18n';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lang_id, title, body', 'required', 'on' => 'save_1'),
			array('lang_id', 'required', 'on' => 'save_0'),

			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, lang_id, news_id, status, title, short, body', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'news' => array(self::BELONGS_TO, 'News', 'news_id'),
			'lang' => array(self::BELONGS_TO, 'Languages', 'lang_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lang_id' => 'Lang',
			'news_id' => 'News',
			'status' => 'Публикация',
			'title' => 'Заголовок',
			'short' => 'Краткое описание',
			'body' => 'Полное описание',
			'og_title' => 'Заголовок',
			'og_description' => 'Описание',
			'og_image' => 'Бейджик',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lang_id',$this->lang_id);
		$criteria->compare('news_id',$this->news_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('short',$this->short,true);
		$criteria->compare('body',$this->body,true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return NewsI18n the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	public function findByTitle($title) {
		$ids = array();
		$segments = explode(' ', $title);
		foreach($segments as $idx => $segment) {
			$segment = trim($segment);
			$segments[$idx] = $segment;
			if( $segment == '' ) {
				unset($segments[$idx]);
			}
		}

		if( !count($segments) ) return $ids;

		$criteria = new CDbCriteria();

		foreach($segments as $segment) {
			$criteria->addSearchCondition('title', $segment);
		}

		$criteria->group = 'news_id';

		$items = self::model()->findAll($criteria);
		foreach($items as $item) {
			$ids[] = $item->news_id;
		}
		return $ids;
	}

	public function getAlias($news_id, $lang_id) {
		$item = self::model()->findByAttributes(['news_id' => $news_id, 'lang_id' => $lang_id]);
		return is_null($item) ? '' : URLify::filter($item->title);
	}

	/**
	 * Для SEO
	 *
	 * @param $news_id
	 * @return self[]
	 */
	public function getNewsItem($news_id) {
		$data = [];
		$items = self::model()->findAllByAttributes(['news_id' => $news_id]);
		foreach($items as $item) {
			$data[ $item->lang_id ] = $item;
		}
		return $data;
	}
}
