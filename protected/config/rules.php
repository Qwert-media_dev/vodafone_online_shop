<?php
/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/5/2015
 * Time: 08:36 PM
 */

$langs = '<lang:(en|uk|ru|)>';

return array(
	$langs => 'site/index',
	'' => 'site/index',
	'set/geo' => 'site/setgeo',
	'captcha' => 'site/captcha',

	$langs.'/news' => 'news/index',
	$langs.'/news/archive' => 'news/archive',
	array('news/archiveshow', 'pattern'=> $langs.'/news/archive/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),
	array('news/show', 'pattern'=> $langs.'/news/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),

	$langs.'/press' => 'press/index',
	$langs.'/press/archive' => 'press/archive',
	array('press/archiveshow', 'pattern'=> $langs.'/press/archive/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),
	array('press/show', 'pattern'=> $langs.'/press/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),

	$langs.'/vacancy' => 'vacancy/index',
	$langs.'/vacancy/archive' => 'vacancy/archive',
	array('vacancy/archiveshow', 'pattern'=> $langs.'/vacancy/archive/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),
	array('vacancy/show', 'pattern'=> $langs.'/vacancy/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),

	$langs.'/dialog/<page:(private|bussines)>' => 'dialog/index',
	array('dialog/show', 'pattern'=> $langs.'/dialog/<page:(private|bussines)>/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),
	#$langs.'/dialog/archive' => 'dialog/archive',
	#array('dialog/archiveshow', 'pattern'=> $langs.'/dialog/archive/<id:\d+>-<alias:[0-9a-z\-]+>', 'urlSuffix'=>'.html', 'caseSensitive'=>false),

	$langs.'/pos' => 'pos/index',

	$langs.'/rates/<type:(private|contract|business)>' => 'rates/index',
	$langs.'/rates/<type:(private|contract|business)>/archive' => 'rates/archive',
	$langs.'/rates/<type:(private|contract|business)>/archive/<alias:[0-9a-z\-]+>' => 'rates/archiveshow',
	$langs.'/rates/<type:(private|contract|business)>/<alias:[0-9a-z\-]+>' => 'rates/show',

	#$langs.'/network' => 'network/index',
	$langs.'/network/settings' => 'network/settings',
	array('network/load', 'pattern'=> $langs.'/network/<key:\w+>', 'urlSuffix'=>'.json', 'caseSensitive'=>false),

	$langs.'/payment' => 'site/payment',
	$langs.'/payment/process' => 'site/process',

	$langs.'/api/feedback' => 'site/feedback',

	$langs.'/sitemap' => 'site/sitemap',
    $langs.'/oldie' => 'site/oldie',
	$langs.'/search' => 'search/index',
	'sitemap' => array('sitemap/index', 'urlSuffix'=>'.xml', 'caseSensitive'=>false),
	'sitemap-'.$langs => array('sitemap/sitemap', 'urlSuffix'=>'.xml', 'caseSensitive'=>false),


	'<module:\w+>/default/<action:\w+>' => '<module>/default/<action>',

	#'<controller:\w+>/<id:\d+>'=>'<controller>/view',
	#'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
	#'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',

	$langs.'/<url:[0-9a-z\-\/]+>' => 'site/page',
);
