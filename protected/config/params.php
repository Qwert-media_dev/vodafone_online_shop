<?php
/**
 * Created by PhpStorm.
 * User: Sasha
 * Date: 11/5/2015
 * Time: 08:35 PM
 */

return array(
	'login_life_time' => 28800,
	'uploadPath' => rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/images/upload/',
	'perNews' => 5,
	'perPos' => 10,
	'urlifyFileLength' => 255,
	'map' => array(
		'lat' => '50.4500',
		'lng' => '30.5233',
		'zoom' => 16
	),
	'seo_redirect_code' => 301,
	'codes' => array(
		'050' => '050',
		'066' => '066',
		'095' => '095',
		'099' => '099'
	),
	'status' => array(
		0 => 'Выкл',
		1 => 'Вкл'
	),
	'archive' => array(
		0 => 'Нет',
		1 => 'Да'
	),
	'network' => array(
		0 => 'Нет',
		1 => 'Да'
	),
	'europe' => array(
		0 => 'Нет',
		1 => 'Да'
	),
	'popular' => array(
		0 => 'Нет',
		1 => 'Да'
	),
	'front_slider_colors' => array(
        'red' => 'Красный',
        'gray' => 'Серый',
        'white' => 'Белый',
		'black' => 'Черный'
    ),
	'plan_type' => array(
		'private' => 'Для препейда',
		'contract' => 'Для контракта',
		'business' => 'Для бизнеса'
	),
	'cache' => array(
		'static' => '/run/vfstatic',
		'scripts' => '/run/vf'
	),
	'skipBusiness' => false,
	'page_types' => array(
		'page' => 'Контентная',
		'link' => 'Ссылочная',
		'module' => 'Модульная',
		'not-a-link' => 'Разводящая'
	),
	'ga' => 'UA-68847547-1',
	'cse' => array(
		//'devKey' => 'AIzaSyBFffsiMzmcJMjtgbZSf6jM_JbEEvUS2QM',
		'devKey' => 'AIzaSyCMGfdDaSfjqv5zYoS0mTJnOT3e9MURWkU',
		'engineID' => '004922831778621447029:9kfkvqpn4yc'
	),
	'domain' => 'www.vodafone.ua',
	'defaultLang' => 'uk',
	'defaultLangId' => 1,
	'smtp' => file_exists(dirname(__FILE__) . '/smtp.php') ? require(dirname(__FILE__) . '/smtp.php') : array(),
	'from' => array(
		'email' => 'do-not-reply@'.$_SERVER['HTTP_HOST'],
        'name' => 'Vodafone.ua Website'
	),
	'adminEmail' => 'sasha.rudenko@gmail.com'
);
