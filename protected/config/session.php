<?php

return array(
    'class' => 'system.web.CHttpSession',
    'sessionName' => 'SID',
    'timeout' => 259200,
    'cookieParams' => array(
        'lifetime' => 31536000,
        'path' => '/',
        'secure' => true,
        'httpOnly' => true,
    ),
);