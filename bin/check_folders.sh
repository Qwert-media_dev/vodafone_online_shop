#!/usr/bin/env bash

if [ !  -d ../protected/runtime ]
then
    mkdir -p ../protected/runtime
    chmod 777 ../protected/runtime
fi;


if [ ! -d ../public/assets ]
then
    mkdir -p ../public/assets
    chmod 777 ../public/assets
fi;

if [ ! -d ../public/images/upload ]
then
    mkdir -p ../public/images/upload
    chmod 777 ../public/images/upload
fi;
