$(function(){
	$(window).scroll(function(){
		if($(this).scrollTop()>$('.tv-stores-text').offset().top - $(window).innerHeight()) $('.tv-stores-text').addClass('visible');
	});

	$(window).resize(function(){
		resizePackages();

	})

	$(document).on('click','.tv-function', function(e){
		var $this = $(this),
			index = $('.tv-function').index($this),
			offset = $this.offset().top-10;

		$this.toggleClass('active');
		$this.siblings().not($this).removeClass('active');

		$('.tv-descr').slideUp();
		
		if($this.hasClass('active')){
			$('.tv-descr:eq('+index+')').slideDown();
		}

		if($(window).innerWidth()<980) offset = $('.tv-descr:eq('+index+')').offset().top-10;

		if ((navigator.userAgent.match(/mozilla/i) || navigator.userAgent.match(/opera/i)) && !navigator.userAgent.match(/webkit/i)){
			$('html').stop().animate({'scrollTop':offset},500);
		}else{
			$('body').stop().animate({'scrollTop':offset},500);
		}
	});

	resizePackages();

	if($(window).innerWidth()<980){
		$('.tv-access video').remove();
	}else{
		$('.tv-access video').attr('autoplay','autoplay');
	}

});

function resizePackages(){
	var maxH = 0;
	$('.tv-package-inner').each(function(){
		maxH = ($(this).outerHeight()>maxH)?$(this).outerHeight():maxH;
	});
	$('.tv-package').height(maxH);
};