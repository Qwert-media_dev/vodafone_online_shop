$(function() {
    // инициализация табов
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('#myTab a:first').tab('show');


    if ($('.counter').length) {
        $('.counter').keyup(function () {
            var limit = $(this).attr('maxlength');
            $(this).next('.input-group-addon').find('.badge').text(limit - $(this).val().length);
        });
        $('.counter').trigger('keyup');
    }

});

/**
 * Генерация ID для виджетов
 */
function getRandomId() {
    return 'id'+Math.random().toString(16).slice(2);
}


var map = {};
function initMap(id) {

    var lat = $('#'+id+'-lat').val();
    var lng = $('#'+id+'-lng').val();
    var zoom = parseInt( $('#'+id+'-zoom').val() );

    var myOptions = {
        zoom: zoom,
        center:new google.maps.LatLng(lat, lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map[id] = new google.maps.Map(document.getElementById('map-'+id), myOptions);
    marker = new google.maps.Marker({map: map[id], position: new google.maps.LatLng(lat, lng),draggable:true});

    // drag stuff
    google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition( id, marker.getPosition() );
    });

}


function geocodePosition(id, pos) {
    $('#'+id+'-lat').val( pos.lat() );
    $('#'+id+'-lng').val( pos.lng() );
    $('#'+id+'-zoom').val( map[id].getZoom() );
}



function bindEditor(id) {
    $('#'+id).redactor({'imageUpload':'/manage/posts/upload','plugins':['table']});
}


function showLoader() {
    $('body').removeClass('loaded');
}

function hideLoader() {
    $('body').addClass('loaded');
}

function log(v) {
    console.log(v);
}