ready();
function ready(){
    var parser = new UAParser();
    var rezult = parser.getResult();
    var brName = rezult.browser.name , brVersion = parseFloat(rezult.browser.version);
    var isOld = false;
      if(brName=='IE' && brVersion<9){
          window.location = oldieLink;
          return;
      }
      if(brName=='Firefox' && brVersion< 30 || brName == 'Chrome' && brVersion < 30 || brName == 'Opera' && brVersion < 25){
          isOld = true;
      }
      if(isOld && window.location.href !== oldieLink){
          window.location.href = oldieLink;
      }
}
