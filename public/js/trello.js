/**
 * Created by Sasha on 9/22/2015.
 */

(function () {
    var $boxesToCopy, $clipboardContainer, $doc, $focusInput, $pasteInput, onKeydown, onKeyup, value;
    $focusInput = $('<input class="absolute-hidden" type="text"/>').appendTo(document.body).focus().remove();
    $doc = $(document);
    $boxesToCopy = $('.box-to-copy');
    $pasteInput = $('.paste-input');
    $clipboardContainer = $('#clipboard-container');
    value = '';
    var row;
    onKeydown = function (e) {
        var $target;
        $target = $(e.target);
        $clipboardContainer.empty().show();
        $('<textarea id=\'clipboard\'></textarea>').val(value).appendTo($clipboardContainer).focus().select();
    };
    onKeyup = function (e) {
        if ($(e.target).is('#clipboard')) {
            $('#clipboard-container').empty().hide();
            if( row != '') {
                $(row).parent().addClass('warning');
                setTimeout(function(){
                    $(row).parent().removeClass('warning');
                }, 100);
            }
            return $pasteInput.focus();
        }
    };
    $doc.on('keydown', function (e) {
        if (value && (e.ctrlKey || e.metaKey) && e.which === 67) {
            return onKeydown(e);
        }
    }).on('keyup', onKeyup);
    $boxesToCopy.on('mouseenter', function () {
        row = this;
        return value = $(this).data('url');
    }).on('mouseleave', function () {
        row = '';
        return value = '';
    });
}.call(this));
