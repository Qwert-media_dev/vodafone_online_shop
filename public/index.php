<?php

date_default_timezone_set('Europe/Kiev');


if( in_array($_SERVER['REMOTE_ADDR'], array(
    '127.0.0.1', '::1', '192.168.33.1', // local IPs
    '91.142.174.227', // office
    '93.126.118.187' // home
)) ) {
    error_reporting(E_ALL ^ E_NOTICE);
    ini_set('display_errors', 1);
    // remove the following lines when in production mode
    defined('YII_DEBUG') or define('YII_DEBUG',true);
    // specify how many levels of call stack should be shown in each log message
    defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
} else {
    error_reporting(0);
    ini_set('display_errors', 0);
}

// change the following paths if necessary
$yii=dirname(__FILE__).'/../framework/yiilite.php';
$config=dirname(__FILE__).'/../protected/config/main.php';
$globals=dirname(__FILE__).'/../protected/helpers/globals.php';


require_once($yii);
require_once($globals);
Yii::createWebApplication($config)->run();
