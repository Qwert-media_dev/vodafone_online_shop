# run with --production option for minified assets

gulp = require 'gulp'
requireDir = require('require-dir')
tasks = requireDir('./gulp')

gulp.task 'default', ['jade', 'fonts', 'img', 'sass', 'js', 'coffee']

gulp.task 'watch', ->
  gulp.watch 'assets/sass/**/*.sass', ['sass']
  gulp.watch ['assets/jade/**/*.jade'], ['jade']
  gulp.watch 'assets/coffee/**/*.coffee', ['coffee']

gulp.task 'sw', ['serve', 'watch']