class BaseController

  constructor: (@container, key)->
    key = key or 'controller'
    return if @container[key]
    @container[key] = this
    @init()
    @bind()
    
  init: ->

  bind: ->