#= require BaseController.coffee
#= require_tree _classes

Velo = if $? then $.Velocity else Velocity

window.Share = Share

do init = ->

  addClass(document.body, 'ie') if isIE()

  if isTouchDevice() then addClass(document.body, 'touch') else addClass(document.body, 'not-touch')

  layout = new LayoutController document.body

  for accordion in document.querySelectorAll('.accordion')
    new AccordionController accordion

  for tabsSwitch in document.querySelectorAll('.tabs-switch')
    new TabsSwitchController tabsSwitch

  for news in document.querySelectorAll('.inner-content-container.news')
    new NewsController news, document.querySelector('.news-page-title-container')

  for shops in document.querySelectorAll('.inner-content-container.shops')
    new ShopsController shops, document.querySelector('.shops-page-title-container')

  for network in document.querySelectorAll('.inner-content-container.network')
    new NetworkController network

  for pay in document.querySelectorAll('.inner-content-container.pay')
    new PayController pay


  for settings in document.querySelectorAll('.inner-content-container.settings')
    new SettingsController settings, 'settings-controller'

  for feedback in document.querySelectorAll('.inner-content-container.feedback')
    new FeedbackController feedback


  for tarif_group in document.querySelectorAll('.tarif-group')
    new TarifsController tarif_group

  for tarif_banners in document.querySelectorAll('.new_tarif-banners')
    new TarifBannersController tarif_banners

#  for tarif_table in document.querySelectorAll('.new_tarif-table')
#    new TarifTableController tarif_table

  for featured in document.querySelectorAll('#container')
    new FeaturedController featured

  for geo in document.querySelectorAll('.global-menu .geo')
    new GeoSelect geo

