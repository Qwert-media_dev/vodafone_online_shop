class ShopsController extends BaseController

  constructor: (@container, @head)->
    super @container
  
  init: ->
    @items = container.querySelectorAll('.shop-item')


  bind: ->
    for item in @items
      ((item)=>
        showMap = item.querySelector('.show-map')
        print = item.querySelector('.print')
        map = item.querySelector('.map-container')
        showMap.addEventListener 'click', =>
          if isHidden map
            Velo map, 'slideDown', complete: =>
              @showMap map, map.getAttribute('data-lat'), map.getAttribute('data-lng')
            addClass item, 'expanded'
          else
            Velo map, 'slideUp', complete: =>
              map.innerHTML = ''
            removeClass item, 'expanded'
        print.addEventListener 'click', => @print map
      )(item)
      
  showMap: (mapElement, lat, lng)->
    mapOptions =
      center: new (google.maps.LatLng)(lat, lng)
      zoom: 16
      zoomControl: true
      zoomControlOptions: style: google.maps.ZoomControlStyle.DEFAULT
      disableDoubleClickZoom: true
      mapTypeControl: false
      scaleControl: true
      scrollwheel: false
      panControl: true
      streetViewControl: false
      draggable: true
      overviewMapControl: true
      overviewMapControlOptions: opened: false
      mapTypeId: google.maps.MapTypeId.ROADMAP
      
    #mapElement = document.getElementById('#map')
    
    map = new (google.maps.Map)(mapElement, mapOptions)
    marker = new (google.maps.Marker)(
      icon: '/img/map-marker-solid.png'
      position: new (google.maps.LatLng)(lat, lng)
      map: map
      #title: title
      #desc: description
      #tel: telephone
      #email: email
      #web: web
      )
    return map
  
  print: (map)->
    printer = null
    printer = window.open('', Math.random(), 'width=800,height=600')
    script = document.getElementById 'g-maps-api'
    
    printerScript = printer.document.createElement('script')
    printer.document.body.appendChild printerScript
    
    printerScript.onload = =>
      printerMap = printer.document.createElement('div')
      printerMap.className = 'map-container'
      printer.document.body.appendChild printerMap
      printerMap.style.height = '100%'
      setTimeout (=>
        popMap = @showMap printerMap, map.getAttribute('data-lat'), map.getAttribute('data-lng')
        popMap.addListener 'tilesloaded', -> setTimeout (->
          printer.print()
          printer.close()
        ), 100
      ), 350
      
    printerScript.src = script.src
    
      
      