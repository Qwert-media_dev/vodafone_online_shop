class PromoSliderController extends BaseController

  init: ->
    @current = 0
    @timer = null
    @animating = false

  bind: ->
    buttons = @container.querySelectorAll('.controls button')

    for button, index in buttons
      if buttons.length <= 1
        button.style.display = 'none'
      else
        button.addEventListener 'click', ((index)=> => @showSlide index)(index)

    if buttons.length <= 1
      @container.querySelector('.fwd').style.display = 'none'
      @container.querySelector('.bwd').style.display = 'none'
    else
      @container.querySelector('.fwd').addEventListener 'click', => @next()
      @container.querySelector('.bwd').addEventListener 'click', => @prev()

      @timer = setTimeout (=> @next()), 5000

      window.addEventListener 'focus', =>
        setTimeout (=> @next()), 5000
      window.addEventListener 'blur', =>
        clearTimeout @timer

  showSlide: (index)->

    return if @animating or index == @current

    slides = @container.querySelectorAll('.slide')
    controls = @container.querySelectorAll('.controls li')

    index = 0 if index >= slides.length
    index = slides.length - 1 if index < 0

    slides[index].style.zIndex = 1
    @animating = true
    Velo slides[index], 'fadeIn',
      display: 'block'
      complete: =>
        slides[index].style.zIndex = ''
        addClass slides[index], 'active'
        for slide, i in slides
          unless i == index
            slide.style.display = 'none'
            removeClass slide, 'active'
        @animating = false

    for control, i in controls
      if i == index
        addClass control, 'active'
      else
        removeClass control, 'active'

    @current = index
    clearTimeout @timer
    @timer = setTimeout (=> @next()), 5000

  next: ->
    @showSlide @current + 1

  prev: ->
    @showSlide @current + 1