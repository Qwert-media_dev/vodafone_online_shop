class AccordionController extends BaseController

  init: ->
    @panels = @container.querySelectorAll('.panel')
    @parentRow = closest @container, '.row'
    @siblings = []

    if @parentRow
      if sib = next(@parentRow, '.row')?.querySelector('.accordion')
        @parentRow.style.paddingBottom = 0
        @siblings.push sib
      if sib = prev(@parentRow, '.row')?.querySelector('.accordion')
        @parentRow.style.paddingTop = 0
        @parentRow.style.marginTop = '-1px'
        @siblings.push sib


  bind: ->
    for panel in @panels
      ((panel)=>
        title = panel.querySelector('.title')
        content = panel.querySelector('.content')

        filter = panel.querySelector('.filter-countries')
        countries = panel.querySelectorAll('.striped tr>th')

        if filter
          filter.addEventListener 'keyup',(event) =>
            search = event.target.value
            counter = 0
            re = new RegExp("^"+search, "i");
            while counter < countries.length
              city = countries[counter].textContent
              if re.test(city) && search != ' '
                removeClass countries[counter].parentNode, 'hidden'
                addClass countries[counter].parentNode, 'show'
              else
                removeClass countries[counter].parentNode, 'show'
                addClass countries[counter].parentNode, 'hidden'
              counter++


        panel.collapse = ->
          Velo this.querySelector('.content'), 'slideUp'
          removeClass this, 'expanded'

        panel.expand = ->
          Velo this.querySelector('.content'), 'slideDown'
          addClass this, 'expanded'

        panel.toggle = ->
          if hasClass this, 'expanded'
            this.collapse()
          else
            this.expand()

        # Fix collapse for expanded elements
        for expanded in @container.querySelectorAll('.expanded')
          expanded.querySelector('.content').style.display = 'block'

        title.addEventListener 'click', =>
          unless hasClass panel, 'expanded'
            expanded.collapse() for expanded in @container.querySelectorAll('.expanded')
            for sibling in @siblings
              expanded.collapse() for expanded in sibling.querySelectorAll('.expanded')
          panel.toggle()
      )(panel)


