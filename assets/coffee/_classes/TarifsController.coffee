class TarifsController extends BaseController

  init: ->
    @startColumn = 0
    @visibleColumns = 4

    @headingsTableContainer = @container.querySelector '.tarif-table .headings'
    @tarifTableContainer = @container.querySelector '.tarif-table .tarifs'
    @totalCols = @tarifTableContainer.querySelector('tr').querySelectorAll('td').length
    
    @panContainer = @container.querySelector '.tarif-table'
    @visibleColumns = parseInt(@panContainer.getAttribute('data-visible')) || 4

    @panTrigger = @panContainer.querySelector '.pan-trigger'
    @panBlockWidth = @tarifTableContainer.querySelector('table').offsetWidth + @headingsTableContainer.offsetWidth
    @panEnabled = !isHidden(@panTrigger) and @getWidth() > @panContainer.offsetParent.offsetWidth
    if @panEnabled
      @panContainer.style.cursor = 'move'
    else
      @panContainer.style.cursor = ''

    @arrows =
      left: @container.querySelector '.tarif-table-control .left'
      right: @container.querySelector '.tarif-table-control .right'
    
    @counter = {
      from: @container.querySelector '.status .from'
      to: @container.querySelector '.status .to'
      total: @container.querySelector '.status .total'
    }

  bind: ->
    column = closest(@container, '.column')
    if column
      column.style.padding = 0
    
    window.addEventListener 'resize', =>
      @panBlockWidth = @tarifTableContainer.querySelector('table').offsetWidth + @headingsTableContainer.offsetWidth

      @panEnabled = !isHidden(@panTrigger) and @getWidth() > @panContainer.offsetParent.offsetWidth
      if @panEnabled
        @panContainer.style.cursor = 'move'
      else
        @panContainer.style.cursor = ''
        
      if @getWidth() < @panContainer.offsetParent.offsetWidth
        @panContainer.style.marginLeft = ''
    
    @arrows.right.addEventListener 'click', => @moveRight()
    @arrows.left.addEventListener 'click', => @moveLeft()

    if @totalCols <= @visibleColumns
      @container.querySelector('.tarif-table-control').style.display = 'none'
      
    hammer = new Hammer @panContainer
    hammer.on 'panstart', (ev)=>
      return unless @panEnabled
      @panContainer.panStartPosition = parseFloat(@panContainer.style.marginLeft) or 0
    hammer.on 'pan', (ev)=>
      return unless @panEnabled
      if @getWidth() < @panContainer.offsetParent.offsetWidth
        @panContainer.style.marginLeft = ''
      else
        margin = @panContainer.panStartPosition + ev.deltaX
        marginMin = @panContainer.offsetParent.offsetWidth - @panBlockWidth - 10
        margin = 10 if margin > 10
        margin = marginMin if margin < marginMin
        @panContainer.style.marginLeft = margin + 'px'
        

    @updateStatus()

  moveRight: ->
    return if @startColumn >= @totalCols
    
    table = @tarifTableContainer.querySelector('table')
    
    return if hasClass table, 'velocity-animating'
    
    w = @tarifTableContainer.querySelector('thead td').offsetWidth
    edge = w * (@visibleColumns - @totalCols) 

    return if parseInt(table.style.marginLeft) <= edge
    
    Velo table, {
      marginLeft: '-=' + w
    }, {
      progress: ->
        if parseInt(table.style.marginLeft) <= edge
          Velo table, 'stop'
          table.style.marginLeft = edge
    }

    @startColumn++
    @updateStatus()

  moveLeft: ->
    return if @startColumn <= 0
    
    table = @tarifTableContainer.querySelector('table')

    return if hasClass table, 'velocity-animating'
    
    w = @tarifTableContainer.querySelector('thead td').offsetWidth
    
    return if parseInt(table.style.marginLeft) >= 0

    Velo table, {
      marginLeft: '+=' + w
    }, {
      progress: ->
        if parseInt(table.style.marginLeft) >= 0
          Velo table, 'stop'
          table.style.marginLeft = 0
    }

    @startColumn--
    @updateStatus()
    
  updateStatus: ->
    @counter.total.innerText = @totalCols
    @counter.from.innerText =  @startColumn + 1
    @counter.to.innerText =  @startColumn + @visibleColumns
    
  getWidth: ->
    @headingsTableContainer.offsetWidth + @tarifTableContainer.querySelector('table').offsetWidth