class FeedbackController extends BaseController

  init: ->
    @inputs = @container.querySelectorAll('select, input, textarea')
    @togglers = Array.prototype.filter.call @container.querySelectorAll('select, input'), (el)-> el.hasAttribute 'data-toggle'
    @submit = @container.querySelector '.submit button'
    @num = @container.querySelector '.num input'
    @form = @container.querySelector 'form'
    @response = @container.querySelector '.response'
    @loader = @container.querySelector '.loader'

  bind: ->
    @filterInputsBind()
    @update()
    for toggler in @togglers
      toggler.addEventListener 'change', (e)=> @update()
    for input in @inputs
      input.addEventListener 'change', (e)=> @updateSubmit()
      input.addEventListener 'keyup', (e)=> @updateSubmit()
      input.addEventListener 'focus', (e)=> addClass closest(e.target, '.group'), 'active'
      input.addEventListener 'blur', (e)=> removeClass closest(e.target, '.group'), 'active'


    iframe = document.createElement('iframe')
    iframe.style.display = 'none'
    iframe.id = 'form-submit-frame'
    iframe.name = 'form-submit-frame'
    document.body.appendChild iframe
    iframe.addEventListener 'load', (e)=>
      # ожидаем json
      resp = JSON.parse(iframe.contentDocument.body.textContent)
      if (resp.success)
        addClass @response, 'success'
        removeClass @response, 'error'
        @response.querySelector('.content').innerText = resp.success
        @response.querySelector('button').setAttribute('type', 'reset')
        @form.reset()
      else
        addClass @response, 'error'
        removeClass @response, 'success'
        @response.querySelector('.content').innerText = resp.error
        @response.querySelector('button').setAttribute('type', 'button')
        for fmc in @container.querySelectorAll '.form-container'
          fmc.style.display = ''
        if (resp.errors)
          for err in resp.errors
            problem = @form.querySelector('[data-find="' + err + '"]')
            addClass(problem, 'error') if problem

      for input in @inputs
        input.removeAttribute('disabled')

      #@response.style.display = 'block'
      Velo @response, 'slideDown'
      removeClass @loader, 'show'

    @form.target = 'form-submit-frame'

    @form.addEventListener 'submit', (e)=>
      #for fmc in @container.querySelectorAll '.form-container'
      #  fmc.style.display = 'none'
      #Velo @form, 'scroll'
      setTimeout (=>
        for input in @inputs
          input.setAttribute('disabled', 'disabled')
      ), 50
      addClass @loader, 'show'


    @form.querySelector('.response button').addEventListener 'click', =>
      for fmc in @container.querySelectorAll '.form-container'
        fmc.style.display = ''
      #@response.style.display = 'none'
      Velo @response, 'slideUp', complete: =>
        removeClass @response, 'success'
        removeClass @response, 'error'
      @response.querySelector('.content').innerText = ''
      setTimeout (=>@update()), 50

    @container.querySelector('.num select').addEventListener 'change', (e)=>
      if (e.target.value == 'null')
        #@num.setAttribute('disabled', 'disabled')
        @num.setAttribute('maxlength', 10)
      else
        #@num.removeAttribute('disabled')
        @num.setAttribute('maxlength', 7)
      @updateSubmit()



  update: ->
    for toggler in @togglers
      @runToggler toggler

  runToggler: (toggler)->
    selector = toggler.getAttribute 'data-toggle'

    if (toggler.getAttribute('type') == 'radio')
      radios = @container.querySelectorAll('input[name="' + toggler.getAttribute('name') + '"]')
      value = null
      for radio in radios
        if radio.checked
          value = radio.value
          break
    else
      value = if @isActive(toggler) then toggler.value else null

    items = @container.querySelectorAll(selector)

    if toggler.name == 'category'
      for item in items
        do (item)->
          if not hasClass(item, 'target-' + value) and not item.hasAttribute('disabled')
            Velo item, 'slideUp', complete: ->
              item.setAttribute('disabled', 'disabled')
              item.removeAttribute('style')

      for item in items
        do (item)->
          if hasClass(item, 'target-' + value) and item.hasAttribute('disabled')
            Velo item, 'slideDown', complete: ->
              item.removeAttribute('disabled')
              item.removeAttribute('style')


    else
      for item in items
        if hasClass(item, 'target-' + value)
          item.removeAttribute('disabled')
        else
          item.setAttribute('disabled', 'disabled')


  isActive: (el)->
    return false if el.hasAttribute('disabled')
    parent = el.parentElement
    return true if parent? and parent.tagName.toLowerCase() == 'form'

    while parent
      return false if parent.hasAttribute('disabled')
      parent = parent.parentElement
      break if parent? and parent.tagName.toLowerCase() == 'form'
    return true

  validate: ->
    inputs = @container.querySelectorAll('select, input, textarea')
    if @num.value.length == 0 and not @num.hasAttribute('disabled')
      gr = closest(@num, '.group')
      removeClass gr, 'error'
      return false
    for input in inputs
      # radios in fact will always be selected
      if input.getAttribute('type') == 'radio' or input.getAttribute('name') == 'attach' or input.getAttribute('name') == 'num'
        continue
      if @isActive(input) and input.value.length == 0
        return false
      else
        gr = closest(input, '.group')
        removeClass gr, 'error'
    return true

  updateSubmit: ->
    if @validate()
      @submit.removeAttribute('disabled')
    else
      @submit.setAttribute('disabled', 'disabled')

  filterInputsBind: ->
    @num._pattern = /[\d]*/g

    filterFunc = (input)=>
      caret = input.selectionStart
      caretEnd = input.selectionEnd
      baseLength = input.value.length
      input.value = input.value.match(input._pattern)?.join('') or ''
      newLength = input.value.length
      newCaret = caret - (baseLength - newLength)
      newCaretEnd = caretEnd - (baseLength - newLength)
      input.setSelectionRange(newCaret, newCaretEnd)
      # for chrome
      #setTimeout (->input.setSelectionRange(start, end)), 0
      @updateSubmit()

    for input in [@num]
      input.addEventListener 'keyup', filterFunc.bind(this, input)
      input.addEventListener 'keydown', filterFunc.bind(this, input)
