class TarifBannersController extends BaseController

  init: ->
    @items = @container.querySelectorAll '.item'
    @itemsFeatures = []
    @itemsAdvantages = []

    for item in @items
      @itemsFeatures.push item.querySelector('.item-features')
      @itemsAdvantages.push item.querySelector('.item-advantages')


  bind: ->
    window.addEventListener 'resize', (=> @setSizes())
    @setSizes()


  clearHeight: ->
    for features in @itemsFeatures
      features.style.height = null
    for advantages in @itemsAdvantages
      advantages.style.height = null


  setHeight: ->
    maxFeaturesHeight = 0
    maxAdvantagesHeight = 0

    for features in @itemsFeatures
      maxFeaturesHeight = if maxFeaturesHeight < features.offsetHeight then features.offsetHeight else maxFeaturesHeight;

    for advantages in @itemsAdvantages
      maxAdvantagesHeight = if maxAdvantagesHeight < advantages.offsetHeight then advantages.offsetHeight else maxAdvantagesHeight;

    for features in @itemsFeatures
      features.style.height = maxFeaturesHeight + 'px'

    for advantages in @itemsAdvantages
      advantages.style.height = maxAdvantagesHeight + 'px'


  setSizes: ->
    @clearHeight()

    if window.innerWidth >= 980
      setTimeout (=> @setHeight()), 10
