class PayController extends BaseController
  
  init: ->
    @form = @container.querySelector 'form'
    @targetsSwitchers = @container.querySelectorAll '.targets .radio input'
    @targets = @container.querySelectorAll '.targets .group input'
    @methods = @container.querySelectorAll '.methods .radio input'
    @sum = @container.querySelector '.sum input'
    @num = @container.querySelector '.num .group input'
    @account = @container.querySelector '.account .group input'
    @button = @container.querySelector '.submit button'
    @presets = @container.querySelectorAll '.presets button'


  bind: ->
    @filterInputsBind()
    @form.addEventListener 'submit', (e)=>
      @filterInputs()
      unless @canContinue()
        e.preventDefault()
        return false
    for radio in @container.querySelectorAll '.radio input'
      radio.addEventListener 'change', => @buttonEnabler()

    do targetsFade = => for sw2 in @targetsSwitchers
      p = closest(sw2, '.radio').parentNode
      if sw2.checked
        removeClass p, 'disabled'
      else
        addClass p, 'disabled'
        
    for sw in @targetsSwitchers
      sw.addEventListener 'change', -> targetsFade()

    @sum.addEventListener 'change', (e)=>
      max = parseFloat e.target.getAttribute('data-max')
      if parseFloat(e.target.value) > max
        e.target.value = max
      for preset in @presets
        removeClass(preset, 'active')
        if @sum.value == preset.getAttribute('data-value')
          addClass(preset, 'active')
      @buttonEnabler()

    for preset in @presets
      ((preset)=>
        preset.addEventListener 'click',  (e)=>
          e.preventDefault()
          @sum.value = preset.getAttribute('data-value')
          fireEvent(@sum, 'change')
      )(preset)

    for inp in [@num, @account]
      inp.addEventListener 'focus', ((inp)=>=>
        sw.checked = false for sw in @targetsSwitchers
        radio = closest(inp, '.group').parentNode.querySelector('.radio input')
        radio.checked = true
        targetsFade()
      )(inp)
    
    @buttonEnabler()
    
  canContinue: ->
    condition1 = nodeListToArray(@targetsSwitchers).filter((e)-> e.checked).length > 0
    condition2 = nodeListToArray(@targets).filter((e)-> e.value.length > 0).length >= @targets.length - 1
    condition3 = nodeListToArray(@methods).filter((e)-> e.checked).length > 0
    condition4 = @sum.value.length > 0 and parseFloat(@sum.value) > parseFloat(@sum.getAttribute('data-min')) and parseFloat(@sum.value) <= parseFloat(@sum.getAttribute('data-max'))
    condition5 = if closest(@num, '.num').querySelector('.radio input').checked then @num.value.length == 7 else true
    condition6 = if closest(@account, '.account').querySelector('.radio input').checked then @account.value.length == 12 else true

    condition1 and condition2 and condition3 and condition4 and condition5 and condition6
    
  buttonEnabler: ->
    @filterInputs()
    if @canContinue()
      @button.disabled = ''
    else
      @button.disabled = 'disabled'
    
  filterInputsBind: ->
    @sum._pattern = /[\d]*/g
    @num._pattern = /[\d]*/g
    @account._pattern = /[\d]*/g

    filterFunc = (input)=>
      caret = input.selectionStart
      #console.log caret
      caretEnd = input.selectionEnd
      baseLength = input.value.length
      input.value = input.value.match(input._pattern)?.join('') or ''
      newLength = input.value.length
      newCaret = caret - (baseLength - newLength)
      console.log newCaret
      newCaretEnd = caretEnd - (baseLength - newLength)
      input.setSelectionRange(newCaret, newCaretEnd)
      # for chrome
      setTimeout (->input.setSelectionRange(newCaret, newCaretEnd)), 0
      @buttonEnabler()

    
    for input in [@num, @account]
      input.addEventListener 'keyup', filterFunc.bind(this, input)
      #input.addEventListener 'keydown', filterFunc.bind(this, input)

  filterInputs: ->
    @sum.value.match(@sum._pattern)?.join('') or ''
    @num.value = @num.value.match(@num._pattern)?.join('') or ''
    @account.value = @account.value.match(@account._pattern)?.join('') or ''