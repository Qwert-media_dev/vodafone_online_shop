class NetworkController extends BaseController
  
  init: ->
    @mapContainer = @container.querySelector '.map'
    @stateSelector = @container.querySelector '.state select'
    @coverageSelector = @container.querySelector '.map-coverages'
    @pointsListsContainer = @container.querySelector '.points'
    @districts = @container.querySelectorAll '.district'
    @mapListing = @container.querySelector '.map-listing'
    @btnListing = @container.querySelector '.btn_listing'
    @btnCloseListing = @container.querySelector '.close_listing'
    @mapSearch = @container.querySelector '.map-search'
    @mapSearchInput = @mapSearch.querySelector 'input'
    @data = {}
    @layers = {}

    dataLoader = promise.join [
      promise.get @stateSelector.getAttribute 'data-data'
      promise.get @coverageSelector.getAttribute 'data-data'
      promise.get @container.querySelector('.legend-3g').getAttribute 'data-data'
    ]
    
    dataLoader.then (results)=>
      @data =
        states: JSON.parse results[0][1]
        coverages: JSON.parse(results[1][1]).coverages
        points_3g: JSON.parse results[2][1]
      @createLayers()
      @ready()
      
  bind: ->
    @stateSelector.addEventListener 'change', => @updateState()

    @btnListing.addEventListener 'click', => @toggleListing()
    @btnCloseListing.addEventListener 'click', => @closeListing()

    for a in @pointsListsContainer.querySelectorAll('a')
      a.addEventListener 'click', (e)=>
        e.preventDefault()
        @mapSearchInput.value = ''
        @showPoint e.target.getAttribute('data-point')
        @closeListing()

    for district in @districts
      ((district)=>
        title = district.querySelector 'strong'
        collapser = district.querySelector '.collapse'
        title.addEventListener 'click', =>
          if hasClass(district, 'collapsed')
            removeClass(district, 'collapsed')
            Velo collapser, 'slideDown'
          else
            addClass(district, 'collapsed')
            Velo collapser, 'slideUp'
      )(district)


  createLayers: ->
    for coverage in @data.coverages
      @layers[coverage.name] = new google.maps.KmlLayer
        url: coverage.coverage_path
        preserveViewport: true
        suppressInfoWindows: true
    
  ready: ->
    @fillSelectors()
    @map = @createMap()
    @updateUserRegion()
    @updateMap()

  fillSelectors: ->
    
    for state in @data.states
      opt = document.createElement 'option'
      opt.value = state.id
      opt.textContent = state.title
      @stateSelector.appendChild opt
    
    for coverage in @data.coverages
      btn = document.createElement 'button'
      btn.textContent = coverage.name
      btn.setAttribute('data-value', coverage.name)
      btn.addEventListener 'click', (e) => @updateCoverage(e)
      @coverageSelector.appendChild btn

    addClass(@coverageSelector.querySelector('button'), 'active')


  updateCursor: ->
    @stateSelector.style.cursor = 'pointer'

  createMap: ->
    mapOptions =
      zoom: 9
      zoomControl: true
      zoomControlOptions: style: google.maps.ZoomControlStyle.DEFAULT
      disableDoubleClickZoom: true
      mapTypeControl: false
      scaleControl: true
      scrollwheel: false
      panControl: true
      streetViewControl: false
      draggable: true
      overviewMapControl: true
      overviewMapControlOptions: opened: false
      mapTypeId: google.maps.MapTypeId.ROADMAP

    map = new (google.maps.Map)(@mapContainer, mapOptions)
    searchBox = new google.maps.places.Autocomplete(@mapSearchInput, { types: ['(cities)'], componentRestrictions: {country: 'ua'} })
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(@mapSearch);
    markers = []

    searchBox.addListener 'place_changed', =>
      console.log('changed')
      place = searchBox.getPlace()
      if !place.geometry
        return
      bounds = new google.maps.LatLngBounds()
      if place.geometry.viewport
        bounds.union(place.geometry.viewport)
      else
        bounds.extend(place.geometry.location)
      map.fitBounds(bounds)

    for region_id, region of @data.points_3g
      items = []
      unless region.groups instanceof Array
        tmp = region.groups
        region.groups = []
        region.groups.push(gr) for gr_id, gr of tmp

      for group in region.groups
        for item in group.items
          items.push(item)

      for point in items
        infowindow = new google.maps.InfoWindow
          content: point.title
        marker = new google.maps.Marker
          #icon: '/img/map-marker-3g.png'
          icon: point.image
          position: new google.maps.LatLng point.lat, point.lon
          map: map
        marker.point_id = point.id
        marker.infowindow = infowindow
        marker.addListener 'click', ((marker, infowindow)->-> infowindow.open(map, marker) )(marker, infowindow)
        markers.push marker

    cluster = {
      textColor: 'white',
      textSize: 18,
      url: '/img/map-cluster.png',
      width: 62,
      height: 61
    }
    clusterStyles = [cluster, cluster, cluster]

    @markers = markers
    new MarkerClusterer map, markers, {styles: clusterStyles}

    return map

  updateMap: ->
    @updateState()
    @updateCoverage()

  toggleListing: ->
    if (hasClass(@btnListing, 'active'))
      @closeListing()
      return
    addClass(@btnListing, 'active')
    addClass(@mapListing, 'opened')

  closeListing: ->
    removeClass(@btnListing, 'active')
    removeClass(@mapListing, 'opened')

  updateCoverage: (e) ->
    if (e)
      btn = e.target
      if (hasClass(btn, 'active'))
        return
      removeClass(@coverageSelector.querySelector('button.active'), 'active')
      addClass(btn, 'active');
      value = btn.getAttribute('data-value')
    else
      value = @coverageSelector.querySelector('button.active').getAttribute('data-value')
    if value.indexOf('2G') >= 0
      addClass(@btnListing, 'disabled')
      @hideMarkers()
      @closeListing();
    else
      removeClass(@btnListing, 'disabled')
      @showMarkers()
    for layer_id, layer of @layers
      layer.setMap null
    @layers[value].setMap(@map)
    
  updateState: ->
    stateId = @stateSelector.value
    state = @data.states.filter (v)-> v.id == stateId
    for list in @pointsListsContainer.querySelectorAll('.list')
      if list.getAttribute('data-state') == stateId
        list.style.display = 'block'
      else
        list.style.display = 'none'
    @map.setCenter new google.maps.LatLng(state[0].lat, state[0].lng)
    @map.setZoom 9

  showPoint: (pointId)->
    console.log pointId
    for region_id, region of @data.points_3g
      for group in region.groups
        #console.log region.groups
        for point in group.items
          console.log point.id
          if point.id == pointId
            target_point = point
            break
    
    return unless target_point?
    @map.setCenter new google.maps.LatLng target_point.lat, target_point.lon
    @map.setZoom 17

    marker = @markers.filter (v)-> v.point_id == pointId
    if marker[0]?
      marker[0].infowindow.open(@map, marker[0])
      
    
    Velo @mapContainer, 'scroll'
    
  updateUserRegion: ->
    setLocation = (pos)=>
      dist = Infinity
      state = null
      for st in @data.states
        d = @__distance pos.lat, pos.lng, st.lat, st.lng
        if d < dist
          dist = d
          state = st
      if state
        @stateSelector.value = state.id
        @updateMap()

    if navigator.geolocation
      navigator.geolocation.getCurrentPosition ((position) ->
        pos =
          lat: position.coords.latitude
          lng: position.coords.longitude
        setLocation pos
      ), ->
        return null

  hideMarkers: ->
    for marker in @markers
      marker.setMap(null)

  showMarkers: ->
    for marker in @markers
      marker.setMap(@map);
        
  __distance: (lat1, lon1, lat2, lon2, unit) ->
    radlat1 = Math.PI * lat1 / 180
    radlat2 = Math.PI * lat2 / 180
    #radlon1 = Math.PI * lon1 / 180
    #radlon2 = Math.PI * lon2 / 180
    theta = lon1 - lon2
    radtheta = Math.PI * theta / 180
    dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta)
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if unit == 'K'
      dist = dist * 1.609344
    if unit == 'N'
      dist = dist * 0.8684
    dist