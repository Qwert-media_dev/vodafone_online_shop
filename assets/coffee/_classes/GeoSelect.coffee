class GeoSelect extends BaseController
  init: ->
    @icon = @container.querySelector('.icon')
    @confirmWindow = @container.querySelector('.confirm')
    @chooseWindow = @container.querySelector('.choose')

  bind: ->
    @icon.addEventListener 'click', @toggleChoose.bind(this)
    @confirmWindow?.querySelector('.no').addEventListener 'click', @openChoose.bind(this)
    #@confirmWindow.querySelector('.yes').addEventListener 'click', @close.bind(this)
    @confirmWindow?.addEventListener('click', (e)-> e.stopPropagation())
    @chooseWindow?.addEventListener('click', (e)-> e.stopPropagation())
    @icon.addEventListener('click', (e)-> e.stopPropagation())

    this.closeFromOuter = this.close.bind(this, null)
    if (hasClass(@container, 'choose') or hasClass(@container, 'confirm'))
      document.addEventListener('click', this.closeFromOuter)


    ###
    for region in @chooseWindow.querySelectorAll('.region')
      ((region)=>
        region.querySelector('a').addEventListener 'click', (e)=>
          e.preventDefault()
          toggleClass(region, 'open')
      )(region)
    ###


  close: (e)->
    console.log('close')
    e?.preventDefault?()
    removeClass(@container, 'choose')
    removeClass(@container, 'confirm')
    document.removeEventListener('click', this.closeFromOuter)

  openConfirm: (e)->
    e?.preventDefault?()
    removeClass(@container, 'choose')
    addClass(@container, 'confirm')
    document.removeEventListener('click', this.closeFromOuter)
    document.addEventListener('click', this.closeFromOuter)

  openChoose: (e)->
    e?.preventDefault?()
    removeClass(@container, 'confirm')
    addClass(@container, 'choose')
    document.removeEventListener('click', this.closeFromOuter)
    document.addEventListener('click', this.closeFromOuter)

  toggleChoose: (e)->
    e?.preventDefault?()
    if hasClass(@container, 'choose') then @close() else @openChoose()