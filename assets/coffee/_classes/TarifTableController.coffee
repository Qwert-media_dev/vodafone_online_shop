class TarifTableController extends BaseController

  init: ->
    @startColumn = 0
    @visibleColumns = 4

    @currentOffset = 0

    @headings = @container.querySelector '.headings'
    @tarifs = @container.querySelector '.tarifs'
    @tarifsInner = @tarifs.querySelector '.inner'
    @totalCols = @tarifs.querySelectorAll('.item, .last').length
    
    @panContainer = @container.querySelector '.tarif-table'
    @visibleColumns = parseInt(@panContainer.getAttribute('data-visible')) || 4

    @panTrigger = @panContainer.querySelector '.pan-trigger'

    @arrows =
      left: @container.querySelector '.controls .left'
      right: @container.querySelector '.controls .right'
    
    @counter = {
      from: @container.querySelector '.status .from'
      to: @container.querySelector '.status .to'
      total: @container.querySelector '.status .total'
    }

    @updateSizes()

  bind: ->
    column = closest(@container, '.column')
    if column
      column.style.padding = 0
    
    window.addEventListener 'resize', =>
      @setOffset(0)
      @updateSizes()
    
    @arrows.right.addEventListener 'click', => @moveRight()
    @arrows.left.addEventListener 'click', => @moveLeft()

    if @totalCols > @visibleColumns
      @container.querySelector('.controls').style.display = 'block'
      
    hammer = new Hammer @panContainer
    hammer.on 'panstart', (ev)=>
      return unless @panEnabled
      @panContainer.panStartPosition = @currentOffset
    hammer.on 'pan', (ev)=>
      return unless @panEnabled
      if @getWidth() <= @container.offsetWidth - @containerHorizontalPadding
        @setOffset(0)
      else
        margin = @panContainer.panStartPosition + ev.deltaX
        marginMin = @panContainer.offsetParent.offsetWidth - @panBlockWidth - 20 - @containerHorizontalPadding
        margin = 0 if margin > 0
        margin = marginMin if margin < marginMin
        @setOffset(margin)

    @updateStatus()


  moveRight: ->
    return if @startColumn >= @totalCols
    
    table = @tarifs.querySelector('table')
    
    return if hasClass table, 'velocity-animating'
    
    w = @tarifs.querySelector('thead td').offsetWidth
    edge = w * (@visibleColumns - @totalCols) 

    return if parseInt(table.style.marginLeft) <= edge
    
    Velo table, {
      marginLeft: '-=' + w
    }, {
      progress: ->
        if parseInt(table.style.marginLeft) <= edge
          Velo table, 'stop'
          table.style.marginLeft = edge
    }

    @startColumn++
    @updateStatus()


  moveLeft: ->
    return if @startColumn <= 0
    
    table = @tarifs.querySelector('table')

    return if hasClass table, 'velocity-animating'
    
    w = @tarifs.querySelector('thead td').offsetWidth
    
    return if parseInt(table.style.marginLeft) >= 0

    Velo table, {
      marginLeft: '+=' + w
    }, {
      progress: ->
        if parseInt(table.style.marginLeft) >= 0
          Velo table, 'stop'
          table.style.marginLeft = 0
    }

    @startColumn--
    @updateStatus()


  updateStatus: ->
    @counter.total.innerText = @totalCols
    @counter.from.innerText =  @startColumn + 1
    @counter.to.innerText =  @startColumn + @visibleColumns


  updateSizes: ->
    @panBlockWidth = @tarifsInner.offsetWidth + @headings.offsetWidth

    style = window.getComputedStyle(@container)
    @containerHorizontalPadding = parseInt(style.getPropertyValue('padding-left')) + parseInt(style.getPropertyValue('padding-right'))

    @panEnabled = !isHidden(@panTrigger) and @getWidth() > @container.offsetWidth - @containerHorizontalPadding
    if @panEnabled
      addClass(@panContainer, 'pannable')
    else
      removeClass(@panContainer, 'pannable')


  setOffset: (offset)->
    @currentOffset = offset
    transform = 'translateX(' + offset + 'px)'
    @panContainer.style['transform'] = transform
    @panContainer.style['-ms-transform'] = transform
    @panContainer.style['-webkit-transform'] = transform

  getWidth: ->
    @headings.offsetWidth + @tarifsInner.offsetWidth