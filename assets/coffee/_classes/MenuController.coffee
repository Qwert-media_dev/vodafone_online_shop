class MenuController extends BaseController

  init: ->
    @globalMenu = @container.querySelector('.global-menu')
    @globalMenuHighlighter = @globalMenu.querySelector('.highlighter .tip')
    @globalMenuActiveSection = @globalMenu.querySelector('.sections .active')
    @geoConirm = @container.querySelector('.promo-section .confirm')

    @sectionMenu = {}
    @sectionMenu.container = @container.querySelector('.section-menu-container')
    @sectionMenu.menu = @sectionMenu.container.querySelector('.section-menu')
    @sectionMenu.burger = @sectionMenu.container.querySelector('.burger')
    @sectionMenu.sectionsSelectors = @sectionMenu.container.querySelectorAll('.sections a, .sections span')
    @sectionMenu.dropdowns = @sectionMenu.menu.querySelectorAll('.dropdown')
    @sectionMenu.search = @sectionMenu.menu.querySelector('.search-input')

    @currentState = null

    @sectionMenu.subControls =
      search: @sectionMenu.menu.querySelector('.search-button')
      langs: @sectionMenu.menu.querySelector('.lang-button')
      geo: @sectionMenu.menu.querySelector('.location-button')

    @sectionMenu.states =
      main: @sectionMenu.menu.querySelector('.main_menu')
      search: @sectionMenu.menu.querySelector('.search')
      langs: @sectionMenu.menu.querySelector('.langs')
      geo: @sectionMenu.menu.querySelector('.locations')

    @sectionMenu.closeButtons = @sectionMenu.container.querySelectorAll('.close')

    @mode = null
    @checkMode()
    that = @
    setTimeout ->
      that.higlightSection()
    ,500
    return

  bind: ->
    window.addEventListener 'resize', =>

      @higlightSection true
      @checkMode()
      return

    @sectionMenu.search.addEventListener 'focus', =>
      addClass @sectionMenu.search.parentNode.parentNode, 'focus'

    @sectionMenu.search.addEventListener 'blur', =>
      removeClass @sectionMenu.search.parentNode.parentNode, 'focus'

    @sectionMenu.burger.addEventListener 'click', => @setState 'main'
    @sectionMenu.subControls.search.addEventListener 'click', => @setState 'search'
    @sectionMenu.subControls.langs.addEventListener 'click', => @setState 'langs'
    @sectionMenu.subControls.geo.addEventListener 'click', => @setState 'geo'

    for selector in @sectionMenu.sectionsSelectors
      selector.addEventListener 'click', do (selector)=> (e)=>
        if selector.getAttribute('data-section')
          e.preventDefault()
          @changeSectionMenu(selector.getAttribute('data-section'))

    for dropDown in @sectionMenu.dropdowns
      dropDown.addEventListener 'click', do (dropDown)=> (e)=> @toggleSectionMenuDropDown(dropDown)

    for close in @sectionMenu.closeButtons
      close.addEventListener 'click', => @setState null

    @geoConirm.querySelector('.no').addEventListener 'click', => @setState 'geo'
    #@geoConirm.querySelector('.yes').addEventListener 'click', => removeClass @geoConirm, 'show'


  changeSectionMenu: (section)->
    return unless isHidden @sectionMenu.states.main.querySelector "ul.menu-list.#{section}"

    otherMenus = []
    targetMenu = @sectionMenu.states.main.querySelector "ul.menu-list.#{section}"

    for s in @sectionMenu.sectionsSelectors
      if s.getAttribute('data-section') == section
        addClass s.parentNode, 'active'
      else
        removeClass s.parentNode, 'active'

    otherMenus = []
    targetMenu = null
    for m in @sectionMenu.states.main.querySelectorAll "ul.menu-list"
      if hasClass m, section
        targetMenu = m
      else
        otherMenus.push m

    Velo otherMenus, {opacity: 0},
      complete: ->
        Velo targetMenu, 'fadeIn'
        setTimeout (->
          for m in otherMenus
            m.style.display = 'none'
          Velo otherMenus, {opacity: 1}, {duration: 0}
        ), 1



  setState: (state)->
    return if @mode == 'desktop'

    # secondary click closes all
    state = null if @currentState == state

    if state?
      addClass(@sectionMenu.container, 'menu-open')
      Velo(@sectionMenu.subControls, 'fadeIn') unless @currentState?
      if state == 'main'
        addClass(@sectionMenu.burger, 'active')
        removeClass(@sectionMenu.burger, 'parent')
      else
        addClass(@sectionMenu.burger, 'parent')
        removeClass(@sectionMenu.burger, 'active')
    else
      removeClass(@sectionMenu.container, 'menu-open')
      Velo @sectionMenu.subControls, 'fadeOut'
      removeClass(@sectionMenu.burger, 'parent')
      removeClass(@sectionMenu.burger, 'active')

    for own key, s of @sectionMenu.states
      if key == state
        Velo s, 'slideDown'
      else
        Velo s, 'slideUp'
        removeClass(@sectionMenu.subControls[key], 'active') if @sectionMenu.subControls[key]?

    addClass(@sectionMenu.subControls[state], 'active') if @sectionMenu.subControls[state]?

    @currentState = state

  checkMode: ->
    mode = if isHidden(@sectionMenu.burger) then 'desktop' else 'mobile'
    changed = @mode != mode
    @mode = mode
    @onModeChanged() if changed
    return mode

  higlightSection: (momentary)->
    if @globalMenuActiveSection
      link = @globalMenuActiveSection.querySelector('a')
      coords = link.getBoundingClientRect()
      point = coords.left + link.offsetWidth / 2
      if momentary
        @globalMenuHighlighter.style.left = point + 'px'
      else
        Velo @globalMenuHighlighter, {left: point}


  toggleSectionMenuDropDown: (dropDown)->
    return if @mode == 'desktop'
    submenu = dropDown.querySelector('.submenu')
    if isHidden submenu
      for drop in @sectionMenu.dropdowns
        dropSub = drop.querySelector('.submenu')
        unless isHidden dropSub
          Velo dropSub, 'slideUp'
          removeClass drop, 'expanded'
      Velo submenu, 'slideDown'
      addClass dropDown, 'expanded'
    else
      Velo submenu, 'slideUp'
      removeClass dropDown, 'expanded'


  onModeChanged: ->
    # resets
    for drop in @sectionMenu.dropdowns
      removeClass drop, 'expanded'
      drop.querySelector('.submenu').style = ''

    for own key, s of @sectionMenu.states
      s.style.display = ''

    for own key, s of @sectionMenu.subControls
      s.style.display = ''
      removeClass(s, 'active')

    removeClass(@sectionMenu.burger, 'parent')
    removeClass(@sectionMenu.burger, 'active')

    @currentState = null

    if @mode == 'desktop'
      #
    else
      #
