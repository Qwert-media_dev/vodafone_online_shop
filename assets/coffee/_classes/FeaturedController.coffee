class FeaturedController extends BaseController

  bind:->
    featured = document.querySelectorAll('.version3 a.featured')
    i = 0
    while i < featured.length
      featured[i].addEventListener 'mouseover', ->
        border_height = getAbsoluteHeight(@) + getAbsoluteHeight(@querySelector('p'))
        @querySelector('.border').setAttribute 'style', 'height:' + border_height + 'px'

      featured[i].addEventListener 'mouseout', ->
        @querySelector('.border').setAttribute 'style', 'height:'+getAbsoluteHeight(@)+'px'
      i++


  getAbsoluteHeight = (el) ->
    el = if typeof el == 'string' then document.querySelector(el) else el
    styles = window.getComputedStyle(el)
    margin = parseFloat(styles['marginTop']) + parseFloat(styles['marginBottom'])
    Math.ceil el.offsetHeight + margin