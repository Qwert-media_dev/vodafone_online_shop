class LayoutController extends BaseController

  init: ->
    @promoSlider = @container.querySelector('.mainpage-slider')
    @logoRomb = @container.querySelector('.logo .romb')
    @consult = document.getElementById('consult')
    @showBtn = @container.querySelector('#consult .show-buttons')
    @textConsultBtn = @container.querySelector('#consult .btn-show-text')
    @videoConsultBtn = @container.querySelector('#consult .btn-show-video')
    #@textConsultOverlay = @container.querySelector('.consult-iframe-text')
    @videoConsultOverlay = @container.querySelector('.consult-iframe-video')

  bind: ->
    #console.log document.body.offsetHeight
    @logoRomb.style.maxHeight = document.body.offsetHeight + 'px'
    @slider = new PromoSliderController(@promoSlider) if @promoSlider
    @menu = new MenuController @container, 'menu'
    
    window.addEventListener 'resize', => @logoRomb.style.maxHeight = document.body.offsetHeight + 'px'

    if @consult
      @domReady =>
        setTimeout(@showFirstBlock, 5000)

      @showBtn.addEventListener 'click', @showSecondBlock.bind(this)

    #if @textConsultBtn
    #  @textConsultBtn.addEventListener 'click', @showTextConsult.bind(this)
    #  @textConsultOverlay.querySelector('.catcher').addEventListener 'click', @hideTextConsult.bind(this)
    #  @textConsultOverlay.querySelector('.close').addEventListener 'click', @hideTextConsult.bind(this)

    if @videoConsultBtn
      @videoConsultBtn.addEventListener 'click', @showVideoConsult.bind(this)
      @videoConsultOverlay.querySelector('.catcher').addEventListener 'click', @hideVideoConsult.bind(this)
      @videoConsultOverlay.querySelector('.close').addEventListener 'click', @hideVideoConsult.bind(this)

  domReady: (callback) ->
    if document.readyState == "interactive" || document.readyState == "complete"
      callback()
    else
      document.addEventListener("DOMContentLoaded", callback)

  showFirstBlock: ->
    @consult.classList.remove('secondVisible')
    @consult.classList.add('firstVisible')

  showSecondBlock: ->
    @consult.classList.remove('firstVisible')
    @consult.classList.add('secondVisible')
    setTimeout(@showFirstBlock, 10000)

  #showTextConsult: ->
  #  frame = @textConsultOverlay.querySelector('iframe')
    #frame.onload = ->
    #  frame.width = frame.contentWindow.document.body.scrollWidth + 'px'
    #  frame.height = frame.contentWindow.document.body.scrollHeight + 'px'
  #  frame.src = frame.getAttribute('data-src')
  #  Velo @textConsultOverlay, 'fadeIn'

  #hideTextConsult: ->
  #  Velo @textConsultOverlay, 'fadeOut'

  showVideoConsult: ->
    frame = @videoConsultOverlay.querySelector('iframe')
    #frame.onload = ->
    #  frame.width = frame.contentWindow.document.body.scrollWidth + 'px'
    #  frame.height = frame.contentWindow.document.body.scrollHeight + 'px'
    frame.src = frame.getAttribute('data-src')
    Velo @videoConsultOverlay, 'fadeIn'

  hideVideoConsult: ->
    Velo @videoConsultOverlay, 'fadeOut'
    