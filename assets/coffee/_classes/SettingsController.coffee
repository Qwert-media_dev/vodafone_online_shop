class SettingsController extends BaseController
  
  init: ->
    @autoSettings = @container.querySelector '#settings-auto'
    @form = @autoSettings.querySelector 'form'
    @code = @autoSettings.querySelector '.code'
    @num = @autoSettings.querySelector '.num .group input'
    @captcha = @autoSettings.querySelector '.captcha .group input'
    @button = @autoSettings.querySelector '.submit button'
    @agreementExpander = @autoSettings.querySelector '.agreement .trigger'
    @agreementDetails = @autoSettings.querySelector '.agreement .details'
    @response = @autoSettings.querySelector '.response'

  bind: ->
    @filterInputsBind()

    @form.addEventListener 'submit', (e)=>
      e.preventDefault()
      @filterInputs()
      unless @canContinue()
        e.preventDefault()
        return false
      else
        @sendRequest()

    @agreementExpander.addEventListener 'click', (e)=>
      e.preventDefault()
      Velo @agreementDetails, if isHidden(@agreementDetails) then 'slideDown' else 'slideUp'

    @response.querySelector('.close').addEventListener 'click', =>
      @response.style.display = ''
      @button.style.display = ''
      @response.querySelector('.content').innerText = ''
      removeClass @response, 'success'
      removeClass @response, 'error'

    @buttonEnabler()
    
  canContinue: ->
    condition1 = @num.value.length == 7
    condition2 =  @captcha.value.length > 0

    condition1 and condition2
    
  buttonEnabler: ->
    @filterInputs()
    if @canContinue()
      @button.disabled = ''
    else
      @button.disabled = 'disabled'
    
  filterInputsBind: ->
    @num._pattern = /[\d]*/g

    filterFunc = (input)=>
      caret = input.selectionStart
      caretEnd = input.selectionEnd
      baseLength = input.value.length
      input.value = input.value.match(input._pattern)?.join('') or ''
      newLength = input.value.length
      newCaret = caret - (baseLength - newLength)
      newCaretEnd = caretEnd - (baseLength - newLength)
      input.setSelectionRange(newCaret, newCaretEnd)
      # for chrome
      #setTimeout (->input.setSelectionRange(start, end)), 0
    
    for input in [@num]
      input.addEventListener 'keyup', filterFunc.bind(this, input)
      input.addEventListener 'keydown', filterFunc.bind(this, input)

    for input in [@num, @captcha]
      input.addEventListener 'keyup', @buttonEnabler.bind(this, input)

  filterInputs: ->
    @num.value = @num.value.match(@num._pattern)?.join('') or ''

  sendRequest: ->
    @button.disabled = 'disabled'
    action = @form.action
    data = {}
    data[@code.name] = @code.value
    data[@num.name] = @num.value
    data[@captcha.name] = @captcha.value
    promise.post(@form.action, data).then (error, resp)=>
      if (error)
        alert('Error')
        console.error resp
        @buttonEnabler()
      else
        @showResponse(resp)

  showResponse: (resp)->
    resp = JSON.parse(resp)
    if (resp.error)
      removeClass @response, 'success'
      addClass @response, 'error'
      @response.querySelector('.content').innerText = resp.error
    else if (resp.success)
      addClass @response, 'success'
      removeClass @response, 'error'
      @response.querySelector('.content').innerText = resp.success

    @response.style.display = 'block'
    @button.style.display = 'none'