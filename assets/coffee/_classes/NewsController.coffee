class NewsController extends BaseController

  constructor: (@container, @head)->
    super @container
  
  init: ->
    @calendar = @head.querySelector('.calendar')
    @calendarTrigger = @head.querySelector('.period-filter')


  bind: ->
    @calendarTrigger.addEventListener 'click', =>
      return if hasClass @calendar, 'velocity-animating'
      if isHidden @calendar
        Velo @calendar, 'slideDown'
      else
        Velo @calendar, 'slideUp'

    @calendar.querySelector('.close').addEventListener 'click', =>
      return if hasClass @calendar, 'Velo-animating'
      Velo @calendar, 'slideUp'