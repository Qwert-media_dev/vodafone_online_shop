class TabsSwitchController extends BaseController

  init: ->
    @links = @container.querySelectorAll('a')
    @update()



  bind: ->
    for link in @links
      do (link)=>
        link.addEventListener 'click', (e)=>
          e.preventDefault()
          removeClass(l, 'active') for l in @links
          addClass(link, 'active')
          @update()

  update: ->
    for link in @links
      do (link)=>
        targetSelector = link.getAttribute('href')
        target = document.querySelector(targetSelector)
        if target
          if hasClass(link, 'active')
            target.style.display = ''
          else
            target.style.display = 'none'